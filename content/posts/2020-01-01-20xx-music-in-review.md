---
title: '20xx Decade of Music In Review'
description: What music was I listening to in 20xx?
tags:
- music
date: 2020-01-01T00:00:00+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: 20xx-music-in-review
series: music-in-review
---
In 20xx, I listened to 267120.03 minutes (4452.0 hours, 185.5 days) of music on Spotify.

<details>
  <summary>Top 200 songs</summary>
  <table>
    <tr>
      <th>Song Title</th>
      <th>Minutes Elapsed (% of the year)</th>
      <th>Hours Elapsed (% of the year)</th>
    </tr>
    <tr>
      <td>deadmau5 - Ghosts 'n' Stuff</td>
      <td>986.63 (0.37)</td>
      <td>16.44 (0.01)</td>
    </tr>
    <tr>
      <td>Various Artists - Voyager</td>
      <td>655.47 (0.25)</td>
      <td>10.92 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Flashback</td>
      <td>645.05 (0.24)</td>
      <td>10.75 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Different</td>
      <td>587.07 (0.22)</td>
      <td>9.78 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - The People</td>
      <td>502.27 (0.19)</td>
      <td>8.37 (0.0)</td>
    </tr>
    <tr>
      <td>Puppet - The Fire</td>
      <td>498.87 (0.19)</td>
      <td>8.31 (0.0)</td>
    </tr>
    <tr>
      <td>Logistics - Triangles</td>
      <td>489.72 (0.18)</td>
      <td>8.16 (0.0)</td>
    </tr>
    <tr>
      <td>Kiasmos - Looped</td>
      <td>469.6 (0.18)</td>
      <td>7.83 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Fly</td>
      <td>464.5 (0.17)</td>
      <td>7.74 (0.0)</td>
    </tr>
    <tr>
      <td>Phaeleh - Breathe in Air</td>
      <td>449.08 (0.17)</td>
      <td>7.48 (0.0)</td>
    </tr>
    <tr>
      <td>Sub Focus - Tidal Wave</td>
      <td>445.28 (0.17)</td>
      <td>7.42 (0.0)</td>
    </tr>
    <tr>
      <td>Peter Gregson - A Little Chaos</td>
      <td>436.13 (0.16)</td>
      <td>7.27 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Side Effects</td>
      <td>425.23 (0.16)</td>
      <td>7.09 (0.0)</td>
    </tr>
    <tr>
      <td>L Plus - Taking Me Higher</td>
      <td>424.45 (0.16)</td>
      <td>7.07 (0.0)</td>
    </tr>
    <tr>
      <td>EDEN - drugs</td>
      <td>423.85 (0.16)</td>
      <td>7.06 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Revolution</td>
      <td>421.88 (0.16)</td>
      <td>7.03 (0.0)</td>
    </tr>
    <tr>
      <td>Carly Rae Jepsen - Run Away With Me</td>
      <td>419.28 (0.16)</td>
      <td>6.99 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Breaking Time</td>
      <td>414.1 (0.16)</td>
      <td>6.9 (0.0)</td>
    </tr>
    <tr>
      <td>Maduk - Never Again</td>
      <td>395.37 (0.15)</td>
      <td>6.59 (0.0)</td>
    </tr>
    <tr>
      <td>EDEN - Nocturne (Pierce Fulton Remix)</td>
      <td>385.37 (0.14)</td>
      <td>6.42 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Outsider</td>
      <td>374.08 (0.14)</td>
      <td>6.23 (0.0)</td>
    </tr>
    <tr>
      <td>Chase & Status - Lost & Not Found</td>
      <td>370.72 (0.14)</td>
      <td>6.18 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - Hold Your Breath - Spectrasoul Remix</td>
      <td>364.3 (0.14)</td>
      <td>6.07 (0.0)</td>
    </tr>
    <tr>
      <td>Bassnectar - Enter the Chamber - 2015 Version</td>
      <td>357.93 (0.13)</td>
      <td>5.97 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - No Human Code</td>
      <td>342.52 (0.13)</td>
      <td>5.71 (0.0)</td>
    </tr>
    <tr>
      <td>Camo & Krooked - Move Around</td>
      <td>334.75 (0.13)</td>
      <td>5.58 (0.0)</td>
    </tr>
    <tr>
      <td>Robin Schulz - Same - Original Mix</td>
      <td>330.68 (0.12)</td>
      <td>5.51 (0.0)</td>
    </tr>
    <tr>
      <td>Kiasmos - Held</td>
      <td>326.77 (0.12)</td>
      <td>5.45 (0.0)</td>
    </tr>
    <tr>
      <td>Phaeleh - Red Light Green Light</td>
      <td>326.53 (0.12)</td>
      <td>5.44 (0.0)</td>
    </tr>
    <tr>
      <td>Daft Punk - Flynn Lives</td>
      <td>326.32 (0.12)</td>
      <td>5.44 (0.0)</td>
    </tr>
    <tr>
      <td>Maduk - Got Me Thinking</td>
      <td>324.13 (0.12)</td>
      <td>5.4 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - City Needs Sleep - Original Mix</td>
      <td>323.52 (0.12)</td>
      <td>5.39 (0.0)</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Underground</td>
      <td>322.4 (0.12)</td>
      <td>5.37 (0.0)</td>
    </tr>
    <tr>
      <td>SomethingALaMode - 28.18 Moment</td>
      <td>319.35 (0.12)</td>
      <td>5.32 (0.0)</td>
    </tr>
    <tr>
      <td>Phaeleh - Whistling in the Dark (feat. Augustus Ghost)</td>
      <td>314.52 (0.12)</td>
      <td>5.24 (0.0)</td>
    </tr>
    <tr>
      <td>Camo And Krooked - Without You</td>
      <td>311.43 (0.12)</td>
      <td>5.19 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Devotion</td>
      <td>309.43 (0.12)</td>
      <td>5.16 (0.0)</td>
    </tr>
    <tr>
      <td>Aurix - Source</td>
      <td>306.65 (0.11)</td>
      <td>5.11 (0.0)</td>
    </tr>
    <tr>
      <td>Phaeleh - Afterglow</td>
      <td>302.87 (0.11)</td>
      <td>5.05 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Téléguidé</td>
      <td>301.2 (0.11)</td>
      <td>5.02 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Unison</td>
      <td>300.75 (0.11)</td>
      <td>5.01 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Angels</td>
      <td>295.82 (0.11)</td>
      <td>4.93 (0.0)</td>
    </tr>
    <tr>
      <td>Maduk - Don't Be Afraid</td>
      <td>289.65 (0.11)</td>
      <td>4.83 (0.0)</td>
    </tr>
    <tr>
      <td>Logistics - Thunder Child</td>
      <td>289.33 (0.11)</td>
      <td>4.82 (0.0)</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Guided Rhythm</td>
      <td>287.57 (0.11)</td>
      <td>4.79 (0.0)</td>
    </tr>
    <tr>
      <td>MitiS - Life Of Sin Pt. 5 - Original Mix</td>
      <td>285.55 (0.11)</td>
      <td>4.76 (0.0)</td>
    </tr>
    <tr>
      <td>Stumbleine - Fade Into You (feat. Steffaloo)</td>
      <td>284.05 (0.11)</td>
      <td>4.73 (0.0)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Clouds Cross Skies</td>
      <td>283.25 (0.11)</td>
      <td>4.72 (0.0)</td>
    </tr>
    <tr>
      <td>Skrillex - With You, Friends (Long Drive)</td>
      <td>281.92 (0.11)</td>
      <td>4.7 (0.0)</td>
    </tr>
    <tr>
      <td>Roald Velden - She's Something Else - Original Mix</td>
      <td>280.78 (0.11)</td>
      <td>4.68 (0.0)</td>
    </tr>
    <tr>
      <td>Ryan Davis - Brun - Original Mix</td>
      <td>279.95 (0.1)</td>
      <td>4.67 (0.0)</td>
    </tr>
    <tr>
      <td>Camo & Krooked - Run Riot</td>
      <td>278.25 (0.1)</td>
      <td>4.64 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Blame It on Love</td>
      <td>275.95 (0.1)</td>
      <td>4.6 (0.0)</td>
    </tr>
    <tr>
      <td>Ed Sheeran - I'm a Mess</td>
      <td>275.2 (0.1)</td>
      <td>4.59 (0.0)</td>
    </tr>
    <tr>
      <td>Mark Barrott - Deep Water</td>
      <td>270.33 (0.1)</td>
      <td>4.51 (0.0)</td>
    </tr>
    <tr>
      <td>Pendulum - The Tempest</td>
      <td>264.95 (0.1)</td>
      <td>4.42 (0.0)</td>
    </tr>
    <tr>
      <td>ShockOne - Light Cycles</td>
      <td>262.5 (0.1)</td>
      <td>4.38 (0.0)</td>
    </tr>
    <tr>
      <td>Ellie Goulding - Goodness Gracious - The Chainsmokers Extended Remix</td>
      <td>259.32 (0.1)</td>
      <td>4.32 (0.0)</td>
    </tr>
    <tr>
      <td>Ed Sheeran - Touch and Go</td>
      <td>258.37 (0.1)</td>
      <td>4.31 (0.0)</td>
    </tr>
    <tr>
      <td>Phaeleh - The Cold in You</td>
      <td>254.67 (0.1)</td>
      <td>4.24 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Action</td>
      <td>254.02 (0.1)</td>
      <td>4.23 (0.0)</td>
    </tr>
    <tr>
      <td>Illenium - Leaving</td>
      <td>253.68 (0.09)</td>
      <td>4.23 (0.0)</td>
    </tr>
    <tr>
      <td>Good Weather For An Airstrike - Little Rain</td>
      <td>253.12 (0.09)</td>
      <td>4.22 (0.0)</td>
    </tr>
    <tr>
      <td>Massive Attack - Angel</td>
      <td>252.8 (0.09)</td>
      <td>4.21 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Human</td>
      <td>251.1 (0.09)</td>
      <td>4.19 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - New Moon</td>
      <td>251.08 (0.09)</td>
      <td>4.18 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - The Core</td>
      <td>250.9 (0.09)</td>
      <td>4.18 (0.0)</td>
    </tr>
    <tr>
      <td>Swedish House Mafia - Antidote - Extended</td>
      <td>250.83 (0.09)</td>
      <td>4.18 (0.0)</td>
    </tr>
    <tr>
      <td>MitiS - Life Of Sin Pt. 4 - Original Mix</td>
      <td>250.73 (0.09)</td>
      <td>4.18 (0.0)</td>
    </tr>
    <tr>
      <td>Mike Saint-Jules - Crater - DJ Eco Remix</td>
      <td>250.02 (0.09)</td>
      <td>4.17 (0.0)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Maverick Souls</td>
      <td>249.32 (0.09)</td>
      <td>4.16 (0.0)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Hydra</td>
      <td>249.3 (0.09)</td>
      <td>4.16 (0.0)</td>
    </tr>
    <tr>
      <td>NoMBe - California Girls</td>
      <td>247.63 (0.09)</td>
      <td>4.13 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - Kuaga - Yotto Remix</td>
      <td>245.48 (0.09)</td>
      <td>4.09 (0.0)</td>
    </tr>
    <tr>
      <td>The Glitch Mob - Drive It Like You Stole It</td>
      <td>244.5 (0.09)</td>
      <td>4.08 (0.0)</td>
    </tr>
    <tr>
      <td>Maduk - Never Stop Loving You - Original Mix</td>
      <td>242.83 (0.09)</td>
      <td>4.05 (0.0)</td>
    </tr>
    <tr>
      <td>Madeon - Imperium</td>
      <td>242.48 (0.09)</td>
      <td>4.04 (0.0)</td>
    </tr>
    <tr>
      <td>Tove Lo - Talking Body - Gryffin Remix</td>
      <td>241.82 (0.09)</td>
      <td>4.03 (0.0)</td>
    </tr>
    <tr>
      <td>Above & Beyond Group Therapy - Flight of the Navigator [ABGT160]</td>
      <td>240.93 (0.09)</td>
      <td>4.02 (0.0)</td>
    </tr>
    <tr>
      <td>Camo & Krooked - Ember - Hybrid Minds Remix</td>
      <td>239.33 (0.09)</td>
      <td>3.99 (0.0)</td>
    </tr>
    <tr>
      <td>Illenium - Sleepwalker (feat. Joni Fatora)</td>
      <td>238.93 (0.09)</td>
      <td>3.98 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Last Codes</td>
      <td>238.5 (0.09)</td>
      <td>3.98 (0.0)</td>
    </tr>
    <tr>
      <td>The Glitch Mob - I Need My Memory Back (feat. Aja Volkman)</td>
      <td>237.95 (0.09)</td>
      <td>3.97 (0.0)</td>
    </tr>
    <tr>
      <td>MitiS - Born - Original Mix</td>
      <td>237.22 (0.09)</td>
      <td>3.95 (0.0)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Major Happy - Frederic Robinson Remix</td>
      <td>237.17 (0.09)</td>
      <td>3.95 (0.0)</td>
    </tr>
    <tr>
      <td>Thirty Seconds To Mars - Kings And Queens</td>
      <td>236.93 (0.09)</td>
      <td>3.95 (0.0)</td>
    </tr>
    <tr>
      <td>Kove - Searching</td>
      <td>236.03 (0.09)</td>
      <td>3.93 (0.0)</td>
    </tr>
    <tr>
      <td>Above & Beyond Group Therapy - Enceladus [ABGT160]</td>
      <td>234.48 (0.09)</td>
      <td>3.91 (0.0)</td>
    </tr>
    <tr>
      <td>MitiS - Forever - Original Mix</td>
      <td>232.2 (0.09)</td>
      <td>3.87 (0.0)</td>
    </tr>
    <tr>
      <td>Andreya Triana - Lullaby - Logistics Remix</td>
      <td>231.67 (0.09)</td>
      <td>3.86 (0.0)</td>
    </tr>
    <tr>
      <td>Lemaitre - Closer</td>
      <td>230.42 (0.09)</td>
      <td>3.84 (0.0)</td>
    </tr>
    <tr>
      <td>State of Mind - Sunking</td>
      <td>228.77 (0.09)</td>
      <td>3.81 (0.0)</td>
    </tr>
    <tr>
      <td>Madeon - The City - Extended Mix</td>
      <td>225.15 (0.08)</td>
      <td>3.75 (0.0)</td>
    </tr>
    <tr>
      <td>Martin Solveig - Do It Right</td>
      <td>225.02 (0.08)</td>
      <td>3.75 (0.0)</td>
    </tr>
    <tr>
      <td>Feint - Vagrant</td>
      <td>224.98 (0.08)</td>
      <td>3.75 (0.0)</td>
    </tr>
    <tr>
      <td>Imagine Dragons - Shots - Broiler Remix</td>
      <td>224.87 (0.08)</td>
      <td>3.75 (0.0)</td>
    </tr>
    <tr>
      <td>Arkana - Vita | Mortis</td>
      <td>224.85 (0.08)</td>
      <td>3.75 (0.0)</td>
    </tr>
    <tr>
      <td>Speed Limits - Palm Of Your Hand - Boxer & Forbes Remix</td>
      <td>224.6 (0.08)</td>
      <td>3.74 (0.0)</td>
    </tr>
    <tr>
      <td>Village - Takeover</td>
      <td>224.23 (0.08)</td>
      <td>3.74 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - Green Valleys - Original Mix</td>
      <td>223.13 (0.08)</td>
      <td>3.72 (0.0)</td>
    </tr>
    <tr>
      <td>Jocelyn Alice - Jackpot (The Him Remix)</td>
      <td>223.08 (0.08)</td>
      <td>3.72 (0.0)</td>
    </tr>
    <tr>
      <td>Eco - The Lonely Soldier</td>
      <td>222.35 (0.08)</td>
      <td>3.71 (0.0)</td>
    </tr>
    <tr>
      <td>Makoto - YGMYC</td>
      <td>220.38 (0.08)</td>
      <td>3.67 (0.0)</td>
    </tr>
    <tr>
      <td>Apex - Inner Space - Original Mix</td>
      <td>220.27 (0.08)</td>
      <td>3.67 (0.0)</td>
    </tr>
    <tr>
      <td>The Glitch Mob - Skullclub</td>
      <td>219.93 (0.08)</td>
      <td>3.67 (0.0)</td>
    </tr>
    <tr>
      <td>Etherwood - Sunlight Splinters</td>
      <td>218.38 (0.08)</td>
      <td>3.64 (0.0)</td>
    </tr>
    <tr>
      <td>Bring Me The Horizon - Blasphemy</td>
      <td>217.97 (0.08)</td>
      <td>3.63 (0.0)</td>
    </tr>
    <tr>
      <td>Danny Byrd - Like A Byrd</td>
      <td>217.15 (0.08)</td>
      <td>3.62 (0.0)</td>
    </tr>
    <tr>
      <td>Stephen - Mr. Man</td>
      <td>216.63 (0.08)</td>
      <td>3.61 (0.0)</td>
    </tr>
    <tr>
      <td>The Knocks - Classic (feat. POWERS)</td>
      <td>215.92 (0.08)</td>
      <td>3.6 (0.0)</td>
    </tr>
    <tr>
      <td>Madeon - Technicolor</td>
      <td>215.43 (0.08)</td>
      <td>3.59 (0.0)</td>
    </tr>
    <tr>
      <td>Feint - The Journey Feat. Veela - Original Mix</td>
      <td>215.28 (0.08)</td>
      <td>3.59 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - Written Emotions - Original Mix</td>
      <td>214.83 (0.08)</td>
      <td>3.58 (0.0)</td>
    </tr>
    <tr>
      <td>Koven - Eternal and You</td>
      <td>214.55 (0.08)</td>
      <td>3.58 (0.0)</td>
    </tr>
    <tr>
      <td>Kiiara - Tennessee</td>
      <td>213.28 (0.08)</td>
      <td>3.55 (0.0)</td>
    </tr>
    <tr>
      <td>Maduk - Feel Good</td>
      <td>212.32 (0.08)</td>
      <td>3.54 (0.0)</td>
    </tr>
    <tr>
      <td>Metrik - Starchaser</td>
      <td>211.93 (0.08)</td>
      <td>3.53 (0.0)</td>
    </tr>
    <tr>
      <td>Netsky - Running Low - Fred V & Grafix Remix</td>
      <td>211.62 (0.08)</td>
      <td>3.53 (0.0)</td>
    </tr>
    <tr>
      <td>The Eden Project - XO</td>
      <td>211.38 (0.08)</td>
      <td>3.52 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Left Unsaid</td>
      <td>211.07 (0.08)</td>
      <td>3.52 (0.0)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Forest Fires - Etherwood Remix</td>
      <td>210.68 (0.08)</td>
      <td>3.51 (0.0)</td>
    </tr>
    <tr>
      <td>Logistics - Wanderlust</td>
      <td>210.37 (0.08)</td>
      <td>3.51 (0.0)</td>
    </tr>
    <tr>
      <td>Pendulum - 9,000 Miles</td>
      <td>210.27 (0.08)</td>
      <td>3.5 (0.0)</td>
    </tr>
    <tr>
      <td>The Glitch Mob - Our Demons - filous Remix</td>
      <td>210.17 (0.08)</td>
      <td>3.5 (0.0)</td>
    </tr>
    <tr>
      <td>Porter Robinson - Language - Extended Mix</td>
      <td>210.13 (0.08)</td>
      <td>3.5 (0.0)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Here With You</td>
      <td>208.98 (0.08)</td>
      <td>3.48 (0.0)</td>
    </tr>
    <tr>
      <td>Flite - Lost On My Own - Flite Remix</td>
      <td>208.98 (0.08)</td>
      <td>3.48 (0.0)</td>
    </tr>
    <tr>
      <td>Speaker of the House - I Know You</td>
      <td>208.42 (0.08)</td>
      <td>3.47 (0.0)</td>
    </tr>
    <tr>
      <td>The Eden Project - Fumes</td>
      <td>207.83 (0.08)</td>
      <td>3.46 (0.0)</td>
    </tr>
    <tr>
      <td>Tangerine Dream - Love On A Real Train</td>
      <td>207.68 (0.08)</td>
      <td>3.46 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Motion</td>
      <td>207.32 (0.08)</td>
      <td>3.46 (0.0)</td>
    </tr>
    <tr>
      <td>Maduk - The End</td>
      <td>206.5 (0.08)</td>
      <td>3.44 (0.0)</td>
    </tr>
    <tr>
      <td>Robin Schulz - Sugar (feat. Francesco Yates)</td>
      <td>206.32 (0.08)</td>
      <td>3.44 (0.0)</td>
    </tr>
    <tr>
      <td>Madden - Golden Light (feat. 6AM)</td>
      <td>206.23 (0.08)</td>
      <td>3.44 (0.0)</td>
    </tr>
    <tr>
      <td>Mind Vortex - Stand High</td>
      <td>205.85 (0.08)</td>
      <td>3.43 (0.0)</td>
    </tr>
    <tr>
      <td>Snow Patrol - Take Back The City</td>
      <td>204.73 (0.08)</td>
      <td>3.41 (0.0)</td>
    </tr>
    <tr>
      <td>Maduk - How Could You</td>
      <td>204.35 (0.08)</td>
      <td>3.41 (0.0)</td>
    </tr>
    <tr>
      <td>The Eden Project - Jupiter</td>
      <td>204.32 (0.08)</td>
      <td>3.41 (0.0)</td>
    </tr>
    <tr>
      <td>Rollz - Capture Me</td>
      <td>203.92 (0.08)</td>
      <td>3.4 (0.0)</td>
    </tr>
    <tr>
      <td>Koven - Never Have I Felt This</td>
      <td>203.9 (0.08)</td>
      <td>3.4 (0.0)</td>
    </tr>
    <tr>
      <td>Eric Prydz - Liberate</td>
      <td>203.45 (0.08)</td>
      <td>3.39 (0.0)</td>
    </tr>
    <tr>
      <td>Martin Solveig - Intoxicated - Radio Edit</td>
      <td>202.67 (0.08)</td>
      <td>3.38 (0.0)</td>
    </tr>
    <tr>
      <td>Street Child World Cup - I Am Somebody (feat. London Elektricity, S.P.Y, and Diane Charlemagne) - S.P.Y Remix</td>
      <td>202.43 (0.08)</td>
      <td>3.37 (0.0)</td>
    </tr>
    <tr>
      <td>ASTR - Operate - The Chainsmokers Remix</td>
      <td>202.13 (0.08)</td>
      <td>3.37 (0.0)</td>
    </tr>
    <tr>
      <td>The Prodigy - Breathe (The Glitch Mob Remix)</td>
      <td>201.57 (0.08)</td>
      <td>3.36 (0.0)</td>
    </tr>
    <tr>
      <td>Fat Boy Slim - Right Here, Right Now</td>
      <td>201.42 (0.08)</td>
      <td>3.36 (0.0)</td>
    </tr>
    <tr>
      <td>The Prodigy - Breathe (Zeds Dead Remix)</td>
      <td>200.78 (0.08)</td>
      <td>3.35 (0.0)</td>
    </tr>
    <tr>
      <td>Coasts - Oceans</td>
      <td>200.73 (0.08)</td>
      <td>3.35 (0.0)</td>
    </tr>
    <tr>
      <td>Metrik - Out Of The Fire</td>
      <td>200.48 (0.08)</td>
      <td>3.34 (0.0)</td>
    </tr>
    <tr>
      <td>Above & Beyond - Good For Me</td>
      <td>200.22 (0.07)</td>
      <td>3.34 (0.0)</td>
    </tr>
    <tr>
      <td>Nigel Good - Nothing Out Here</td>
      <td>200.13 (0.07)</td>
      <td>3.34 (0.0)</td>
    </tr>
    <tr>
      <td>Netsky - Prisma</td>
      <td>199.87 (0.07)</td>
      <td>3.33 (0.0)</td>
    </tr>
    <tr>
      <td>Illenium - Fractures (feat. Nevve)</td>
      <td>199.72 (0.07)</td>
      <td>3.33 (0.0)</td>
    </tr>
    <tr>
      <td>Hudson Mohawke - Scud Books</td>
      <td>198.92 (0.07)</td>
      <td>3.32 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - Reach - Original Mix</td>
      <td>198.72 (0.07)</td>
      <td>3.31 (0.0)</td>
    </tr>
    <tr>
      <td>Stephen - Line It Up</td>
      <td>196.98 (0.07)</td>
      <td>3.28 (0.0)</td>
    </tr>
    <tr>
      <td>C41 - Cardamom Mountains</td>
      <td>196.48 (0.07)</td>
      <td>3.27 (0.0)</td>
    </tr>
    <tr>
      <td>Evocativ - The Lovers, Pt. 2</td>
      <td>196.43 (0.07)</td>
      <td>3.27 (0.0)</td>
    </tr>
    <tr>
      <td>Kiasmos - Bent</td>
      <td>195.98 (0.07)</td>
      <td>3.27 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Uprising</td>
      <td>195.75 (0.07)</td>
      <td>3.26 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - The Wolves - Lenzman Remix</td>
      <td>195.27 (0.07)</td>
      <td>3.25 (0.0)</td>
    </tr>
    <tr>
      <td>Wilkinson - Take You Higher</td>
      <td>195.27 (0.07)</td>
      <td>3.25 (0.0)</td>
    </tr>
    <tr>
      <td>Dimension - Delight</td>
      <td>195.17 (0.07)</td>
      <td>3.25 (0.0)</td>
    </tr>
    <tr>
      <td>Muffler - Northern Lights</td>
      <td>195.13 (0.07)</td>
      <td>3.25 (0.0)</td>
    </tr>
    <tr>
      <td>Helios - Halving the Compass (Rhian Sheehan Remix)</td>
      <td>194.62 (0.07)</td>
      <td>3.24 (0.0)</td>
    </tr>
    <tr>
      <td>Metrik - Freefall - xKore Remix</td>
      <td>193.43 (0.07)</td>
      <td>3.22 (0.0)</td>
    </tr>
    <tr>
      <td>NCT - No One Home</td>
      <td>193.08 (0.07)</td>
      <td>3.22 (0.0)</td>
    </tr>
    <tr>
      <td>Arty - Together We Are (feat. Chris James)</td>
      <td>191.77 (0.07)</td>
      <td>3.2 (0.0)</td>
    </tr>
    <tr>
      <td>Jessie Siren - The Sway</td>
      <td>190.2 (0.07)</td>
      <td>3.17 (0.0)</td>
    </tr>
    <tr>
      <td>Stephen - Crossfire</td>
      <td>190.18 (0.07)</td>
      <td>3.17 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - When You're Ready - Original Mix</td>
      <td>189.4 (0.07)</td>
      <td>3.16 (0.0)</td>
    </tr>
    <tr>
      <td>Speaker of the House - Seein' Stars</td>
      <td>188.98 (0.07)</td>
      <td>3.15 (0.0)</td>
    </tr>
    <tr>
      <td>Shockline - Maya Bay</td>
      <td>188.65 (0.07)</td>
      <td>3.14 (0.0)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - 3D Glasses</td>
      <td>188.53 (0.07)</td>
      <td>3.14 (0.0)</td>
    </tr>
    <tr>
      <td>Raise Spirit - Don't Let Go</td>
      <td>188.05 (0.07)</td>
      <td>3.13 (0.0)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Minor Happy</td>
      <td>188.0 (0.07)</td>
      <td>3.13 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - Pessimist</td>
      <td>187.5 (0.07)</td>
      <td>3.13 (0.0)</td>
    </tr>
    <tr>
      <td>Jenaux - Get It On</td>
      <td>186.78 (0.07)</td>
      <td>3.11 (0.0)</td>
    </tr>
    <tr>
      <td>The Chainsmokers - Roses</td>
      <td>186.68 (0.07)</td>
      <td>3.11 (0.0)</td>
    </tr>
    <tr>
      <td>Kaskade - Lessons In Love (feat. Neon Trees)</td>
      <td>186.27 (0.07)</td>
      <td>3.1 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - Synchronize (feat. Aaron Richards)</td>
      <td>185.83 (0.07)</td>
      <td>3.1 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - Just A Thought</td>
      <td>185.72 (0.07)</td>
      <td>3.1 (0.0)</td>
    </tr>
    <tr>
      <td>Above & Beyond Group Therapy - Bigger Than Love [ABGT170]</td>
      <td>185.45 (0.07)</td>
      <td>3.09 (0.0)</td>
    </tr>
    <tr>
      <td>Wilkinson - Breathe</td>
      <td>185.35 (0.07)</td>
      <td>3.09 (0.0)</td>
    </tr>
    <tr>
      <td>Massive Attack - Teardrop</td>
      <td>185.08 (0.07)</td>
      <td>3.08 (0.0)</td>
    </tr>
    <tr>
      <td>ZHU - Automatic - Kasbo Remix</td>
      <td>185.07 (0.07)</td>
      <td>3.08 (0.0)</td>
    </tr>
    <tr>
      <td>Electus - Metaphysics</td>
      <td>184.6 (0.07)</td>
      <td>3.08 (0.0)</td>
    </tr>
    <tr>
      <td>Strife II - Year In The Rain</td>
      <td>184.57 (0.07)</td>
      <td>3.08 (0.0)</td>
    </tr>
    <tr>
      <td>Pendulum - Hold Your Colour</td>
      <td>184.47 (0.07)</td>
      <td>3.07 (0.0)</td>
    </tr>
    <tr>
      <td>Kiiara - Gold</td>
      <td>184.45 (0.07)</td>
      <td>3.07 (0.0)</td>
    </tr>
    <tr>
      <td>Mazde - Wicked Winds</td>
      <td>184.15 (0.07)</td>
      <td>3.07 (0.0)</td>
    </tr>
    <tr>
      <td>Dyro - Leprechauns & Unicorns - Original Mix</td>
      <td>184.02 (0.07)</td>
      <td>3.07 (0.0)</td>
    </tr>
    <tr>
      <td>Porter Robinson - Shelter</td>
      <td>183.77 (0.07)</td>
      <td>3.06 (0.0)</td>
    </tr>
    <tr>
      <td>Of Monsters And Men - Little Talks</td>
      <td>183.62 (0.07)</td>
      <td>3.06 (0.0)</td>
    </tr>
    <tr>
      <td>Above & Beyond Group Therapy - No More [ABGT160] - Instrumental Mix</td>
      <td>182.82 (0.07)</td>
      <td>3.05 (0.0)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Shine</td>
      <td>182.82 (0.07)</td>
      <td>3.05 (0.0)</td>
    </tr>
    <tr>
      <td>Logistics - Watching The World Go By</td>
      <td>182.52 (0.07)</td>
      <td>3.04 (0.0)</td>
    </tr>
    <tr>
      <td>EDEN - Billie Jean</td>
      <td>182.18 (0.07)</td>
      <td>3.04 (0.0)</td>
    </tr>
    <tr>
      <td>SomethingALaMode - Toy Spark Gun</td>
      <td>181.75 (0.07)</td>
      <td>3.03 (0.0)</td>
    </tr>
    <tr>
      <td>Tiësto - The Right Song</td>
      <td>181.03 (0.07)</td>
      <td>3.02 (0.0)</td>
    </tr>
  </table>
</details>

<details>
  <summary>Top 50 artists</summary>
  <table>
    <tr>
      <th>Artist</th>
      <th>Minutes Elapsed (% of the year)</th>
      <th>Hours Elapsed (% of the year)</th>
    </tr>
    <tr>
      <td>Various Artists</td>
      <td>12768.22 (4.78)</td>
      <td>212.8 (0.08)</td>
    </tr>
    <tr>
      <td>Uppermost</td>
      <td>11728.03 (4.39)</td>
      <td>195.47 (0.07)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix</td>
      <td>4949.88 (1.85)</td>
      <td>82.5 (0.03)</td>
    </tr>
    <tr>
      <td>Maduk</td>
      <td>3636.88 (1.36)</td>
      <td>60.61 (0.02)</td>
    </tr>
    <tr>
      <td>Phaeleh</td>
      <td>3603.15 (1.35)</td>
      <td>60.05 (0.02)</td>
    </tr>
    <tr>
      <td>Ed Sheeran</td>
      <td>3164.27 (1.18)</td>
      <td>52.74 (0.02)</td>
    </tr>
    <tr>
      <td>Above & Beyond Group Therapy</td>
      <td>3095.43 (1.16)</td>
      <td>51.59 (0.02)</td>
    </tr>
    <tr>
      <td>Logistics</td>
      <td>2460.38 (0.92)</td>
      <td>41.01 (0.02)</td>
    </tr>
    <tr>
      <td>ShockOne</td>
      <td>2270.37 (0.85)</td>
      <td>37.84 (0.01)</td>
    </tr>
    <tr>
      <td>MitiS</td>
      <td>2211.63 (0.83)</td>
      <td>36.86 (0.01)</td>
    </tr>
    <tr>
      <td>The Glitch Mob</td>
      <td>2170.52 (0.81)</td>
      <td>36.18 (0.01)</td>
    </tr>
    <tr>
      <td>Camo & Krooked</td>
      <td>2057.93 (0.77)</td>
      <td>34.3 (0.01)</td>
    </tr>
    <tr>
      <td>SomethingALaMode</td>
      <td>1956.37 (0.73)</td>
      <td>32.61 (0.01)</td>
    </tr>
    <tr>
      <td>Pendulum</td>
      <td>1860.75 (0.7)</td>
      <td>31.01 (0.01)</td>
    </tr>
    <tr>
      <td>Illenium</td>
      <td>1853.82 (0.69)</td>
      <td>30.9 (0.01)</td>
    </tr>
    <tr>
      <td>Madeon</td>
      <td>1849.45 (0.69)</td>
      <td>30.82 (0.01)</td>
    </tr>
    <tr>
      <td>deadmau5</td>
      <td>1632.42 (0.61)</td>
      <td>27.21 (0.01)</td>
    </tr>
    <tr>
      <td>EDEN</td>
      <td>1566.03 (0.59)</td>
      <td>26.1 (0.01)</td>
    </tr>
    <tr>
      <td>Tantrum Desire</td>
      <td>1533.07 (0.57)</td>
      <td>25.55 (0.01)</td>
    </tr>
    <tr>
      <td>Metrik</td>
      <td>1506.32 (0.56)</td>
      <td>25.11 (0.01)</td>
    </tr>
    <tr>
      <td>Daft Punk</td>
      <td>1388.43 (0.52)</td>
      <td>23.14 (0.01)</td>
    </tr>
    <tr>
      <td>Chase & Status</td>
      <td>1335.83 (0.5)</td>
      <td>22.26 (0.01)</td>
    </tr>
    <tr>
      <td>Pierce Fulton</td>
      <td>1269.0 (0.48)</td>
      <td>21.15 (0.01)</td>
    </tr>
    <tr>
      <td>Koven</td>
      <td>1265.02 (0.47)</td>
      <td>21.08 (0.01)</td>
    </tr>
    <tr>
      <td>Yellowcard</td>
      <td>1197.72 (0.45)</td>
      <td>19.96 (0.01)</td>
    </tr>
    <tr>
      <td>Kiasmos</td>
      <td>1171.23 (0.44)</td>
      <td>19.52 (0.01)</td>
    </tr>
    <tr>
      <td>Puppet</td>
      <td>1077.67 (0.4)</td>
      <td>17.96 (0.01)</td>
    </tr>
    <tr>
      <td>Feint</td>
      <td>1064.63 (0.4)</td>
      <td>17.74 (0.01)</td>
    </tr>
    <tr>
      <td>Porter Robinson</td>
      <td>1029.48 (0.39)</td>
      <td>17.16 (0.01)</td>
    </tr>
    <tr>
      <td>Keeno</td>
      <td>1001.75 (0.38)</td>
      <td>16.7 (0.01)</td>
    </tr>
    <tr>
      <td>Netsky</td>
      <td>988.05 (0.37)</td>
      <td>16.47 (0.01)</td>
    </tr>
    <tr>
      <td>Hybrid Minds</td>
      <td>979.83 (0.37)</td>
      <td>16.33 (0.01)</td>
    </tr>
    <tr>
      <td>Sub Focus</td>
      <td>967.5 (0.36)</td>
      <td>16.13 (0.01)</td>
    </tr>
    <tr>
      <td>Martin Garrix</td>
      <td>937.92 (0.35)</td>
      <td>15.63 (0.01)</td>
    </tr>
    <tr>
      <td>The Eden Project</td>
      <td>910.92 (0.34)</td>
      <td>15.18 (0.01)</td>
    </tr>
    <tr>
      <td>Gryffin</td>
      <td>893.0 (0.33)</td>
      <td>14.88 (0.01)</td>
    </tr>
    <tr>
      <td>L Plus</td>
      <td>878.63 (0.33)</td>
      <td>14.64 (0.01)</td>
    </tr>
    <tr>
      <td>Seven Lions</td>
      <td>875.6 (0.33)</td>
      <td>14.59 (0.01)</td>
    </tr>
    <tr>
      <td>Stephen</td>
      <td>856.35 (0.32)</td>
      <td>14.27 (0.01)</td>
    </tr>
    <tr>
      <td>The Chainsmokers</td>
      <td>826.0 (0.31)</td>
      <td>13.77 (0.01)</td>
    </tr>
    <tr>
      <td>Above & Beyond</td>
      <td>796.0 (0.3)</td>
      <td>13.27 (0.0)</td>
    </tr>
    <tr>
      <td>Linkin Park</td>
      <td>795.28 (0.3)</td>
      <td>13.25 (0.0)</td>
    </tr>
    <tr>
      <td>Snow Patrol</td>
      <td>794.77 (0.3)</td>
      <td>13.25 (0.0)</td>
    </tr>
    <tr>
      <td>Morgan Page</td>
      <td>775.52 (0.29)</td>
      <td>12.93 (0.0)</td>
    </tr>
    <tr>
      <td>Martin Solveig</td>
      <td>756.52 (0.28)</td>
      <td>12.61 (0.0)</td>
    </tr>
    <tr>
      <td>Robin Schulz</td>
      <td>726.18 (0.27)</td>
      <td>12.1 (0.0)</td>
    </tr>
    <tr>
      <td>Aurix</td>
      <td>724.83 (0.27)</td>
      <td>12.08 (0.0)</td>
    </tr>
    <tr>
      <td>Wilkinson</td>
      <td>700.9 (0.26)</td>
      <td>11.68 (0.0)</td>
    </tr>
    <tr>
      <td>Rudimental</td>
      <td>687.67 (0.26)</td>
      <td>11.46 (0.0)</td>
    </tr>
    <tr>
      <td>OneRepublic</td>
      <td>673.77 (0.25)</td>
      <td>11.23 (0.0)</td>
    </tr>
  </table>
</details>

<details>
  <summary>Top 50 albums</summary>
  <table>
    <tr>
      <th>Album Title</th>
      <th>Minutes Elapsed (% of the year)</th>
      <th>Hours Elapsed (% of the year)</th>
    </tr>
    <tr>
      <td>Uppermost - One</td>
      <td>2307.53 (0.86)</td>
      <td>38.46 (0.01)</td>
    </tr>
    <tr>
      <td>Various Artists - Galaxy of Dreams 2</td>
      <td>2262.48 (0.85)</td>
      <td>37.71 (0.01)</td>
    </tr>
    <tr>
      <td>Above & Beyond Group Therapy - Group Therapy 160</td>
      <td>2187.93 (0.82)</td>
      <td>36.47 (0.01)</td>
    </tr>
    <tr>
      <td>Ed Sheeran - x</td>
      <td>2187.52 (0.82)</td>
      <td>36.46 (0.01)</td>
    </tr>
    <tr>
      <td>Uppermost - Revolution</td>
      <td>2133.2 (0.8)</td>
      <td>35.55 (0.01)</td>
    </tr>
    <tr>
      <td>Uppermost - Evolution</td>
      <td>1910.22 (0.72)</td>
      <td>31.84 (0.01)</td>
    </tr>
    <tr>
      <td>ShockOne - Universus</td>
      <td>1811.17 (0.68)</td>
      <td>30.19 (0.01)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Recognise</td>
      <td>1724.52 (0.65)</td>
      <td>28.74 (0.01)</td>
    </tr>
    <tr>
      <td>Fred V & Grafix - Unrecognisable</td>
      <td>1721.82 (0.64)</td>
      <td>28.7 (0.01)</td>
    </tr>
    <tr>
      <td>Pendulum - In Silico</td>
      <td>1413.72 (0.53)</td>
      <td>23.56 (0.01)</td>
    </tr>
    <tr>
      <td>Daft Punk - TRON: Legacy</td>
      <td>1179.73 (0.44)</td>
      <td>19.66 (0.01)</td>
    </tr>
    <tr>
      <td>Uppermost - Origins (2011-2016)</td>
      <td>1081.47 (0.4)</td>
      <td>18.02 (0.01)</td>
    </tr>
    <tr>
      <td>Kiasmos - Kiasmos</td>
      <td>1078.63 (0.4)</td>
      <td>17.98 (0.01)</td>
    </tr>
    <tr>
      <td>Phaeleh - Clarity</td>
      <td>1048.85 (0.39)</td>
      <td>17.48 (0.01)</td>
    </tr>
    <tr>
      <td>Uppermost - New Moon - EP</td>
      <td>998.25 (0.37)</td>
      <td>16.64 (0.01)</td>
    </tr>
    <tr>
      <td>Logistics - Polyphony</td>
      <td>969.92 (0.36)</td>
      <td>16.17 (0.01)</td>
    </tr>
    <tr>
      <td>Uppermost - Action</td>
      <td>931.85 (0.35)</td>
      <td>15.53 (0.01)</td>
    </tr>
    <tr>
      <td>MitiS - Life Of Sin Series</td>
      <td>909.32 (0.34)</td>
      <td>15.16 (0.01)</td>
    </tr>
    <tr>
      <td>Illenium - Ashes</td>
      <td>826.02 (0.31)</td>
      <td>13.77 (0.01)</td>
    </tr>
    <tr>
      <td>Madeon - The City</td>
      <td>752.48 (0.28)</td>
      <td>12.54 (0.0)</td>
    </tr>
    <tr>
      <td>deadmau5 - For Lack Of A Better Name</td>
      <td>693.73 (0.26)</td>
      <td>11.56 (0.0)</td>
    </tr>
    <tr>
      <td>Various Artists - Summer Vibes 2012</td>
      <td>686.95 (0.26)</td>
      <td>11.45 (0.0)</td>
    </tr>
    <tr>
      <td>EDEN - i think you think too much of me</td>
      <td>668.25 (0.25)</td>
      <td>11.14 (0.0)</td>
    </tr>
    <tr>
      <td>Maduk - Never Give Up</td>
      <td>658.67 (0.25)</td>
      <td>10.98 (0.0)</td>
    </tr>
    <tr>
      <td>Stephen - Sincerely</td>
      <td>649.37 (0.24)</td>
      <td>10.82 (0.0)</td>
    </tr>
    <tr>
      <td>Madeon - Adventure (Deluxe)</td>
      <td>614.6 (0.23)</td>
      <td>10.24 (0.0)</td>
    </tr>
    <tr>
      <td>Thirty Seconds To Mars - This Is War</td>
      <td>591.1 (0.22)</td>
      <td>9.85 (0.0)</td>
    </tr>
    <tr>
      <td>SomethingALaMode - 28.18 Moment</td>
      <td>590.3 (0.22)</td>
      <td>9.84 (0.0)</td>
    </tr>
    <tr>
      <td>Camo & Krooked - Zeitgeist</td>
      <td>564.42 (0.21)</td>
      <td>9.41 (0.0)</td>
    </tr>
    <tr>
      <td>Uppermost - Given by Nature</td>
      <td>557.83 (0.21)</td>
      <td>9.3 (0.0)</td>
    </tr>
    <tr>
      <td>Phaeleh - Fallen Light</td>
      <td>552.45 (0.21)</td>
      <td>9.21 (0.0)</td>
    </tr>
    <tr>
      <td>Ryan Davis - From Within EP</td>
      <td>548.87 (0.21)</td>
      <td>9.15 (0.0)</td>
    </tr>
    <tr>
      <td>Hybrid Minds - Mountains Remixed</td>
      <td>525.3 (0.2)</td>
      <td>8.75 (0.0)</td>
    </tr>
    <tr>
      <td>L Plus - Amazing EP</td>
      <td>511.73 (0.19)</td>
      <td>8.53 (0.0)</td>
    </tr>
    <tr>
      <td>Maduk - Feel Good</td>
      <td>501.98 (0.19)</td>
      <td>8.37 (0.0)</td>
    </tr>
    <tr>
      <td>Puppet - The Fire</td>
      <td>498.87 (0.19)</td>
      <td>8.31 (0.0)</td>
    </tr>
    <tr>
      <td>SomethingALaMode - Endless Stairs</td>
      <td>496.82 (0.19)</td>
      <td>8.28 (0.0)</td>
    </tr>
    <tr>
      <td>OneRepublic - Dreaming Out Loud</td>
      <td>494.1 (0.18)</td>
      <td>8.24 (0.0)</td>
    </tr>
    <tr>
      <td>Camo & Krooked - Cross The Line</td>
      <td>493.28 (0.18)</td>
      <td>8.22 (0.0)</td>
    </tr>
    <tr>
      <td>SomethingALaMode - SomethingALaMode</td>
      <td>488.82 (0.18)</td>
      <td>8.15 (0.0)</td>
    </tr>
    <tr>
      <td>Ed Sheeran - ÷</td>
      <td>472.93 (0.18)</td>
      <td>7.88 (0.0)</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Diversified</td>
      <td>471.13 (0.18)</td>
      <td>7.85 (0.0)</td>
    </tr>
    <tr>
      <td>Bring Me The Horizon - That's The Spirit</td>
      <td>469.65 (0.18)</td>
      <td>7.83 (0.0)</td>
    </tr>
    <tr>
      <td>Sub Focus - Torus</td>
      <td>461.45 (0.17)</td>
      <td>7.69 (0.0)</td>
    </tr>
    <tr>
      <td>The Eden Project - Bipolar Paradise</td>
      <td>446.85 (0.17)</td>
      <td>7.45 (0.0)</td>
    </tr>
    <tr>
      <td>Peter Gregson - A Little Chaos (Original Motion Picture Soundtrack)</td>
      <td>436.3 (0.16)</td>
      <td>7.27 (0.0)</td>
    </tr>
    <tr>
      <td>Koven - Hereinafter</td>
      <td>433.23 (0.16)</td>
      <td>7.22 (0.0)</td>
    </tr>
    <tr>
      <td>Phaeleh - The Cold in You</td>
      <td>432.22 (0.16)</td>
      <td>7.2 (0.0)</td>
    </tr>
    <tr>
      <td>Massive Attack - Mezzanine</td>
      <td>431.53 (0.16)</td>
      <td>7.19 (0.0)</td>
    </tr>
    <tr>
      <td>Phaeleh - Tides</td>
      <td>430.33 (0.16)</td>
      <td>7.17 (0.0)</td>
    </tr>
  </table>
</details>
