{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-01-28T10:24:04+00:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/236961705/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/236961705/"
    ],
    "published": [
      "2017-01-28T10:24:04+00:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Women-In-Tech-Nottingham/events/236961705/"
      ],
      "name": [
        "Hack24 and The 12 Principles of Animation"
      ],
      "start": [
        "2017-02-02T18:30:00Z"
      ],
      "end": [
        "2017-02-02T18:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Accelerate Places, The Poynt, 45 Wollaton Street, Nottingham, NG1 5FW"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/01/mczby",
  "aliases": [
    "/mf2/09e8d099-3607-41ac-bf60-ae314a461109/",
    "/mf2/2017/01/mczby",
    "/mf2/2017/01/mCzby"
  ]
}
