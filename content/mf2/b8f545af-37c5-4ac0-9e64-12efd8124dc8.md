{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottAgile/events/258462490/"
    ],
    "published": [
      "2019-05-19T20:50:58+0100"
    ],
    "category": [
      "agile-nottingham"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottAgile/events/258462490/"
      ],
      "name": [
        "Agile Nottingham - Design Thinking Dad* Why I take work home (and you should too)"
      ],
      "start": [
        "2019-05-22T18:30:00+0100"
      ],
      "end": [
        "2019-05-22T21:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Capital One (Europe) plc, Trent House, Station Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/b8f545af-37c5-4ac0-9e64-12efd8124dc8/",
    "/mf2/b8f545af-37c5-4ac0-9e64-12efd8124dc8/",
    "/mf2/2019/05/1czyk",
    "/mf2/2019/05/1Czyk"
  ],
  "h": "h-entry",
  "date": "2019-05-19T20:50:58+0100",
  "tags": [
    "agile-nottingham"
  ],
  "kind": "rsvps",
  "slug": "2019/05/1czyk"
}
