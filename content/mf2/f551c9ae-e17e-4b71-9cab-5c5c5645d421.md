{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/qhnpfqyzlbrb/"
    ],
    "published": [
      "2019-07-31T08:02:00+0100"
    ],
    "category": [
      "notts-js"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/qhnpfqyzlbrb/"
      ],
      "name": [
        "JS in CSS, the magic of Houdini"
      ],
      "start": [
        "2019-08-13T18:00:00+0100"
      ],
      "end": [
        "2019-08-13T21:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Capital One (Europe) plc, Trent House, Station Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/f551c9ae-e17e-4b71-9cab-5c5c5645d421/",
    "/mf2/f551c9ae-e17e-4b71-9cab-5c5c5645d421/",
    "/mf2/2019/07/s7oq4",
    "/mf2/2019/07/S7OQ4"
  ],
  "h": "h-entry",
  "date": "2019-07-31T08:02:00+0100",
  "tags": [
    "notts-js"
  ],
  "kind": "rsvps",
  "slug": "2019/07/s7oq4"
}
