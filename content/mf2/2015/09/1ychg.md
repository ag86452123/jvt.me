{
  "kind": "rsvps",
  "slug": "2015/09/1ychg",
  "date": "2015-09-25T09:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/uon-annual-week1-programming-competition-2015-tickets-18508636831"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/uon-annual-week1-programming-competition-2015-tickets-18508636831"
    ],
    "published": [
      "2015-09-25T09:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/uon-annual-week1-programming-competition-2015-tickets-18508636831"
      ],
      "name": [
        "UoN Annual Week1 Programming Competition 2015"
      ],
      "start": [
        "2015-09-25T09:00:00Z"
      ],
      "end": [
        "2015-09-25T15:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Computer Science, Jubilee Tampus"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG8 1BB"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
