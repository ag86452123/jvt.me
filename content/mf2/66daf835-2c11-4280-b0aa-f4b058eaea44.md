{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2016-03-28T14:28:19+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/229931414/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/229931414/"
    ],
    "published": [
      "2016-03-28T14:28:19+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/229931414/"
      ],
      "name": [
        "Tech Nottingham April 2016 - Pusher presents An Introduction to React"
      ],
      "start": [
        "2016-04-04T18:30:00+01:00"
      ],
      "end": [
        "2016-04-04T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2016/03/w0uby",
  "aliases": [
    "/mf2/66daf835-2c11-4280-b0aa-f4b058eaea44/",
    "/mf2/2016/03/w0uby",
    "/mf2/2016/03/w0Uby"
  ]
}
