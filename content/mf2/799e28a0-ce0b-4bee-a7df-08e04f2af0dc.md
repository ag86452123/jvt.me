{
  "properties": {
    "rsvp": [
      "yes"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/261461207/"
    ],
    "published": [
      "2019-05-16T20:51:53+0100"
    ],
    "category": [
      "wit-notts"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Women-In-Tech-Nottingham/events/261461207/"
      ],
      "name": [
        "Women In Tech June 2019 - Negotiating For Fun And Profit"
      ],
      "start": [
        "2019-06-06T18:30:00+0100"
      ],
      "end": [
        "2019-06-06T21:00:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/rsvps/799e28a0-ce0b-4bee-a7df-08e04f2af0dc/",
    "/mf2/799e28a0-ce0b-4bee-a7df-08e04f2af0dc/",
    "/mf2/2019/05/akaga",
    "/mf2/2019/05/AkAgA"
  ],
  "h": "h-entry",
  "date": "2019-05-16T20:51:53+0100",
  "tags": [
    "wit-notts"
  ],
  "kind": "rsvps",
  "slug": "2019/05/akaga"
}
