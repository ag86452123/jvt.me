{
  "kind": "rsvps",
  "slug": "2016/11/pa0o4",
  "date": "2016-11-19T09:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.com/e/oxfordhack-2016-tickets-28395675229"
    ],
    "in-reply-to": [
      "https://www.eventbrite.com/e/oxfordhack-2016-tickets-28395675229"
    ],
    "published": [
      "2016-11-19T09:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.com/e/oxfordhack-2016-tickets-28395675229"
      ],
      "name": [
        "OxfordHack 2016"
      ],
      "start": [
        "2016-11-19T09:00:00Z"
      ],
      "end": [
        "2016-11-20T16:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Maths Department, Andrew Wiles Building, University of Oxford, Radcliffe Observatory Quater, Woodstock Rd"
          ],
          "locality": [
            "Oxford"
          ],
          "postal-code": [
            "OX2 6GG"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
