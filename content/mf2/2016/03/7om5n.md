{
  "kind": "rsvps",
  "slug": "2016/03/7om5n",
  "date": "2016-03-19T10:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/hack24-nottingham-2016-tickets-21282173554"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/hack24-nottingham-2016-tickets-21282173554"
    ],
    "published": [
      "2016-03-19T10:30:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/hack24-nottingham-2016-tickets-21282173554"
      ],
      "name": [
        "Hack24 Nottingham 2016"
      ],
      "start": [
        "2016-03-19T10:30:00Z"
      ],
      "end": [
        "2016-03-20T18:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Old Market Square"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 2DT"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
