{
  "kind": "rsvps",
  "slug": "2016/12/xnei8",
  "date": "2016-12-08T19:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/notttechparty-the-nottingham-tech-community-christmas-party-tickets-29264117764"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/notttechparty-the-nottingham-tech-community-christmas-party-tickets-29264117764"
    ],
    "published": [
      "2016-12-08T19:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/notttechparty-the-nottingham-tech-community-christmas-party-tickets-29264117764"
      ],
      "name": [
        "#NottTechParty - The Nottingham Tech Community Christmas Party"
      ],
      "start": [
        "2016-12-08T19:00:00Z"
      ],
      "end": [
        "2016-12-08T22:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "24-32 Carlton Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 1NN"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
