{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-04-18T17:11:10+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/239298899/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/239298899/"
    ],
    "published": [
      "2017-04-18T17:11:10+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/239298899/"
      ],
      "name": [
        "Tech Nottingham May 2017: Fun with Clojure!"
      ],
      "start": [
        "2017-05-08T18:30:00+01:00"
      ],
      "end": [
        "2017-05-08T18:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/04/ao93p",
  "aliases": [
    "/mf2/7619f7af-e286-4c05-bd54-76ca5d3a9798/",
    "/mf2/2017/04/ao93p",
    "/mf2/2017/04/AO93p"
  ]
}
