{
  "properties": {
    "name": [
      "https://medium.com/@skpodila/why-do-i-care-c2ef43a25837"
    ],
    "bookmark-of": [
      "https://medium.com/@skpodila/why-do-i-care-c2ef43a25837"
    ],
    "content": [
      {
        "value": "As with 'You Guys', this is another great view into why using inclusive phrases are incredibly important.",
        "html": ""
      }
    ],
    "published": [
      "2019-06-17T19:56:35+0100"
    ],
    "category": [
      "diversity-and-inclusion",
      "communication",
      "guys"
    ]
  },
  "aliases": [
    "/bookmarks/ba1d3053-ce84-4f2f-903f-13f5cbe9566d/",
    "/mf2/ba1d3053-ce84-4f2f-903f-13f5cbe9566d/",
    "/mf2/2019/06/q5tro",
    "/mf2/2019/06/Q5tRo"
  ],
  "h": "h-entry",
  "date": "2019-06-17T19:56:35+0100",
  "tags": [
    "diversity-and-inclusion",
    "communication",
    "guys"
  ],
  "kind": "bookmarks",
  "slug": "2019/06/q5tro"
}
