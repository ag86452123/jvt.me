{
  "kind": "rsvps",
  "date": "2019-09-12T22:58:00+0100",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.jvt.me/events/homebrew-website-club-nottingham/2019/10/02/"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/10/02/"
    ],
    "published": [
      "2019-09-12T22:58:00+0100"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/10/02/"
      ],
      "name": [
        "Homebrew Website Club: Nottingham"
      ],
      "start": [
        "2019-10-02T17:30:00+0100"
      ],
      "end": [
        "2019-10-02T19:30:00+0100"
      ],
      "location": {
        "properties": {
          "street-address": [
            "72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2019/09/a5y2u",
  "aliases": [
    "/mf2/6c024ae0-1c12-47c6-9630-b7349791593b/",
    "/mf2/2019/09/a5y2u",
    "/mf2/2019/09/a5Y2U"
  ]
}
