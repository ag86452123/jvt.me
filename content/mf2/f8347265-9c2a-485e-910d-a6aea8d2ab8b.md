{
  "properties": {
    "name": [
      "https://monzo.com/blog/2019/06/20/why-bank-transfers-failed-on-30th-may-2019/"
    ],
    "bookmark-of": [
      "https://monzo.com/blog/2019/06/20/why-bank-transfers-failed-on-30th-may-2019/"
    ],
    "content": [
      {
        "value": "This is a really interesting read from Monzo about a recent incident they had. I really enjoy reading their incident management writeups because they show a tonne of detail, yet are stakeholder-friendly.\n\nIt's always interesting to see how other banks deal with issues like this, and what they would do to make things better next time.",
        "html": ""
      }
    ],
    "published": [
      "2019-06-23T21:25:20+0100"
    ],
    "category": [
      "incident-management"
    ]
  },
  "aliases": [
    "/bookmarks/f8347265-9c2a-485e-910d-a6aea8d2ab8b/",
    "/mf2/f8347265-9c2a-485e-910d-a6aea8d2ab8b/",
    "/mf2/2019/06/8ebb3",
    "/mf2/2019/06/8eBB3"
  ],
  "h": "h-entry",
  "date": "2019-06-23T21:25:20+0100",
  "tags": [
    "incident-management"
  ],
  "kind": "bookmarks",
  "slug": "2019/06/8ebb3"
}
