{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2016-07-23T19:01:59+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/232824430/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/232824430/"
    ],
    "published": [
      "2016-07-23T19:01:59+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/232824430/"
      ],
      "name": [
        "Tech Nottingham August 2016 - Life After Flash with Harry Boyes"
      ],
      "start": [
        "2016-08-01T18:30:00+01:00"
      ],
      "end": [
        "2016-08-01T18:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2016/07/tywdi",
  "aliases": [
    "/mf2/1afb4712-bf75-43ff-9cbb-6a4acbb4fb17/",
    "/mf2/2016/07/tywdi",
    "/mf2/2016/07/tYWdI"
  ]
}
