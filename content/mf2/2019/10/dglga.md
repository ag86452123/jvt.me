{
  "kind": "rsvps",
  "slug": "2019/10/dglga",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-10-23T16:00:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/265770253/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/265770253/"
    ],
    "published": [
      "2019-10-23T16:00:00+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Women-In-Tech-Nottingham/events/265770253/"
      ],
      "name": [
        "C̶h̶e̶c̶k̶ Test yourself before you wreck yourself"
      ],
      "start": [
        "2019-11-07T18:30:00Z"
      ],
      "end": [
        "2019-11-07T21:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Beck St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/mf2/2019/10/dglga",
    "/mf2/2019/10/DglGa"
  ]
}
