{
  "kind": "bookmarks",
  "slug": "2019/10/9irjl",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-10-28T20:23:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "A push gone very wrong in the name of what, exactly?"
    ],
    "bookmark-of": [
      "https://rachelbythebay.com/w/2019/10/25/enabler/"
    ],
    "published": [
      "2019-10-28T20:23:00Z"
    ]
  },
  "aliases": [
    "/mf2/2019/10/9irjl",
    "/mf2/2019/10/9iRjL"
  ]
}
