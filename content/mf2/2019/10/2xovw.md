{
  "kind": "rsvps",
  "slug": "2019/10/2xovw",
  "date": "2019-10-26T17:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.com/e/pubconf-nottingham-tickets-72233483293?"
    ],
    "in-reply-to": [
      "https://www.eventbrite.com/e/pubconf-nottingham-tickets-72233483293?"
    ],
    "published": [
      "2019-10-26T17:30:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.com/e/pubconf-nottingham-tickets-72233483293?"
      ],
      "name": [
        "PubConf Nottingham"
      ],
      "start": [
        "2019-10-26T17:30:00Z"
      ],
      "end": [
        "2019-10-26T22:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Trinity Square"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 4AF"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
