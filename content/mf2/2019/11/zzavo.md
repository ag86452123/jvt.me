{
  "kind": "rsvps",
  "slug": "2019/11/zzavo",
  "client_id": "https://micropublish.net",
  "date": "2019-11-19T23:12:07.537+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/dotnetnotts/events/265504966/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/dotnetnotts/events/265504966/"
    ],
    "published": [
      "2019-11-19T23:12:07.537+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "content": [
      {
        "html": "",
        "value": ""
      }
    ],
    "event": {
      "url": [
        "https://www.meetup.com/dotnetnotts/events/265504966/"
      ],
      "name": [
        "Jon Skeet - C# 8 New Features"
      ],
      "start": [
        "2019-11-25T18:30:00Z"
      ],
      "end": [
        "2019-11-25T21:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "BJSS, 16 King St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
