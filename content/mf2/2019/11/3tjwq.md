{
  "kind": "rsvps",
  "slug": "2019/11/3tjwq",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-11-05T14:51:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/266227370/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/266227370/"
    ],
    "published": [
      "2019-11-05T14:51:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "content": [
      {
        "html": "",
        "value": ""
      }
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/266227370/"
      ],
      "name": [
        "#NottTechParty - The Nottingham Tech Community Christmas Party 2019"
      ],
      "start": [
        "2019-12-02T18:30:00Z"
      ],
      "end": [
        "2019-12-02T22:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Penny Lane, 9 Fletcher Gate"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/mf2/2019/11/3tjwq",
    "/mf2/2019/11/3TjWQ"
  ]
}
