{
  "kind": "rsvps",
  "slug": "2019/11/urcrg",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-11-28T17:32:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/notttechparty-the-nottingham-tech-community-christmas-party-2019-tickets-78355696999"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/notttechparty-the-nottingham-tech-community-christmas-party-2019-tickets-78355696999"
    ],
    "published": [
      "2019-11-28T17:32:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/notttechparty-the-nottingham-tech-community-christmas-party-2019-tickets-78355696999"
      ],
      "name": [
        "#NottTechParty - The Nottingham Tech Community Christmas Party 2019"
      ],
      "start": [
        "2019-12-02T18:30:00Z"
      ],
      "end": [
        "2019-12-02T22:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Fletcher Gate, null"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": null,
          "postal-code": [
            "NG1 1QQ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
