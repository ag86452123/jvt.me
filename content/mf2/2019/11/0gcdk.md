{
  "kind": "rsvps",
  "slug": "2019/11/0gcdk",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-11-05T22:54:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.jvt.me/events/homebrew-website-club-nottingham/2019/11/13/"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/11/13/"
    ],
    "published": [
      "2019-11-05T22:54:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "content": [
      {
        "value": "",
        "html": "<p>Unfortunately I'm going to be unable to make this due to family circumstances - I hope some of you still make it and it's a good one!</p>"
      }
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2019/11/13/"
      ],
      "name": [
        "Homebrew Website Club: Nottingham"
      ],
      "start": [
        "2019-11-13T17:30:00+0000"
      ],
      "end": [
        "2019-11-13T19:30:00+0000"
      ],
      "location": {
        "properties": {
          "street-address": [
            "72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "aliases": [
    "/mf2/2019/11/0gcdk",
    "/mf2/2019/11/0GCDk"
  ]
}
