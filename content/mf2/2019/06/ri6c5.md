{
  "kind": "rsvps",
  "slug": "2019/06/ri6c5",
  "date": "2019-06-29T09:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/speakers-workshop-tickets-61755351920"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/speakers-workshop-tickets-61755351920"
    ],
    "published": [
      "2019-06-29T09:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/speakers-workshop-tickets-61755351920"
      ],
      "name": [
        "Speakers Workshop"
      ],
      "start": [
        "2019-06-29T09:00:00Z"
      ],
      "end": [
        "2019-06-29T16:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "3rd Floor, 16 King Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 2AS"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
