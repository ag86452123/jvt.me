{
  "kind": "rsvps",
  "slug": "2019/12/dqxah",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-12-04T07:54:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/NottsJS/events/vjnvhryzqbnb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/vjnvhryzqbnb/"
    ],
    "published": [
      "2019-12-04T07:54:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/vjnvhryzqbnb/"
      ],
      "name": [
        "DotJS debrief and Christmas quiz!"
      ],
      "start": [
        "2019-12-10T18:00:00Z"
      ],
      "end": [
        "2019-12-10T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Capital One (Europe) plc, Station St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
