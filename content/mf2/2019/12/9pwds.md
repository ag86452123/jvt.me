{
  "kind": "rsvps",
  "slug": "2019/12/9pwds",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-12-28T20:15:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.jvt.me/events/homebrew-website-club-nottingham/2020/01/22/"
    ],
    "in-reply-to": [
      "https://www.jvt.me/events/homebrew-website-club-nottingham/2020/01/22/"
    ],
    "published": [
      "2019-12-28T20:15:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "content": [
      {
        "html": "",
        "value": "I won't be able to make it as I'm in Tenerife, but hope it's a good one!"
      }
    ],
    "event": {
      "url": [
        "https://www.jvt.me/events/homebrew-website-club-nottingham/2020/01/22/"
      ],
      "name": [
        "Homebrew Website Club: Nottingham"
      ],
      "start": [
        "2020-01-22T17:30:00+0000"
      ],
      "end": [
        "2020-01-22T19:30:00+0000"
      ],
      "location": {
        "properties": {
          "street-address": [
            "72 Maid Marian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ],
          "postal-code": [
            "NG1 6BJ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
