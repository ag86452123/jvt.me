{
  "kind": "rsvps",
  "slug": "2019/12/orlfs",
  "client_id": "https://indigenous.realize.be",
  "date": "2019-12-19T14:26:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/DevOps-Notts/events/266765057/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/DevOps-Notts/events/266765057/"
    ],
    "published": [
      "2019-12-19T14:26:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/DevOps-Notts/events/266765057/"
      ],
      "name": [
        "DevOps Notts - January 2020"
      ],
      "start": [
        "2020-01-28T18:00:00Z"
      ],
      "end": [
        "2020-01-28T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Rebel Recruiters, Huntingdon St"
          ],
          "locality": [
            "Nottinghamshire"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
