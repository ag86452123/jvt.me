{
  "kind": "rsvps",
  "slug": "2018/11/kpl3s",
  "date": "2018-11-10T10:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/lincolnhack-2018-tickets-50209435774"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/lincolnhack-2018-tickets-50209435774"
    ],
    "published": [
      "2018-11-10T10:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/lincolnhack-2018-tickets-50209435774"
      ],
      "name": [
        "LincolnHack 2018"
      ],
      "start": [
        "2018-11-10T10:00:00Z"
      ],
      "end": [
        "2018-11-11T17:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Ruston Way"
          ],
          "locality": [
            "Lincoln"
          ],
          "postal-code": [
            "LN6 7FL"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
