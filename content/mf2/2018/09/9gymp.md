{
  "kind": "rsvps",
  "slug": "2018/09/9gymp",
  "date": "2018-09-20T08:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.com/e/devopsdays-london-2018-tickets-45511364717"
    ],
    "in-reply-to": [
      "https://www.eventbrite.com/e/devopsdays-london-2018-tickets-45511364717"
    ],
    "published": [
      "2018-09-20T08:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.com/e/devopsdays-london-2018-tickets-45511364717"
      ],
      "name": [
        "Devopsdays London 2018"
      ],
      "start": [
        "2018-09-20T08:00:00Z"
      ],
      "end": [
        "2018-09-21T16:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Broad Sanctuary , Westminster"
          ],
          "locality": [
            "London"
          ],
          "postal-code": [
            "SW1P 3EE"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
