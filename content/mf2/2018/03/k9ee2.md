{
  "kind": "rsvps",
  "slug": "2018/03/k9ee2",
  "date": "2018-03-10T09:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/hack24-2018-registration-42703082068"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/hack24-2018-registration-42703082068"
    ],
    "published": [
      "2018-03-10T09:30:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/hack24-2018-registration-42703082068"
      ],
      "name": [
        "Hack24 2018"
      ],
      "start": [
        "2018-03-10T09:30:00Z"
      ],
      "end": [
        "2018-03-11T17:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Old Market Square"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 2DT"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
