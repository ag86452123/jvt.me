{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2018-09-17T11:16:07+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Tech-Nottingham/events/254754472/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Tech-Nottingham/events/254754472/"
    ],
    "published": [
      "2018-09-17T11:16:07+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Tech-Nottingham/events/254754472/"
      ],
      "name": [
        "Tech Nottingham October 2018: APIness & Accessibility"
      ],
      "start": [
        "2018-10-08T18:30:00+01:00"
      ],
      "end": [
        "2018-10-08T21:00:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, Beck Street"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2018/09/dy966",
  "aliases": [
    "/mf2/72655817-d792-4d08-8054-b8ad28cbffd0/",
    "/mf2/2018/09/dy966"
  ]
}
