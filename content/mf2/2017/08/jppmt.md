{
  "kind": "rsvps",
  "slug": "2017/08/jppmt",
  "date": "2017-08-22T17:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/monzo-meetup-nottingham-edition-tickets-36742464701"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/monzo-meetup-nottingham-edition-tickets-36742464701"
    ],
    "published": [
      "2017-08-22T17:30:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/monzo-meetup-nottingham-edition-tickets-36742464701"
      ],
      "name": [
        "Monzo Meetup: Nottingham Edition"
      ],
      "start": [
        "2017-08-22T17:30:00Z"
      ],
      "end": [
        "2017-08-22T19:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "9A Beck St"
          ],
          "locality": [
            "Nottingham"
          ],
          "postal-code": [
            "NG1 1EQ"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
