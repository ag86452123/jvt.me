{
  "kind": "rsvps",
  "slug": "2014/11/4rdzu",
  "date": "2014-11-15T09:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.eventbrite.co.uk/e/women-in-tech-tickets-13115080559"
    ],
    "in-reply-to": [
      "https://www.eventbrite.co.uk/e/women-in-tech-tickets-13115080559"
    ],
    "published": [
      "2014-11-15T09:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.eventbrite.co.uk/e/women-in-tech-tickets-13115080559"
      ],
      "name": [
        "Women in Tech"
      ],
      "start": [
        "2014-11-15T09:00:00Z"
      ],
      "end": [
        "2014-11-15T17:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "The University of Nottingham - Jubilee Campus, Wollaton Road"
          ],
          "locality": [
            null
          ],
          "postal-code": [
            "NG8 1BB"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
