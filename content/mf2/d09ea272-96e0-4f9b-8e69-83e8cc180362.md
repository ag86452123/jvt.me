{
  "kind": "rsvps",
  "client_id": "https://www.jvt.me/",
  "date": "2017-03-29T18:30:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "DevOps Nottingham"
    ],
    "in-reply-to": [
      "https://www.meetup.com/DevOps-Nottingham/events/237366441/"
    ],
    "published": [
      "2017-03-29T18:30:00+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/DevOps-Nottingham/events/237366441/"
      ],
      "name": [
        "DevOps Nottingham"
      ],
      "start": [
        "2017-03-29T18:30:00+01:00"
      ],
      "end": [
        "2017-03-29T20:30:00+01:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Accelerate Places, The Poynt, 45 Wollaton Street, Nottingham, NG1 5FW"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  },
  "slug": "2017/03/9dqjl",
  "aliases": [
    "/mf2/d09ea272-96e0-4f9b-8e69-83e8cc180362/",
    "/mf2/2017/03/9dqjl",
    "/mf2/2017/03/9dqJl"
  ]
}
