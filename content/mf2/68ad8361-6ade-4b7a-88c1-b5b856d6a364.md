{
  "properties": {
    "name": [
      "How we respond to incidents"
    ],
    "bookmark-of": [
      "https://monzo.com/blog/2019/07/08/how-we-respond-to-incidents"
    ],
    "content": [
      {
        "value": "As I've said before, I'm a big fan of how Monzo handles their production incidents because it's quite polished and transparent",
        "html": ""
      }
    ],
    "published": [
      "2019-07-18T18:20:56+0200"
    ],
    "category": [
      "incident-management"
    ]
  },
  "aliases": [
    "/bookmarks/68ad8361-6ade-4b7a-88c1-b5b856d6a364/",
    "/mf2/68ad8361-6ade-4b7a-88c1-b5b856d6a364/",
    "/mf2/2019/07/ps41m",
    "/mf2/2019/07/PS41M"
  ],
  "h": "h-entry",
  "date": "2019-07-18T18:20:56+0200",
  "tags": [
    "incident-management"
  ],
  "kind": "bookmarks",
  "slug": "2019/07/ps41m"
}
