{
  "date" : "2020-05-21T20:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/alicegoldfuss/status/1263537556924198912" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1263550675365044226" ],
    "name" : [ "Reply to https://twitter.com/alicegoldfuss/status/1263537556924198912" ],
    "published" : [ "2020-05-21T20:18:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I had \"a chat\" with a Google recruiter who then started asking a lot of questions I wasn't quite prepared for - but only once 🤷🏽‍♂️"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/fhr0h",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263537556924198912" ],
      "url" : [ "https://twitter.com/alicegoldfuss/status/1263537556924198912" ],
      "published" : [ "2020-05-21T18:30:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:alicegoldfuss" ],
          "numeric-id" : [ "163154809" ],
          "name" : [ "bletchley punk is a fullmetal engineer" ],
          "nickname" : [ "alicegoldfuss" ],
          "url" : [ "https://twitter.com/alicegoldfuss", "http://blog.alicegoldfuss.com/", "http://twitch.tv/bletchleypunk" ],
          "published" : [ "2010-07-05T17:51:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "/usr/local/sin" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1149452202827759616/1blf87Kn.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Dear white cis men of tech: have you ever had a nontechnical interview turn into a surprise technical one? And then your lack of prep used against you?\n\nThis has happened to several of my friends (including me) and it sure seems like there's a theme.",
        "html" : "<div style=\"white-space: pre\">Dear white cis men of tech: have you ever had a nontechnical interview turn into a surprise technical one? And then your lack of prep used against you?\n\nThis has happened to several of my friends (including me) and it sure seems like there's a theme.</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
