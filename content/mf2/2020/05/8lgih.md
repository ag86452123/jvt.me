{
  "date" : "2020-05-09T11:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/BernieSanders/status/1258841099407372289" ],
    "name" : [ "Like of @BernieSanders's tweet" ],
    "published" : [ "2020-05-09T11:06:00+01:00" ],
    "like-of" : [ "https://twitter.com/BernieSanders/status/1258841099407372289" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/8lgih",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258841099407372289" ],
      "url" : [ "https://twitter.com/BernieSanders/status/1258841099407372289" ],
      "published" : [ "2020-05-08T19:28:00+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:BernieSanders" ],
          "numeric-id" : [ "216776631" ],
          "name" : [ "Bernie Sanders" ],
          "nickname" : [ "BernieSanders" ],
          "url" : [ "https://twitter.com/BernieSanders", "https://BernieSanders.com" ],
          "published" : [ "2010-11-17T17:53:52+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Vermont" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1097820307388334080/9ddg5F6v.png" ]
        }
      } ],
      "content" : [ "In Denmark, starting pay at McDonald’s is about $22 an hour. The workers there get six weeks of paid vacation, a year’s paid maternity leave and a pension plan. Like all Danes, they enjoy universal medical insurance and paid sick leave. This is what a civilized society is about." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
