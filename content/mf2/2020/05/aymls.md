{
  "date" : "2020-05-02T16:13:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/AlSweigart/status/1256297829544255488" ],
    "name" : [ "Like of @AlSweigart's tweet" ],
    "published" : [ "2020-05-02T16:13:00+01:00" ],
    "like-of" : [ "https://twitter.com/AlSweigart/status/1256297829544255488" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/aymls",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1256297829544255488" ],
      "url" : [ "https://twitter.com/AlSweigart/status/1256297829544255488" ],
      "published" : [ "2020-05-01T19:01:58+00:00" ],
      "in-reply-to" : [ "https://twitter.com/existentialcoms/status/1256287173579431936" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:AlSweigart" ],
          "numeric-id" : [ "14738418" ],
          "name" : [ "Al Sweigart" ],
          "nickname" : [ "AlSweigart" ],
          "url" : [ "https://twitter.com/AlSweigart", "https://inventwithpython.com" ],
          "published" : [ "2008-05-12T00:22:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1114284492703670272/XZMVbwfR.png" ]
        }
      } ],
      "content" : [ {
        "html" : "\n<a class=\"u-mention\" href=\"https://twitter.com/existentialcoms\"></a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EW9EMqnVcAA4yOU.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
