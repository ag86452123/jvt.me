{
  "date" : "2020-05-08T13:40:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/rwdrich/status/1258725773676105730" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1258740054350008320" ],
    "name" : [ "Reply to https://twitter.com/rwdrich/status/1258725773676105730" ],
    "published" : [ "2020-05-08T13:40:00+01:00" ],
    "category" : [ "morph" ],
    "content" : [ {
      "html" : "",
      "value" : "I've spent the last hour in such an awkward position because he's been sleeping on my hand 🥰"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/qs0p6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258725773676105730" ],
      "url" : [ "https://twitter.com/rwdrich/status/1258725773676105730" ],
      "published" : [ "2020-05-08T11:49:45+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1258724115009191937" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Envy! Summer days with kitty cuddles are the best",
        "html" : "Envy! Summer days with kitty cuddles are the best\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/anna_hax\"></a>"
      } ]
    }
  },
  "tags" : [ "morph" ],
  "client_id" : "https://indigenous.realize.be"
}
