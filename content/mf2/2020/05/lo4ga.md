{
  "date" : "2020-05-10T18:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/cassidoo/status/1259524868023803904" ],
    "name" : [ "Like of @cassidoo's tweet" ],
    "published" : [ "2020-05-10T18:15:00+01:00" ],
    "category" : [ "guys" ],
    "like-of" : [ "https://twitter.com/cassidoo/status/1259524868023803904" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/lo4ga",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259524868023803904" ],
      "url" : [ "https://twitter.com/cassidoo/status/1259524868023803904" ],
      "published" : [ "2020-05-10T16:45:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:cassidoo" ],
          "numeric-id" : [ "400286802" ],
          "name" : [ "Cassidy Williams" ],
          "nickname" : [ "cassidoo" ],
          "url" : [ "https://twitter.com/cassidoo", "http://cassidoo.co" ],
          "published" : [ "2011-10-28T19:54:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle, WA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/718548236580098048/OgV0pPQY.jpg" ]
        }
      } ],
      "content" : [ "Fellas often tweet me asking a question and start it with, “hey man” and I always respond to that with “hey woman” and they *always* delete their original tweet, it’s a shame" ]
    }
  },
  "tags" : [ "guys" ],
  "client_id" : "https://indigenous.realize.be"
}
