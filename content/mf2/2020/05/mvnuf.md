{
  "date" : "2020-05-19T02:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/katekaput/status/1262068343294615553" ],
    "name" : [ "Like of @katekaput's tweet" ],
    "published" : [ "2020-05-19T02:39:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/katekaput/status/1262068343294615553" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/mvnuf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262068343294615553" ],
      "url" : [ "https://twitter.com/katekaput/status/1262068343294615553" ],
      "published" : [ "2020-05-17T17:11:55+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:katekaput" ],
          "numeric-id" : [ "13593692" ],
          "name" : [ "Kate Kaput" ],
          "nickname" : [ "katekaput" ],
          "url" : [ "https://twitter.com/katekaput", "http://www.greatestescapist.com" ],
          "published" : [ "2008-02-17T19:22:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cleveland, OH" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1231794501523517441/s2XXRmcl.jpg" ]
        }
      } ],
      "content" : [ "The 👏🏻 pandemic 👏🏻 isn’t 👏🏻 over 👏🏻 just 👏🏻 because 👏🏻 you’re 👏🏻 bored 👏🏻" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
