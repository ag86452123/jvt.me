{
  "date" : "2020-05-28T17:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/fredericmarx/status/1265923456035688448" ],
    "name" : [ "Like of @fredericmarx's tweet" ],
    "published" : [ "2020-05-28T17:51:00+01:00" ],
    "like-of" : [ "https://twitter.com/fredericmarx/status/1265923456035688448" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/1hbin",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265923456035688448" ],
      "url" : [ "https://twitter.com/fredericmarx/status/1265923456035688448" ],
      "published" : [ "2020-05-28T08:30:46+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:fredericmarx" ],
          "numeric-id" : [ "484596867" ],
          "name" : [ "Frederic Marx" ],
          "nickname" : [ "fredericmarx" ],
          "url" : [ "https://twitter.com/fredericmarx", "http://fmarx.com/", "http://pronoun.is/he" ],
          "published" : [ "2012-02-06T08:53:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Berlin, Germany" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1183007930871963648/H1gURgf0.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "“Nooo you can’t just use semantic markup for visual layout, that’s what CSS is for.”\n\n“Haha HTML go <br><br><br><br>”",
        "html" : "<div style=\"white-space: pre\">“Nooo you can’t just use semantic markup for visual layout, that’s what CSS is for.”\n\n“Haha HTML go &lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;”</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
