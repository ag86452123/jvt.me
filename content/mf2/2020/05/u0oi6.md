{
  "date" : "2020-05-24T18:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/GuileneMarco/status/1264225653362171905" ],
    "name" : [ "Like of @GuileneMarco's tweet" ],
    "published" : [ "2020-05-24T18:44:00+01:00" ],
    "category" : [ "coronavirus", "politics" ],
    "like-of" : [ "https://twitter.com/GuileneMarco/status/1264225653362171905" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/u0oi6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264225653362171905" ],
      "url" : [ "https://twitter.com/GuileneMarco/status/1264225653362171905" ],
      "published" : [ "2020-05-23T16:04:18+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:GuileneMarco" ],
          "numeric-id" : [ "1044979817777704960" ],
          "name" : [ "Guilene Marco" ],
          "nickname" : [ "GuileneMarco" ],
          "url" : [ "https://twitter.com/GuileneMarco", "https://www.womensequality.org.uk/islington" ],
          "published" : [ "2018-09-26T15:59:38+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1044979899172376576/XVnJTxTH.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Hey Dominic Cummings, you’re an Islington resident. Did you know, we have a wonderful network of Mutual Aid groups supporting residents self-isolating with C-19? We can do shopping, pick-up prescription, even walk your dog. \nWe support everyone no matter who they are.",
        "html" : "<div style=\"white-space: pre\">Hey Dominic Cummings, you’re an Islington resident. Did you know, we have a wonderful network of Mutual Aid groups supporting residents self-isolating with C-19? We can do shopping, pick-up prescription, even walk your dog. \nWe support everyone no matter who they are.</div>"
      } ]
    }
  },
  "tags" : [ "coronavirus", "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
