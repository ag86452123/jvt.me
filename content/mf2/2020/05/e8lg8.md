{
  "date" : "2020-05-22T23:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kefimochi/status/1263636178026196994" ],
    "name" : [ "Like of @kefimochi's tweet" ],
    "published" : [ "2020-05-22T23:15:00+01:00" ],
    "category" : [ "battlestations" ],
    "like-of" : [ "https://twitter.com/kefimochi/status/1263636178026196994" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/e8lg8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263636178026196994" ],
      "url" : [ "https://twitter.com/kefimochi/status/1263636178026196994" ],
      "published" : [ "2020-05-22T01:01:56+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kefimochi" ],
          "numeric-id" : [ "1009594769817796608" ],
          "name" : [ "Kate Efimova 💫" ],
          "nickname" : [ "kefimochi" ],
          "url" : [ "https://twitter.com/kefimochi", "http://github.com/kefimochi", "http://etsy.com/shop/KefiStore", "http://instagram.com/kefimochi" ],
          "published" : [ "2018-06-21T00:32:05+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Bay Area, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1261484538959507456/DBQ4L5sO.jpg" ]
        }
      } ],
      "content" : [ "Does anyone have inspirational dream setup pictures similar to this? 👀✨" ],
      "photo" : [ "https://pbs.twimg.com/media/EYlWTrFUcAAxuTN.jpg" ]
    }
  },
  "tags" : [ "battlestations" ],
  "client_id" : "https://indigenous.realize.be"
}
