{
  "date" : "2020-05-10T11:57:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-05-10T11:57:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1259183960892018688" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/lg6uk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259183960892018688" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1259183960892018688" ],
      "published" : [ "2020-05-09T18:10:25+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1257444638941683719/mhnu1CKS.jpg" ]
        }
      } ],
      "content" : [ "I nipped inside to get a drink and have come back out to this 😐🤷🏻‍♀️" ],
      "photo" : [ "https://pbs.twimg.com/media/EXmFHzaWsAI08HL.jpg" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
