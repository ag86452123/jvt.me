{
  "date" : "2020-05-05T17:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @wakomeup's tweet" ],
    "published" : [ "2020-05-05T17:46:00+01:00" ],
    "like-of" : [ "https://twitter.com/wakomeup/status/1257299480807956481" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/6apdw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257299480807956481" ],
      "url" : [ "https://twitter.com/wakomeup/status/1257299480807956481" ],
      "published" : [ "2020-05-04T13:22:10+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:wakomeup" ],
          "numeric-id" : [ "4627179101" ],
          "name" : [ "Wako 👁️" ],
          "nickname" : [ "wakomeup" ],
          "url" : [ "https://twitter.com/wakomeup" ],
          "published" : [ "2015-12-28T01:15:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "The capital of suburbia" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/681630006339043328/k1zTwXRN.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "C++ is an interpreted language.\n\nEvery compiler interprets the standard differently.",
        "html" : "<div style=\"white-space: pre\">C++ is an interpreted language.\n\nEvery compiler interprets the standard differently.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
