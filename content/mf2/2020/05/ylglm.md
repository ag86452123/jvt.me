{
  "date" : "2020-05-20T22:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Twitter/status/1263203262968344576" ],
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Reply to https://twitter.com/Twitter/status/1263203262968344576" ],
    "published" : [ "2020-05-20T22:11:00+01:00" ],
    "category" : [ "indieweb" ],
    "content" : [ {
      "html" : "",
      "value" : "I can reply on my own <a href=\"/tags/indieweb/\">#IndieWeb</a> website so 🤷🏽‍♂️"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/ylglm",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263203262968344576" ],
      "url" : [ "https://twitter.com/Twitter/status/1263203262968344576" ],
      "published" : [ "2020-05-20T20:21:41+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Twitter" ],
          "numeric-id" : [ "783214" ],
          "name" : [ "Twitter" ],
          "nickname" : [ "Twitter" ],
          "url" : [ "https://twitter.com/Twitter", "https://about.twitter.com/" ],
          "published" : [ "2007-02-20T14:35:54+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Everywhere" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1111729635610382336/_65QFl7B.png" ]
        }
      } ],
      "content" : [ "Reply if there’s a better app" ]
    }
  },
  "tags" : [ "indieweb" ],
  "client_id" : "https://indigenous.realize.be"
}
