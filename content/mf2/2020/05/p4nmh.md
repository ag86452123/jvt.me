{
  "date" : "2020-05-10T23:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1259605060952211457" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-05-10T23:08:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1259605060952211457" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/p4nmh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259605060952211457" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1259605060952211457" ],
      "published" : [ "2020-05-10T22:03:43+00:00" ],
      "in-reply-to" : [ "https://twitter.com/MrAndrew/status/1259600770477035528" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1257444638941683719/mhnu1CKS.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Is it because earlier when I was making dinner, I suddenly stopped and pointed a knife at you and said \"you know something I really like? Sharpening knives.\"  ❓",
        "html" : "Is it because earlier when I was making dinner, I suddenly stopped and pointed a knife at you and said \"you know something I really like? Sharpening knives.\"  ❓\n<a class=\"u-mention\" href=\"https://twitter.com/MrAndrew\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
