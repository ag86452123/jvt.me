{
  "date" : "2020-05-30T21:37:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kieranmch/status/1266821538495463424" ],
    "name" : [ "Like of @kieranmch's tweet" ],
    "published" : [ "2020-05-30T21:37:00+01:00" ],
    "category" : [ "spacex" ],
    "like-of" : [ "https://twitter.com/kieranmch/status/1266821538495463424" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/xezy1",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1266821538495463424" ],
      "url" : [ "https://twitter.com/kieranmch/status/1266821538495463424" ],
      "published" : [ "2020-05-30T19:59:25+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kieranmch" ],
          "numeric-id" : [ "50420707" ],
          "name" : [ "Kieran McHugh" ],
          "nickname" : [ "kieranmch" ],
          "url" : [ "https://twitter.com/kieranmch", "https://kieran.engineer" ],
          "published" : [ "2009-06-24T20:13:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1170973779675275264/cCZMxVEg.jpg" ]
        }
      } ],
      "content" : [ "Yes computers are annoying, break all the time, and are full of bugs - but they can also land a pencil vertically on a moving postage stamp after said pencil has been thrown off the Burj Khalifa." ]
    }
  },
  "tags" : [ "spacex" ],
  "client_id" : "https://indigenous.realize.be"
}
