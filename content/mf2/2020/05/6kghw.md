{
  "date" : "2020-05-06T08:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/rawkode/status/1257809080103636994" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1257940047518740480" ],
    "name" : [ "Reply to https://twitter.com/rawkode/status/1257809080103636994" ],
    "published" : [ "2020-05-06T08:18:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Remember the `=ref` otherwise its not as safe as you think https://www.jvt.me/posts/2018/09/18/safely-force-git-push/"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/6kghw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257809080103636994" ],
      "url" : [ "https://twitter.com/rawkode/status/1257809080103636994" ],
      "published" : [ "2020-05-05T23:07:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rawkode" ],
          "numeric-id" : [ "22761152" ],
          "name" : [ "David McKay" ],
          "nickname" : [ "rawkode" ],
          "url" : [ "https://twitter.com/rawkode", "https://rawkode.com", "http://calendly.com/rawkode" ],
          "published" : [ "2009-03-04T12:06:23+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Scotland, Europe" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1242222745938800643/JAu0QRcc.jpg" ]
        }
      } ],
      "content" : [ "--force-with-lease" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
