{
  "date" : "2020-05-23T11:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TatianaTMac/status/1264078383681376256" ],
    "name" : [ "Like of @TatianaTMac's tweet" ],
    "published" : [ "2020-05-23T11:23:00+01:00" ],
    "like-of" : [ "https://twitter.com/TatianaTMac/status/1264078383681376256" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/xvees",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264078383681376256" ],
      "url" : [ "https://twitter.com/TatianaTMac/status/1264078383681376256" ],
      "published" : [ "2020-05-23T06:19:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TatianaTMac" ],
          "numeric-id" : [ "1529722572" ],
          "name" : [ "Tatiana Mac" ],
          "nickname" : [ "TatianaTMac" ],
          "url" : [ "https://twitter.com/TatianaTMac", "https://www.tatianamac.com" ],
          "published" : [ "2013-06-19T04:59:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Insatiably curious" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1252086543302275072/AJ5Bzs20.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "In a year my photo reel will have no evidence of who or where I was right now—its emptiness a snapshot of how I feel◾️\n\n#RhymesForTryingTimes",
        "html" : "<div style=\"white-space: pre\">In a year my photo reel will have no evidence of who or where I was right now—its emptiness a snapshot of how I feel◾️\n\n<a href=\"https://twitter.com/search?q=%23RhymesForTryingTimes\">#RhymesForTryingTimes</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EYrolnOVAAAnqb5.jpg", "https://pbs.twimg.com/media/EYrolnNVAAIAxmn.jpg", "https://pbs.twimg.com/media/EYroloqVcAA-vPZ.jpg", "https://pbs.twimg.com/media/EYrolqyUMAcwE6u.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
