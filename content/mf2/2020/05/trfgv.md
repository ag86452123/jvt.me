{
  "date" : "2020-05-24T14:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1264425483044884487" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-05-24T14:06:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/holly/status/1264425483044884487" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/trfgv",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264425483044884487" ],
      "url" : [ "https://twitter.com/holly/status/1264425483044884487" ],
      "published" : [ "2020-05-24T05:18:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ "Every tile infects one other tile" ],
      "photo" : [ "https://pbs.twimg.com/media/EYwkRR2XkAAt5YG.jpg" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
