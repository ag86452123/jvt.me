{
  "date" : "2020-05-05T20:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @StephenAtHome's tweet" ],
    "published" : [ "2020-05-05T20:01:00+01:00" ],
    "like-of" : [ "https://twitter.com/StephenAtHome/status/1257469406860501001" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/zabx7",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257469406860501001" ],
      "url" : [ "https://twitter.com/StephenAtHome/status/1257469406860501001" ],
      "published" : [ "2020-05-05T00:37:23+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:StephenAtHome" ],
          "numeric-id" : [ "16303106" ],
          "name" : [ "Stephen Colbert" ],
          "nickname" : [ "StephenAtHome" ],
          "url" : [ "https://twitter.com/StephenAtHome" ],
          "published" : [ "2008-09-15T22:31:44+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/627669832549441536/hv1AMpO0.jpg" ]
        }
      } ],
      "content" : [ "\"May the Fourth\" is really exciting this year because it's the first time I've known what day it is in the last two months." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
