{
  "date" : "2020-05-25T17:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/SaraSoueidan/status/1264945217318330369" ],
    "name" : [ "Like of @SaraSoueidan's tweet" ],
    "published" : [ "2020-05-25T17:51:00+01:00" ],
    "category" : [ "blogging" ],
    "like-of" : [ "https://twitter.com/SaraSoueidan/status/1264945217318330369" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/8fq7k",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264945217318330369" ],
      "url" : [ "https://twitter.com/SaraSoueidan/status/1264945217318330369" ],
      "published" : [ "2020-05-25T15:43:36+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:SaraSoueidan" ],
          "numeric-id" : [ "717654410" ],
          "name" : [ "Sara Soueidan" ],
          "nickname" : [ "SaraSoueidan" ],
          "url" : [ "https://twitter.com/SaraSoueidan", "https://sarasoueidan.com" ],
          "published" : [ "2012-07-26T07:57:43+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Lebanon ✈️🌎" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1010126354078208001/MpkO7-qK.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "If you write technical articles, please add a publish date to the article. Too many articles contain outdated information & I see people being misinformed because of that. \n\nIf you write technicla articles, PLEASE INCLUDE A PUBLISH DATE.\n\nThank you!",
        "html" : "<div style=\"white-space: pre\">If you write technical articles, please add a publish date to the article. Too many articles contain outdated information &amp; I see people being misinformed because of that. \n\nIf you write technicla articles, PLEASE INCLUDE A PUBLISH DATE.\n\nThank you!</div>"
      } ]
    }
  },
  "tags" : [ "blogging" ],
  "client_id" : "https://indigenous.realize.be"
}
