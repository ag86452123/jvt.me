{
  "date" : "2020-05-24T22:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @efinlay24's tweet" ],
    "published" : [ "2020-05-24T22:17:00+01:00" ],
    "like-of" : [ "https://twitter.com/efinlay24/status/1264651353340743687" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/i1o0t",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264651353340743687" ],
      "url" : [ "https://twitter.com/efinlay24/status/1264651353340743687" ],
      "published" : [ "2020-05-24T20:15:53+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:efinlay24" ],
          "numeric-id" : [ "921314477907304448" ],
          "name" : [ "Euan Finlay" ],
          "nickname" : [ "efinlay24" ],
          "url" : [ "https://twitter.com/efinlay24" ],
          "published" : [ "2017-10-20T09:57:43+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1195902695959339014/YWhBj5RQ.jpg" ]
        }
      } ],
      "content" : [ "personally if I was going to go out in a blaze of glory, I'd reset 2FA, change the password, switch the recovery email, then go to town" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
