{
  "date" : "2020-05-07T23:40:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CapitalOneUK/status/1258383990815367170" ],
    "name" : [ "Like of @CapitalOneUK's tweet" ],
    "published" : [ "2020-05-07T23:40:00+01:00" ],
    "category" : [ "capital-one" ],
    "like-of" : [ "https://twitter.com/CapitalOneUK/status/1258383990815367170" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/wnjvt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258383990815367170" ],
      "url" : [ "https://twitter.com/CapitalOneUK/status/1258383990815367170" ],
      "published" : [ "2020-05-07T13:11:37+00:00" ],
      "in-reply-to" : [ "https://twitter.com/CapitalOneUK/status/1258383028134477824" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CapitalOneUK" ],
          "numeric-id" : [ "102626728" ],
          "name" : [ "Capital One UK" ],
          "nickname" : [ "CapitalOneUK" ],
          "url" : [ "https://twitter.com/CapitalOneUK", "http://capitalone.co.uk" ],
          "published" : [ "2010-01-07T09:19:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1164481057669996549/QroAIhwM.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "A huge thank you to every key worker as well. \n\nWe're clapping for you too!  👏 👏 👏 #ClapForKeyWorkers",
        "html" : "<div style=\"white-space: pre\">A huge thank you to every key worker as well. \n\nWe're clapping for you too!  👏 👏 👏 <a href=\"https://twitter.com/search?q=%23ClapForKeyWorkers\">#ClapForKeyWorkers</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EXaswv_XsAIWIJX.jpg" ]
    }
  },
  "tags" : [ "capital-one" ],
  "client_id" : "https://indigenous.realize.be"
}
