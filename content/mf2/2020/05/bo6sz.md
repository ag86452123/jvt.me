{
  "date" : "2020-05-11T18:09:18.75Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/rwdrich/status/1259903165350191106" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1259910126053531659" ],
    "name" : [ "Reply to https://twitter.com/rwdrich/status/1259903165350191106" ],
    "published" : [ "2020-05-11T18:09:18.75Z" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I want to set up a website for him similar to https://indiewebcat.com/ but deciding on whose domain he sits on is a contentious point with @annadodson.co.uk 🙊 https://www.jvt.me/tags/morph/ is available in the meantime!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/bo6sz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259903165350191106" ],
      "url" : [ "https://twitter.com/rwdrich/status/1259903165350191106" ],
      "published" : [ "2020-05-11T17:48:17+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1259878976232751120" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "How long until Morph has his own twitter account - I need notifications for pictures of him!!",
        "html" : "How long until Morph has his own twitter account - I need notifications for pictures of him!!\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://micropublish.net"
}
