{
  "date" : "2020-05-21T00:10:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @aimeegamble's tweet" ],
    "published" : [ "2020-05-21T00:10:00+01:00" ],
    "category" : [ "indieweb", "microformats" ],
    "like-of" : [ "https://twitter.com/aimeegamble/status/1262424032097513475" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/vnpok",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262424032097513475" ],
      "url" : [ "https://twitter.com/aimeegamble/status/1262424032097513475" ],
      "published" : [ "2020-05-18T16:45:18+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:aimeegamble" ],
          "numeric-id" : [ "2241475709" ],
          "name" : [ "Aimee Gamble-Milner" ],
          "nickname" : [ "aimeegamble" ],
          "url" : [ "https://twitter.com/aimeegamble", "http://www.aimes.me.uk" ],
          "published" : [ "2013-12-24T11:33:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1224109975729065984/wPvt-5Zj.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:695b360b8171c433" ],
          "name" : [ "Stratford, London" ]
        }
      } ],
      "content" : [ {
        "value" : "The past couple of weeks I’ve been working on a side project that actually made it to being finished and released!  🙌 A JavaScript #microformats parser aimes.me.uk/2020/05/18/wri…",
        "html" : "The past couple of weeks I’ve been working on a side project that actually made it to being finished and released!  🙌 A JavaScript <a href=\"https://twitter.com/search?q=%23microformats\">#microformats</a> parser <a href=\"https://www.aimes.me.uk/2020/05/18/writing-microformats-parser/\">aimes.me.uk/2020/05/18/wri…</a>"
      } ]
    }
  },
  "tags" : [ "indieweb", "microformats" ],
  "client_id" : "https://indigenous.realize.be"
}
