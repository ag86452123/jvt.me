{
  "date" : "2020-05-25T14:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Women-In-Tech-Nottingham/events/270793819/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/270793819/" ],
    "published" : [ "2020-05-25T14:19:00+01:00" ],
    "event" : {
      "start" : [ "2020-08-06T18:30:00+01:00" ],
      "name" : [ "Women in Tech 6th August - How knitting and computing can be the same thing" ],
      "end" : [ "2020-08-06T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Women-In-Tech-Nottingham/events/270793819/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/jntii",
  "client_id" : "https://indigenous.realize.be"
}
