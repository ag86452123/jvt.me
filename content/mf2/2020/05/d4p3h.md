{
  "date" : "2020-05-28T09:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/sdh100Shaun/status/1265911279962730496" ],
    "name" : [ "Like of @sdh100Shaun's tweet" ],
    "published" : [ "2020-05-28T09:58:00+01:00" ],
    "like-of" : [ "https://twitter.com/sdh100Shaun/status/1265911279962730496" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/d4p3h",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265911279962730496" ],
      "url" : [ "https://twitter.com/sdh100Shaun/status/1265911279962730496" ],
      "published" : [ "2020-05-28T07:42:23+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:sdh100Shaun" ],
          "numeric-id" : [ "257573" ],
          "name" : [ "Shaun" ],
          "nickname" : [ "sdh100Shaun" ],
          "url" : [ "https://twitter.com/sdh100Shaun", "http://shaunhare.co.uk" ],
          "published" : [ "2006-12-26T09:09:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "257.34,-1.47" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/908055077520379905/mAVLMD60.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "The talk this morning by @R_van_Tol talking about blurring boundaries , actually like that I can blur some boundaries #lockdown  🤷‍♂️.  @NottsTechfast",
        "html" : "The talk this morning by <a href=\"https://twitter.com/R_van_Tol\">@R_van_Tol</a> talking about blurring boundaries , actually like that I can blur some boundaries <a href=\"https://twitter.com/search?q=%23lockdown\">#lockdown</a>  🤷‍♂️.  <a href=\"https://twitter.com/NottsTechfast\">@NottsTechfast</a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
