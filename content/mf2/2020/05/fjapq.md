{
  "date" : "2020-05-15T13:40:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rwdrich/status/1261242024973553666" ],
    "name" : [ "Like of @rwdrich's tweet" ],
    "published" : [ "2020-05-15T13:40:00+01:00" ],
    "like-of" : [ "https://twitter.com/rwdrich/status/1261242024973553666" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/fjapq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261242024973553666" ],
      "url" : [ "https://twitter.com/rwdrich/status/1261242024973553666" ],
      "published" : [ "2020-05-15T10:28:26+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Don't assign a reviewer to the pull request, someone will pick it up and review it. \n\nPR slowly rots away....",
        "html" : "<div style=\"white-space: pre\">Don't assign a reviewer to the pull request, someone will pick it up and review it. \n\nPR slowly rots away....</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
