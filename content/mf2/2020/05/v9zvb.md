{
  "date" : "2020-05-14T20:29:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/KatCodes/status/1261014724390641664" ],
    "name" : [ "Like of @KatCodes's tweet" ],
    "published" : [ "2020-05-14T20:29:00+01:00" ],
    "like-of" : [ "https://twitter.com/KatCodes/status/1261014724390641664" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/v9zvb",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261014724390641664" ],
      "url" : [ "https://twitter.com/KatCodes/status/1261014724390641664" ],
      "published" : [ "2020-05-14T19:25:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:KatCodes" ],
          "numeric-id" : [ "342868129" ],
          "name" : [ "katie walker" ],
          "nickname" : [ "KatCodes" ],
          "url" : [ "https://twitter.com/KatCodes", "http://kat.codes" ],
          "published" : [ "2011-07-26T18:14:50+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1234597964351442947/j3ioCAzu.jpg" ]
        }
      } ],
      "content" : [ "I bought my partner a vintage drinks cabinet online for his birthday and it is WAY bigger than it looked in the photos. No way is it going to fit in my one bed flat 🤦‍♀️" ],
      "photo" : [ "https://pbs.twimg.com/media/EYAGNFgXgAQxRkD.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
