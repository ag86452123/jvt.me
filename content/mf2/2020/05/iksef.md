{
  "date" : "2020-05-23T22:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1264308649910042627" ],
    "published" : [ "2020-05-23T22:30:00+01:00" ],
    "repost-of" : [ "https://twitter.com/jrawson/status/1264121909345767425" ],
    "category" : [ "politics" ]
  },
  "kind" : "reposts",
  "slug" : "2020/05/iksef",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264121909345767425" ],
      "url" : [ "https://twitter.com/jrawson/status/1264121909345767425" ],
      "published" : [ "2020-05-23T09:12:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jrawson" ],
          "numeric-id" : [ "134459894" ],
          "name" : [ "James" ],
          "nickname" : [ "jrawson" ],
          "url" : [ "https://twitter.com/jrawson" ],
          "published" : [ "2010-04-18T13:43:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1011649673684307968/RtVOHFvb.jpg" ]
        }
      } ],
      "content" : [ "Hearing rumours that Dominic Cummings only travelled to see family in Durham because he'd been rejected by his family in Oxford and Cambridge" ]
    }
  },
  "tags" : [ "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
