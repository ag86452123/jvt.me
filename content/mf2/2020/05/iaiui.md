{
  "date" : "2020-05-08T22:02:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/technottingham/status/1258828785824206853" ],
    "name" : [ "Like of @technottingham's tweet" ],
    "published" : [ "2020-05-08T22:02:00+01:00" ],
    "like-of" : [ "https://twitter.com/technottingham/status/1258828785824206853" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/iaiui",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258828785824206853" ],
      "url" : [ "https://twitter.com/technottingham/status/1258828785824206853" ],
      "video" : [ "https://video.twimg.com/tweet_video/EXhCGseWoAAe7ZL.mp4" ],
      "published" : [ "2020-05-08T18:39:05+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Happy bank holiday, friends ☀️\n\nHope you're all enjoying the sunshine, and hope to see you all on Monday at #TechNott 🍉\n\nWe'll have an awesome talk by @annashipman, and some good old community fun. See you there! 👋",
        "html" : "<div style=\"white-space: pre\">Happy bank holiday, friends ☀️\n\nHope you're all enjoying the sunshine, and hope to see you all on Monday at <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> 🍉\n\nWe'll have an awesome talk by <a href=\"https://twitter.com/annashipman\">@annashipman</a>, and some good old community fun. See you there! 👋</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
