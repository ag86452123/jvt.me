{
  "date" : "2020-05-10T23:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrAndrew/status/1259605453333553152" ],
    "name" : [ "Like of @MrAndrew's tweet" ],
    "published" : [ "2020-05-10T23:08:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrAndrew/status/1259605453333553152" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/hqwfs",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259605453333553152" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1259605453333553152" ],
      "published" : [ "2020-05-10T22:05:17+00:00" ],
      "in-reply-to" : [ "https://twitter.com/MrsEmma/status/1259605060952211457" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1258160750641606657/CR5g9eOt.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:50f2d0272381533f" ],
          "name" : [ "East Midlands, England" ]
        }
      } ],
      "content" : [ {
        "value" : "This is part of why I maintain 2 metres distance",
        "html" : "This is part of why I maintain 2 metres distance\n<a class=\"u-mention\" href=\"https://twitter.com/MrsEmma\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
