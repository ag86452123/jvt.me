{
  "date" : "2020-05-15T20:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Iconawrites/status/1260924988393226241" ],
    "name" : [ "Like of @Iconawrites's tweet" ],
    "published" : [ "2020-05-15T20:56:00+01:00" ],
    "like-of" : [ "https://twitter.com/Iconawrites/status/1260924988393226241" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/shyyt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1260924988393226241" ],
      "url" : [ "https://twitter.com/Iconawrites/status/1260924988393226241" ],
      "published" : [ "2020-05-14T13:28:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Iconawrites" ],
          "numeric-id" : [ "931812084236550144" ],
          "name" : [ "Icona📚" ],
          "nickname" : [ "Iconawrites" ],
          "url" : [ "https://twitter.com/Iconawrites" ],
          "published" : [ "2017-11-18T09:11:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "England, United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1156022409717112833/AWvMTOn6.jpg" ]
        }
      } ],
      "content" : [ "Don’t subtweet your bad boyfriend. Be like Mary Shelley and write a whole sci-fi novel in which he’s a mad scientist who builds a monster, destroys his family through his arrogance and hubris, and dies alone and disgraced and freezing in the Arctic Ocean." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
