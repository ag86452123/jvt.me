{
  "date" : "2020-05-24T10:36:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TheKateMulgrew/status/1264303776179027969" ],
    "name" : [ "Like of @TheKateMulgrew's tweet" ],
    "published" : [ "2020-05-24T10:36:00+01:00" ],
    "category" : [ "star-trek" ],
    "like-of" : [ "https://twitter.com/TheKateMulgrew/status/1264303776179027969" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/vnrap",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264303776179027969" ],
      "url" : [ "https://twitter.com/TheKateMulgrew/status/1264303776179027969" ],
      "published" : [ "2020-05-23T21:14:44+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TheKateMulgrew" ],
          "numeric-id" : [ "2693837167" ],
          "name" : [ "Kate Mulgrew" ],
          "nickname" : [ "TheKateMulgrew" ],
          "url" : [ "https://twitter.com/TheKateMulgrew", "http://www.katemulgrew.com/" ],
          "published" : [ "2014-07-30T20:04:31+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1075164422581178370/xRwT-xem.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "On this day in 2001, the Voyager finale “Endgame” aired. On Tuesday, the cast will reunite for a virtual panel in these unprecedented times. \n\nVoyagers: assemble. \n\n#StarTrek #Voyager #Endgame",
        "html" : "<div style=\"white-space: pre\">On this day in 2001, the Voyager finale “Endgame” aired. On Tuesday, the cast will reunite for a virtual panel in these unprecedented times. \n\nVoyagers: assemble. \n\n<a href=\"https://twitter.com/search?q=%23StarTrek\">#StarTrek</a> <a href=\"https://twitter.com/search?q=%23Voyager\">#Voyager</a> <a href=\"https://twitter.com/search?q=%23Endgame\">#Endgame</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EYu1lDvX0AM2QC2.jpg" ]
    }
  },
  "tags" : [ "star-trek" ],
  "client_id" : "https://indigenous.realize.be"
}
