{
  "date" : "2020-05-28T23:02:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.jvt.me/events/personal-events/2020-05-xxfcr/" ],
    "name" : [ "RSVP yes to https://www.jvt.me/events/personal-events/2020-05-xxfcr/" ],
    "published" : [ "2020-05-28T23:02:00+01:00" ],
    "event" : {
      "start" : [ "2020-06-16T18:00:00+0100" ],
      "name" : [ "Escape Room" ],
      "end" : [ "2020-06-16T20:30:00+0100" ],
      "url" : [ "https://www.jvt.me/events/personal-events/2020-05-xxfcr/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/ecphp",
  "client_id" : "https://indigenous.realize.be"
}
