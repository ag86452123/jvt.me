{
  "date" : "2020-05-25T15:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/eclairpupperdog/status/1264547438884458501" ],
    "name" : [ "Like of @eclairpupperdog's tweet" ],
    "published" : [ "2020-05-25T15:39:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/eclairpupperdog/status/1264547438884458501" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/jvafd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264547438884458501" ],
      "url" : [ "https://twitter.com/eclairpupperdog/status/1264547438884458501" ],
      "published" : [ "2020-05-24T13:22:58+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:eclairpupperdog" ],
          "numeric-id" : [ "993673705535533057" ],
          "name" : [ "Eclair Pupperdog ✨" ],
          "nickname" : [ "eclairpupperdog" ],
          "url" : [ "https://twitter.com/eclairpupperdog", "https://linktr.ee/eclairpupperdog" ],
          "published" : [ "2018-05-08T02:07:27+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Ohio" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1253747432061571079/zrepQzkY.jpg" ]
        }
      } ],
      "content" : [ "tonight.. we dine like kings" ],
      "photo" : [ "https://pbs.twimg.com/media/EYyTMR0WsAAGGf-.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
