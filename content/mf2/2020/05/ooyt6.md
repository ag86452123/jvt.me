{
  "date" : "2020-05-18T22:10:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TartanLlama/status/1262441440942215170" ],
    "name" : [ "Like of @TartanLlama's tweet" ],
    "published" : [ "2020-05-18T22:10:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/TartanLlama/status/1262441440942215170" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/ooyt6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262441440942215170" ],
      "url" : [ "https://twitter.com/TartanLlama/status/1262441440942215170" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1262441266140393474/pu/vid/720x1280/APyb_uTbeKKUdERz.mp4?tag=10" ],
      "published" : [ "2020-05-18T17:54:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TartanLlama" ],
          "numeric-id" : [ "36158849" ],
          "name" : [ "Sy Brand" ],
          "nickname" : [ "TartanLlama" ],
          "url" : [ "https://twitter.com/TartanLlama" ],
          "published" : [ "2009-04-28T19:38:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Edinburgh, Scotland" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1215574109477883905/QCts-vX0.jpg" ]
        }
      } ],
      "content" : [ "Have you ever wanted to build and run your CMake project on Linux from Visual Studio? Watch me explain it to my cat in 45 seconds." ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
