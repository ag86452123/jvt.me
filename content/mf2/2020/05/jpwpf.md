{
  "date" : "2020-05-06T09:46:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/seaotta/status/1257771480164655105" ],
    "name" : [ "Like of @seaotta's tweet" ],
    "published" : [ "2020-05-06T09:46:00+01:00" ],
    "like-of" : [ "https://twitter.com/seaotta/status/1257771480164655105" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/jpwpf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257771480164655105" ],
      "url" : [ "https://twitter.com/seaotta/status/1257771480164655105" ],
      "published" : [ "2020-05-05T20:37:43+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:seaotta" ],
          "numeric-id" : [ "107281171" ],
          "name" : [ "Stephanie Stimac 🔮 Casting Spells" ],
          "nickname" : [ "seaotta" ],
          "url" : [ "https://twitter.com/seaotta", "https://stephaniestimac.com" ],
          "published" : [ "2010-01-22T02:20:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle but prefer Scotland" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1229643051099774976/q7xJbvhv.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I keep coming across podcasts and articles where people mention my blog post I wrote last year on performance and continue to be absolutely humbled by it. \n\nTo think I almost didn't post that because of imposter syndrome.",
        "html" : "<div style=\"white-space: pre\">I keep coming across podcasts and articles where people mention my blog post I wrote last year on performance and continue to be absolutely humbled by it. \n\nTo think I almost didn't post that because of imposter syndrome.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
