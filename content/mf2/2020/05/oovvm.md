{
  "date" : "2020-05-01T08:28:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/erikaheidi/status/1255797767257063429" ],
    "name" : [ "Like of @erikaheidi's tweet" ],
    "published" : [ "2020-05-01T08:28:00+01:00" ],
    "like-of" : [ "https://twitter.com/erikaheidi/status/1255797767257063429" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/oovvm",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1255797767257063429" ],
      "url" : [ "https://twitter.com/erikaheidi/status/1255797767257063429" ],
      "published" : [ "2020-04-30T09:54:53+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:erikaheidi" ],
          "numeric-id" : [ "19625601" ],
          "name" : [ "Erika Heidi" ],
          "nickname" : [ "erikaheidi" ],
          "url" : [ "https://twitter.com/erikaheidi", "https://eheidi.dev" ],
          "published" : [ "2009-01-27T23:55:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "The Hague, The Netherlands" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228345079473020928/izVGqsEi.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "This one never gets old ♥️👩‍💻 🖤\n\n/cc @djangogirls",
        "html" : "<div style=\"white-space: pre\">This one never gets old ♥️👩‍💻 🖤\n\n/cc <a href=\"https://twitter.com/djangogirls\">@djangogirls</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EW19aDgWoAArRl8.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
