{
  "date" : "2020-05-05T22:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @PeevedFerret's tweet" ],
    "published" : [ "2020-05-05T22:14:00+01:00" ],
    "like-of" : [ "https://twitter.com/PeevedFerret/status/1257721044347076609" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/dpv0z",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257721044347076609" ],
      "url" : [ "https://twitter.com/PeevedFerret/status/1257721044347076609" ],
      "published" : [ "2020-05-05T17:17:18+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:PeevedFerret" ],
          "numeric-id" : [ "932958809038966784" ],
          "name" : [ "Disgruntled Ferret" ],
          "nickname" : [ "PeevedFerret" ],
          "url" : [ "https://twitter.com/PeevedFerret" ],
          "published" : [ "2017-11-21T13:08:08+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Up North" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1210304294001483776/7igAbQcA.jpg" ]
        }
      } ],
      "content" : [ "Katsu Dinosaurs." ],
      "photo" : [ "https://pbs.twimg.com/media/EXRSnEYXkAY-ags.jpg", "https://pbs.twimg.com/media/EXRSnrUX0AITKTI.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
