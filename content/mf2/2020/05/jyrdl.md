{
  "date" : "2020-05-30T16:43:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mentnelson/status/1266574695111016450" ],
    "name" : [ "Like of @mentnelson's tweet" ],
    "published" : [ "2020-05-30T16:43:00+01:00" ],
    "like-of" : [ "https://twitter.com/mentnelson/status/1266574695111016450" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/jyrdl",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1266574695111016450" ],
      "url" : [ "https://twitter.com/mentnelson/status/1266574695111016450" ],
      "published" : [ "2020-05-30T03:38:33+00:00" ],
      "in-reply-to" : [ "https://twitter.com/mentnelson/status/1266569363588616192" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mentnelson" ],
          "numeric-id" : [ "1607408977" ],
          "name" : [ "ment" ],
          "nickname" : [ "mentnelson" ],
          "url" : [ "https://twitter.com/mentnelson", "http://instagram.com/mentnelson", "http://mentnelson.com" ],
          "published" : [ "2013-07-20T04:06:22+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "South Carolina, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1242179329913688064/mXwb1tva.jpg" ]
        }
      } ],
      "content" : [ "I also love the beautiful symbolism of “Killer” Mike being the voice of reason at a time like this" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
