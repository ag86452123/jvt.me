{
  "date" : "2020-05-10T15:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rwdrich/status/1259422686301782017" ],
    "name" : [ "Like of @rwdrich's tweet" ],
    "published" : [ "2020-05-10T15:58:00+01:00" ],
    "like-of" : [ "https://twitter.com/rwdrich/status/1259422686301782017" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/luyz9",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259422686301782017" ],
      "url" : [ "https://twitter.com/rwdrich/status/1259422686301782017" ],
      "published" : [ "2020-05-10T09:59:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "WHY DOES EVERYONE ELSE'S ISLAND LOOK SO GOOD, AND MINE LOOKS LIKE I'M ABOUT TO FIND WILSON ANY MINUTE NOW OMG" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
