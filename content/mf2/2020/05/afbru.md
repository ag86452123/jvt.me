{
  "date" : "2020-05-19T18:37:01.149Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/05/online-homebrew-website-club-nottingham-Rqs1Cfcxii4D" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/05/online-homebrew-website-club-nottingham-Rqs1Cfcxii4D" ],
    "published" : [ "2020-05-19T18:37:01.149Z" ],
    "event" : {
      "start" : [ "2020-05-27T17:30:00+01:00" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-05-27T19:30:00+01:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ],
          "latitude" : [ "52.9509448" ],
          "longitude" : [ "-1.1522525" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/05/online-homebrew-website-club-nottingham-Rqs1Cfcxii4D" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/afbru",
  "client_id" : "https://micropublish.net"
}
