{
  "date" : "2020-05-07T14:48:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/QuinnyPig/status/1258194264841785345" ],
    "name" : [ "Like of @QuinnyPig's tweet" ],
    "published" : [ "2020-05-07T14:48:00+01:00" ],
    "like-of" : [ "https://twitter.com/QuinnyPig/status/1258194264841785345" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/djeoj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258194264841785345" ],
      "url" : [ "https://twitter.com/QuinnyPig/status/1258194264841785345" ],
      "published" : [ "2020-05-07T00:37:43+00:00" ],
      "in-reply-to" : [ "https://twitter.com/halvarflake/status/1258161778770542594" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:QuinnyPig" ],
          "numeric-id" : [ "97114171" ],
          "name" : [ "Corey Quinn" ],
          "nickname" : [ "QuinnyPig" ],
          "url" : [ "https://twitter.com/QuinnyPig", "http://www.duckbillgroup.com", "http://lastweekinaws.com", "http://screaminginthecloud.com" ],
          "published" : [ "2009-12-16T02:19:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1247254348498743300/vTTJS5wW.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Because I'm the world's greatest Cloud Economist, I'd like to point out that changing the .json to .csv at the end of that link shaves 300MB off of the file size. \n\nMY WORK IS DONE",
        "html" : "<div style=\"white-space: pre\">Because I'm the world's greatest Cloud Economist, I'd like to point out that changing the .json to .csv at the end of that link shaves 300MB off of the file size. \n\nMY WORK IS DONE</div>\n<a class=\"u-mention\" href=\"https://twitter.com/halvarflake\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
