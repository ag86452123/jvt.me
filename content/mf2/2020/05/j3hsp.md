{
  "date" : "2020-05-27T23:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Brunty/status/1265598523220819974" ],
    "name" : [ "Like of @Brunty's tweet" ],
    "published" : [ "2020-05-27T23:25:00+01:00" ],
    "category" : [ "medium" ],
    "like-of" : [ "https://twitter.com/Brunty/status/1265598523220819974" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/j3hsp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265598523220819974" ],
      "url" : [ "https://twitter.com/Brunty/status/1265598523220819974" ],
      "published" : [ "2020-05-27T10:59:36+00:00" ],
      "in-reply-to" : [ "https://twitter.com/alexellisuk/status/1265595759719862272" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt 🇪🇺" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://brunty.me/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "it's not clear from the screenshot posted above, it might be missing some context but having to know there's a partner program behind the scenes isn't something most people will do.\n\nThe UI & messaging on that page looks like it needs addressing if that's the case.",
        "html" : "<div style=\"white-space: pre\">it's not clear from the screenshot posted above, it might be missing some context but having to know there's a partner program behind the scenes isn't something most people will do.\n\nThe UI &amp; messaging on that page looks like it needs addressing if that's the case.</div>\n<a class=\"u-mention\" href=\"https://twitter.com/alexellisuk\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/mattiasgeniar\"></a>"
      } ]
    }
  },
  "tags" : [ "medium" ],
  "client_id" : "https://indigenous.realize.be"
}
