{
  "date" : "2020-05-02T15:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1256580972520321024" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-05-02T15:16:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1256580972520321024" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/lrfbe",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1256580972520321024" ],
      "url" : [ "https://twitter.com/edent/status/1256580972520321024" ],
      "published" : [ "2020-05-02T13:47:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ "Apparently, I can't set my password to \"Mr. Owl Ate My Metal Worm\"" ],
      "photo" : [ "https://pbs.twimg.com/media/EXBFqCUWoAAUSfy.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
