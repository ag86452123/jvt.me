{
  "date" : "2020-05-30T10:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/SoVeryBritish/status/1266461706840137728" ],
    "name" : [ "Like of @SoVeryBritish's tweet" ],
    "published" : [ "2020-05-30T10:39:00+01:00" ],
    "like-of" : [ "https://twitter.com/SoVeryBritish/status/1266461706840137728" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/uwdsy",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1266461706840137728" ],
      "url" : [ "https://twitter.com/SoVeryBritish/status/1266461706840137728" ],
      "published" : [ "2020-05-29T20:09:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:SoVeryBritish" ],
          "numeric-id" : [ "1023072078" ],
          "name" : [ "VeryBritishProblems" ],
          "nickname" : [ "SoVeryBritish" ],
          "url" : [ "https://twitter.com/SoVeryBritish", "http://www.verybritishproblems.com", "http://amzn.to/2QJS9Yt", "http://verybritishproblemstshirts.com", "http://bit.ly/3drkGMk" ],
          "published" : [ "2012-12-19T23:12:45+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/2996456104/b707959f192bba5c31c07058f91a183b.png" ]
        }
      } ],
      "content" : [ "Not going out is much more fun when it’s possible to go out" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
