{
  "date" : "2020-05-01T13:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/anna_hax/status/1256193292234100739" ],
    "name" : [ "Like of @anna_hax's tweet" ],
    "published" : [ "2020-05-01T13:42:00+01:00" ],
    "like-of" : [ "https://twitter.com/anna_hax/status/1256193292234100739" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/ewove",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1256193292234100739" ],
      "url" : [ "https://twitter.com/anna_hax/status/1256193292234100739" ],
      "published" : [ "2020-05-01T12:06:34+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:anna_hax" ],
          "numeric-id" : [ "394235377" ],
          "name" : [ "Anna 🏠" ],
          "nickname" : [ "anna_hax" ],
          "url" : [ "https://twitter.com/anna_hax", "https://annadodson.co.uk" ],
          "published" : [ "2011-10-19T19:37:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Cute #homebrewWebsiteClub t-shirt on #AnimalCrossing just for @JamieTanna #indieWeb",
        "html" : "Cute <a href=\"https://twitter.com/search?q=%23homebrewWebsiteClub\">#homebrewWebsiteClub</a> t-shirt on <a href=\"https://twitter.com/search?q=%23AnimalCrossing\">#AnimalCrossing</a> just for <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a> <a href=\"https://twitter.com/search?q=%23indieWeb\">#indieWeb</a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EW7lJDEXYAAiXE-.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
