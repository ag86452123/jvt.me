{
  "date" : "2020-05-24T21:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/peterwalker99/status/1264597243035877377" ],
    "name" : [ "Like of @peterwalker99's tweet" ],
    "published" : [ "2020-05-24T21:18:00+01:00" ],
    "category" : [ "coronavirus", "politics" ],
    "like-of" : [ "https://twitter.com/peterwalker99/status/1264597243035877377" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/ddfo0",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264597243035877377" ],
      "url" : [ "https://twitter.com/peterwalker99/status/1264597243035877377" ],
      "published" : [ "2020-05-24T16:40:52+00:00" ],
      "in-reply-to" : [ "https://twitter.com/peterwalker99/status/1264595398821400577" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:peterwalker99" ],
          "numeric-id" : [ "15206103" ],
          "name" : [ "Peter Walker" ],
          "nickname" : [ "peterwalker99" ],
          "url" : [ "https://twitter.com/peterwalker99", "http://www.theguardian.com/profile/peterwalker", "http://amzn.to/2k8Vuxs", "http://bit.ly/29xs4sa" ],
          "published" : [ "2008-06-23T11:46:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/822132273562714112/dasTMEWB.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Yesterday I spoke to someone who encountered Cummings a lot in the education world. They predicted Cummings would stay: \"The man's a sociopath. If he's sacked he won't stay quiet, and he knows where too many bodies are buried.\"\n\nIt's as convincing an explanation as anything.",
        "html" : "<div style=\"white-space: pre\">Yesterday I spoke to someone who encountered Cummings a lot in the education world. They predicted Cummings would stay: \"The man's a sociopath. If he's sacked he won't stay quiet, and he knows where too many bodies are buried.\"\n\nIt's as convincing an explanation as anything.</div>"
      } ]
    }
  },
  "tags" : [ "coronavirus", "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
