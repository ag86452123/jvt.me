{
  "date" : "2020-05-24T14:05:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/TatianaTMac/status/1264277943825694721" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1264543904503914502" ],
    "name" : [ "Reply to https://twitter.com/TatianaTMac/status/1264277943825694721" ],
    "published" : [ "2020-05-24T14:05:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "+1 on AGPL. I've seen a lot of corps have policies strongly against even looking at the README let along integrating the project. And folks who do use it make it better for everyone as contributions are shared 👍🏽"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/ns4n8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264277943825694721" ],
      "url" : [ "https://twitter.com/TatianaTMac/status/1264277943825694721" ],
      "published" : [ "2020-05-23T19:32:05+00:00" ],
      "in-reply-to" : [ "https://twitter.com/ManishEarth/status/1264277251941490688" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TatianaTMac" ],
          "numeric-id" : [ "1529722572" ],
          "name" : [ "Tatiana Mac" ],
          "nickname" : [ "TatianaTMac" ],
          "url" : [ "https://twitter.com/TatianaTMac", "https://www.tatianamac.com" ],
          "published" : [ "2013-06-19T04:59:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Insatiably curious" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1252086543302275072/AJ5Bzs20.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "You're THE BEST!!! I was going to default to MIT at first then I thought, that's probably a bad idea.",
        "html" : "You're THE BEST!!! I was going to default to MIT at first then I thought, that's probably a bad idea.\n<a class=\"u-mention\" href=\"https://twitter.com/ManishEarth\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
