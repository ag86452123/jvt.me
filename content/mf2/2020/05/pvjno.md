{
  "date" : "2020-05-22T10:35:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Notts-Techfast/events/270722934/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Notts-Techfast/events/270722934/" ],
    "published" : [ "2020-05-22T10:35:00+01:00" ],
    "event" : {
      "start" : [ "2020-05-28T08:00:00+01:00" ],
      "name" : [ "Home Working: A Psychological Perspective 🧠" ],
      "end" : [ "2020-05-28T09:00:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Notts-Techfast/events/270722934/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/pvjno",
  "client_id" : "https://indigenous.realize.be"
}
