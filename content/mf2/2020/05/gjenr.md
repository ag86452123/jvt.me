{
  "date" : "2020-05-15T16:05:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1261273429845643264" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-05-15T16:05:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1261273429845643264" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/gjenr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261273429845643264" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1261273429845643264" ],
      "published" : [ "2020-05-15T12:33:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1257444638941683719/mhnu1CKS.jpg" ]
        }
      } ],
      "content" : [ "I'm an auntie again, this time to this cute-as-a-button pup! And I can't actually cope with how adorable he is 😭😍 He is getting ALL the cuddles when lockdown ends 💛" ],
      "photo" : [ "https://pbs.twimg.com/media/EYDxfxsXYAIa8DR.jpg" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
