{
  "date" : "2020-05-16T21:38:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/MrsEmma/status/1261746025598984193" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1261759338454147078" ],
    "name" : [ "Reply to https://twitter.com/MrsEmma/status/1261746025598984193" ],
    "published" : [ "2020-05-16T21:38:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "I'm enjoying the fact that I can enjoy watching it through @annadodson.co.uk's eyes so I don't actually need to play it 😅"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/g7jdj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261746025598984193" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1261746025598984193" ],
      "video" : [ "https://video.twimg.com/tweet_video/EYKfUU4X0AEwZ7D.mp4" ],
      "published" : [ "2020-05-16T19:51:09+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1261662129452650497/o55TWMlp.jpg" ]
        }
      } ],
      "content" : [ "Does everyone I know have a switch and animal crossing right now? I'm feeling like the last person on earth who has yet to succumb to its seemingly inevitable charms..." ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
