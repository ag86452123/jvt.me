{
  "date" : "2020-05-28T07:35:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/konstructivizm/status/1265722885626363908" ],
    "name" : [ "Like of @konstructivizm's tweet" ],
    "published" : [ "2020-05-28T07:35:00+01:00" ],
    "category" : [ "nature" ],
    "like-of" : [ "https://twitter.com/konstructivizm/status/1265722885626363908" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/yvkwy",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265722885626363908" ],
      "url" : [ "https://twitter.com/konstructivizm/status/1265722885626363908" ],
      "published" : [ "2020-05-27T19:13:46+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:konstructivizm" ],
          "numeric-id" : [ "138882249" ],
          "name" : [ "Black Hole" ],
          "nickname" : [ "konstructivizm" ],
          "url" : [ "https://twitter.com/konstructivizm" ],
          "published" : [ "2010-04-30T22:23:32+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1229760579650363392/LSX8S-Dq.jpg" ]
        }
      } ],
      "content" : [ "This absurd Icelandic landscape" ],
      "photo" : [ "https://pbs.twimg.com/media/EZDAQC_X0AEFNEr.jpg" ]
    }
  },
  "tags" : [ "nature" ],
  "client_id" : "https://indigenous.realize.be"
}
