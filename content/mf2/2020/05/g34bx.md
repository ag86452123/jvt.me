{
  "date" : "2020-05-05T18:27:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @django's tweet" ],
    "published" : [ "2020-05-05T18:27:00+01:00" ],
    "like-of" : [ "https://twitter.com/django/status/1257488051548413959" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/g34bx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257488051548413959" ],
      "url" : [ "https://twitter.com/django/status/1257488051548413959" ],
      "published" : [ "2020-05-05T01:51:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:django" ],
          "numeric-id" : [ "331141273" ],
          "name" : [ "Django Gold" ],
          "nickname" : [ "django" ],
          "url" : [ "https://twitter.com/django", "http://djangogold.com" ],
          "published" : [ "2011-07-07T18:18:43+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New York, NY" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1054781683009011717/cWCAWWpP.jpg" ]
        }
      } ],
      "content" : [ "looking back on it, it's even funnier how those celebrities decided it was time to sing us that \"Imagine\" montage after being stuck inside for like 36 hours" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
