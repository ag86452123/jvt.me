{
  "date" : "2020-05-17T15:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ErinInTheMorn/status/1261759360851722240" ],
    "name" : [ "Like of @ErinInTheMorn's tweet" ],
    "published" : [ "2020-05-17T15:08:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/ErinInTheMorn/status/1261759360851722240" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/vcw1h",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261759360851722240" ],
      "url" : [ "https://twitter.com/ErinInTheMorn/status/1261759360851722240" ],
      "published" : [ "2020-05-16T20:44:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ErinInTheMorn" ],
          "numeric-id" : [ "1115826452715511809" ],
          "name" : [ "Erin, at a safe social distance" ],
          "nickname" : [ "ErinInTheMorn" ],
          "url" : [ "https://twitter.com/ErinInTheMorn", "http://bit.ly/2IANvbk" ],
          "published" : [ "2019-04-10T03:58:52+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Washington, DC" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1257489100397035520/fO2C9gL-.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I keep finding out people aren’t aware.. The Matrix is a trans allegory written by two closeted trans women. \n\n“You’ve felt it your entire life, that there’s something wrong with the world. You don’t know what it is, but it’s there, like a splinter in your mind, driving you mad.”",
        "html" : "<div style=\"white-space: pre\">I keep finding out people aren’t aware.. The Matrix is a trans allegory written by two closeted trans women. \n\n“You’ve felt it your entire life, that there’s something wrong with the world. You don’t know what it is, but it’s there, like a splinter in your mind, driving you mad.”</div>"
      } ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
