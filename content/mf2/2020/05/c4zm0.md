{
  "date" : "2020-05-08T18:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Brunty/status/1258786519596314624" ],
    "name" : [ "Like of @Brunty's tweet" ],
    "published" : [ "2020-05-08T18:15:00+01:00" ],
    "like-of" : [ "https://twitter.com/Brunty/status/1258786519596314624" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/c4zm0",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258786519596314624" ],
      "url" : [ "https://twitter.com/Brunty/status/1258786519596314624" ],
      "published" : [ "2020-05-08T15:51:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Brunty" ],
          "numeric-id" : [ "1099517012" ],
          "name" : [ "Matt Brunt 🇪🇺" ],
          "nickname" : [ "Brunty" ],
          "url" : [ "https://twitter.com/Brunty", "https://mfyu.co.uk/now" ],
          "published" : [ "2013-01-17T23:19:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leicester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1151043501288824832/TjB9WT83.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Working on a new site design, making a theme in @GoHugoIO and porting across my old site content",
        "html" : "Working on a new site design, making a theme in <a href=\"https://twitter.com/GoHugoIO\">@GoHugoIO</a> and porting across my old site content"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EXgbkKfWoAA7LOH.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
