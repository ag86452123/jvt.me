{
  "date" : "2020-05-16T20:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kristinlow/status/1261259846470361088" ],
    "name" : [ "Like of @kristinlow's tweet" ],
    "published" : [ "2020-05-16T20:16:00+01:00" ],
    "like-of" : [ "https://twitter.com/kristinlow/status/1261259846470361088" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/54ghr",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1261259846470361088" ],
      "url" : [ "https://twitter.com/kristinlow/status/1261259846470361088" ],
      "published" : [ "2020-05-15T11:39:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kristinlow" ],
          "numeric-id" : [ "16167886" ],
          "name" : [ "Kristin Low" ],
          "nickname" : [ "kristinlow" ],
          "url" : [ "https://twitter.com/kristinlow", "http://www.kristinlow.com" ],
          "published" : [ "2008-09-07T09:48:22+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Hong Kong" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1260892811500511233/sRqgn4pf.jpg" ]
        }
      } ],
      "content" : [ "What do you think this means?" ],
      "photo" : [ "https://pbs.twimg.com/media/EYDlI1eUcAAWA6G.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
