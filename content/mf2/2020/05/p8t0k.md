{
  "date" : "2020-05-04T10:45:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1257243702256447489" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-05-04T10:45:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1257243702256447489" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/p8t0k",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1257243702256447489" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1257243702256447489" ],
      "published" : [ "2020-05-04T09:40:31+00:00" ],
      "in-reply-to" : [ "https://twitter.com/rizbizkits/status/1257241087342792706" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Yuppp, I’ve been following @JamieTanna’s public love affair with Morph very closely 💛",
        "html" : "Yuppp, I’ve been following <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a>’s public love affair with Morph very closely 💛\n<a class=\"u-mention\" href=\"https://twitter.com/rizbizkits\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
