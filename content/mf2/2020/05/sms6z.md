{
  "date" : "2020-05-25T15:36:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @moderndadpages's tweet" ],
    "published" : [ "2020-05-25T15:36:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/moderndadpages/status/1264922444134924293" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/sms6z",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264922444134924293" ],
      "url" : [ "https://twitter.com/moderndadpages/status/1264922444134924293" ],
      "published" : [ "2020-05-25T14:13:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:moderndadpages" ],
          "numeric-id" : [ "2794319396" ],
          "name" : [ "🇨🇦 Modern Dad Pages 🇨🇦" ],
          "nickname" : [ "moderndadpages" ],
          "url" : [ "https://twitter.com/moderndadpages", "http://www.moderndadpages.com" ],
          "published" : [ "2014-09-06T16:47:34+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Surrey BC Canada" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1224321657801564162/5BRyrX7o.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:09efd2065c8ce28e" ],
          "name" : [ "Surrey, British Columbia" ]
        }
      } ],
      "content" : [ {
        "value" : "Two very different breakfast plates \n\nWhich one do you choose? \n\nLeft or right?",
        "html" : "<div style=\"white-space: pre\">Two very different breakfast plates \n\nWhich one do you choose? \n\nLeft or right?</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EY3oQXtVAAA5Tjm.jpg", "https://pbs.twimg.com/media/EY3oQXtU4AA822z.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
