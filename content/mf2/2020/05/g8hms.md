{
  "date" : "2020-05-25T20:57:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/TashJNorris/status/1264986740659494918" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1265010694409379840" ],
    "name" : [ "Reply to https://twitter.com/TashJNorris/status/1264986740659494918" ],
    "published" : [ "2020-05-25T20:57:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "🤗"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/05/g8hms",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264986740659494918" ],
      "url" : [ "https://twitter.com/TashJNorris/status/1264986740659494918" ],
      "published" : [ "2020-05-25T18:28:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TashJNorris" ],
          "numeric-id" : [ "19965271" ],
          "name" : [ "Tash Norris" ],
          "nickname" : [ "TashJNorris" ],
          "url" : [ "https://twitter.com/TashJNorris", "https://medium.com/@tashjnorris" ],
          "published" : [ "2009-02-03T11:25:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1084014099510558720/_2lwD_ZS.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Me to my hubby: *Some raw honesty about how I feel right now* \nHubby: Less depressy more repressy",
        "html" : "<div style=\"white-space: pre\">Me to my hubby: *Some raw honesty about how I feel right now* \nHubby: Less depressy more repressy</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
