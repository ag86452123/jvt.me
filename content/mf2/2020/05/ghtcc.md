{
  "date" : "2020-05-31T10:38:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kelseyhightower/status/1266952531793416192" ],
    "name" : [ "Like of @kelseyhightower's tweet" ],
    "published" : [ "2020-05-31T10:38:00+01:00" ],
    "like-of" : [ "https://twitter.com/kelseyhightower/status/1266952531793416192" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/ghtcc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1266952531793416192" ],
      "url" : [ "https://twitter.com/kelseyhightower/status/1266952531793416192" ],
      "published" : [ "2020-05-31T04:39:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kelseyhightower" ],
          "numeric-id" : [ "159822053" ],
          "name" : [ "Kelsey Hightower" ],
          "nickname" : [ "kelseyhightower" ],
          "url" : [ "https://twitter.com/kelseyhightower" ],
          "published" : [ "2010-06-26T12:24:45+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Portland, OR" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1204077305271705606/j5XjhPAt.jpg" ]
        }
      } ],
      "content" : [ "To my fellow technologist hoping to change the world, you must care for the people who live in it; and those who had that right taken away." ],
      "photo" : [ "https://pbs.twimg.com/media/EZUb74oUEAAnhgp.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
