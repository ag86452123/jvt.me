{
  "date" : "2020-05-28T08:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1265910275917656066" ],
    "published" : [ "2020-05-28T08:31:00+01:00" ],
    "repost-of" : [ "https://twitter.com/anna_hax/status/1265908233769754624" ],
    "category" : [ "tech-fast", "morph" ],
    "content" : [ {
      "html" : "",
      "value" : "Really interesting talk at <a href=\"/tags/tech-fast/\">#TechFast</a>, and <a href=\"/tags/morph/\">#Morph</a> is enjoying having breakfast with us ☺"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2020/05/3ko1r",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265908233769754624" ],
      "url" : [ "https://twitter.com/anna_hax/status/1265908233769754624" ],
      "published" : [ "2020-05-28T07:30:17+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:anna_hax" ],
          "numeric-id" : [ "394235377" ],
          "name" : [ "Anna 🏠" ],
          "nickname" : [ "anna_hax" ],
          "url" : [ "https://twitter.com/anna_hax", "https://annadodson.co.uk" ],
          "published" : [ "2011-10-19T19:37:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I'm enjoying listening to #TechFast this morning with @R_van_Tol talking about how we're all coping with the current situation whilst getting ready for the day 💻💕🐈 #Morph",
        "html" : "I'm enjoying listening to <a href=\"https://twitter.com/search?q=%23TechFast\">#TechFast</a> this morning with <a href=\"https://twitter.com/R_van_Tol\">@R_van_Tol</a> talking about how we're all coping with the current situation whilst getting ready for the day 💻💕🐈 <a href=\"https://twitter.com/search?q=%23Morph\">#Morph</a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EZFo0tHXQAEMUs-.jpg" ]
    }
  },
  "tags" : [ "tech-fast", "morph" ],
  "client_id" : "https://indigenous.realize.be"
}
