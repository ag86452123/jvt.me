{
  "date" : "2020-05-24T15:04:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/frontstuff_io/status/1264189583220244480" ],
    "name" : [ "Like of @frontstuff_io's tweet" ],
    "published" : [ "2020-05-24T15:04:00+01:00" ],
    "like-of" : [ "https://twitter.com/frontstuff_io/status/1264189583220244480" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/z0suw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1264189583220244480" ],
      "url" : [ "https://twitter.com/frontstuff_io/status/1264189583220244480" ],
      "published" : [ "2020-05-23T13:40:58+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:frontstuff_io" ],
          "numeric-id" : [ "937000244650192897" ],
          "name" : [ "Sarah Dayan" ],
          "nickname" : [ "frontstuff_io" ],
          "url" : [ "https://twitter.com/frontstuff_io", "https://sarahdayan.dev/" ],
          "published" : [ "2017-12-02T16:47:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Paris, France" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/977873484759158784/mOItIR7M.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "One of my favorite software development practices is to wrap dependencies into custom abstractions.\n\nI hate it when a third-party leaks all over my code and a refactor takes hours (if not days) because of it.",
        "html" : "<div style=\"white-space: pre\">One of my favorite software development practices is to wrap dependencies into custom abstractions.\n\nI hate it when a third-party leaks all over my code and a refactor takes hours (if not days) because of it.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
