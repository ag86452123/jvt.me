{
  "date" : "2020-05-27T16:37:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/edent/status/1265628903059722246" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-05-27T16:37:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1265628903059722246" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/wejcy",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265628903059722246" ],
      "url" : [ "https://twitter.com/edent/status/1265628903059722246" ],
      "published" : [ "2020-05-27T13:00:19+00:00" ],
      "in-reply-to" : [ "https://twitter.com/edent/status/1262717587122794498" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "😂 I was expecting one or two old stickers in order to decorate my new laptop. Now have enough to decorate my house!\n\nAnyone want to do some swapsies?",
        "html" : "<div style=\"white-space: pre\">😂 I was expecting one or two old stickers in order to decorate my new laptop. Now have enough to decorate my house!\n\nAnyone want to do some swapsies?</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EZBmxXOXQAAH_7Q.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
