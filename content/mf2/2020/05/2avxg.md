{
  "date" : "2020-05-01T07:10:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ElleArmageddon/status/1255870742727585792" ],
    "name" : [ "Like of @ElleArmageddon's tweet" ],
    "published" : [ "2020-05-01T07:10:00+01:00" ],
    "like-of" : [ "https://twitter.com/ElleArmageddon/status/1255870742727585792" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/2avxg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1255870742727585792" ],
      "url" : [ "https://twitter.com/ElleArmageddon/status/1255870742727585792" ],
      "published" : [ "2020-04-30T14:44:52+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ElleArmageddon" ],
          "numeric-id" : [ "385847731" ],
          "name" : [ "the apocalypse, but fashion 🦝" ],
          "nickname" : [ "ElleArmageddon" ],
          "url" : [ "https://twitter.com/ElleArmageddon", "http://blog.totallynotmalware.net" ],
          "published" : [ "2011-10-06T07:23:59+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Oakland, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1182485650894835713/knpWHlV3.jpg" ]
        }
      } ],
      "content" : [ "This is your unscheduled reminder that telling early-in-career engineers stories of times you messed something up real bad is a good way to help them combat their own impostor syndrome." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
