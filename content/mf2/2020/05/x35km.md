{
  "date" : "2020-05-20T07:35:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/jjdelc/status/1262959774364241926" ],
    "name" : [ "Like of @jjdelc's tweet" ],
    "published" : [ "2020-05-20T07:35:00+01:00" ],
    "category" : [ "personal-website" ],
    "like-of" : [ "https://twitter.com/jjdelc/status/1262959774364241926" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/x35km",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262959774364241926" ],
      "url" : [ "https://twitter.com/jjdelc/status/1262959774364241926" ],
      "published" : [ "2020-05-20T04:14:09+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jjdelc" ],
          "numeric-id" : [ "5028991" ],
          "name" : [ "Jj" ],
          "nickname" : [ "jjdelc" ],
          "url" : [ "https://twitter.com/jjdelc", "https://www.isgeek.net/", "https://jj.isgeek.net" ],
          "published" : [ "2007-04-17T18:53:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Lima, Perú" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1134723427359703040/LR5hB1n5.png" ]
        }
      } ],
      "content" : [ {
        "value" : "I'm enjoying doing infinite small fixes to my personal website. Lovely opportunity to experiment or just tune infinitely. #indieweb jj.isgeek.net/2020/05/19-075…",
        "html" : "I'm enjoying doing infinite small fixes to my personal website. Lovely opportunity to experiment or just tune infinitely. <a href=\"https://twitter.com/search?q=%23indieweb\">#indieweb</a> <a href=\"https://jj.isgeek.net/2020/05/19-075433/\">jj.isgeek.net/2020/05/19-075…</a>"
      } ]
    }
  },
  "tags" : [ "personal-website" ],
  "client_id" : "https://indigenous.realize.be"
}
