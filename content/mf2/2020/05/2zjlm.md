{
  "date" : "2020-05-31T21:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rwdrich/status/1267108423218483200" ],
    "name" : [ "Like of @rwdrich's tweet" ],
    "published" : [ "2020-05-31T21:53:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/rwdrich/status/1267108423218483200" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/2zjlm",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1267108423218483200" ],
      "url" : [ "https://twitter.com/rwdrich/status/1267108423218483200" ],
      "published" : [ "2020-05-31T14:59:24+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "British summer sun isn't the same without some scones. Now I just need to find some strawberries..." ],
      "photo" : [ "https://pbs.twimg.com/media/EZWsZCvWoAEtI8D.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
