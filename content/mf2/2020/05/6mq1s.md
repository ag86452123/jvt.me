{
  "date" : "2020-05-21T18:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/tobi/status/1263483496087064579" ],
    "name" : [ "Like of @tobi's tweet" ],
    "published" : [ "2020-05-21T18:30:00+01:00" ],
    "category" : [ "remote-work" ],
    "like-of" : [ "https://twitter.com/tobi/status/1263483496087064579" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/6mq1s",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263483496087064579" ],
      "url" : [ "https://twitter.com/tobi/status/1263483496087064579" ],
      "published" : [ "2020-05-21T14:55:14+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tobi" ],
          "numeric-id" : [ "676573" ],
          "name" : [ "Tobi Lutke 🌳🌲🛒🕹" ],
          "nickname" : [ "tobi" ],
          "url" : [ "https://twitter.com/tobi", "http://tobi.lutke.com" ],
          "published" : [ "2007-01-21T20:23:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Canada" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1245084664244699140/tk5QtiZx.png" ]
        }
      } ],
      "content" : [ "As of today, Shopify is a digital by default company. We will keep our offices closed until 2021 so that we can rework them for this new reality. And after that, most will permanently work remotely. Office centricity is over." ]
    }
  },
  "tags" : [ "remote-work" ],
  "client_id" : "https://indigenous.realize.be"
}
