{
  "date" : "2020-05-25T14:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.meetup.com/Women-In-Tech-Nottingham/events/270791448/" ],
    "syndication" : [ "https://brid.gy/publish/meetup" ],
    "name" : [ "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/270791448/" ],
    "published" : [ "2020-05-25T14:19:00+01:00" ],
    "event" : {
      "start" : [ "2020-07-02T18:30:00+01:00" ],
      "name" : [ "Women in Tech 2nd July - Finding your passion in community" ],
      "end" : [ "2020-07-02T20:30:00+01:00" ],
      "location" : [ "Online" ],
      "url" : [ "https://www.meetup.com/Women-In-Tech-Nottingham/events/270791448/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/v0vvb",
  "client_id" : "https://indigenous.realize.be"
}
