{
  "date" : "2020-05-28T22:54:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @ScottMonty's tweet" ],
    "published" : [ "2020-05-28T22:54:00+01:00" ],
    "category" : [ "politics" ],
    "like-of" : [ "https://twitter.com/ScottMonty/status/1265802070432546816" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/l6msk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1265802070432546816" ],
      "url" : [ "https://twitter.com/ScottMonty/status/1265802070432546816" ],
      "published" : [ "2020-05-28T00:28:25+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ScottMonty" ],
          "numeric-id" : [ "1035491" ],
          "name" : [ "Scott Monty" ],
          "nickname" : [ "ScottMonty" ],
          "url" : [ "https://twitter.com/ScottMonty", "https://scottmonty.com", "http://smonty.co/Timeless" ],
          "published" : [ "2007-03-12T19:32:06+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Ann Arbor, MI" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/491443752741785600/BGpBfhzd.jpeg" ]
        }
      } ],
      "content" : [ {
        "value" : "Signing an #ExecutiveOrder on social media tomorrow?\n\nI wish he responded to coronavirus as vigorously as he responds to this made-up Twitter crisis. reuters.com/article/us-twi…",
        "html" : "<div style=\"white-space: pre\">Signing an <a href=\"https://twitter.com/search?q=%23ExecutiveOrder\">#ExecutiveOrder</a> on social media tomorrow?\n\nI wish he responded to coronavirus as vigorously as he responds to this made-up Twitter crisis. <a href=\"https://www.reuters.com/article/us-twitter-trump-idUSKBN2331NK\">reuters.com/article/us-twi…</a></div>"
      } ]
    }
  },
  "tags" : [ "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
