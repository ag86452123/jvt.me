{
  "date" : "2020-05-23T11:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/sash1_k/status/1263964622119411715" ],
    "name" : [ "Like of @sash1_k's tweet" ],
    "published" : [ "2020-05-23T11:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/sash1_k/status/1263964622119411715" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/arwuy",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263964622119411715" ],
      "url" : [ "https://twitter.com/sash1_k/status/1263964622119411715" ],
      "published" : [ "2020-05-22T22:47:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:sash1_k" ],
          "numeric-id" : [ "2294846018" ],
          "name" : [ "Sashi👨‍💻" ],
          "nickname" : [ "sash1_k" ],
          "url" : [ "https://twitter.com/sash1_k" ],
          "published" : [ "2014-01-16T18:32:19+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Munich, Bavaria" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1232406181135646720/11X3cSYT.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I'm super honored and grateful to be the 𝗠𝗩𝗣 of GitLab 13.0 release! 🤩 Thanks @gitlab ❤️\n\nabout.gitlab.com/releases/2020/…",
        "html" : "<div style=\"white-space: pre\">I'm super honored and grateful to be the 𝗠𝗩𝗣 of GitLab 13.0 release! 🤩 Thanks <a href=\"https://twitter.com/gitlab\">@gitlab</a> ❤️\n\n<a href=\"https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/\">about.gitlab.com/releases/2020/…</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EYp-XqnXkAAGz6Q.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
