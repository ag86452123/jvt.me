{
  "date" : "2020-05-11T20:04:04.374Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.jvt.me/events/personal-events/2020-05-o3xsq/" ],
    "name" : [ "RSVP yes to https://www.jvt.me/events/personal-events/2020-05-o3xsq/" ],
    "published" : [ "2020-05-11T20:04:04.374Z" ],
    "event" : {
      "start" : [ "2020-05-19T17:30:00+0100" ],
      "name" : [ "Escape Room" ],
      "end" : [ "2020-05-19T19:30:00+0100" ],
      "url" : [ "https://www.jvt.me/events/personal-events/2020-05-o3xsq/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/9dwru",
  "client_id" : "https://micropublish.net"
}
