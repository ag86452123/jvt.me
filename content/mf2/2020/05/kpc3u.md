{
  "date" : "2020-05-01T12:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @ebruenig's tweet" ],
    "published" : [ "2020-05-01T12:39:00+01:00" ],
    "like-of" : [ "https://twitter.com/ebruenig/status/1256040892625977344" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/kpc3u",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1256040892625977344" ],
      "url" : [ "https://twitter.com/ebruenig/status/1256040892625977344" ],
      "published" : [ "2020-05-01T02:00:59+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ebruenig" ],
          "numeric-id" : [ "1471542956" ],
          "name" : [ "Elizabeth Bruenig" ],
          "nickname" : [ "ebruenig" ],
          "url" : [ "https://twitter.com/ebruenig" ],
          "published" : [ "2013-05-31T07:06:34+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1220780316874493959/UIVTRjTT.jpg" ]
        }
      } ],
      "content" : [ "bought a costco sheet cake bc i figured we could use some cheer around here. toddler completely freaked out, demanding to know whose birthday it was. i explained you can just buy a cake when you want if you're grown up, etc. long back and forth. eventually agreed it's my birthday" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
