{
  "date" : "2020-05-20T14:40:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/chickpee_/status/1262468430504099840" ],
    "name" : [ "Like of @chickpee_'s tweet" ],
    "published" : [ "2020-05-20T14:40:00+01:00" ],
    "like-of" : [ "https://twitter.com/chickpee_/status/1262468430504099840" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/g3jsv",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1262468430504099840" ],
      "url" : [ "https://twitter.com/chickpee_/status/1262468430504099840" ],
      "published" : [ "2020-05-18T19:41:44+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:chickpee_" ],
          "numeric-id" : [ "1640570382" ],
          "name" : [ "leah" ],
          "nickname" : [ "chickpee_" ],
          "url" : [ "https://twitter.com/chickpee_", "http://instagram.com/leahxhoward" ],
          "published" : [ "2013-08-02T14:35:52+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Ireland" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1251884981384884225/NQ8CNxsY.jpg" ]
        }
      } ],
      "content" : [ "oh to be browsing the crisp aisle in a foreign country with someone you love" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
