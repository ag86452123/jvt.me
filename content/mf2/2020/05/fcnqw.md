{
  "date" : "2020-05-29T12:15:52.324Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/06/online-homebrew-website-club-nottingham-LrUgUvSF1Z23" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/06/online-homebrew-website-club-nottingham-LrUgUvSF1Z23" ],
    "published" : [ "2020-05-29T12:15:52.324Z" ],
    "event" : {
      "start" : [ "2020-06-24T17:30:00+01:00" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-06-24T19:30:00+01:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ],
          "latitude" : [ "52.9509448" ],
          "longitude" : [ "-1.1522525" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/06/online-homebrew-website-club-nottingham-LrUgUvSF1Z23" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/fcnqw",
  "client_id" : "https://micropublish.net"
}
