{
  "date" : "2020-05-11T22:45:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/DallasMocha/status/1259507874566418438" ],
    "name" : [ "Like of @DallasMocha's tweet" ],
    "published" : [ "2020-05-11T22:45:00+01:00" ],
    "like-of" : [ "https://twitter.com/DallasMocha/status/1259507874566418438" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/i7pbe",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1259507874566418438" ],
      "url" : [ "https://twitter.com/DallasMocha/status/1259507874566418438" ],
      "published" : [ "2020-05-10T15:37:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:DallasMocha" ],
          "numeric-id" : [ "62958783" ],
          "name" : [ "Jerica" ],
          "nickname" : [ "DallasMocha" ],
          "url" : [ "https://twitter.com/DallasMocha" ],
          "published" : [ "2009-08-04T23:09:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ ". Dallas via Iowa ." ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1225278822427381761/WfTn05f_.jpg" ]
        }
      } ],
      "content" : [ "Sometimes life comes at you fast • 🤷🏻‍♀️" ],
      "photo" : [ "https://pbs.twimg.com/media/EXqrvCZWsAUz7-x.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
