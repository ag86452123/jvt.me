{
  "date" : "2020-05-22T21:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @ellegist's tweet" ],
    "published" : [ "2020-05-22T21:52:00+01:00" ],
    "like-of" : [ "https://twitter.com/ellegist/status/1263871546088710146" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/3jzno",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1263871546088710146" ],
      "url" : [ "https://twitter.com/ellegist/status/1263871546088710146" ],
      "published" : [ "2020-05-22T16:37:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ellegist" ],
          "numeric-id" : [ "106504679" ],
          "name" : [ "socielle distancing" ],
          "nickname" : [ "ellegist" ],
          "url" : [ "https://twitter.com/ellegist", "https://www.instagram.com/elle.gist/" ],
          "published" : [ "2010-01-19T19:47:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1239881233649348609/ELM5rH-E.jpg" ]
        }
      } ],
      "content" : [ "decided I might as well jump on the bandwagon! here's mine 💁‍♀️" ],
      "photo" : [ "https://pbs.twimg.com/media/EYosVfVXYAEkzFU.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
