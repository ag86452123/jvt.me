{
  "date" : "2020-05-06T17:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rwdrich/status/1258061605495099398" ],
    "name" : [ "Like of @rwdrich's tweet" ],
    "published" : [ "2020-05-06T17:44:00+01:00" ],
    "like-of" : [ "https://twitter.com/rwdrich/status/1258061605495099398" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/ircyf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1258061605495099398" ],
      "url" : [ "https://twitter.com/rwdrich/status/1258061605495099398" ],
      "published" : [ "2020-05-06T15:50:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "I think it should be mandatory for at least one pet to always be in online calls. Fed up of my calls being 100% human..." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
