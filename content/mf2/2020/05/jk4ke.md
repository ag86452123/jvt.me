{
  "date" : "2020-05-29T12:15:38.484Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/06/online-homebrew-website-club-nottingham-hSxvQepCBqQl" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/06/online-homebrew-website-club-nottingham-hSxvQepCBqQl" ],
    "published" : [ "2020-05-29T12:15:38.484Z" ],
    "event" : {
      "start" : [ "2020-06-10T17:30:00+01:00" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-06-10T19:30:00+01:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ],
          "latitude" : [ "52.9509448" ],
          "longitude" : [ "-1.1522525" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/06/online-homebrew-website-club-nottingham-hSxvQepCBqQl" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/05/jk4ke",
  "client_id" : "https://micropublish.net"
}
