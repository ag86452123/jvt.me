{
  "date" : "2020-05-02T11:10:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/andybudd/status/1256177502248189953" ],
    "name" : [ "Like of @andybudd's tweet" ],
    "published" : [ "2020-05-02T11:10:00+01:00" ],
    "category" : [ "coronavirus", "mental-health" ],
    "like-of" : [ "https://twitter.com/andybudd/status/1256177502248189953" ]
  },
  "kind" : "likes",
  "slug" : "2020/05/awiuz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1256177502248189953" ],
      "url" : [ "https://twitter.com/andybudd/status/1256177502248189953" ],
      "published" : [ "2020-05-01T11:03:49+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:andybudd" ],
          "numeric-id" : [ "60033" ],
          "name" : [ "Andy Budd" ],
          "nickname" : [ "andybudd" ],
          "url" : [ "https://twitter.com/andybudd", "http://www.andybudd.com" ],
          "published" : [ "2006-12-12T10:44:17+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Brighton, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/756889658290765824/mVz2gO19.jpg" ]
        }
      } ],
      "content" : [ "Clearleft working from home recommendations." ],
      "photo" : [ "https://pbs.twimg.com/media/EW7WvsBWsAEWEsS.jpg" ]
    }
  },
  "tags" : [ "coronavirus", "mental-health" ],
  "client_id" : "https://indigenous.realize.be"
}
