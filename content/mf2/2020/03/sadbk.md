{
  "kind": "rsvps",
  "slug": "2020/03/sadbk",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-15T23:16:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/LevelUp-Notts/events/269181977/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/LevelUp-Notts/events/269181977/"
    ],
    "published": [
      "2020-03-15T23:16:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/LevelUp-Notts/events/269181977/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Rebel Recruiters, Huntingdon Street, Nottinghamshire, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/LevelUp-Notts/events/269181977/"
      ],
      "name": [
        "\"What's life *really* like as a dev?\""
      ],
      "start": [
        "2020-03-26T18:00:00Z"
      ],
      "end": [
        "2020-03-26T20:30:00Z"
      ]
    }
  }
}
