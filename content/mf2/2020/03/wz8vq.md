{
  "kind": "likes",
  "slug": "2020/03/wz8vq",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239308785224519681"
      ],
      "url": [
        "https://twitter.com/alicegoldfuss/status/1239308785224519681"
      ],
      "published": [
        "2020-03-15T21:53:34+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:alicegoldfuss"
            ],
            "numeric-id": [
              "163154809"
            ],
            "name": [
              "bletchley punk is keeping her distance"
            ],
            "nickname": [
              "alicegoldfuss"
            ],
            "url": [
              "https://twitter.com/alicegoldfuss",
              "http://blog.alicegoldfuss.com/",
              "http://twitch.tv/bletchleypunk"
            ],
            "published": [
              "2010-07-05T17:51:34+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "/usr/local/sin"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1149452202827759616/1blf87Kn.jpg"
            ]
          }
        }
      ],
      "content": [
        "*whispers* you know this is going to take longer than 2 weeks, right?"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-15T23:19:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @alicegoldfuss's tweet"
    ],
    "like-of": [
      "https://twitter.com/alicegoldfuss/status/1239308785224519681"
    ],
    "published": [
      "2020-03-15T23:19:00Z"
    ],
    "syndication": [
      "https://twitter.com/alicegoldfuss/status/1239308785224519681"
    ]
  }
}
