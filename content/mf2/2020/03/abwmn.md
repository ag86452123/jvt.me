{
  "kind": "likes",
  "slug": "2020/03/abwmn",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241725030851117056"
      ],
      "url": [
        "https://twitter.com/MissEllieMae/status/1241725030851117056"
      ],
      "published": [
        "2020-03-22T13:54:52+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MissEllieMae"
            ],
            "numeric-id": [
              "20675211"
            ],
            "name": [
              "Ellie Mae O'Hagan 🏴󠁧󠁢󠁷󠁬󠁳󠁿"
            ],
            "nickname": [
              "MissEllieMae"
            ],
            "url": [
              "https://twitter.com/MissEllieMae",
              "https://about.me/elliemaeohagan"
            ],
            "published": [
              "2009-02-12T13:05:21+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Hackney, London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1191838001971834882/8ql1cg-t.jpg"
            ]
          }
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETt-SoOXsAEkioT.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-23T07:45:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MissEllieMae's tweet"
    ],
    "like-of": [
      "https://twitter.com/MissEllieMae/status/1241725030851117056"
    ],
    "published": [
      "2020-03-23T07:45:00Z"
    ],
    "syndication": [
      "https://twitter.com/MissEllieMae/status/1241725030851117056"
    ]
  }
}
