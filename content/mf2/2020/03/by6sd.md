{
  "kind": "likes",
  "slug": "2020/03/by6sd",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235963895883448327"
      ],
      "url": [
        "https://twitter.com/MrsEmma/status/1235963895883448327"
      ],
      "published": [
        "2020-03-06T16:22:10+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/MrsEmma/status/1235963750546649088"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrsEmma"
            ],
            "numeric-id": [
              "19500955"
            ],
            "name": [
              "Emma Seward"
            ],
            "nickname": [
              "MrsEmma"
            ],
            "url": [
              "https://twitter.com/MrsEmma",
              "https://www.emmasgarden.co.uk/jewellery"
            ],
            "published": [
              "2009-01-25T19:32:42+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1229726485923160064/HgP3BpFu.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "✨ @anna_hax One of the funniest women I know, always smiling, worries about everyone, changed career direction completely and is smashing it (and the patriarchy). Organises #WiTNotts & #TechNott and empowers women. She’s amazing.",
          "html": "✨ <a href=\"https://twitter.com/anna_hax\">@anna_hax</a> One of the funniest women I know, always smiling, worries about everyone, changed career direction completely and is smashing it (and the patriarchy). Organises <a href=\"https://twitter.com/search?q=%23WiTNotts\">#WiTNotts</a> &amp; <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> and empowers women. She’s amazing."
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-07T12:13:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MrsEmma's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrsEmma/status/1235963895883448327"
    ],
    "published": [
      "2020-03-07T12:13:00Z"
    ],
    "syndication": [
      "https://twitter.com/MrsEmma/status/1235963895883448327"
    ]
  }
}
