{
  "kind": "likes",
  "slug": "2020/03/czsic",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237098740382445568"
      ],
      "url": [
        "https://twitter.com/arb/status/1237098740382445568"
      ],
      "published": [
        "2020-03-09T19:31:38+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:arb"
            ],
            "numeric-id": [
              "16444936"
            ],
            "name": [
              "amy"
            ],
            "nickname": [
              "arb"
            ],
            "url": [
              "https://twitter.com/arb",
              "http://arb.substack.com"
            ],
            "published": [
              "2008-09-25T02:01:06+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "sf bay area"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1227463673616195584/GF9zhr9x.jpg"
            ]
          }
        }
      ],
      "content": [
        "working from home day 1: my husband just army-crawled through the dining room to stay out of frame of my zoom call"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-10T07:44:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @arb's tweet"
    ],
    "like-of": [
      "https://twitter.com/arb/status/1237098740382445568"
    ],
    "published": [
      "2020-03-10T07:44:00Z"
    ],
    "syndication": [
      "https://twitter.com/arb/status/1237098740382445568"
    ]
  }
}
