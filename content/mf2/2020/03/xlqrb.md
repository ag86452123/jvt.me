{
  "kind": "likes",
  "slug": "2020/03/xlqrb",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237108227038425088"
      ],
      "url": [
        "https://twitter.com/Themolian/status/1237108227038425088"
      ],
      "published": [
        "2020-03-09T20:09:20+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Themolian"
            ],
            "numeric-id": [
              "53710525"
            ],
            "name": [
              "Jamie C"
            ],
            "nickname": [
              "Themolian"
            ],
            "url": [
              "https://twitter.com/Themolian",
              "http://www.jamiecurran.tech/"
            ],
            "published": [
              "2009-07-04T16:58:59+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Coalville, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1143059634137305089/fyDbAYRi.png"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Doin' a LERN about CERN #TechNott",
          "html": "Doin' a LERN about CERN <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ESsXXiTXQAI-zLU.jpg"
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-03-09T21:12:18.395+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @Themolian's tweet"
    ],
    "like-of": [
      "https://twitter.com/Themolian/status/1237108227038425088"
    ],
    "published": [
      "2020-03-09T21:12:18.395+01:00"
    ],
    "syndication": [
      "https://twitter.com/Themolian/status/1237108227038425088"
    ]
  }
}
