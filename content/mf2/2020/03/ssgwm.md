{
  "kind": "likes",
  "slug": "2020/03/ssgwm",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241034268588957696"
      ],
      "url": [
        "https://twitter.com/rothgar/status/1241034268588957696"
      ],
      "published": [
        "2020-03-20T16:10:01+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rothgar"
            ],
            "numeric-id": [
              "14361975"
            ],
            "name": [
              "Justin 🤘 Garrison"
            ],
            "nickname": [
              "rothgar"
            ],
            "url": [
              "https://twitter.com/rothgar",
              "https://justingarrison.com",
              "http://cnibook.info",
              "http://imdb.to/2FikCiY",
              "http://bit.ly/2tJM3yt"
            ],
            "published": [
              "2008-04-11T15:59:21+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "LAland"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/938285374824214528/IfkIWpni.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "My son has to listen to my mechanical keyboard while doing his homework.\n\nI'm doing my part to teach him a critical life lesson",
          "html": "<div style=\"white-space: pre\">My son has to listen to my mechanical keyboard while doing his homework.\n\nI'm doing my part to teach him a critical life lesson</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-20T17:22:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @rothgar's tweet"
    ],
    "like-of": [
      "https://twitter.com/rothgar/status/1241034268588957696"
    ],
    "published": [
      "2020-03-20T17:22:00Z"
    ],
    "syndication": [
      "https://twitter.com/rothgar/status/1241034268588957696"
    ]
  }
}
