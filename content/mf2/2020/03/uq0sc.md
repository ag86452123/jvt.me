{
  "kind": "likes",
  "slug": "2020/03/uq0sc",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238868585168207873"
      ],
      "url": [
        "https://twitter.com/MatthewFoldi/status/1238868585168207873"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1238318137231228930/pu/vid/720x1280/2_TvsHbE7lYMj2f5.mp4?tag=10"
      ],
      "published": [
        "2020-03-14T16:44:22+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MatthewFoldi"
            ],
            "numeric-id": [
              "1644734071"
            ],
            "name": [
              "Matthew Foldi"
            ],
            "nickname": [
              "MatthewFoldi"
            ],
            "url": [
              "https://twitter.com/MatthewFoldi",
              "http://SmugIndustries.com"
            ],
            "published": [
              "2013-08-04T08:15:40+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "@SmugIndustries"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1225039178083205120/5EBBP5Xd.jpg"
            ]
          }
        }
      ],
      "content": [
        "“How’s nationwide self-quarantine going?” "
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-14T21:50:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MatthewFoldi's tweet"
    ],
    "like-of": [
      "https://twitter.com/MatthewFoldi/status/1238868585168207873"
    ],
    "published": [
      "2020-03-14T21:50:00Z"
    ],
    "syndication": [
      "https://twitter.com/MatthewFoldi/status/1238868585168207873"
    ]
  }
}
