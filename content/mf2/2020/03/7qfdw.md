{
  "kind": "likes",
  "slug": "2020/03/7qfdw",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1243982776044589057"
      ],
      "url": [
        "https://twitter.com/short_louise/status/1243982776044589057"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EUODvKLWsAI2A1L.mp4"
      ],
      "published": [
        "2020-03-28T19:26:20+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:short_louise"
            ],
            "numeric-id": [
              "276855748"
            ],
            "name": [
              "Louise Paling"
            ],
            "nickname": [
              "short_louise"
            ],
            "url": [
              "https://twitter.com/short_louise"
            ],
            "published": [
              "2011-04-04T06:40:11+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1204077832483360771/iEm9QMFA.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Current dilema: do I make chocolate fudge or not?\n\nPros: I have the ingredients. It's easy to make. I want chocolate fudge. It'll make a month's supply.\n\nCons: I'll eat the month's supply within two days.",
          "html": "<div style=\"white-space: pre\">Current dilema: do I make chocolate fudge or not?\n\nPros: I have the ingredients. It's easy to make. I want chocolate fudge. It'll make a month's supply.\n\nCons: I'll eat the month's supply within two days.</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-28T22:50:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @short_louise's tweet"
    ],
    "like-of": [
      "https://twitter.com/short_louise/status/1243982776044589057"
    ],
    "published": [
      "2020-03-28T22:50:00Z"
    ],
    "syndication": [
      "https://twitter.com/short_louise/status/1243982776044589057"
    ]
  }
}
