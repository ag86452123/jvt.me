{
  "kind": "replies",
  "slug": "2020/03/dwuwj",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238865791245856775"
      ],
      "url": [
        "https://twitter.com/rockey5520/status/1238865791245856775"
      ],
      "published": [
        "2020-03-14T16:33:16+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1238844910536740865"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rockey5520"
            ],
            "numeric-id": [
              "28721097"
            ],
            "name": [
              "Rakesh Mothukuri"
            ],
            "nickname": [
              "rockey5520"
            ],
            "url": [
              "https://twitter.com/rockey5520",
              "https://medium.com/@rockey5520",
              "http://bit.ly/2O0hXyg",
              "http://bit.ly/3aN9QyK"
            ],
            "published": [
              "2009-04-04T02:38:33+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1221513497378480128/jRmJT-un.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "@JamieTanna these pointers would definitely help my site, as a matter of fact I've building my own static site using Hugo from 2 weeks and Hugo is super convenient to build with for a person like me who is not very good at JS.",
          "html": "@JamieTanna these pointers would definitely help my site, as a matter of fact I've building my own static site using Hugo from 2 weeks and Hugo is super convenient to build with for a person like me who is not very good at JS.\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-03-14T18:03:58.376+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/rockey5520/status/1238865791245856775"
    ],
    "in-reply-to": [
      "https://twitter.com/rockey5520/status/1238865791245856775"
    ],
    "published": [
      "2020-03-14T18:03:58.376+01:00"
    ],
    "category": [
      "homebrew-website-club",
      "coronavirus"
    ],
    "content": [
      {
        "html": "",
        "value": "Nice! If you manage to come along to <a href=\"/tags/homebrew-website-club/\">#HomebrewWebsiteClub</a> Nottingham we'd be happy to have you to come work on your site 👍 The next one (https://events.indieweb.org/2020/03/homebrew-website-club-nottingham-anniversary-edition--Rcujt5SykHv1) is likely going to be a meal out, so https://events.indieweb.org/2020/04/homebrew-website-club-nottingham-UpVd9JZeVzx6 will be the next one (but I may make it remote-only due to <a href=\"/tags/coronavirus/\">#coronavirus</a>)"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251560318989459457"
    ]
  },
  "tags": [
    "homebrew-website-club",
    "coronavirus"
  ]
}
