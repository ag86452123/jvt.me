{
  "kind": "likes",
  "slug": "2020/03/vq0wz",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237610724240678919"
      ],
      "url": [
        "https://twitter.com/EmilyKager/status/1237610724240678919"
      ],
      "published": [
        "2020-03-11T05:26:05+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/EmilyKager/status/1237607424678215680"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:EmilyKager"
            ],
            "numeric-id": [
              "2347475803"
            ],
            "name": [
              "Emily Kager"
            ],
            "nickname": [
              "EmilyKager"
            ],
            "url": [
              "https://twitter.com/EmilyKager",
              "https://www.emilykager.com/"
            ],
            "published": [
              "2014-02-16T21:34:12+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Oakland and SF "
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1184591701966385152/KdEIgb-J.jpg"
            ]
          }
        }
      ],
      "content": [
        "Ending the patriarchy one sassy DM at a time 💅"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ESzgRbzUUAAk3NO.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-11T07:51:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @EmilyKager's tweet"
    ],
    "like-of": [
      "https://twitter.com/EmilyKager/status/1237610724240678919"
    ],
    "published": [
      "2020-03-11T07:51:00Z"
    ],
    "syndication": [
      "https://twitter.com/EmilyKager/status/1237610724240678919"
    ]
  }
}
