{
  "kind": "likes",
  "slug": "2020/03/8eu2v",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1242888525059477504"
      ],
      "url": [
        "https://twitter.com/nocontextpawnee/status/1242888525059477504"
      ],
      "published": [
        "2020-03-25T18:58:10+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:nocontextpawnee"
            ],
            "numeric-id": [
              "2863580894"
            ],
            "name": [
              "out of context parks & rec"
            ],
            "nickname": [
              "nocontextpawnee"
            ],
            "url": [
              "https://twitter.com/nocontextpawnee"
            ],
            "published": [
              "2014-10-18T22:28:26+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Pawnee, IN"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1231858194978230272/qxXBNNmg.jpg"
            ]
          }
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ET-ggiRVAAI-tW_.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-25T22:22:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @nocontextpawnee's tweet"
    ],
    "like-of": [
      "https://twitter.com/nocontextpawnee/status/1242888525059477504"
    ],
    "published": [
      "2020-03-25T22:22:00Z"
    ],
    "syndication": [
      "https://twitter.com/nocontextpawnee/status/1242888525059477504"
    ]
  }
}
