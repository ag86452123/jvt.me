{
  "kind": "likes",
  "slug": "2020/03/ybbx5",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237907573313024001"
      ],
      "url": [
        "https://twitter.com/NotNikyatu/status/1237907573313024001"
      ],
      "published": [
        "2020-03-12T01:05:39+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:NotNikyatu"
            ],
            "numeric-id": [
              "18832563"
            ],
            "name": [
              "Octavia Butler knew..."
            ],
            "nickname": [
              "NotNikyatu"
            ],
            "url": [
              "https://twitter.com/NotNikyatu",
              "http://nikyatu.com"
            ],
            "published": [
              "2009-01-10T11:49:44+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "In your head / IG: Nikyatu"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1211094239133798401/Lto2zsIT.jpg"
            ]
          }
        }
      ],
      "content": [
        "Ya'll are gonna eat all your quarantine snacks in one night."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-12T07:43:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @NotNikyatu's tweet"
    ],
    "like-of": [
      "https://twitter.com/NotNikyatu/status/1237907573313024001"
    ],
    "published": [
      "2020-03-12T07:43:00Z"
    ],
    "syndication": [
      "https://twitter.com/NotNikyatu/status/1237907573313024001"
    ]
  }
}
