{
  "kind": "reposts",
  "slug": "2020/03/tnjv8",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1236919194794459136"
      ],
      "url": [
        "https://twitter.com/winyeemichelle/status/1236919194794459136"
      ],
      "published": [
        "2020-03-09T07:38:11+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:winyeemichelle"
            ],
            "numeric-id": [
              "22541903"
            ],
            "name": [
              "Michelle Chai"
            ],
            "nickname": [
              "winyeemichelle"
            ],
            "url": [
              "https://twitter.com/winyeemichelle",
              "http://www.daisybutter.com",
              "http://www.instagram.com/winyeemichelle"
            ],
            "published": [
              "2009-03-02T22:02:27+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Hertfordshire"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1201066041012629504/tr4naI-z.jpg"
            ]
          }
        }
      ],
      "content": [
        "This is going to sound kinda mad, but this week, pls consider making your weekly takeout a Chinese takeaway. My family's businesses have all been impacted hugely by coronavirus panic 😭"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-10T22:28:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-10T22:28:00Z"
    ],
    "repost-of": [
      "https://twitter.com/winyeemichelle/status/1236919194794459136"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1237506927489363969"
    ]
  }
}
