{
  "kind": "reposts",
  "slug": "2020/03/qauq0",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238475687830355970"
      ],
      "url": [
        "https://twitter.com/BeardedGenius/status/1238475687830355970"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1238376470747271169/pu/vid/720x720/89-n9xZJ9nZpgMcU.mp4?tag=10"
      ],
      "published": [
        "2020-03-13T14:43:08+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:BeardedGenius"
            ],
            "numeric-id": [
              "176754253"
            ],
            "name": [
              "Nooruddean"
            ],
            "nickname": [
              "BeardedGenius"
            ],
            "url": [
              "https://twitter.com/BeardedGenius",
              "https://www.instagram.com/beardedgenius/"
            ],
            "published": [
              "2010-08-10T10:29:01+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1234704023422480386/UmxoyzPa.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "This 18 seconds is probably the best single piece of advice I’ve heard about the #coronavirus ",
          "html": "This 18 seconds is probably the best single piece of advice I’ve heard about the <a href=\"https://twitter.com/search?q=%23coronavirus\">#coronavirus</a> "
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-13T18:22:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-13T18:22:00Z"
    ],
    "repost-of": [
      "https://twitter.com/BeardedGenius/status/1238475687830355970"
    ],
    "category": [
      "coronavirus"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1238533139703779330"
    ]
  },
  "tags": [
    "coronavirus"
  ]
}
