{
  "kind": "likes",
  "slug": "2020/03/wbjug",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1242789502634123264"
      ],
      "url": [
        "https://twitter.com/kvlly/status/1242789502634123264"
      ],
      "published": [
        "2020-03-25T12:24:41+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:kvlly"
            ],
            "numeric-id": [
              "123543103"
            ],
            "name": [
              "Kelly Vaughn 🐞"
            ],
            "nickname": [
              "kvlly"
            ],
            "url": [
              "https://twitter.com/kvlly",
              "http://kellyvaughn.co"
            ],
            "published": [
              "2010-03-16T12:15:39+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Atlanta, GA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1240319235714408456/EqySP4c8.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Happy Friday, everyone\nOr is it Tuesday?\nMaybe it's Sunday?",
          "html": "<div style=\"white-space: pre\">Happy Friday, everyone\nOr is it Tuesday?\nMaybe it's Sunday?</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-25T22:12:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @kvlly's tweet"
    ],
    "like-of": [
      "https://twitter.com/kvlly/status/1242789502634123264"
    ],
    "published": [
      "2020-03-25T22:12:00Z"
    ],
    "syndication": [
      "https://twitter.com/kvlly/status/1242789502634123264"
    ]
  }
}
