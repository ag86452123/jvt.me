{
  "kind": "likes",
  "slug": "2020/03/xflzt",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241470477459230722"
      ],
      "url": [
        "https://twitter.com/emmabethgall/status/1241470477459230722"
      ],
      "published": [
        "2020-03-21T21:03:21+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:emmabethgall"
            ],
            "numeric-id": [
              "4212443183"
            ],
            "name": [
              "Emma 🇸🇩"
            ],
            "nickname": [
              "emmabethgall"
            ],
            "url": [
              "https://twitter.com/emmabethgall"
            ],
            "published": [
              "2015-11-17T20:06:38+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1000741991880196097/cDkvCllg.jpg"
            ]
          }
        }
      ],
      "content": [
        "Three generations of social distancing as my dad meets his grandson for the first time 😭😭😭"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETqWzvRXQAYNTGg.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-22T00:36:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @emmabethgall's tweet"
    ],
    "like-of": [
      "https://twitter.com/emmabethgall/status/1241470477459230722"
    ],
    "published": [
      "2020-03-22T00:36:00Z"
    ],
    "syndication": [
      "https://twitter.com/emmabethgall/status/1241470477459230722"
    ]
  }
}
