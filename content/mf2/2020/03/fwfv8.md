{
  "kind": "likes",
  "slug": "2020/03/fwfv8",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237030678518267904"
      ],
      "url": [
        "https://twitter.com/SBinLondon/status/1237030678518267904"
      ],
      "published": [
        "2020-03-09T15:01:11+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:SBinLondon"
            ],
            "numeric-id": [
              "180283911"
            ],
            "name": [
              "Kate"
            ],
            "nickname": [
              "SBinLondon"
            ],
            "url": [
              "https://twitter.com/SBinLondon",
              "http://katebeard.co"
            ],
            "published": [
              "2010-08-19T07:16:23+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Where the good coffee is"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1222510978732036102/Dx46JyVK.jpg"
            ]
          }
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ESrQxzuWkAcRrNr.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-09T23:45:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @SBinLondon's tweet"
    ],
    "like-of": [
      "https://twitter.com/SBinLondon/status/1237030678518267904"
    ],
    "published": [
      "2020-03-09T23:45:00Z"
    ],
    "syndication": [
      "https://twitter.com/SBinLondon/status/1237030678518267904"
    ]
  }
}
