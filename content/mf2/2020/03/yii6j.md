{
  "kind": "replies",
  "slug": "2020/03/yii6j",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1236973101935624194"
      ],
      "url": [
        "https://twitter.com/pete_codes/status/1236973101935624194"
      ],
      "published": [
        "2020-03-09T11:12:24+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:pete_codes"
            ],
            "numeric-id": [
              "15156749"
            ],
            "name": [
              "Pete Gallagher - Azure #MVPBuzz"
            ],
            "nickname": [
              "pete_codes"
            ],
            "url": [
              "https://twitter.com/pete_codes",
              "https://www.petecodes.co.uk"
            ],
            "published": [
              "2008-06-18T11:20:03+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1005728851882921985/r3eA9KtW.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "So close! Ha... #NearlyAt1000",
          "html": "So close! Ha... <a href=\"https://twitter.com/search?q=%23NearlyAt1000\">#NearlyAt1000</a>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ESqceaLXgAE9E5_.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-09T11:37:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/pete_codes/status/1236973101935624194"
    ],
    "in-reply-to": [
      "https://twitter.com/pete_codes/status/1236973101935624194"
    ],
    "published": [
      "2020-03-09T11:37:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "I've spent the last few weeks going up over the threshold and then dipping under 😅"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1236981178361679882"
    ]
  }
}
