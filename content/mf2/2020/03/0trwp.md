{
  "kind": "likes",
  "slug": "2020/03/0trwp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1244174567905669120"
      ],
      "url": [
        "https://twitter.com/ClaireEGamble/status/1244174567905669120"
      ],
      "published": [
        "2020-03-29T08:08:27+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ClaireEGamble"
            ],
            "numeric-id": [
              "92779207"
            ],
            "name": [
              "Claire Gamble"
            ],
            "nickname": [
              "ClaireEGamble"
            ],
            "url": [
              "https://twitter.com/ClaireEGamble",
              "http://www.weareunhooked.com"
            ],
            "published": [
              "2009-11-26T15:51:52+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Manchester"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1231613344970346496/QQ_ixzD_.jpg"
            ]
          }
        }
      ],
      "content": [
        "Guys, I just checked the Ts & Cs of Zoom, House Party, FaceTime and Skype. Turns out you don’t have to take a photo of your screen and share it on social media every time you have a video call 👍"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-30T08:11:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ClaireEGamble's tweet"
    ],
    "like-of": [
      "https://twitter.com/ClaireEGamble/status/1244174567905669120"
    ],
    "published": [
      "2020-03-30T08:11:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/ClaireEGamble/status/1244174567905669120"
    ]
  }
}
