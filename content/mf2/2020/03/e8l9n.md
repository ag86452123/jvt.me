{
  "kind": "replies",
  "slug": "2020/03/e8l9n",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239881456987656193"
      ],
      "url": [
        "https://twitter.com/niceotherwise/status/1239881456987656193"
      ],
      "published": [
        "2020-03-17T11:49:09+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:niceotherwise"
            ],
            "numeric-id": [
              "867383993796308992"
            ],
            "name": [
              "velvet! (at home!)"
            ],
            "nickname": [
              "niceotherwise"
            ],
            "url": [
              "https://twitter.com/niceotherwise",
              "http://workshop.niceotherwise.space"
            ],
            "published": [
              "2017-05-24T14:17:14+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "www"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/867386997966610432/c9GXx_TF.jpg"
            ]
          }
        }
      ],
      "content": [
        "are there any services that allow for a private git repo shared with others, without a monthly fee? (a single repo would suffice, merci!)"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-20T07:38:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/niceotherwise/status/1239881456987656193"
    ],
    "in-reply-to": [
      "https://twitter.com/niceotherwise/status/1239881456987656193"
    ],
    "published": [
      "2020-03-20T07:38:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "https://gitlab.com has unlimited collaborators on unlimited private git repos, as well as a tonne of other stuff built in if you want it 👍🏽"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1240906886326738944"
    ]
  }
}
