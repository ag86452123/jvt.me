{
  "kind": "likes",
  "slug": "2020/03/h8xcn",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239857459231182848"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1239857459231182848"
      ],
      "published": [
        "2020-03-17T10:13:48+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1239827385001938944"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna ✨🙅‍♀️✨"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "It was supposed to be bucks fizz but we're both working today!! The tango was left over from a KFC deliveroo 😂 🥂",
          "html": "It was supposed to be bucks fizz but we're both working today!! The tango was left over from a KFC deliveroo 😂 🥂\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-17T10:25:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @anna_hax's tweet"
    ],
    "like-of": [
      "https://twitter.com/anna_hax/status/1239857459231182848"
    ],
    "published": [
      "2020-03-17T10:25:00Z"
    ],
    "syndication": [
      "https://twitter.com/anna_hax/status/1239857459231182848"
    ]
  }
}
