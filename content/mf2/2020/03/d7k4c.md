{
  "kind": "likes",
  "slug": "2020/03/d7k4c",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235224466390646784"
      ],
      "url": [
        "https://twitter.com/kieranmch/status/1235224466390646784"
      ],
      "published": [
        "2020-03-04T15:23:56+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:kieranmch"
            ],
            "numeric-id": [
              "50420707"
            ],
            "name": [
              "Kieran McHugh"
            ],
            "nickname": [
              "kieranmch"
            ],
            "url": [
              "https://twitter.com/kieranmch",
              "https://kieran.engineer"
            ],
            "published": [
              "2009-06-24T20:13:29+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1170973779675275264/cCZMxVEg.jpg"
            ]
          }
        }
      ],
      "content": [
        "“Yes, I want coronavirus...” 😂"
      ]
    },
    "children": [
      {
        "type": [
          "u-quotation-of",
          "h-cite"
        ],
        "properties": {
          "uid": [
            "tag:twitter.com:1234965911099985920"
          ],
          "url": [
            "https://twitter.com/juliehubs/status/1234965911099985920"
          ],
          "published": [
            "2020-03-03T22:16:32+00:00"
          ],
          "author": [
            {
              "type": [
                "h-card"
              ],
              "properties": {
                "uid": [
                  "tag:twitter.com:juliehubs"
                ],
                "numeric-id": [
                  "2440732321"
                ],
                "name": [
                  "Julie Hubschman"
                ],
                "nickname": [
                  "juliehubs"
                ],
                "url": [
                  "https://twitter.com/juliehubs",
                  "http://juliehubs.com"
                ],
                "published": [
                  "2014-04-12T23:39:05+00:00"
                ],
                "location": [
                  {
                    "type": [
                      "h-card",
                      "p-location"
                    ],
                    "properties": {
                      "name": [
                        "Seattle, WA"
                      ]
                    }
                  }
                ],
                "photo": [
                  "https://pbs.twimg.com/profile_images/1221217896505864192/QGWbn-zr.jpg"
                ]
              }
            }
          ],
          "content": [
            "Not quite what I was hoping for Edge."
          ],
          "photo": [
            "https://pbs.twimg.com/media/ESN683rUwAAW_k4.jpg"
          ]
        }
      }
    ]
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-04T17:03:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @kieranmch's tweet"
    ],
    "like-of": [
      "https://twitter.com/kieranmch/status/1235224466390646784"
    ],
    "published": [
      "2020-03-04T17:03:00Z"
    ],
    "syndication": [
      "https://twitter.com/kieranmch/status/1235224466390646784"
    ]
  }
}
