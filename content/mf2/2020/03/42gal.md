{
  "kind": "replies",
  "slug": "2020/03/42gal",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1243921820585201664"
      ],
      "url": [
        "https://twitter.com/swentel/status/1243921820585201664"
      ],
      "published": [
        "2020-03-28T15:24:07+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:swentel"
            ],
            "numeric-id": [
              "48998488"
            ],
            "name": [
              "Kristof De Jaeger"
            ],
            "nickname": [
              "swentel"
            ],
            "url": [
              "https://twitter.com/swentel",
              "https://realize.be"
            ],
            "published": [
              "2009-06-20T12:51:48+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/653664367259590656/eYkRsNYV.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Started working on Indigenous Desktop, because, well why not :) Currently connects anonymously like the Android version. Written using Electron, so will be available for all platforms! github.com/swentel/indige… #indieweb",
          "html": "Started working on Indigenous Desktop, because, well why not :) Currently connects anonymously like the Android version. Written using Electron, so will be available for all platforms! <a href=\"https://github.com/swentel/indigenous-desktop\">github.com/swentel/indige…</a> <a href=\"https://twitter.com/search?q=%23indieweb\">#indieweb</a>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUNMS5EXQAQL8Xq.jpg",
        "https://pbs.twimg.com/media/EUNMTVdXgAA-IT5.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-28T17:27:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/swentel/status/1243921820585201664"
    ],
    "in-reply-to": [
      "https://twitter.com/swentel/status/1243921820585201664"
    ],
    "published": [
      "2020-03-28T17:27:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Would you consider making a Web client as well? I'd probably opt for Web then Electron if possible, but I appreciate that's probably more work"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1243954076943032320"
    ]
  }
}
