{
  "kind": "likes",
  "slug": "2020/03/xm6uf",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235402837745532929"
      ],
      "url": [
        "https://twitter.com/JackWellborn/status/1235402837745532929"
      ],
      "published": [
        "2020-03-05T03:12:43+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:JackWellborn"
            ],
            "numeric-id": [
              "29244555"
            ],
            "name": [
              "Jack Wellborn"
            ],
            "nickname": [
              "JackWellborn"
            ],
            "url": [
              "https://twitter.com/JackWellborn",
              "https://jackwellborn.com"
            ],
            "published": [
              "2009-04-06T17:16:33+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Austin, TX"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1207687649827397632/J4RdrP4M.jpg"
            ]
          }
        }
      ],
      "content": [
        "Posting to Twitter from my WordPress blog using a Webmention and Bridgy. That’s cool."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-05T07:52:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @JackWellborn's tweet"
    ],
    "like-of": [
      "https://twitter.com/JackWellborn/status/1235402837745532929"
    ],
    "published": [
      "2020-03-05T07:52:00Z"
    ],
    "syndication": [
      "https://twitter.com/JackWellborn/status/1235402837745532929"
    ]
  }
}
