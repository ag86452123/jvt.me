{
  "kind": "likes",
  "slug": "2020/03/psuwt",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239952416411353089"
      ],
      "url": [
        "https://twitter.com/TheAnaka/status/1239952416411353089"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1239952350267224064/pu/vid/1280x720/IxJrOfwTsEIW2j5g.mp4?tag=10"
      ],
      "published": [
        "2020-03-17T16:31:07+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:TheAnaka"
            ],
            "numeric-id": [
              "898437493"
            ],
            "name": [
              "Anna Jenelius 🦔"
            ],
            "nickname": [
              "TheAnaka"
            ],
            "url": [
              "https://twitter.com/TheAnaka",
              "http://anaka.se",
              "http://valiant.se"
            ],
            "published": [
              "2012-10-22T21:13:11+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Stockholm"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1103635078683467776/EpGqfLuv.jpg"
            ]
          }
        }
      ],
      "content": [
        "The feeling when a productive workday is over and it's time to go home 🥰"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-18T22:42:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @TheAnaka's tweet"
    ],
    "like-of": [
      "https://twitter.com/TheAnaka/status/1239952416411353089"
    ],
    "published": [
      "2020-03-18T22:42:00Z"
    ],
    "category": [
      "coronavirus"
    ],
    "syndication": [
      "https://twitter.com/TheAnaka/status/1239952416411353089"
    ]
  },
  "tags": [
    "coronavirus"
  ]
}
