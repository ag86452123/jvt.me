{
  "kind": "likes",
  "slug": "2020/03/bn6hc",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241848544396541956"
      ],
      "url": [
        "https://twitter.com/rwdrich/status/1241848544396541956"
      ],
      "published": [
        "2020-03-22T22:05:40+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rwdrich"
            ],
            "numeric-id": [
              "107548386"
            ],
            "name": [
              "Richard Davies"
            ],
            "nickname": [
              "rwdrich"
            ],
            "url": [
              "https://twitter.com/rwdrich",
              "http://rwdrich.co.uk"
            ],
            "published": [
              "2010-01-22T23:14:30+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cambridge, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Remain Indoors! youtube.com/watch?v=wnd1jK…",
          "html": "Remain Indoors! <a href=\"https://www.youtube.com/watch?v=wnd1jKcfBRE\">youtube.com/watch?v=wnd1jK…</a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-22T22:31:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @rwdrich's tweet"
    ],
    "like-of": [
      "https://twitter.com/rwdrich/status/1241848544396541956"
    ],
    "published": [
      "2020-03-22T22:31:00Z"
    ],
    "syndication": [
      "https://twitter.com/rwdrich/status/1241848544396541956"
    ]
  }
}
