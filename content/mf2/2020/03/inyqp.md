{
  "kind": "replies",
  "slug": "2020/03/inyqp",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1243155093031129093"
      ],
      "url": [
        "https://twitter.com/craigburgess/status/1243155093031129093"
      ],
      "published": [
        "2020-03-26T12:37:25+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/calum_ryan/status/1243147661806575617"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:craigburgess"
            ],
            "numeric-id": [
              "14912335"
            ],
            "name": [
              "Craig Burgess"
            ],
            "nickname": [
              "craigburgess"
            ],
            "url": [
              "https://twitter.com/craigburgess",
              "https://www.getdoingthings.com"
            ],
            "published": [
              "2008-05-26T19:26:06+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Barnsley, South Yorkshire, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1162042955974283266/7PD6uyTD.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Sure, could be fun!\n\nI planned on dropping into others last night but just didn't get the chance.",
          "html": "<div style=\"white-space: pre\">Sure, could be fun!\n\nI planned on dropping into others last night but just didn't get the chance.</div>\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/calum_ryan\"></a>"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-03-27T21:23:29.05+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/craigburgess/status/1243155093031129093"
    ],
    "in-reply-to": [
      "https://twitter.com/craigburgess/status/1243155093031129093"
    ],
    "published": [
      "2020-03-27T21:23:29.05+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Sorry for the late reply - yeah I'm up for it! I'd still fancy primarily keeping my slot, just cause I'm a creature of habit 😅"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1243636804600938498"
    ]
  }
}
