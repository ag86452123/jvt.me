{
  "kind": "replies",
  "slug": "2020/03/tdans",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1242772472883224576"
      ],
      "url": [
        "https://twitter.com/craigburgess/status/1242772472883224576"
      ],
      "published": [
        "2020-03-25T11:17:01+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/craigburgess/status/1242771429738852357"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:craigburgess"
            ],
            "numeric-id": [
              "14912335"
            ],
            "name": [
              "Craig Burgess"
            ],
            "nickname": [
              "craigburgess"
            ],
            "url": [
              "https://twitter.com/craigburgess",
              "https://www.getdoingthings.com"
            ],
            "published": [
              "2008-05-26T19:26:06+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Barnsley, South Yorkshire, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1162042955974283266/7PD6uyTD.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Would be cool to see @JamieTanna \n\nAlso updated events.indieweb.org/2020/03/homebr…",
          "html": "<div style=\"white-space: pre\">Would be cool to see <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a> \n\nAlso updated <a href=\"https://events.indieweb.org/2020/03/homebrew-website-club-barnsley-hGOqaDqcyIPP\">events.indieweb.org/2020/03/homebr…</a></div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-25T11:54:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/craigburgess/status/1242772472883224576"
    ],
    "in-reply-to": [
      "https://twitter.com/craigburgess/status/1242772472883224576"
    ],
    "published": [
      "2020-03-25T11:54:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Nice, I'll see if I'm able to drop in to say hi 👋🏽"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1242783464472141824"
    ]
  }
}
