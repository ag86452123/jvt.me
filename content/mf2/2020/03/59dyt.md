{
  "kind": "likes",
  "slug": "2020/03/59dyt",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1242941018305187840"
      ],
      "url": [
        "https://twitter.com/MrsEmma/status/1242941018305187840"
      ],
      "published": [
        "2020-03-25T22:26:46+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1240371850795986946"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrsEmma"
            ],
            "numeric-id": [
              "19500955"
            ],
            "name": [
              "Emma Seward"
            ],
            "nickname": [
              "MrsEmma"
            ],
            "url": [
              "https://twitter.com/MrsEmma",
              "https://www.emmasgarden.co.uk/jewellery"
            ],
            "published": [
              "2009-01-25T19:32:42+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1239225071186653184/gm6PZrtm.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "html": "\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ET_QRNBXgAUI8Km.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-25T22:44:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MrsEmma's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrsEmma/status/1242941018305187840"
    ],
    "published": [
      "2020-03-25T22:44:00Z"
    ],
    "category": [
      "morph"
    ],
    "syndication": [
      "https://twitter.com/MrsEmma/status/1242941018305187840"
    ]
  },
  "tags": [
    "morph"
  ]
}
