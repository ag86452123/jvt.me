{
  "kind": "likes",
  "slug": "2020/03/eewru",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241931990560935937"
      ],
      "url": [
        "https://twitter.com/bradfitz/status/1241931990560935937"
      ],
      "published": [
        "2020-03-23T03:37:15+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:bradfitz"
            ],
            "numeric-id": [
              "650013"
            ],
            "name": [
              "Brad Fitzpatrick"
            ],
            "nickname": [
              "bradfitz"
            ],
            "url": [
              "https://twitter.com/bradfitz",
              "http://bradfitz.com/"
            ],
            "published": [
              "2007-01-16T23:05:57+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Seattle, WA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/952380818126589952/381x2wpW.jpg"
            ]
          }
        }
      ],
      "content": [
        "We're only three episodes in, but the show \"Brooklyn Nine-Nine\" might be the only thing that gets us through this pandemic with a modicum of sanity."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-23T07:46:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @bradfitz's tweet"
    ],
    "like-of": [
      "https://twitter.com/bradfitz/status/1241931990560935937"
    ],
    "published": [
      "2020-03-23T07:46:00Z"
    ],
    "syndication": [
      "https://twitter.com/bradfitz/status/1241931990560935937"
    ]
  }
}
