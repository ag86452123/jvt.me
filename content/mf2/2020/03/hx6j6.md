{
  "kind": "likes",
  "slug": "2020/03/hx6j6",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238336175909158914"
      ],
      "url": [
        "https://twitter.com/TatianaTMac/status/1238336175909158914"
      ],
      "published": [
        "2020-03-13T05:28:46+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:TatianaTMac"
            ],
            "numeric-id": [
              "1529722572"
            ],
            "name": [
              "Tatiana Mac"
            ],
            "nickname": [
              "TatianaTMac"
            ],
            "url": [
              "https://twitter.com/TatianaTMac",
              "https://www.tatianamac.com"
            ],
            "published": [
              "2013-06-19T04:59:25+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "she/they/hän 💖💛💙"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1214212889122181123/1lCP2C5T.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Solidarity with my single friends who don’t have (a) partner(s)/cohabitators right now who are there to help you prepare and to cope. While distancing and isolation is the right thing, it can compound existing feelings of loneliness.\n\nI know it does for me.",
          "html": "<div style=\"white-space: pre\">Solidarity with my single friends who don’t have (a) partner(s)/cohabitators right now who are there to help you prepare and to cope. While distancing and isolation is the right thing, it can compound existing feelings of loneliness.\n\nI know it does for me.</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-13T07:57:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @TatianaTMac's tweet"
    ],
    "like-of": [
      "https://twitter.com/TatianaTMac/status/1238336175909158914"
    ],
    "published": [
      "2020-03-13T07:57:00Z"
    ],
    "syndication": [
      "https://twitter.com/TatianaTMac/status/1238336175909158914"
    ]
  }
}
