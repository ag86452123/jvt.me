{
  "kind": "likes",
  "slug": "2020/03/rhnun",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239951227405901825"
      ],
      "url": [
        "https://twitter.com/sdh100Shaun/status/1239951227405901825"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/ETUxEFUWoAY3q77.mp4"
      ],
      "published": [
        "2020-03-17T16:26:24+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1239827385001938944"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:sdh100Shaun"
            ],
            "numeric-id": [
              "257573"
            ],
            "name": [
              "Shaun"
            ],
            "nickname": [
              "sdh100Shaun"
            ],
            "url": [
              "https://twitter.com/sdh100Shaun",
              "http://shaunhare.co.uk"
            ],
            "published": [
              "2006-12-26T09:09:15+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "257.34,-1.47"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/908055077520379905/mAVLMD60.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "html": "\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-17T16:28:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @sdh100Shaun's tweet"
    ],
    "like-of": [
      "https://twitter.com/sdh100Shaun/status/1239951227405901825"
    ],
    "published": [
      "2020-03-17T16:28:00Z"
    ],
    "syndication": [
      "https://twitter.com/sdh100Shaun/status/1239951227405901825"
    ]
  }
}
