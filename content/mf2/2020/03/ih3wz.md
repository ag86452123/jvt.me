{
  "kind": "likes",
  "slug": "2020/03/ih3wz",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233487766668922881"
      ],
      "url": [
        "https://twitter.com/paplovescats/status/1233487766668922881"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1233160328659079168/pu/vid/720x1280/AhNU0qmIuQ_aCrRu.mp4?tag=10"
      ],
      "published": [
        "2020-02-28T20:22:55+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:paplovescats"
            ],
            "numeric-id": [
              "1055966035646517248"
            ],
            "name": [
              "𝒫"
            ],
            "nickname": [
              "paplovescats"
            ],
            "url": [
              "https://twitter.com/paplovescats",
              "http://curiouscat.me/paplovescats"
            ],
            "published": [
              "2018-10-26T23:34:56+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Norway"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1231701830335451137/33DI-UjG.jpg"
            ]
          }
        }
      ],
      "content": [
        "But can ur dog be this cute while playing fetch? "
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-01T07:50:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @paplovescats's tweet"
    ],
    "like-of": [
      "https://twitter.com/paplovescats/status/1233487766668922881"
    ],
    "published": [
      "2020-03-01T07:50:00Z"
    ],
    "category": [
      "cute"
    ],
    "syndication": [
      "https://twitter.com/paplovescats/status/1233487766668922881"
    ]
  },
  "tags": [
    "cute"
  ]
}
