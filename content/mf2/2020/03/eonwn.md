{
  "kind": "likes",
  "slug": "2020/03/eonwn",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241128843131285504"
      ],
      "url": [
        "https://twitter.com/nocontextpawnee/status/1241128843131285504"
      ],
      "published": [
        "2020-03-20T22:25:49+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:nocontextpawnee"
            ],
            "numeric-id": [
              "2863580894"
            ],
            "name": [
              "out of context parks & rec"
            ],
            "nickname": [
              "nocontextpawnee"
            ],
            "url": [
              "https://twitter.com/nocontextpawnee",
              "https://twitter.com/iilithsternin"
            ],
            "published": [
              "2014-10-18T22:28:26+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Pawnee, IN"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1231858194978230272/qxXBNNmg.jpg"
            ]
          }
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETlgF6CUUAAOyno.jpg",
        "https://pbs.twimg.com/media/ETlgF6RUEAALrrm.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-20T22:48:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @nocontextpawnee's tweet"
    ],
    "like-of": [
      "https://twitter.com/nocontextpawnee/status/1241128843131285504"
    ],
    "published": [
      "2020-03-20T22:48:00Z"
    ],
    "syndication": [
      "https://twitter.com/nocontextpawnee/status/1241128843131285504"
    ]
  }
}
