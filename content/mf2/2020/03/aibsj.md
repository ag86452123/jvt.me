{
  "kind": "replies",
  "slug": "2020/03/aibsj",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233929091960754177"
      ],
      "url": [
        "https://twitter.com/nodebotanist/status/1233929091960754177"
      ],
      "published": [
        "2020-03-01T01:36:35+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:nodebotanist"
            ],
            "numeric-id": [
              "31204696"
            ],
            "name": [
              "Mxs. Kassian Rosner Wren"
            ],
            "nickname": [
              "nodebotanist"
            ],
            "url": [
              "https://twitter.com/nodebotanist",
              "http://twitch.tv/nodebotanist"
            ],
            "published": [
              "2009-04-14T19:37:43+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Austin, TX"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1206989160038445056/RR1YU361.jpg"
            ]
          }
        }
      ],
      "content": [
        "Devs: where/what platform are we using for blogs this decade? I don't wanna use Medium because of the paywalls n stuff."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-01T07:40:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/nodebotanist/status/1233929091960754177"
    ],
    "in-reply-to": [
      "https://twitter.com/nodebotanist/status/1233929091960754177"
    ],
    "published": [
      "2020-03-01T07:40:00Z"
    ],
    "category": [
      "personal-website"
    ],
    "content": [
      {
        "html": "",
        "value": "I'd thoroughly recommend self hosting - I use Hugo and Netlify for mine and it works really well. It's also got the bonus that because it's my own site and platform that I can use it as I want, such as replying to your tweet from my website"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1234021782379749376"
    ]
  },
  "tags": [
    "personal-website"
  ]
}
