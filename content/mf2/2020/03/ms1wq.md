{
  "kind": "likes",
  "slug": "2020/03/ms1wq",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235255161796399105"
      ],
      "url": [
        "https://twitter.com/MsJuneDiane/status/1235255161796399105"
      ],
      "published": [
        "2020-03-04T17:25:55+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MsJuneDiane"
            ],
            "numeric-id": [
              "73060860"
            ],
            "name": [
              "June Diane Raphael"
            ],
            "nickname": [
              "MsJuneDiane"
            ],
            "url": [
              "https://twitter.com/MsJuneDiane"
            ],
            "published": [
              "2009-09-10T06:51:57+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/845504939723931649/8AA48C3v.jpg"
            ]
          }
        }
      ],
      "content": [
        "Stop telling your daughters they can be president when you are unwilling to vote for a woman president."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-06T12:58:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MsJuneDiane's tweet"
    ],
    "like-of": [
      "https://twitter.com/MsJuneDiane/status/1235255161796399105"
    ],
    "published": [
      "2020-03-06T12:58:00Z"
    ],
    "syndication": [
      "https://twitter.com/MsJuneDiane/status/1235255161796399105"
    ]
  }
}
