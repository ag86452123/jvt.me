{
  "kind": "likes",
  "slug": "2020/03/8zoxz",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1235841117330927617"
      ],
      "url": [
        "https://twitter.com/rwdrich/status/1235841117330927617"
      ],
      "published": [
        "2020-03-06T08:14:17+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rwdrich"
            ],
            "numeric-id": [
              "107548386"
            ],
            "name": [
              "Richard Davies"
            ],
            "nickname": [
              "rwdrich"
            ],
            "url": [
              "https://twitter.com/rwdrich",
              "http://rwdrich.co.uk"
            ],
            "published": [
              "2010-01-22T23:14:30+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cambridge, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg"
            ]
          }
        }
      ],
      "content": [
        "I think today is a \"headphones at max volume\" kind of day..."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-06T12:37:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @rwdrich's tweet"
    ],
    "like-of": [
      "https://twitter.com/rwdrich/status/1235841117330927617"
    ],
    "published": [
      "2020-03-06T12:37:00Z"
    ],
    "syndication": [
      "https://twitter.com/rwdrich/status/1235841117330927617"
    ]
  }
}
