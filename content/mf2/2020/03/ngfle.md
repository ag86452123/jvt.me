{
  "kind": "likes",
  "slug": "2020/03/ngfle",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238055678322884608"
      ],
      "url": [
        "https://twitter.com/PandelisZ/status/1238055678322884608"
      ],
      "published": [
        "2020-03-12T10:54:10+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:PandelisZ"
            ],
            "numeric-id": [
              "2837561099"
            ],
            "name": [
              "‏Pα̹̖̺̃̊ͩ̑̽ͥν͍̯̳̳τ̗̘̺̥̦͊̉̃ͥͫͅε̝͗̎͗ͮλ̒̓̿͆is🇪🇺"
            ],
            "nickname": [
              "PandelisZ"
            ],
            "url": [
              "https://twitter.com/PandelisZ",
              "https://pa.ndel.is/"
            ],
            "published": [
              "2014-10-19T20:06:41+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Beerlin"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1090981237722877952/m04GP5lj.jpg"
            ]
          }
        }
      ],
      "content": [
        "Mood"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ES51EYdWoAAyP7B.png"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-12T18:52:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @PandelisZ's tweet"
    ],
    "like-of": [
      "https://twitter.com/PandelisZ/status/1238055678322884608"
    ],
    "published": [
      "2020-03-12T18:52:00Z"
    ],
    "syndication": [
      "https://twitter.com/PandelisZ/status/1238055678322884608"
    ]
  }
}
