{
  "kind": "rsvps",
  "slug": "2020/03/cnom6",
  "client_id": "https://micropublish.net",
  "date": "2020-03-01T08:59:47.626+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/Nottingham-IoT-Meetup/events/268851498/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Nottingham-IoT-Meetup/events/268851498/"
    ],
    "published": [
      "2020-03-01T08:59:47.626+01:00"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/Nottingham-IoT-Meetup/events/268851498/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Rebel Recruiters, Huntingdon Street, Nottinghamshire, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/Nottingham-IoT-Meetup/events/268851498/"
      ],
      "name": [
        "Bad Dog Designs, Steve Spencer and Rob Miles Triple Bill!"
      ],
      "start": [
        "2020-03-12T18:00:00Z"
      ],
      "end": [
        "2020-03-12T21:00:00Z"
      ]
    }
  }
}
