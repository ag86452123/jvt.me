{
  "kind": "likes",
  "slug": "2020/03/xdb1s",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1236539462705762304"
      ],
      "url": [
        "https://twitter.com/sigje/status/1236539462705762304"
      ],
      "published": [
        "2020-03-08T06:29:16+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:sigje"
            ],
            "numeric-id": [
              "17527655"
            ],
            "name": [
              "Jennifer Davis"
            ],
            "nickname": [
              "sigje"
            ],
            "url": [
              "https://twitter.com/sigje",
              "http://jennifer.dev",
              "http://pronoun.is/she?or=they"
            ],
            "published": [
              "2008-11-21T00:59:20+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Sparklelandia"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1139283374038667264/ig-OWhGv.jpg"
            ]
          }
        }
      ],
      "content": [
        "It's International Women's Day tomorrow. It's awesome to celebrate folks accomplishments every day! Before you tweet out a list of awesome women to celebrate IWD, check that each and every person is a woman."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-08T16:36:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @sigje's tweet"
    ],
    "like-of": [
      "https://twitter.com/sigje/status/1236539462705762304"
    ],
    "published": [
      "2020-03-08T16:36:00Z"
    ],
    "syndication": [
      "https://twitter.com/sigje/status/1236539462705762304"
    ]
  }
}
