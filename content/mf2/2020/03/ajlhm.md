{
  "kind": "likes",
  "slug": "2020/03/ajlhm",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1234568744568930304"
      ],
      "url": [
        "https://twitter.com/mattcompiles/status/1234568744568930304"
      ],
      "published": [
        "2020-03-02T19:58:20+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:mattcompiles"
            ],
            "numeric-id": [
              "1655361650"
            ],
            "name": [
              "Matt Jones"
            ],
            "nickname": [
              "mattcompiles"
            ],
            "url": [
              "https://twitter.com/mattcompiles"
            ],
            "published": [
              "2013-08-08T13:10:10+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Melbourne, Victoria"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1052807075963625473/xsNER2gi.jpg"
            ]
          }
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ESIRuZuVAAEWMd6.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-03T15:01:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @mattcompiles's tweet"
    ],
    "like-of": [
      "https://twitter.com/mattcompiles/status/1234568744568930304"
    ],
    "published": [
      "2020-03-03T15:01:00Z"
    ],
    "syndication": [
      "https://twitter.com/mattcompiles/status/1234568744568930304"
    ]
  }
}
