{
  "kind": "likes",
  "slug": "2020/03/3az5a",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1238782316492750849"
      ],
      "url": [
        "https://twitter.com/emfcamp/status/1238782316492750849"
      ],
      "published": [
        "2020-03-14T11:01:34+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:emfcamp"
            ],
            "numeric-id": [
              "363056019"
            ],
            "name": [
              "Electromagnetic Field"
            ],
            "nickname": [
              "emfcamp"
            ],
            "url": [
              "https://twitter.com/emfcamp",
              "http://www.emfcamp.org/"
            ],
            "published": [
              "2011-08-27T13:13:37+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Eastnor Castle Deer Park, Herefordshire"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/745287962791936001/WytSNM2z.jpg"
            ]
          }
        }
      ],
      "content": [
        "I think we can confidently say that COVID-19 has not affected the enthusiasm of people to get tickets."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-14T11:37:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @emfcamp's tweet"
    ],
    "like-of": [
      "https://twitter.com/emfcamp/status/1238782316492750849"
    ],
    "published": [
      "2020-03-14T11:37:00Z"
    ],
    "syndication": [
      "https://twitter.com/emfcamp/status/1238782316492750849"
    ]
  }
}
