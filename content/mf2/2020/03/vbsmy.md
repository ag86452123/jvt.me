{
  "kind": "likes",
  "slug": "2020/03/vbsmy",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245053541888245765"
      ],
      "url": [
        "https://twitter.com/lisaironcutter/status/1245053541888245765"
      ],
      "published": [
        "2020-03-31T18:21:11+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:lisaironcutter"
            ],
            "numeric-id": [
              "55498554"
            ],
            "name": [
              "Lisa Tagliaferri"
            ],
            "nickname": [
              "lisaironcutter"
            ],
            "url": [
              "https://twitter.com/lisaironcutter",
              "http://lisatagliaferri.org"
            ],
            "published": [
              "2009-07-10T07:18:38+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1172266573534498816/ZdiGUzaT.jpg"
            ]
          }
        }
      ],
      "content": [
        "Thank you, Google, for this algorithmic honor 🙏"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUdRmI2XkAMLYHT.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-31T20:43:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @lisaironcutter's tweet"
    ],
    "like-of": [
      "https://twitter.com/lisaironcutter/status/1245053541888245765"
    ],
    "published": [
      "2020-03-31T20:43:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/lisaironcutter/status/1245053541888245765"
    ]
  }
}
