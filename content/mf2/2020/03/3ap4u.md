{
  "kind": "likes",
  "slug": "2020/03/3ap4u",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1242851612227440643"
      ],
      "url": [
        "https://twitter.com/ianmiell/status/1242851612227440643"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1242851552139886611/pu/vid/1280x720/y9p12Cx3sZhk1nqf.mp4?tag=10"
      ],
      "published": [
        "2020-03-25T16:31:30+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ianmiell"
            ],
            "numeric-id": [
              "58017706"
            ],
            "name": [
              "Ian Miell"
            ],
            "nickname": [
              "ianmiell"
            ],
            "url": [
              "https://twitter.com/ianmiell",
              "http://zwischenzugs.com"
            ],
            "published": [
              "2009-07-18T19:59:32+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1120618937253793793/xb6_9WMl.jpg"
            ]
          }
        }
      ],
      "content": [
        "I found watching my coffee granules move like celestial objects around my cup strangely relaxing..."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-25T22:11:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ianmiell's tweet"
    ],
    "like-of": [
      "https://twitter.com/ianmiell/status/1242851612227440643"
    ],
    "published": [
      "2020-03-25T22:11:00Z"
    ],
    "syndication": [
      "https://twitter.com/ianmiell/status/1242851612227440643"
    ]
  }
}
