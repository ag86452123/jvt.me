{
  "kind": "likes",
  "slug": "2020/03/xpk5z",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1237880129575157760"
      ],
      "url": [
        "https://twitter.com/amdar1ing/status/1237880129575157760"
      ],
      "published": [
        "2020-03-11T23:16:36+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:amdar1ing"
            ],
            "numeric-id": [
              "2717355787"
            ],
            "name": [
              "Anne Marie Darling"
            ],
            "nickname": [
              "amdar1ing"
            ],
            "url": [
              "https://twitter.com/amdar1ing"
            ],
            "published": [
              "2014-08-08T15:29:08+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1207465184824086528/7TrUkH4H.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Lots of science-y folks are posting this graph. But if there is one thing I have learned from being on the internet, it is this:\n\nData/graphs: Not compelling to many.\n\nKitties: Compelling to many.\n\nSo I present: #Catteningthecurve.\n\n#scicomm #epitwitter",
          "html": "<div style=\"white-space: pre\">Lots of science-y folks are posting this graph. But if there is one thing I have learned from being on the internet, it is this:\n\nData/graphs: Not compelling to many.\n\nKitties: Compelling to many.\n\nSo I present: <a href=\"https://twitter.com/search?q=%23Catteningthecurve\">#Catteningthecurve</a>.\n\n<a href=\"https://twitter.com/search?q=%23scicomm\">#scicomm</a> <a href=\"https://twitter.com/search?q=%23epitwitter\">#epitwitter</a></div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ES3VTrgUMAAyEFp.jpg"
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-03-12T21:08:51.591+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @amdar1ing's tweet"
    ],
    "like-of": [
      "https://twitter.com/amdar1ing/status/1237880129575157760"
    ],
    "published": [
      "2020-03-12T21:08:51.591+01:00"
    ],
    "syndication": [
      "https://twitter.com/amdar1ing/status/1237880129575157760"
    ]
  }
}
