{
  "kind": "replies",
  "slug": "2020/03/ax9oj",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1241719263460810753"
      ],
      "url": [
        "https://twitter.com/penelope_zone/status/1241719263460810753"
      ],
      "published": [
        "2020-03-22T13:31:57+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:penelope_zone"
            ],
            "numeric-id": [
              "44341873"
            ],
            "name": [
              "Penelope Phippen"
            ],
            "nickname": [
              "penelope_zone"
            ],
            "url": [
              "https://twitter.com/penelope_zone",
              "https://penelope.zone"
            ],
            "published": [
              "2009-06-03T11:41:30+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1236061537640632321/oZbkCMit.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Hello cis people this is once again a reminder to put your pronouns in your bio.\n\nReply to this tweet with a screenshot once you've done it :)",
          "html": "<div style=\"white-space: pre\">Hello cis people this is once again a reminder to put your pronouns in your bio.\n\nReply to this tweet with a screenshot once you've done it :)</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-22T14:11:00Z",
  "h": "h-entry",
  "properties": {
    "photo": [
      {
        "photo": "https://media.jvt.me/ewhpz.png",
        "alt": "Jamie's twitter bio, which shows his pronouns as \"he/him\""
      }
    ],
    "name": [
      "Reply to https://twitter.com/penelope_zone/status/1241719263460810753"
    ],
    "in-reply-to": [
      "https://twitter.com/penelope_zone/status/1241719263460810753"
    ],
    "published": [
      "2020-03-22T14:11:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Not only in my bio, but marked up in a machine-parseable way on my website 🙌🏼 https://www.jvt.me/posts/2019/04/10/pronouns-microformats/"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1241730673670488065"
    ]
  }
}
