{
  "kind": "likes",
  "slug": "2020/03/k7ive",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1243081525400424449"
      ],
      "url": [
        "https://twitter.com/IainRowan/status/1243081525400424449"
      ],
      "published": [
        "2020-03-26T07:45:05+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:IainRowan"
            ],
            "numeric-id": [
              "366251112"
            ],
            "name": [
              "Iain Rowan"
            ],
            "nickname": [
              "IainRowan"
            ],
            "url": [
              "https://twitter.com/IainRowan",
              "http://iainrowan.com"
            ],
            "published": [
              "2011-09-01T21:03:37+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Sunderland, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1223576012174372864/_0uL8k7U.jpg"
            ]
          }
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUBQDI7XYAI5VSw.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-27T17:48:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @IainRowan's tweet"
    ],
    "like-of": [
      "https://twitter.com/IainRowan/status/1243081525400424449"
    ],
    "published": [
      "2020-03-27T17:48:00Z"
    ],
    "syndication": [
      "https://twitter.com/IainRowan/status/1243081525400424449"
    ]
  }
}
