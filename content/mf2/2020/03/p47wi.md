{
  "kind": "likes",
  "slug": "2020/03/p47wi",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1242773275324882944"
      ],
      "url": [
        "https://twitter.com/AgileEngPodcast/status/1242773275324882944"
      ],
      "published": [
        "2020-03-25T11:20:13+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:AgileEngPodcast"
            ],
            "numeric-id": [
              "1214329601033482240"
            ],
            "name": [
              "Agile Engineering Podcast"
            ],
            "nickname": [
              "AgileEngPodcast"
            ],
            "url": [
              "https://twitter.com/AgileEngPodcast"
            ],
            "published": [
              "2020-01-06T23:35:26+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1218122865758887936/q_aTO5K0.jpg"
            ]
          }
        }
      ],
      "content": [
        "Tonight we're going to be recording episode 4, with all of our hosts completely remote - our topic is Working effectively as a distributed team!"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-25T11:56:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @AgileEngPodcast's tweet"
    ],
    "like-of": [
      "https://twitter.com/AgileEngPodcast/status/1242773275324882944"
    ],
    "published": [
      "2020-03-25T11:56:00Z"
    ],
    "syndication": [
      "https://twitter.com/AgileEngPodcast/status/1242773275324882944"
    ]
  }
}
