{
  "kind": "replies",
  "slug": "2020/03/xcdrh",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245101234584989696"
      ],
      "url": [
        "https://twitter.com/KatCodes/status/1245101234584989696"
      ],
      "published": [
        "2020-03-31T21:30:41+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1245096155685564416"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:KatCodes"
            ],
            "numeric-id": [
              "342868129"
            ],
            "name": [
              "katie walker"
            ],
            "nickname": [
              "KatCodes"
            ],
            "url": [
              "https://twitter.com/KatCodes",
              "http://kat.codes"
            ],
            "published": [
              "2011-07-26T18:14:50+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1234597964351442947/j3ioCAzu.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Is that a bed sheet?? 😂",
          "html": "Is that a bed sheet?? 😂\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/anna_hax\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-31T22:32:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/KatCodes/status/1245101234584989696"
    ],
    "in-reply-to": [
      "https://twitter.com/KatCodes/status/1245101234584989696"
    ],
    "published": [
      "2020-03-31T22:32:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "It does the job perfectly, okay?? 😝"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1245103005667659779"
    ]
  }
}
