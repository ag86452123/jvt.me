{
  "kind": "likes",
  "slug": "2020/03/2qmsx",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233795764800720896"
      ],
      "url": [
        "https://twitter.com/samsanders/status/1233795764800720896"
      ],
      "published": [
        "2020-02-29T16:46:47+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:samsanders"
            ],
            "numeric-id": [
              "16442379"
            ],
            "name": [
              "Sam Sanders"
            ],
            "nickname": [
              "samsanders"
            ],
            "url": [
              "https://twitter.com/samsanders",
              "https://n.pr/2hir3pj",
              "https://Instagram.com/samsanders/"
            ],
            "published": [
              "2008-09-24T22:35:18+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "L.A. (for the second time)"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/882698164997808128/Mym6RZ3M.jpg"
            ]
          }
        }
      ],
      "content": [
        "I don’t know who needs to hear this, but you don’t have to wait for a global pandemic to wash your hands"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-02T08:07:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @samsanders's tweet"
    ],
    "like-of": [
      "https://twitter.com/samsanders/status/1233795764800720896"
    ],
    "published": [
      "2020-03-02T08:07:00Z"
    ],
    "syndication": [
      "https://twitter.com/samsanders/status/1233795764800720896"
    ]
  }
}
