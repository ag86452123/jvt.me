{
  "kind": "reposts",
  "slug": "2020/03/cgtoo",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1239128475153416193"
      ],
      "url": [
        "https://twitter.com/JessPWhite/status/1239128475153416193"
      ],
      "published": [
        "2020-03-15T09:57:05+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:JessPWhite"
            ],
            "numeric-id": [
              "3021209961"
            ],
            "name": [
              "Jessica White 🦎"
            ],
            "nickname": [
              "JessPWhite"
            ],
            "url": [
              "https://twitter.com/JessPWhite",
              "https://jesswhite.co.uk"
            ],
            "published": [
              "2015-02-06T10:02:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1186021421350375424/lYEBBAc4.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I'm 29. I climb mountains, code, speak publicly in front on big screens & do other stuff.\n\nI also have fistulating Crohn's & AF - I take immunosuppressants & have chosen to isolate.\n\nNot all high risk people look vulnerable 🤘",
          "html": "<div style=\"white-space: pre\">I'm 29. I climb mountains, code, speak publicly in front on big screens &amp; do other stuff.\n\nI also have fistulating Crohn's &amp; AF - I take immunosuppressants &amp; have chosen to isolate.\n\nNot all high risk people look vulnerable 🤘</div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ETJExowXsAEXrr1.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-15T11:44:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-03-15T11:44:00Z"
    ],
    "repost-of": [
      "https://twitter.com/JessPWhite/status/1239128475153416193"
    ],
    "category": [
      "diversity-and-inclusion"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251560333233336326"
    ]
  },
  "tags": [
    "diversity-and-inclusion"
  ]
}
