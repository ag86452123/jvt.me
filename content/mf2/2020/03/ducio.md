{
  "kind": "replies",
  "slug": "2020/03/ducio",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1240568578203623425"
      ],
      "url": [
        "https://twitter.com/biglesp/status/1240568578203623425"
      ],
      "published": [
        "2020-03-19T09:19:32+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/ipedrazas/status/1240565738655662080"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:biglesp"
            ],
            "numeric-id": [
              "17296531"
            ],
            "name": [
              "biglesp"
            ],
            "nickname": [
              "biglesp"
            ],
            "url": [
              "https://twitter.com/biglesp",
              "https://bigl.es",
              "http://ko-fi.com/biglesp"
            ],
            "published": [
              "2008-11-10T22:11:11+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Blackpool, Lancashire"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1237116807430975489/ncnF-53L.png"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Fair shout :) Thanks",
          "html": "Fair shout :) Thanks\n<a class=\"u-mention\" href=\"https://twitter.com/ipedrazas\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/monzo\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-03-19T10:06:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/biglesp/status/1240568578203623425"
    ],
    "in-reply-to": [
      "https://twitter.com/biglesp/status/1240568578203623425"
    ],
    "published": [
      "2020-03-19T10:06:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Yep this is due to the Strong Customer Authentication regulation that is now in effect as of March 14th - you can read more about it at https://monzo.com/blog/2019/08/22/strong-customer-authentication - but it's implemented differently per bank 🤷🏽‍♂️"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1240581575764574214"
    ]
  }
}
