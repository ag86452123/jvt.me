{
  "kind": "rsvps",
  "slug": "2020/01/k8kjz",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-01-14T18:07:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/LevelUp-Notts/events/267902436/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/LevelUp-Notts/events/267902436/"
    ],
    "published": [
      "2020-01-14T18:07:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/LevelUp-Notts/events/267902436/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/LevelUp-Notts/events/267902436/"
      ],
      "name": [
        "\"Where Do I Start?\" - Tips and Tricks: Getting Your First Job in Tech!"
      ],
      "start": [
        "2020-01-30T18:00:00Z"
      ],
      "end": [
        "2020-01-30T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Rebel Recruiters, Huntingdon St"
          ],
          "locality": [
            "Nottinghamshire"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
