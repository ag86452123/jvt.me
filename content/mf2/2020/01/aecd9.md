{
  "kind": "rsvps",
  "slug": "2020/01/aecd9",
  "client_id": "https://micropublish.net",
  "date": "2020-01-27T23:58:28.113+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/NottsJS/events/vjnvhrybcdbpb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/NottsJS/events/vjnvhrybcdbpb/"
    ],
    "published": [
      "2020-01-27T23:58:28.113+01:00"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/NottsJS/events/vjnvhrybcdbpb/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/NottsJS/events/vjnvhrybcdbpb/"
      ],
      "name": [
        "How to talk really good"
      ],
      "start": [
        "2020-02-11T18:00:00Z"
      ],
      "end": [
        "2020-02-11T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Capital One (Europe) plc, Station St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
