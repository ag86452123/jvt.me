{
  "kind": "rsvps",
  "slug": "2020/01/kfogl",
  "client_id": "https://micropublish.net",
  "date": "2020-01-27T23:57:51.753+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/Notts-Dev-Workshop/events/267988124/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Notts-Dev-Workshop/events/267988124/"
    ],
    "published": [
      "2020-01-27T23:57:51.753+01:00"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/Notts-Dev-Workshop/events/267988124/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Notts-Dev-Workshop/events/267988124/"
      ],
      "name": [
        "How to Talk Really Good - A Workshop - Jess White and Moreton Brockley"
      ],
      "start": [
        "2020-02-04T18:30:00Z"
      ],
      "end": [
        "2020-02-04T20:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Rebel Recruitment, Rebel Recruitment, 2nd floor, Blenheim Court"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
