{
  "kind": "rsvps",
  "slug": "2020/01/bbz26",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-01-23T15:43:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://events.indieweb.org/2020/02/homebrew-website-club-nottingham-8IgcYeAQhIKX"
    ],
    "in-reply-to": [
      "https://events.indieweb.org/2020/02/homebrew-website-club-nottingham-8IgcYeAQhIKX"
    ],
    "published": [
      "2020-01-23T15:43:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "event": {
      "url": [
        "https://events.indieweb.org/2020/02/homebrew-website-club-nottingham-8IgcYeAQhIKX"
      ],
      "name": [
        "Homebrew Website Club: Nottingham"
      ],
      "start": [
        "2020-02-05T17:30:00+00:00"
      ],
      "end": [
        "2020-02-05T19:30:00+00:00"
      ],
      "location": {
        "properties": {
          "street-address": [
            "72 Maid Marian Way"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
