{
  "kind": "rsvps",
  "slug": "2020/01/f6q6o",
  "client_id": "https://micropublish.net",
  "date": "2020-01-27T23:58:15.102+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgrybcdbhb/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgrybcdbhb/"
    ],
    "published": [
      "2020-01-27T23:58:15.102+01:00"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgrybcdbhb/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/ministry-of-testing-nottingham/events/jgthgrybcdbhb/"
      ],
      "name": [
        "#NottsTest - BDD Workshop"
      ],
      "start": [
        "2020-02-05T19:00:00Z"
      ],
      "end": [
        "2020-02-05T22:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Experian - The Sir John Peace Building, Experian Way"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
