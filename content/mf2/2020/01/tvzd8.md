{
  "kind": "rsvps",
  "slug": "2020/01/tvzd8",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-01-11T14:00:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/Women-In-Tech-Nottingham/events/267730091/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/267730091/"
    ],
    "published": [
      "2020-01-11T14:00:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "syndication": [
      "https://www.meetup.com/Women-In-Tech-Nottingham/events/267730091/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Women-In-Tech-Nottingham/events/267730091/"
      ],
      "name": [
        "Women in Tech 6th February - What does a Business Psychologist know about Tech?"
      ],
      "start": [
        "2020-02-06T18:30:00Z"
      ],
      "end": [
        "2020-02-06T21:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Antenna, 9A Beck St"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
