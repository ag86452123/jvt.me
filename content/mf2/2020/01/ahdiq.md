{
  "kind": "rsvps",
  "slug": "2020/01/ahdiq",
  "client_id": "https://micropublish.net",
  "date": "2020-01-11T16:00:27.915+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/267831472/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/267831472/"
    ],
    "published": [
      "2020-01-11T16:00:27.915+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "syndication": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/267831472/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/267831472/"
      ],
      "name": [
        "PHPMiNDS January 2019: Shaun Hare - Talk TBC"
      ],
      "start": [
        "2020-01-16T19:00:00Z"
      ],
      "end": [
        "2020-01-16T21:00:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "JH,  34a Stoney Street, Nottingham, NG1 1NB."
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
