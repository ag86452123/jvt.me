{
  "kind": "rsvps",
  "slug": "2020/01/wfuoh",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-01-31T15:20:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/LevelUp-Notts/events/268350680/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/LevelUp-Notts/events/268350680/"
    ],
    "published": [
      "2020-01-31T15:20:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/LevelUp-Notts/events/268350680/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Rebel Recruiters, Huntingdon St, Nottinghamshire, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/LevelUp-Notts/events/268350680/"
      ],
      "name": [
        "\"Portfolios & GitHub...what do?!\" - Coding & Development Workshop"
      ],
      "start": [
        "2020-02-27T18:00:00Z"
      ],
      "end": [
        "2020-02-27T21:00:00Z"
      ]
    }
  }
}
