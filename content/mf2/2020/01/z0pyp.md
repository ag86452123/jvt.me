{
  "kind": "rsvps",
  "slug": "2020/01/z0pyp",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-01-18T21:12:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/Notts-Dev-Workshop/events/267988124/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Notts-Dev-Workshop/events/267988124/"
    ],
    "published": [
      "2020-01-18T21:12:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "content": [
      {
        "html": "",
        "value": "Both Jess and Moreton are great speakers, so this is no doubt going to be a great event full of lots of great tips for being better at public speaking!"
      }
    ],
    "syndication": [
      "https://www.meetup.com/Notts-Dev-Workshop/events/267988124/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "url": [
        "https://www.meetup.com/Notts-Dev-Workshop/events/267988124/"
      ],
      "name": [
        "How to Talk Really Good - A Workshop - Jess White and Moreton Brockley"
      ],
      "start": [
        "2020-02-04T18:30:00Z"
      ],
      "end": [
        "2020-02-04T20:30:00Z"
      ],
      "location": {
        "properties": {
          "street-address": [
            "Rebel Recruitment, Rebel Recruitment, 2nd floor, Blenheim Court"
          ],
          "locality": [
            "Nottingham"
          ],
          "country-name": [
            "United Kingdom"
          ]
        },
        "type": [
          "h-adr"
        ]
      }
    }
  }
}
