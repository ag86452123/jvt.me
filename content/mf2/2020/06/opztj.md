{
  "date" : "2020-06-17T07:37:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.jvt.me/events/personal-events/2020-07-6kvrp/" ],
    "name" : [ "RSVP yes to https://www.jvt.me/events/personal-events/2020-07-6kvrp/" ],
    "published" : [ "2020-06-17T07:37:00+01:00" ],
    "event" : {
      "start" : [ "2020-07-14T18:00:00+0100" ],
      "name" : [ "Escape Room" ],
      "end" : [ "2020-07-14T20:30:00+0100" ],
      "url" : [ "https://www.jvt.me/events/personal-events/2020-07-6kvrp/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/06/opztj",
  "client_id" : "https://indigenous.realize.be"
}
