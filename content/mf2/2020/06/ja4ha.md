{
  "date" : "2020-06-02T20:35:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/technottingham/status/1267863485221928960" ],
    "name" : [ "Like of @technottingham's tweet" ],
    "published" : [ "2020-06-02T20:35:00+01:00" ],
    "like-of" : [ "https://twitter.com/technottingham/status/1267863485221928960" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ja4ha",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1267863485221928960" ],
      "url" : [ "https://twitter.com/technottingham/status/1267863485221928960" ],
      "published" : [ "2020-06-02T16:59:45+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "There is a sickness in our world and we will not be blind to it. People are suffering, people are dying. Just as we do not tolerate hate in our community we will not accept it in the world we live.\n\nWe stand with those who are fighting for justice. #BlackLivesMatter \n\n#TechNott",
        "html" : "<div style=\"white-space: pre\">There is a sickness in our world and we will not be blind to it. People are suffering, people are dying. Just as we do not tolerate hate in our community we will not accept it in the world we live.\n\nWe stand with those who are fighting for justice. <a href=\"https://twitter.com/search?q=%23BlackLivesMatter\">#BlackLivesMatter</a> \n\n<a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EZhZ-iUWoAEl89H.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
