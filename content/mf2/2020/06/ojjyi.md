{
  "date" : "2020-06-15T08:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/_elletownsend/status/1272199152097730561" ],
    "name" : [ "Like of @_elletownsend's tweet" ],
    "published" : [ "2020-06-15T08:21:00+01:00" ],
    "category" : [ "blogging" ],
    "like-of" : [ "https://twitter.com/_elletownsend/status/1272199152097730561" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ojjyi",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272199152097730561" ],
      "url" : [ "https://twitter.com/_elletownsend/status/1272199152097730561" ],
      "published" : [ "2020-06-14T16:08:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:_elletownsend" ],
          "numeric-id" : [ "4308006796" ],
          "name" : [ "Elle Townsend✨" ],
          "nickname" : [ "_elletownsend" ],
          "url" : [ "https://twitter.com/_elletownsend", "http://www.elletownsend.co.uk" ],
          "published" : [ "2015-11-28T14:28:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "England, United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1237757944089006086/67Kjy-LA.jpg" ]
        }
      } ],
      "content" : [ "I want to get a liiiiiittle more frequent with my blog posts, once a month is not frequent enough for all these ideas I have 📝🧐" ]
    }
  },
  "tags" : [ "blogging" ],
  "client_id" : "https://indigenous.realize.be"
}
