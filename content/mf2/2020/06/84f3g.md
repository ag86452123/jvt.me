{
  "date" : "2020-06-21T15:10:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/nytimes/status/1274554545100730368" ],
    "name" : [ "Like of @nytimes's tweet" ],
    "published" : [ "2020-06-21T15:10:00+01:00" ],
    "category" : [ "politics" ],
    "like-of" : [ "https://twitter.com/nytimes/status/1274554545100730368" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/84f3g",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274554545100730368" ],
      "url" : [ "https://twitter.com/nytimes/status/1274554545100730368" ],
      "published" : [ "2020-06-21T04:07:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:nytimes" ],
          "numeric-id" : [ "807095" ],
          "name" : [ "The New York Times" ],
          "nickname" : [ "nytimes" ],
          "url" : [ "https://twitter.com/nytimes", "http://www.nytimes.com/", "http://nyti.ms/2FVHq9v" ],
          "published" : [ "2007-03-02T20:41:42+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New York City" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1098244578472280064/gjkVMelR.png" ]
        }
      } ],
      "content" : [ {
        "value" : "Teenage TikTok users and K-pop fans claimed to have registered potentially hundreds of thousands of tickets for President Trump's campaign rally as a prank to ruin the event. nyti.ms/2YhJG35",
        "html" : "Teenage TikTok users and K-pop fans claimed to have registered potentially hundreds of thousands of tickets for President Trump's campaign rally as a prank to ruin the event. <a href=\"https://nyti.ms/2YhJG35\">nyti.ms/2YhJG35</a>"
      } ]
    }
  },
  "tags" : [ "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
