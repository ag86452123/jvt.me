{
  "date" : "2020-06-09T00:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @King_of_Sams_'s tweet" ],
    "published" : [ "2020-06-09T00:01:00+01:00" ],
    "like-of" : [ "https://twitter.com/King_of_Sams_/status/1270088365266866177" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/hh0yq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1270088365266866177" ],
      "url" : [ "https://twitter.com/King_of_Sams_/status/1270088365266866177" ],
      "published" : [ "2020-06-08T20:20:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:King_of_Sams_" ],
          "numeric-id" : [ "1113309653914537984" ],
          "name" : [ "Samuel King" ],
          "nickname" : [ "King_of_Sams_" ],
          "url" : [ "https://twitter.com/King_of_Sams_" ],
          "published" : [ "2019-04-03T05:18:00+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1158865930929221633/k6qBQ0eA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Two brilliant talks tonight from @Codling and @chatterboxCoder. Thank you as ever to the @technottingham team. Missing Nottingham now more than ever so it's always great to see some familiar faces from the Nottingham tech scene 😊 #technott",
        "html" : "Two brilliant talks tonight from <a href=\"https://twitter.com/Codling\">@Codling</a> and <a href=\"https://twitter.com/chatterboxCoder\">@chatterboxCoder</a>. Thank you as ever to the <a href=\"https://twitter.com/technottingham\">@technottingham</a> team. Missing Nottingham now more than ever so it's always great to see some familiar faces from the Nottingham tech scene 😊 <a href=\"https://twitter.com/search?q=%23technott\">#technott</a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
