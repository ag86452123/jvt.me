{
  "date" : "2020-06-24T08:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/dlitchfield/status/1275590205345423360" ],
    "name" : [ "Like of @dlitchfield's tweet" ],
    "published" : [ "2020-06-24T08:30:00+01:00" ],
    "category" : [ "nature" ],
    "like-of" : [ "https://twitter.com/dlitchfield/status/1275590205345423360" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/y5esj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275590205345423360" ],
      "url" : [ "https://twitter.com/dlitchfield/status/1275590205345423360" ],
      "published" : [ "2020-06-24T00:42:58+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:dlitchfield" ],
          "numeric-id" : [ "117931480" ],
          "name" : [ "David Litchfield" ],
          "nickname" : [ "dlitchfield" ],
          "url" : [ "https://twitter.com/dlitchfield", "http://www.davidlitchfield.com/" ],
          "published" : [ "2010-02-27T02:37:45+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/938337406117281792/qtTAT652.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Lake #tahoe #storm",
        "html" : "Lake <a href=\"https://twitter.com/search?q=%23tahoe\">#tahoe</a> <a href=\"https://twitter.com/search?q=%23storm\">#storm</a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EbPOhlsUYAACiY5.jpg" ]
    }
  },
  "tags" : [ "nature" ],
  "client_id" : "https://indigenous.realize.be"
}
