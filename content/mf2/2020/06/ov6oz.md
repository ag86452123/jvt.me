{
  "date" : "2020-06-01T08:22:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @edent's tweet" ],
    "published" : [ "2020-06-01T08:22:00+01:00" ],
    "like-of" : [ "https://twitter.com/edent/status/1267174459267526656" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ov6oz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1267174459267526656" ],
      "url" : [ "https://twitter.com/edent/status/1267174459267526656" ],
      "published" : [ "2020-05-31T19:21:48+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I have baked some AMAZING chilli chocolate brownies. 100% vegan.\nCan't wait to share them with my team tomorrow.\n\nBy which I mean SCOFF THE LOT MYSELF!",
        "html" : "<div style=\"white-space: pre\">I have baked some AMAZING chilli chocolate brownies. 100% vegan.\nCan't wait to share them with my team tomorrow.\n\nBy which I mean SCOFF THE LOT MYSELF!</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
