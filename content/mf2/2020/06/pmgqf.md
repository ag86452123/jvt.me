{
  "date" : "2020-06-14T17:10:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/HannahAlOthman/status/1272120779501719553" ],
    "name" : [ "Like of @HannahAlOthman's tweet" ],
    "published" : [ "2020-06-14T17:10:00+01:00" ],
    "like-of" : [ "https://twitter.com/HannahAlOthman/status/1272120779501719553" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/pmgqf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272120779501719553" ],
      "url" : [ "https://twitter.com/HannahAlOthman/status/1272120779501719553" ],
      "published" : [ "2020-06-14T10:56:43+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:HannahAlOthman" ],
          "numeric-id" : [ "1467580478" ],
          "name" : [ "Hannah Al-Othman" ],
          "nickname" : [ "HannahAlOthman" ],
          "url" : [ "https://twitter.com/HannahAlOthman" ],
          "published" : [ "2013-05-29T15:39:21+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London via Manchester" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/763679542645026816/i8SYZEpC.jpg" ]
        }
      } ],
      "content" : [ "What a hill to die on" ],
      "photo" : [ "https://pbs.twimg.com/media/Ead7GtSXYAAB8mH.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
