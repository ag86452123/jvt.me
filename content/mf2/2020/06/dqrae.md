{
  "date" : "2020-06-18T19:07:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/dev_nikema/status/1273601934948089856" ],
    "name" : [ "Like of @dev_nikema's tweet" ],
    "published" : [ "2020-06-18T19:07:00+01:00" ],
    "like-of" : [ "https://twitter.com/dev_nikema/status/1273601934948089856" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/dqrae",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1273601934948089856" ],
      "url" : [ "https://twitter.com/dev_nikema/status/1273601934948089856" ],
      "published" : [ "2020-06-18T13:02:18+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:dev_nikema" ],
          "numeric-id" : [ "744302714226544640" ],
          "name" : [ "Nikema Prophet ❤️✨" ],
          "nickname" : [ "dev_nikema" ],
          "url" : [ "https://twitter.com/dev_nikema", "https://opencollective.com/zinc-community#section-contribute" ],
          "published" : [ "2016-06-18T22:56:10+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "California, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1251319075579424768/ZTcWcIpF.jpg" ]
        }
      } ],
      "content" : [ "PSA: Tech jobs are jobs. You shouldn’t have to become a celebrity, join a cult, or crawl through miles of metaphorical glass to land a technical role. Software engineers, sorry to tell you all, but you’re not special. You learned skills to do a job." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
