{
  "date" : "2020-06-18T19:18:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/iknowdavehouse/status/1273646134230159365" ],
    "name" : [ "Like of @iknowdavehouse's tweet" ],
    "published" : [ "2020-06-18T19:18:00+01:00" ],
    "like-of" : [ "https://twitter.com/iknowdavehouse/status/1273646134230159365" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/vzfdu",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1273646134230159365" ],
      "url" : [ "https://twitter.com/iknowdavehouse/status/1273646134230159365" ],
      "published" : [ "2020-06-18T15:57:56+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:iknowdavehouse" ],
          "numeric-id" : [ "18898108" ],
          "name" : [ "Dave House" ],
          "nickname" : [ "iknowdavehouse" ],
          "url" : [ "https://twitter.com/iknowdavehouse", "https://www.das.house/", "http://GOV.UK" ],
          "published" : [ "2009-01-12T11:21:02+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Staines-upon-Thames" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/378800000709128951/16223f6eba491bd20a2b13c376e37fbe.jpeg" ]
        }
      } ],
      "content" : [ "Today in a meeting I said “if your product is a potato then your MVP should be a small potato not half a big potato” - is this thought leadership?" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
