{
  "date" : "2020-06-06T16:40:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1269294969422000128" ],
    "published" : [ "2020-06-06T16:40:00+01:00" ],
    "repost-of" : [ "https://twitter.com/karenkho/status/1269105868836126721" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/qf6co",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1269105868836126721" ],
      "url" : [ "https://twitter.com/karenkho/status/1269105868836126721" ],
      "published" : [ "2020-06-06T03:16:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:karenkho" ],
          "numeric-id" : [ "17768442" ],
          "name" : [ "Karen K. Ho" ],
          "nickname" : [ "karenkho" ],
          "url" : [ "https://twitter.com/karenkho", "http://www.karenho.ca", "http://karenkho.substack.com" ],
          "published" : [ "2008-12-01T01:52:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "NYC + Toronto" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1158750162203885568/EK8zVc6q.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "hey, are you doomscrolling? maybe you're writing and deleting a bunch of tweets? \n\nIt's been a long week. You and your mental health deserve some high-quality rest and time away from screens.",
        "html" : "<div style=\"white-space: pre\">hey, are you doomscrolling? maybe you're writing and deleting a bunch of tweets? \n\nIt's been a long week. You and your mental health deserve some high-quality rest and time away from screens.</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
