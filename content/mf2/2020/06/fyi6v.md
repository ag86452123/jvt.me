{
  "date" : "2020-06-09T11:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1270264667647234048" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-06-09T11:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1270264667647234048" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/fyi6v",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1270264667647234048" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1270264667647234048" ],
      "published" : [ "2020-06-09T08:01:11+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1268578361103826947/LOaufScI.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Happy birthday @MrAndrew 🎉 \n\n37 years old and some things never change: still loves a shirt and jumper combo; still loves Lego; still loves an audience 😂",
        "html" : "<div style=\"white-space: pre\">Happy birthday <a href=\"https://twitter.com/MrAndrew\">@MrAndrew</a> 🎉 \n\n37 years old and some things never change: still loves a shirt and jumper combo; still loves Lego; still loves an audience 😂</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EaDi-tmWoAEpcMF.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
