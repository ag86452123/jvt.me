{
  "date" : "2020-06-24T21:39:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1275894695965724674" ],
    "published" : [ "2020-06-24T21:39:00+01:00" ],
    "repost-of" : [ "https://twitter.com/biscuitsgod/status/1275390201699991552" ],
    "category" : [ "coronavirus" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/tlucf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275390201699991552" ],
      "url" : [ "https://twitter.com/biscuitsgod/status/1275390201699991552" ],
      "published" : [ "2020-06-23T11:28:14+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:biscuitsgod" ],
          "numeric-id" : [ "262649346" ],
          "name" : [ "Ian Harris" ],
          "nickname" : [ "biscuitsgod" ],
          "url" : [ "https://twitter.com/biscuitsgod" ],
          "published" : [ "2011-03-08T13:34:59+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Manchester" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1052934230186123264/hfXDdusq.jpg" ]
        }
      } ],
      "content" : [ "When the pubs re-open, it doesn't mean the virus has gone away.  It just means they have room for you in intensive care." ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
