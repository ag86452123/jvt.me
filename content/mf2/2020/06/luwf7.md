{
  "date" : "2020-06-24T11:15:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/NedHartley/status/1275450377983291398" ],
    "name" : [ "Like of @NedHartley's tweet" ],
    "published" : [ "2020-06-24T11:15:00+01:00" ],
    "category" : [ "coronavirus" ],
    "like-of" : [ "https://twitter.com/NedHartley/status/1275450377983291398" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/luwf7",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275450377983291398" ],
      "url" : [ "https://twitter.com/NedHartley/status/1275450377983291398" ],
      "published" : [ "2020-06-23T15:27:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:NedHartley" ],
          "numeric-id" : [ "21085906" ],
          "name" : [ "Ned Hartley" ],
          "nickname" : [ "NedHartley" ],
          "url" : [ "https://twitter.com/NedHartley", "http://www.nedhartley.com" ],
          "published" : [ "2009-02-17T12:52:58+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1204380956670545920/FIyzNdTk.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EbNPWnpU8AA-7ut.jpg" ]
    }
  },
  "tags" : [ "coronavirus" ],
  "client_id" : "https://indigenous.realize.be"
}
