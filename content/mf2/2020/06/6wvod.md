{
  "date" : "2020-06-05T20:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1268995600273616896" ],
    "published" : [ "2020-06-05T20:52:00+01:00" ],
    "repost-of" : [ "https://twitter.com/NadiaInDC/status/1268915975199621120" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/6wvod",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268915975199621120" ],
      "url" : [ "https://twitter.com/NadiaInDC/status/1268915975199621120" ],
      "published" : [ "2020-06-05T14:41:58+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:NadiaInDC" ],
          "numeric-id" : [ "1528695163" ],
          "name" : [ "Nadia N. Aziz" ],
          "nickname" : [ "NadiaInDC" ],
          "url" : [ "https://twitter.com/NadiaInDC" ],
          "published" : [ "2013-06-18T19:47:40+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1042863213241663488/naQ1CnRm.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Black lives matter mural in front of the White House. ❤️\n\n#BlackLivesMatter #BlackLivesMatterDC \n#dcprotest \n#dcmural",
        "html" : "<div style=\"white-space: pre\">Black lives matter mural in front of the White House. ❤️\n\n<a href=\"https://twitter.com/search?q=%23BlackLivesMatter\">#BlackLivesMatter</a> <a href=\"https://twitter.com/search?q=%23BlackLivesMatterDC\">#BlackLivesMatterDC</a> \n<a href=\"https://twitter.com/search?q=%23dcprotest\">#dcprotest</a> \n<a href=\"https://twitter.com/search?q=%23dcmural\">#dcmural</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EZwYWIjXsAIrahY.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
