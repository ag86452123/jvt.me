{
  "date" : "2020-06-20T12:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/StephenMangan/status/1274262858142973953" ],
    "name" : [ "Like of @StephenMangan's tweet" ],
    "published" : [ "2020-06-20T12:11:00+01:00" ],
    "like-of" : [ "https://twitter.com/StephenMangan/status/1274262858142973953" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/sfpir",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274262858142973953" ],
      "url" : [ "https://twitter.com/StephenMangan/status/1274262858142973953" ],
      "published" : [ "2020-06-20T08:48:34+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:StephenMangan" ],
          "numeric-id" : [ "20136892" ],
          "name" : [ "Stephen Mangan" ],
          "nickname" : [ "StephenMangan" ],
          "url" : [ "https://twitter.com/StephenMangan" ],
          "published" : [ "2009-02-05T10:47:17+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/807377623437475840/LKiNctCU.jpg" ]
        }
      } ],
      "content" : [ "Today is the longest day. All the others this year have just felt like it." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
