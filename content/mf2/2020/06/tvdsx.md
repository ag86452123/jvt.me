{
  "date" : "2020-06-23T17:06:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CaseyNewton/status/1275177758188949504" ],
    "name" : [ "Like of @CaseyNewton's tweet" ],
    "published" : [ "2020-06-23T17:06:00+01:00" ],
    "category" : [ "apple", "ethics" ],
    "like-of" : [ "https://twitter.com/CaseyNewton/status/1275177758188949504" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/tvdsx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275177758188949504" ],
      "url" : [ "https://twitter.com/CaseyNewton/status/1275177758188949504" ],
      "published" : [ "2020-06-22T21:24:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CaseyNewton" ],
          "numeric-id" : [ "69426451" ],
          "name" : [ "Casey Newton" ],
          "nickname" : [ "CaseyNewton" ],
          "url" : [ "https://twitter.com/CaseyNewton", "https://www.theverge.com/theinterface", "http://bit.ly/2yTbZcK" ],
          "published" : [ "2009-08-27T22:37:09+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1053792942966497280/BOEYtrXe.jpg" ]
        }
      } ],
      "content" : [ "If Apple Watch can detect hand washing now then it can probably detect other activities involving vigorous hand motions and I for one would like to know what Apple is doing with the data" ]
    }
  },
  "tags" : [ "apple", "ethics" ],
  "client_id" : "https://indigenous.realize.be"
}
