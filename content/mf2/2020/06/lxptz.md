{
  "date" : "2020-06-07T23:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ACLUMN/status/1269738051498106880" ],
    "name" : [ "Like of @ACLUMN's tweet" ],
    "published" : [ "2020-06-07T23:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/ACLUMN/status/1269738051498106880" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/lxptz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1269738051498106880" ],
      "url" : [ "https://twitter.com/ACLUMN/status/1269738051498106880" ],
      "published" : [ "2020-06-07T21:08:36+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ACLUMN" ],
          "numeric-id" : [ "117194908" ],
          "name" : [ "ACLU of Minnesota" ],
          "nickname" : [ "ACLUMN" ],
          "url" : [ "https://twitter.com/ACLUMN", "http://www.aclu-mn.org" ],
          "published" : [ "2010-02-24T21:06:12+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Minnesota" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1100822985651425280/FgMP8k6f.png" ]
        }
      } ],
      "content" : [ "BREAKING: Minneapolis City Council members have announced their intent to disband the Minneapolis Police Department and invest in community-led public safety." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
