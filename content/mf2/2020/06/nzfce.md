{
  "date" : "2020-06-19T23:19:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Like of @etiene_d's tweet" ],
    "published" : [ "2020-06-19T23:19:00+01:00" ],
    "like-of" : [ "https://twitter.com/etiene_d/status/1274081741154725888" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/nzfce",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274081741154725888" ],
      "url" : [ "https://twitter.com/etiene_d/status/1274081741154725888" ],
      "published" : [ "2020-06-19T20:48:53+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:etiene_d" ],
          "numeric-id" : [ "356299886" ],
          "name" : [ "Etiene Dalcol" ],
          "nickname" : [ "etiene_d" ],
          "url" : [ "https://twitter.com/etiene_d", "http://etiene.net" ],
          "published" : [ "2011-08-16T17:00:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Brazil, Living in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/861576776606208001/e0Rc-qd-.jpg" ]
        }
      } ],
      "content" : [ "TIL mandarin for penguin is \"business goose\" and my day just got 200% better" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
