{
  "date" : "2020-06-13T14:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kvlly/status/1271611161465827329" ],
    "name" : [ "Like of @kvlly's tweet" ],
    "published" : [ "2020-06-13T14:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/kvlly/status/1271611161465827329" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/qqru7",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271611161465827329" ],
      "url" : [ "https://twitter.com/kvlly/status/1271611161465827329" ],
      "published" : [ "2020-06-13T01:11:40+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kvlly" ],
          "numeric-id" : [ "123543103" ],
          "name" : [ "Kelly Vaughn 🐞" ],
          "nickname" : [ "kvlly" ],
          "url" : [ "https://twitter.com/kvlly", "http://kvlly.com", "http://startfreelancing.today" ],
          "published" : [ "2010-03-16T12:15:39+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Atlanta, GA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1264588340747255808/M26TcGUw.jpg" ]
        }
      } ],
      "content" : [ "New hobbies include sneaking up to my friends’ homes, staring into their doorbell camera while they’re not home, and leaving" ],
      "photo" : [ "https://pbs.twimg.com/media/EaWrm2UWkAYgheK.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
