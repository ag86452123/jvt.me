{
  "date" : "2020-06-03T23:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/dhh/status/1268238202336206848" ],
    "name" : [ "Like of @dhh's tweet" ],
    "published" : [ "2020-06-03T23:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/dhh/status/1268238202336206848" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/dqnta",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268238202336206848" ],
      "url" : [ "https://twitter.com/dhh/status/1268238202336206848" ],
      "published" : [ "2020-06-03T17:48:44+00:00" ],
      "in-reply-to" : [ "https://twitter.com/camkidman/status/1268237867597230081" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:dhh" ],
          "numeric-id" : [ "14561327" ],
          "name" : [ "DHH" ],
          "nickname" : [ "dhh" ],
          "url" : [ "https://twitter.com/dhh", "https://dhh.dk" ],
          "published" : [ "2008-04-27T20:19:25+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/975876868455809024/eK7mDppU.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "We do. Will be open-sourcing soon. console1984. Great work by @jorgemanru ✌️",
        "html" : "We do. Will be open-sourcing soon. console1984. Great work by <a href=\"https://twitter.com/jorgemanru\">@jorgemanru</a> ✌️\n<a class=\"u-mention\" href=\"https://twitter.com/camkidman\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
