{
  "date" : "2020-06-06T16:37:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/dhh/status/1269050808215015425" ],
    "name" : [ "Like of @dhh's tweet" ],
    "published" : [ "2020-06-06T16:37:00+01:00" ],
    "like-of" : [ "https://twitter.com/dhh/status/1269050808215015425" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/tjljw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1269050808215015425" ],
      "url" : [ "https://twitter.com/dhh/status/1269050808215015425" ],
      "published" : [ "2020-06-05T23:37:45+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:dhh" ],
          "numeric-id" : [ "14561327" ],
          "name" : [ "DHH" ],
          "nickname" : [ "dhh" ],
          "url" : [ "https://twitter.com/dhh", "https://dhh.dk" ],
          "published" : [ "2008-04-27T20:19:25+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/975876868455809024/eK7mDppU.jpg" ]
        }
      } ],
      "content" : [ "If reality was a fictional show, we'd be rolling our eyes at writers who couldn't wrap up a single story line before kicking off the next." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
