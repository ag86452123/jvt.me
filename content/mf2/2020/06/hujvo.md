{
  "date" : "2020-06-13T22:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TwitchSensei/status/1271834980134596613" ],
    "name" : [ "Like of @TwitchSensei's tweet" ],
    "published" : [ "2020-06-13T22:59:00+01:00" ],
    "category" : [ "battlestations" ],
    "like-of" : [ "https://twitter.com/TwitchSensei/status/1271834980134596613" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/hujvo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271834980134596613" ],
      "url" : [ "https://twitter.com/TwitchSensei/status/1271834980134596613" ],
      "published" : [ "2020-06-13T16:01:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TwitchSensei" ],
          "numeric-id" : [ "2545233194" ],
          "name" : [ "SenseiCJ 🥋⛩" ],
          "nickname" : [ "TwitchSensei" ],
          "url" : [ "https://twitter.com/TwitchSensei", "http://twitch.tv/senseicj" ],
          "published" : [ "2014-06-04T04:27:56+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1268825349594460161/UK6vnAVm.jpg" ]
        }
      } ],
      "content" : [ "Every Sensei must be comfortable in their Dojo. This revamp turns things around. First time using monitor mounts and I highly suggest them!" ],
      "photo" : [ "https://pbs.twimg.com/media/EaZ3K8VWsAEiWD0.jpg", "https://pbs.twimg.com/media/EaZ3K8YWsAIxtY2.jpg", "https://pbs.twimg.com/media/EaZ3K8XXgAI4X1_.jpg", "https://pbs.twimg.com/media/EaZ3K8YXgAIWLNp.jpg" ]
    }
  },
  "tags" : [ "battlestations" ],
  "client_id" : "https://indigenous.realize.be"
}
