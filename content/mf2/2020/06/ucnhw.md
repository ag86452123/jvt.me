{
  "date" : "2020-06-11T20:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/MrAndrew/status/1271154213846990848" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1271159624465625090" ],
    "name" : [ "Reply to https://twitter.com/MrAndrew/status/1271154213846990848" ],
    "published" : [ "2020-06-11T20:14:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Not yet, but it did arrive today! Will let you know what the first night was like ☺"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/ucnhw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271154213846990848" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1271154213846990848" ],
      "published" : [ "2020-06-11T18:55:56+00:00" ],
      "in-reply-to" : [ "https://twitter.com/johnwithbeard/status/1271133445960466433" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1258160750641606657/CR5g9eOt.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "It was @JamieTanna asking about it, I believe he went with an Eve mattress. Presumably he wrote a blog post about it",
        "html" : "It was <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a> asking about it, I believe he went with an Eve mattress. Presumably he wrote a blog post about it\n<a class=\"u-mention\" href=\"https://twitter.com/bagwaa\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/johnwithbeard\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
