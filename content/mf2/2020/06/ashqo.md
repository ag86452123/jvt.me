{
  "date" : "2020-06-19T22:25:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/SoSplush/status/1274080527104999424" ],
    "name" : [ "Like of @SoSplush's tweet" ],
    "published" : [ "2020-06-19T22:25:00+01:00" ],
    "like-of" : [ "https://twitter.com/SoSplush/status/1274080527104999424" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ashqo",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274080527104999424" ],
      "url" : [ "https://twitter.com/SoSplush/status/1274080527104999424" ],
      "published" : [ "2020-06-19T20:44:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:SoSplush" ],
          "numeric-id" : [ "719007619596292097" ],
          "name" : [ "Kelly Mahoney 🍑 Commissions OPEN 💚" ],
          "nickname" : [ "SoSplush" ],
          "url" : [ "https://twitter.com/SoSplush", "http://sosplush.com" ],
          "published" : [ "2016-04-10T03:42:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "St Petersburg, FL" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1269996172707586048/g28ncRZy.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Raise your hand if you're going through burnout right now. \n\n🙋‍♀️",
        "html" : "<div style=\"white-space: pre\">Raise your hand if you're going through burnout right now. \n\n🙋‍♀️</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
