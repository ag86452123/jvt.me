{
  "date" : "2020-06-08T14:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rwdrich/status/1269933109841219584" ],
    "name" : [ "Like of @rwdrich's tweet" ],
    "published" : [ "2020-06-08T14:17:00+01:00" ],
    "like-of" : [ "https://twitter.com/rwdrich/status/1269933109841219584" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/568ev",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1269933109841219584" ],
      "url" : [ "https://twitter.com/rwdrich/status/1269933109841219584" ],
      "published" : [ "2020-06-08T10:03:42+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ "Absolutely love nagging people to review my pull requests. Definitely doesn't make me feel like the work that I'm doing is a waste of time or anything" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
