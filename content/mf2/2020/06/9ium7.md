{
  "date" : "2020-06-16T17:07:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/PenelopeHindle/status/1272858130163081216" ],
    "name" : [ "Like of @PenelopeHindle's tweet" ],
    "published" : [ "2020-06-16T17:07:00+01:00" ],
    "like-of" : [ "https://twitter.com/PenelopeHindle/status/1272858130163081216" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/9ium7",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272858130163081216" ],
      "url" : [ "https://twitter.com/PenelopeHindle/status/1272858130163081216" ],
      "published" : [ "2020-06-16T11:46:41+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:PenelopeHindle" ],
          "numeric-id" : [ "1027853484400562177" ],
          "name" : [ "Penny Hindle" ],
          "nickname" : [ "PenelopeHindle" ],
          "url" : [ "https://twitter.com/PenelopeHindle", "http://instagram.com/penny_hindle" ],
          "published" : [ "2018-08-10T09:45:42+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Leeds, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1250009753511702528/YlHPHLnJ.jpg" ]
        }
      } ],
      "content" : [ "I had free school meals all my life, along with several bursaries and grants throughout college and uni. If it wasn’t for that support then I wouldn’t be in the position I am today - going to earn enough to pay back in tax more than I ever took" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
