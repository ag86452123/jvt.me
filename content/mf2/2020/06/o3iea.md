{
  "date" : "2020-06-04T21:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/dhh/status/1268637186221461504" ],
    "name" : [ "Like of @dhh's tweet" ],
    "published" : [ "2020-06-04T21:51:00+01:00" ],
    "like-of" : [ "https://twitter.com/dhh/status/1268637186221461504" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/o3iea",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268637186221461504" ],
      "url" : [ "https://twitter.com/dhh/status/1268637186221461504" ],
      "published" : [ "2020-06-04T20:14:09+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:dhh" ],
          "numeric-id" : [ "14561327" ],
          "name" : [ "DHH" ],
          "nickname" : [ "dhh" ],
          "url" : [ "https://twitter.com/dhh", "https://dhh.dk" ],
          "published" : [ "2008-04-27T20:19:25+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/975876868455809024/eK7mDppU.jpg" ]
        }
      } ],
      "content" : [ "It continues to blow my mind that \"it's just a few bad apples\" is being used as an excuse to do nothing, when the proverb is literally \"a few bad apples SPOILS THE BUNCH\". The moral lesson is not about the existence of bad apples, but that failure to remove them SPOILS THE BUNCH." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
