{
  "date" : "2020-06-05T18:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/QuinnyPig/status/1268725477721509890" ],
    "name" : [ "Like of @QuinnyPig's tweet" ],
    "published" : [ "2020-06-05T18:21:00+01:00" ],
    "category" : [ "linux" ],
    "like-of" : [ "https://twitter.com/QuinnyPig/status/1268725477721509890" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/gpafm",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268725477721509890" ],
      "url" : [ "https://twitter.com/QuinnyPig/status/1268725477721509890" ],
      "published" : [ "2020-06-05T02:05:00+00:00" ],
      "in-reply-to" : [ "https://twitter.com/PaintYourDragon/status/1268680517672558592" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:QuinnyPig" ],
          "numeric-id" : [ "97114171" ],
          "name" : [ "HydroxyCoreyQuinn" ],
          "nickname" : [ "QuinnyPig" ],
          "url" : [ "https://twitter.com/QuinnyPig", "http://www.duckbillgroup.com", "http://lastweekinaws.com", "http://screaminginthecloud.com" ],
          "published" : [ "2009-12-16T02:19:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1247254348498743300/vTTJS5wW.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Linux made it to orbit before it made it to the desktop.",
        "html" : "Linux made it to orbit before it made it to the desktop.\n<a class=\"u-mention\" href=\"https://twitter.com/PaintYourDragon\"></a>"
      } ]
    }
  },
  "tags" : [ "linux" ],
  "client_id" : "https://indigenous.realize.be"
}
