{
  "date" : "2020-06-19T21:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/dylanp__/status/1274078133730377733" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1275769599968059394" ],
    "name" : [ "Reply to https://twitter.com/dylanp__/status/1274078133730377733" ],
    "published" : [ "2020-06-19T21:59:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Massive congrats Dylan 🎉"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/rx8zg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1274078133730377733" ],
      "url" : [ "https://twitter.com/dylanp__/status/1274078133730377733" ],
      "published" : [ "2020-06-19T20:34:32+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:dylanp__" ],
          "numeric-id" : [ "3217620251" ],
          "name" : [ "Dylan Phelps" ],
          "nickname" : [ "dylanp__" ],
          "url" : [ "https://twitter.com/dylanp__" ],
          "published" : [ "2015-04-28T15:39:37+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1111036895272804352/qQ2Mo1V2.jpg" ]
        }
      } ],
      "content" : [ "... somehow I've managed to go from a 2.ii last year, to a 1st this year! I was definitely not expecting it, but so happy, despite being at home, that this is how my time in Cambridge ends!" ],
      "photo" : [ "https://pbs.twimg.com/media/Ea5ujq1XsAE8pv9.png" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
