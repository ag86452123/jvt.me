{
  "date" : "2020-06-22T22:16:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TashJNorris/status/1275171729862733830" ],
    "name" : [ "Like of @TashJNorris's tweet" ],
    "published" : [ "2020-06-22T22:16:00+01:00" ],
    "category" : [ "diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/TashJNorris/status/1275171729862733830" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/vjcgk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275171729862733830" ],
      "url" : [ "https://twitter.com/TashJNorris/status/1275171729862733830" ],
      "published" : [ "2020-06-22T21:00:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TashJNorris" ],
          "numeric-id" : [ "19965271" ],
          "name" : [ "Tash Norris" ],
          "nickname" : [ "TashJNorris" ],
          "url" : [ "https://twitter.com/TashJNorris", "https://medium.com/@tashjnorris" ],
          "published" : [ "2009-02-03T11:25:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1084014099510558720/_2lwD_ZS.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "FYI. #lovewhoever #pride",
        "html" : "FYI. <a href=\"https://twitter.com/search?q=%23lovewhoever\">#lovewhoever</a> <a href=\"https://twitter.com/search?q=%23pride\">#pride</a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EbJR6uNXQAUeNcT.jpg" ]
    }
  },
  "tags" : [ "diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
