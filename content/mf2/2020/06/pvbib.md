{
  "date" : "2020-06-15T07:48:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/shanselman/status/1272377809109790720" ],
    "name" : [ "Like of @shanselman's tweet" ],
    "published" : [ "2020-06-15T07:48:00+01:00" ],
    "category" : [ "artemis-fowl" ],
    "like-of" : [ "https://twitter.com/shanselman/status/1272377809109790720" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/pvbib",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272377809109790720" ],
      "url" : [ "https://twitter.com/shanselman/status/1272377809109790720" ],
      "published" : [ "2020-06-15T03:58:03+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:shanselman" ],
          "numeric-id" : [ "5676102" ],
          "name" : [ "Scott Hanselman" ],
          "nickname" : [ "shanselman" ],
          "url" : [ "https://twitter.com/shanselman", "http://hanselman.com" ],
          "published" : [ "2007-05-01T05:55:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Portland, Oregon" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1133122333290291200/xV9gO-D6.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Watching #artemisfowl. This is aggressively bad. No one involved in this movie read the book. This is painful and sad and a waste of money. Worst Spy Kids movie in memory.",
        "html" : "Watching <a href=\"https://twitter.com/search?q=%23artemisfowl\">#artemisfowl</a>. This is aggressively bad. No one involved in this movie read the book. This is painful and sad and a waste of money. Worst Spy Kids movie in memory."
      } ]
    }
  },
  "tags" : [ "artemis-fowl" ],
  "client_id" : "https://indigenous.realize.be"
}
