{
  "date" : "2020-06-07T14:03:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1269601431067246594" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-06-07T14:03:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1269601431067246594" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/mpprq",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1269601431067246594" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1269601431067246594" ],
      "published" : [ "2020-06-07T12:05:43+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Yes Nottingham 💛✊ #BlackLivesMatter",
        "html" : "Yes Nottingham 💛✊ <a href=\"https://twitter.com/search?q=%23BlackLivesMatter\">#BlackLivesMatter</a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EZ6HmMeXYAIxiJt.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
