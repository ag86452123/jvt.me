{
  "date" : "2020-06-24T11:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/DevopsNotts/status/1275727997815926786" ],
    "name" : [ "Like of @DevopsNotts's tweet" ],
    "published" : [ "2020-06-24T11:11:00+01:00" ],
    "category" : [ "devops-notts" ],
    "like-of" : [ "https://twitter.com/DevopsNotts/status/1275727997815926786" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/arw23",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275727997815926786" ],
      "url" : [ "https://twitter.com/DevopsNotts/status/1275727997815926786" ],
      "published" : [ "2020-06-24T09:50:31+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:DevopsNotts" ],
          "numeric-id" : [ "1141066068032741376" ],
          "name" : [ "DevOps Notts" ],
          "nickname" : [ "DevopsNotts" ],
          "url" : [ "https://twitter.com/DevopsNotts" ],
          "published" : [ "2019-06-18T19:32:05+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1148214665882849281/l7RvVFSV.png" ]
        }
      } ],
      "content" : [ {
        "value" : "📢 LESS THAN A WEEK TO GO📢\n\nNext Tuesday, we've a special edition of #DevOpsNotts to discuss and celebrate diversity in tech 🙌🎉\n\nThe line up; \n@anna_hax @CarolSaysThings @MicaSophieBell @balhayre @HannahMulligan @NinaSwanwick @short_louise \n\nRSVP; meetu.ps/e/J2xD5/jRrk8/d",
        "html" : "<div style=\"white-space: pre\">📢 LESS THAN A WEEK TO GO📢\n\nNext Tuesday, we've a special edition of <a href=\"https://twitter.com/search?q=%23DevOpsNotts\">#DevOpsNotts</a> to discuss and celebrate diversity in tech 🙌🎉\n\nThe line up; \n<a href=\"https://twitter.com/anna_hax\">@anna_hax</a> <a href=\"https://twitter.com/CarolSaysThings\">@CarolSaysThings</a> <a href=\"https://twitter.com/MicaSophieBell\">@MicaSophieBell</a> <a href=\"https://twitter.com/balhayre\">@balhayre</a> <a href=\"https://twitter.com/HannahMulligan\">@HannahMulligan</a> <a href=\"https://twitter.com/NinaSwanwick\">@NinaSwanwick</a> <a href=\"https://twitter.com/short_louise\">@short_louise</a> \n\nRSVP; <a href=\"http://meetu.ps/e/J2xD5/jRrk8/d\">meetu.ps/e/J2xD5/jRrk8/d</a></div>"
      } ]
    }
  },
  "tags" : [ "devops-notts" ],
  "client_id" : "https://indigenous.realize.be"
}
