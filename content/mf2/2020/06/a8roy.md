{
  "date" : "2020-06-10T11:02:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/lucewhitley/status/1270439826035179521" ],
    "name" : [ "Like of @lucewhitley's tweet" ],
    "published" : [ "2020-06-10T11:02:00+01:00" ],
    "like-of" : [ "https://twitter.com/lucewhitley/status/1270439826035179521" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/a8roy",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1270439826035179521" ],
      "url" : [ "https://twitter.com/lucewhitley/status/1270439826035179521" ],
      "published" : [ "2020-06-09T19:37:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:lucewhitley" ],
          "numeric-id" : [ "43072680" ],
          "name" : [ "Lu" ],
          "nickname" : [ "lucewhitley" ],
          "url" : [ "https://twitter.com/lucewhitley" ],
          "published" : [ "2009-05-28T08:22:00+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1270498040068128768/r3B44NH7.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Got a parcel today, the delivery man asked my surname. I told him Whitley, and he said he had a parcel for my husband...?! I told him I don’t have one and he said ‘oh it’s adressed to Dr Whitley’. Told him that was me!?! #everydaysexism",
        "html" : "Got a parcel today, the delivery man asked my surname. I told him Whitley, and he said he had a parcel for my husband...?! I told him I don’t have one and he said ‘oh it’s adressed to Dr Whitley’. Told him that was me!?! <a href=\"https://twitter.com/search?q=%23everydaysexism\">#everydaysexism</a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
