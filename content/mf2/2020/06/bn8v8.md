{
  "date" : "2020-06-11T17:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/QuinnyPig/status/1271103421857193984" ],
    "name" : [ "Like of @QuinnyPig's tweet" ],
    "published" : [ "2020-06-11T17:42:00+01:00" ],
    "like-of" : [ "https://twitter.com/QuinnyPig/status/1271103421857193984" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/bn8v8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1271103421857193984" ],
      "url" : [ "https://twitter.com/QuinnyPig/status/1271103421857193984" ],
      "published" : [ "2020-06-11T15:34:06+00:00" ],
      "in-reply-to" : [ "https://twitter.com/QuinnyPig/status/1271102882012512259" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:QuinnyPig" ],
          "numeric-id" : [ "97114171" ],
          "name" : [ "HydroxyCoreyQuinn" ],
          "nickname" : [ "QuinnyPig" ],
          "url" : [ "https://twitter.com/QuinnyPig", "http://www.duckbillgroup.com", "http://lastweekinaws.com", "http://screaminginthecloud.com" ],
          "published" : [ "2009-12-16T02:19:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1247254348498743300/vTTJS5wW.jpg" ]
        }
      } ],
      "content" : [ "“If I accept a job offer here, what will make me regret it the most?”" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
