{
  "date" : "2020-06-22T21:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/moriahmaney/status/1275124476821323776" ],
    "name" : [ "Like of @moriahmaney's tweet" ],
    "published" : [ "2020-06-22T21:17:00+01:00" ],
    "category" : [ "Diversity-and-inclusion" ],
    "like-of" : [ "https://twitter.com/moriahmaney/status/1275124476821323776" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/drnbd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275124476821323776" ],
      "url" : [ "https://twitter.com/moriahmaney/status/1275124476821323776" ],
      "published" : [ "2020-06-22T17:52:20+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:moriahmaney" ],
          "numeric-id" : [ "928949262" ],
          "name" : [ "Moriah Maney" ],
          "nickname" : [ "moriahmaney" ],
          "url" : [ "https://twitter.com/moriahmaney", "http://moriah.dev" ],
          "published" : [ "2012-11-06T03:59:00+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1181365917546143744/YBbgKfy8.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Hi. I'm 25 years old. Sometimes I have to hold cups with two hands because my tremors get so bad I'll drop the cup if I don't. Often, I use two hands to hold a cup and still need assistance to drink from it.\n\nWhen you punch down, you hit ppl like me.",
        "html" : "<div style=\"white-space: pre\">Hi. I'm 25 years old. Sometimes I have to hold cups with two hands because my tremors get so bad I'll drop the cup if I don't. Often, I use two hands to hold a cup and still need assistance to drink from it.\n\nWhen you punch down, you hit ppl like me.</div>"
      } ]
    }
  },
  "tags" : [ "Diversity-and-inclusion" ],
  "client_id" : "https://indigenous.realize.be"
}
