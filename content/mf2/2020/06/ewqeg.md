{
  "date" : "2020-06-05T18:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/PaintYourDragon/status/1268680517672558592" ],
    "name" : [ "Like of @PaintYourDragon's tweet" ],
    "published" : [ "2020-06-05T18:21:00+01:00" ],
    "category" : [ "linux" ],
    "like-of" : [ "https://twitter.com/PaintYourDragon/status/1268680517672558592" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ewqeg",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268680517672558592" ],
      "url" : [ "https://twitter.com/PaintYourDragon/status/1268680517672558592" ],
      "published" : [ "2020-06-04T23:06:21+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:PaintYourDragon" ],
          "numeric-id" : [ "1378704151" ],
          "name" : [ "That Dragon Guy" ],
          "nickname" : [ "PaintYourDragon" ],
          "url" : [ "https://twitter.com/PaintYourDragon" ],
          "published" : [ "2013-04-25T05:04:42+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "California, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/499804360985935873/s0MVZE2y.jpeg" ]
        }
      } ],
      "content" : [ "The real reason why there’s no sound in space." ],
      "photo" : [ "https://pbs.twimg.com/media/EZtCNLFVAAA2m8f.jpg" ]
    }
  },
  "tags" : [ "linux" ],
  "client_id" : "https://indigenous.realize.be"
}
