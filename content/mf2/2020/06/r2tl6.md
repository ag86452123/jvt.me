{
  "date" : "2020-06-04T21:05:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrAndrew/status/1268627389602463745" ],
    "name" : [ "Like of @MrAndrew's tweet" ],
    "published" : [ "2020-06-04T21:05:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrAndrew/status/1268627389602463745" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/r2tl6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268627389602463745" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1268627389602463745" ],
      "published" : [ "2020-06-04T19:35:14+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1258160750641606657/CR5g9eOt.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "With our #TechNott and #WiTNotts online events we see quite a few people leave after the talk but the workshop in the 2nd half is by far my favourite part - today I'm chatting and drawing with four lovely people in my group and it feels like the before times ☺️",
        "html" : "With our <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> and <a href=\"https://twitter.com/search?q=%23WiTNotts\">#WiTNotts</a> online events we see quite a few people leave after the talk but the workshop in the 2nd half is by far my favourite part - today I'm chatting and drawing with four lovely people in my group and it feels like the before times ☺️"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
