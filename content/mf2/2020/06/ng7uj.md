{
  "date" : "2020-06-23T17:53:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Loftio/status/1275467795740983296" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1275473048683057152" ],
    "name" : [ "Reply to https://twitter.com/Loftio/status/1275467795740983296" ],
    "published" : [ "2020-06-23T17:53:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Maybe it's a new security question 🤔"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/ng7uj",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275467795740983296" ],
      "url" : [ "https://twitter.com/Loftio/status/1275467795740983296" ],
      "published" : [ "2020-06-23T16:36:34+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Loftio" ],
          "numeric-id" : [ "19788371" ],
          "name" : [ "Lex Lofthouse ✨" ],
          "nickname" : [ "Loftio" ],
          "url" : [ "https://twitter.com/Loftio", "http://loftio.co.uk" ],
          "published" : [ "2009-01-30T21:03:02+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1138757469176508417/Hc1FJP7a.png" ]
        }
      } ],
      "content" : [ "This may be the weirdest DM I've ever received 🙃" ],
      "photo" : [ "https://pbs.twimg.com/media/EbNfG3wWAAEjMcW.png" ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
