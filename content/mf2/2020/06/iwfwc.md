{
  "date" : "2020-06-23T10:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/mcclure111/status/1275193601828552704" ],
    "name" : [ "Like of @mcclure111's tweet" ],
    "published" : [ "2020-06-23T10:42:00+01:00" ],
    "category" : [ "mr-robot" ],
    "like-of" : [ "https://twitter.com/mcclure111/status/1275193601828552704" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/iwfwc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275193601828552704" ],
      "url" : [ "https://twitter.com/mcclure111/status/1275193601828552704" ],
      "published" : [ "2020-06-22T22:27:01+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:mcclure111" ],
          "numeric-id" : [ "312426579" ],
          "name" : [ "mcc 🏳️‍⚧️🏳️‍🌈" ],
          "nickname" : [ "mcclure111" ],
          "url" : [ "https://twitter.com/mcclure111", "http://runhello.com/", "http://mastodon.social/@mcc" ],
          "published" : [ "2011-06-07T03:20:19+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/852519442047225857/juzc1JeR.jpg" ]
        }
      } ],
      "content" : [ "We've been watching \"Mr. Robot\" lately and I'm realizing a thing that feels nice about watching it is it is really a wish fulfillment fantasy, where the fantasy is a bunch of people are casually using Linux and they never lose time screwing with video drivers or making wifi work" ]
    }
  },
  "tags" : [ "mr-robot" ],
  "client_id" : "https://indigenous.realize.be"
}
