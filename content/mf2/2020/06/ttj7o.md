{
  "date" : "2020-06-24T17:22:53.125Z",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/cassidoo/status/1275605680242610177" ],
    "name" : [ "Like of @cassidoo's tweet" ],
    "published" : [ "2020-06-24T17:22:53.125Z" ],
    "like-of" : [ "https://twitter.com/cassidoo/status/1275605680242610177" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ttj7o",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275605680242610177" ],
      "url" : [ "https://twitter.com/cassidoo/status/1275605680242610177" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1275605643244630016/pu/vid/720x1072/vEnlzSwxfp4i958g.mp4?tag=10" ],
      "published" : [ "2020-06-24T01:44:28+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:cassidoo" ],
          "numeric-id" : [ "400286802" ],
          "name" : [ "Cassidy Williams" ],
          "nickname" : [ "cassidoo" ],
          "url" : [ "https://twitter.com/cassidoo", "http://cassidoo.co" ],
          "published" : [ "2011-10-28T19:54:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle, WA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/718548236580098048/OgV0pPQY.jpg" ]
        }
      } ],
      "content" : [ "ASMR for developers" ]
    }
  },
  "client_id" : "https://micropublish.net"
}
