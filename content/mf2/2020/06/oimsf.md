{
  "date" : "2020-06-15T08:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/pczarkowski/status/1272243213873618944" ],
    "name" : [ "Like of @pczarkowski's tweet" ],
    "published" : [ "2020-06-15T08:17:00+01:00" ],
    "like-of" : [ "https://twitter.com/pczarkowski/status/1272243213873618944" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/oimsf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272243213873618944" ],
      "url" : [ "https://twitter.com/pczarkowski/status/1272243213873618944" ],
      "published" : [ "2020-06-14T19:03:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:pczarkowski" ],
          "numeric-id" : [ "163997887" ],
          "name" : [ "Czarknado 🦈🌪️" ],
          "nickname" : [ "pczarkowski" ],
          "url" : [ "https://twitter.com/pczarkowski", "http://tech.paulcz.net" ],
          "published" : [ "2010-07-07T19:59:49+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Austin, TX" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/826231488286896128/rRCwQvRh.jpg" ]
        }
      } ],
      "content" : [ "Actually, its only SRE if it's from the cloud division of Google, otherwise it's just sparkling devops." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
