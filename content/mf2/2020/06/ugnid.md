{
  "date" : "2020-06-09T21:57:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrsEmma/status/1270393959488147456" ],
    "name" : [ "Like of @MrsEmma's tweet" ],
    "published" : [ "2020-06-09T21:57:00+01:00" ],
    "like-of" : [ "https://twitter.com/MrsEmma/status/1270393959488147456" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ugnid",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1270393959488147456" ],
      "url" : [ "https://twitter.com/MrsEmma/status/1270393959488147456" ],
      "published" : [ "2020-06-09T16:34:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrsEmma" ],
          "numeric-id" : [ "19500955" ],
          "name" : [ "Emma Seward" ],
          "nickname" : [ "MrsEmma" ],
          "url" : [ "https://twitter.com/MrsEmma" ],
          "published" : [ "2009-01-25T19:32:42+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1268578361103826947/LOaufScI.jpg" ]
        }
      } ],
      "content" : [ "Two birthdays, 19 years apart. I think we have *slightly* better hair these days 😂" ],
      "photo" : [ "https://pbs.twimg.com/media/EaFYjRxWoAIG-UV.jpg", "https://pbs.twimg.com/media/EaFYj44WsAQnYLt.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
