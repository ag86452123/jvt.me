{
  "date" : "2020-06-06T14:08:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/jackyalcine/status/1269166422032175105" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1269259199378272256" ],
    "name" : [ "Reply to https://twitter.com/jackyalcine/status/1269166422032175105" ],
    "published" : [ "2020-06-06T14:08:00+01:00" ],
    "category" : [ "microformats" ],
    "content" : [ {
      "html" : "",
      "value" : "I thought I was the only one seeing this! Fortunately I've got https://granary.io producing <a href=\"/tags/microformats/\">#Microformats</a> feeds so I can still read Twitter from the https://indieweb.org/reader I want"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/06/zkort",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1269166422032175105" ],
      "url" : [ "https://twitter.com/jackyalcine/status/1269166422032175105" ],
      "published" : [ "2020-06-06T07:17:09+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:jackyalcine" ],
          "numeric-id" : [ "1262855987993563136" ],
          "name" : [ "jacky. 📟🇭🇹✊🏾" ],
          "nickname" : [ "jackyalcine" ],
          "url" : [ "https://twitter.com/jackyalcine", "https://v2.jacky.wtf", "http://jacky.wtf" ],
          "published" : [ "2020-05-19T21:22:07+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Bay Area, California" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1268478442187579392/hnRrPysi.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "This happens like four times a day for the last week I've had this installed. Back to using more reliable #indieweb tooling that's at least transparent",
        "html" : "This happens like four times a day for the last week I've had this installed. Back to using more reliable <a href=\"https://twitter.com/search?q=%23indieweb\">#indieweb</a> tooling that's at least transparent"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EZz8Ic-UwAcWcb_.jpg" ]
    }
  },
  "tags" : [ "microformats" ],
  "client_id" : "https://indigenous.realize.be"
}
