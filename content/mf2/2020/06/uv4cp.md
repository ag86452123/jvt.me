{
  "date" : "2020-06-17T11:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/ZeroistheBest42/status/1273014986592473094" ],
    "name" : [ "Like of @ZeroistheBest42's tweet" ],
    "published" : [ "2020-06-17T11:21:00+01:00" ],
    "category" : [ "mechanical-keyboard" ],
    "like-of" : [ "https://twitter.com/ZeroistheBest42/status/1273014986592473094" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/uv4cp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1273014986592473094" ],
      "url" : [ "https://twitter.com/ZeroistheBest42/status/1273014986592473094" ],
      "published" : [ "2020-06-16T22:09:58+00:00" ],
      "in-reply-to" : [ "https://twitter.com/threathuntergrl/status/1273013673724768256" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ZeroistheBest42" ],
          "numeric-id" : [ "979346414852591616" ],
          "name" : [ "Nichole" ],
          "nickname" : [ "ZeroistheBest42" ],
          "url" : [ "https://twitter.com/ZeroistheBest42" ],
          "published" : [ "2018-03-29T13:15:55+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Austin, TX" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1079123876796399616/KCPCA3xo.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Fingers are crossed for you!",
        "html" : "Fingers are crossed for you!\n<a class=\"u-mention\" href=\"https://twitter.com/threathuntergrl\"></a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EaqoYarXkAAGzMu.jpg" ]
    }
  },
  "tags" : [ "mechanical-keyboard" ],
  "client_id" : "https://indigenous.realize.be"
}
