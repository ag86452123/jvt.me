{
  "date" : "2020-06-25T09:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1276074966564438023" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-06-25T09:58:00+01:00" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1276074966564438023" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/uvvqh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1276074966564438023" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1276074966564438023" ],
      "published" : [ "2020-06-25T08:49:15+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 😅" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I’m taking a few days off work to finish the 123241 things I need to do, but instead a started a new thing.\n\n👇\n👇\n👇\n\n🤡 👈 me",
        "html" : "<div style=\"white-space: pre\">I’m taking a few days off work to finish the 123241 things I need to do, but instead a started a new thing.\n\n👇\n👇\n👇\n\n🤡 👈 me</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
