{
  "date" : "2020-06-01T20:48:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/realDonaldTrump/status/446461592029630464" ],
    "name" : [ "Like of @ianmiell's tweet" ],
    "published" : [ "2020-06-01T20:48:00+01:00" ],
    "category" : [ "politics" ],
    "like-of" : [ "https://twitter.com/ianmiell/status/1267470301358669826" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/qawlf",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1267470301358669826" ],
      "url" : [ "https://twitter.com/ianmiell/status/1267470301358669826" ],
      "published" : [ "2020-06-01T14:57:22+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:ianmiell" ],
          "numeric-id" : [ "58017706" ],
          "name" : [ "Ian Miell" ],
          "nickname" : [ "ianmiell" ],
          "url" : [ "https://twitter.com/ianmiell", "http://zwischenzugs.com", "http://bit.ly/35X34Ew" ],
          "published" : [ "2009-07-18T19:59:32+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1120618937253793793/xb6_9WMl.jpg" ]
        }
      } ],
      "content" : [ "This one took me a while... but like Clinton (!) cards, there really is one for every occasion." ]
    },
    "children" : [ {
      "type" : [ "u-quotation-of", "h-cite" ],
      "properties" : {
        "uid" : [ "tag:twitter.com:446461592029630464" ],
        "url" : [ "https://twitter.com/realDonaldTrump/status/446461592029630464" ],
        "published" : [ "2014-03-20T01:41:53+00:00" ],
        "author" : [ {
          "type" : [ "h-card" ],
          "properties" : {
            "uid" : [ "tag:twitter.com:realDonaldTrump" ],
            "numeric-id" : [ "25073877" ],
            "name" : [ "Donald J. Trump" ],
            "nickname" : [ "realDonaldTrump" ],
            "url" : [ "https://twitter.com/realDonaldTrump", "http://www.Instagram.com/realDonaldTrump" ],
            "published" : [ "2009-03-18T13:46:38+00:00" ],
            "location" : [ {
              "type" : [ "h-card", "p-location" ],
              "properties" : {
                "name" : [ "Washington, DC" ]
              }
            } ],
            "photo" : [ "https://pbs.twimg.com/profile_images/874276197357596672/kUuht00m.jpg" ]
          }
        } ],
        "content" : [ "It's almost like the United States has no President - we are a rudderless ship heading for a major disaster. Good luck everyone!" ]
      }
    } ]
  },
  "tags" : [ "politics" ],
  "client_id" : "https://indigenous.realize.be"
}
