{
  "date" : "2020-06-22T18:23:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/dotnetnotts/status/1275073661284671491" ],
    "name" : [ "Like of @dotnetnotts's tweet" ],
    "published" : [ "2020-06-22T18:23:00+01:00" ],
    "category" : [ "dotnetnotts" ],
    "like-of" : [ "https://twitter.com/dotnetnotts/status/1275073661284671491" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/6ifz5",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275073661284671491" ],
      "url" : [ "https://twitter.com/dotnetnotts/status/1275073661284671491" ],
      "published" : [ "2020-06-22T14:30:25+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:dotnetnotts" ],
          "numeric-id" : [ "2425322600" ],
          "name" : [ "dot net notts" ],
          "nickname" : [ "dotnetnotts" ],
          "url" : [ "https://twitter.com/dotnetnotts", "http://dotnetnotts.co" ],
          "published" : [ "2014-04-03T09:06:46+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/453460616389218305/1zO_xETE.png" ]
        }
      } ],
      "content" : [ {
        "value" : "ANNOUNCEMENT: We are thrilled to announce that the fantastic @JessPWhite is joining our organisational team here at #dotnetnotts! We're over the moon to have Jess onboard and can't wait to harness some of her boundless energy! Whoopee!",
        "html" : "ANNOUNCEMENT: We are thrilled to announce that the fantastic <a href=\"https://twitter.com/JessPWhite\">@JessPWhite</a> is joining our organisational team here at <a href=\"https://twitter.com/search?q=%23dotnetnotts\">#dotnetnotts</a>! We're over the moon to have Jess onboard and can't wait to harness some of her boundless energy! Whoopee!"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EbH3Q45UYAQ2ciI.jpg" ]
    }
  },
  "tags" : [ "dotnetnotts" ],
  "client_id" : "https://indigenous.realize.be"
}
