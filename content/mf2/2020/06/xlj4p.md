{
  "date" : "2020-06-15T07:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rashadrobinson/status/1272140337105035265" ],
    "name" : [ "Like of @rashadrobinson's tweet" ],
    "published" : [ "2020-06-15T07:52:00+01:00" ],
    "like-of" : [ "https://twitter.com/rashadrobinson/status/1272140337105035265" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/xlj4p",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1272140337105035265" ],
      "url" : [ "https://twitter.com/rashadrobinson/status/1272140337105035265" ],
      "published" : [ "2020-06-14T12:14:26+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rashadrobinson" ],
          "numeric-id" : [ "21456548" ],
          "name" : [ "Rashad Robinson" ],
          "nickname" : [ "rashadrobinson" ],
          "url" : [ "https://twitter.com/rashadrobinson", "http://colorofchange.org" ],
          "published" : [ "2009-02-21T02:18:36+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New York City" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1185323362018611201/qKyq2KiB.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EaeM5LMXQAEB818.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
