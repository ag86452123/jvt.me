{
  "date" : "2020-06-24T13:49:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1275750887772880896" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-06-24T13:49:00+01:00" ],
    "like-of" : [ "https://twitter.com/holly/status/1275750887772880896" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ceix8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275750887772880896" ],
      "url" : [ "https://twitter.com/holly/status/1275750887772880896" ],
      "published" : [ "2020-06-24T11:21:28+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "When it's hot outside so your boss sends you a 27 KILO BLOCK OF ICE\n\n@jimmy_wales you are bonkers in the best way",
        "html" : "<div style=\"white-space: pre\">When it's hot outside so your boss sends you a 27 KILO BLOCK OF ICE\n\n<a href=\"https://twitter.com/jimmy_wales\">@jimmy_wales</a> you are bonkers in the best way</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EbRglYiWkAAMjsv.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
