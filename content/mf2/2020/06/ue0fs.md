{
  "date" : "2020-06-05T18:55:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/alexisohanian/status/1268944657679036422" ],
    "name" : [ "Like of @alexisohanian's tweet" ],
    "published" : [ "2020-06-05T18:55:00+01:00" ],
    "like-of" : [ "https://twitter.com/alexisohanian/status/1268944657679036422" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/ue0fs",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1268944657679036422" ],
      "url" : [ "https://twitter.com/alexisohanian/status/1268944657679036422" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1268944544218914820/pu/vid/720x900/RhnfBYWsY8IKEcS1.mp4?tag=10" ],
      "published" : [ "2020-06-05T16:35:56+00:00" ],
      "in-reply-to" : [ "https://twitter.com/alexisohanian/status/1268944081562013696" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:alexisohanian" ],
          "numeric-id" : [ "811350" ],
          "name" : [ "Alexis Ohanian Sr. 🚀" ],
          "nickname" : [ "alexisohanian" ],
          "url" : [ "https://twitter.com/alexisohanian", "http://BusinessDad.com" ],
          "published" : [ "2007-03-05T03:26:51+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Florida, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1261740138406739969/MGSEKhR4.jpg" ]
        }
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
