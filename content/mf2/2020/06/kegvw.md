{
  "date" : "2020-06-10T07:49:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rockbot/status/1270400995567169536" ],
    "name" : [ "Like of @rockbot's tweet" ],
    "published" : [ "2020-06-10T07:49:00+01:00" ],
    "like-of" : [ "https://twitter.com/rockbot/status/1270400995567169536" ]
  },
  "kind" : "likes",
  "slug" : "2020/06/kegvw",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1270400995567169536" ],
      "url" : [ "https://twitter.com/rockbot/status/1270400995567169536" ],
      "published" : [ "2020-06-09T17:02:54+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rockbot" ],
          "numeric-id" : [ "26330898" ],
          "name" : [ "Raquel Vélez" ],
          "nickname" : [ "rockbot" ],
          "url" : [ "https://twitter.com/rockbot", "http://rckbt.me" ],
          "published" : [ "2009-03-24T21:38:23+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco, CA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/824994177075277824/e3ItWefi.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Found on a US government website.\n\nEveryone: *this* is what systemic racism is. It's not overt. It's a systematic principle by which some folks are excluded, forcing them to change who they are in order to fit into a system that never considered them in the first place.",
        "html" : "<div style=\"white-space: pre\">Found on a US government website.\n\nEveryone: *this* is what systemic racism is. It's not overt. It's a systematic principle by which some folks are excluded, forcing them to change who they are in order to fit into a system that never considered them in the first place.</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EaFc_puVAAAYmCz.png" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
