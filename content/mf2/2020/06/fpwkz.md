{
  "date" : "2020-06-24T08:22:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1275692921933697024" ],
    "published" : [ "2020-06-24T08:22:00+01:00" ],
    "repost-of" : [ "https://twitter.com/Sotherans/status/1275011253958819846" ]
  },
  "kind" : "reposts",
  "slug" : "2020/06/fpwkz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1275011253958819846" ],
      "url" : [ "https://twitter.com/Sotherans/status/1275011253958819846" ],
      "published" : [ "2020-06-22T10:22:26+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Sotherans" ],
          "numeric-id" : [ "458643002" ],
          "name" : [ "Henry Sotheran Ltd" ],
          "nickname" : [ "Sotherans" ],
          "url" : [ "https://twitter.com/Sotherans", "http://www.sotherans.co.uk" ],
          "published" : [ "2012-01-08T19:58:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, Piccadilly - W1S 3DP" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/910081231395737600/OcNwmQZ1.jpg" ]
        }
      } ],
      "content" : [ "the disturbing lack of time travellers arriving to stop 2020 happening suggests we never actually invent it" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
