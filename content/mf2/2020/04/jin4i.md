{
  "date" : "2020-04-22T23:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Nigella_Lawson/status/1252982910778585093" ],
    "name" : [ "Like of @Nigella_Lawson's tweet" ],
    "published" : [ "2020-04-22T23:42:00+01:00" ],
    "like-of" : [ "https://twitter.com/Nigella_Lawson/status/1252982910778585093" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/jin4i",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252982910778585093" ],
      "url" : [ "https://twitter.com/Nigella_Lawson/status/1252982910778585093" ],
      "published" : [ "2020-04-22T15:29:39+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Nigella_Lawson" ],
          "numeric-id" : [ "173195708" ],
          "name" : [ "Nigella Lawson" ],
          "nickname" : [ "Nigella_Lawson" ],
          "url" : [ "https://twitter.com/Nigella_Lawson", "http://www.nigella.com" ],
          "published" : [ "2010-07-31T18:08:47+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/875700331019206656/Oe9Y7ELU.jpg" ]
        }
      } ],
      "content" : [ "I haven’t left the house since mid March, and I’ve just had a card put through my letter box saying “We tried to deliver your parcel but you were out.” 🤷🏻‍♀️" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
