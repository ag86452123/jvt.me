{
  "date": "2020-04-11T19:42:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/MrsEmma/status/1249035116355125254"
    ],
    "name": [
      "Like of @MrsEmma's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrsEmma/status/1249035116355125254"
    ],
    "published": [
      "2020-04-11T19:42:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/rl1g0",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249035116355125254"
      ],
      "url": [
        "https://twitter.com/MrsEmma/status/1249035116355125254"
      ],
      "published": [
        "2020-04-11T18:02:32+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrsEmma"
            ],
            "numeric-id": [
              "19500955"
            ],
            "name": [
              "Emma Seward"
            ],
            "nickname": [
              "MrsEmma"
            ],
            "url": [
              "https://twitter.com/MrsEmma",
              "https://www.emmasgarden.co.uk/jewellery"
            ],
            "published": [
              "2009-01-25T19:32:42+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1247624012190158852/rhR91wjJ.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "#Fleabag stage show tonight with my lovely sister 🙌🥂\n\n(Remotely of course, I'm not a dick)\n\nSo in love with Phoebe it's slightly ridiculous...",
          "html": "<div style=\"white-space: pre\"><a href=\"https://twitter.com/search?q=%23Fleabag\">#Fleabag</a> stage show tonight with my lovely sister 🙌🥂\n\n(Remotely of course, I'm not a dick)\n\nSo in love with Phoebe it's slightly ridiculous...</div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EVV20EqWsAEJlb2.jpg"
      ]
    }
  },
  "tags": null,
  "client_id": "https://indigenous.realize.be"
}
