{
  "date" : "2020-04-29T22:22:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/lee_crossley/status/1255503080004747266" ],
    "name" : [ "Like of @lee_crossley's tweet" ],
    "published" : [ "2020-04-29T22:22:00+01:00" ],
    "like-of" : [ "https://twitter.com/lee_crossley/status/1255503080004747266" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/lzmax",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1255503080004747266" ],
      "url" : [ "https://twitter.com/lee_crossley/status/1255503080004747266" ],
      "published" : [ "2020-04-29T14:23:55+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:lee_crossley" ],
          "numeric-id" : [ "27271165" ],
          "name" : [ "λee Crossley" ],
          "nickname" : [ "lee_crossley" ],
          "url" : [ "https://twitter.com/lee_crossley", "http://bjss.com" ],
          "published" : [ "2009-03-28T17:38:28+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/781757122585526272/oUJ0TNjB.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Seems like a fair swap to me ⁦@Ocado⁩",
        "html" : "Seems like a fair swap to me ⁦<a href=\"https://twitter.com/Ocado\">@Ocado</a>⁩"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EWxxZRiXYAAWy7l.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
