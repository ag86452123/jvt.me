{
  "date" : "2020-04-25T12:41:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/CarolSaysThings/status/1253981594299310081" ],
    "name" : [ "Like of @CarolSaysThings's tweet" ],
    "published" : [ "2020-04-25T12:41:00+01:00" ],
    "category" : [ "food" ],
    "like-of" : [ "https://twitter.com/CarolSaysThings/status/1253981594299310081" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/on2yd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253981594299310081" ],
      "url" : [ "https://twitter.com/CarolSaysThings/status/1253981594299310081" ],
      "published" : [ "2020-04-25T09:38:04+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:CarolSaysThings" ],
          "numeric-id" : [ "36382927" ],
          "name" : [ "Carol 🌴living the island life ✨" ],
          "nickname" : [ "CarolSaysThings" ],
          "url" : [ "https://twitter.com/CarolSaysThings", "https://carolgilabert.me/" ],
          "published" : [ "2009-04-29T15:22:13+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "🇧🇷🇪🇸🇬🇧 · Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg" ]
        }
      } ],
      "content" : [ "Pancake time 🥞💛" ],
      "photo" : [ "https://pbs.twimg.com/media/EWcJnEfWAAADKJv.jpg" ]
    }
  },
  "tags" : [ "food" ],
  "client_id" : "https://indigenous.realize.be"
}
