{
  "date": "2020-04-09T16:27:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1248272570719375362"
    ],
    "repost-of": [
      "https://twitter.com/mufasatweetss/status/1248043317440532482"
    ],
    "published": [
      "2020-04-09T16:27:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Can't help but smile at this!"
      }
    ]
  },
  "kind": "reposts",
  "slug": "2020/04/kiber",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248043317440532482"
      ],
      "url": [
        "https://twitter.com/mufasatweetss/status/1248043317440532482"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1248043152839245825/pu/vid/430x720/p11ItuJR5sBmWpUJ.mp4?tag=10"
      ],
      "published": [
        "2020-04-09T00:21:29+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:mufasatweetss"
            ],
            "numeric-id": [
              "960639150377627648"
            ],
            "name": [
              "mufasa"
            ],
            "nickname": [
              "mufasatweetss"
            ],
            "url": [
              "https://twitter.com/mufasatweetss"
            ],
            "published": [
              "2018-02-05T22:19:56+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "United States"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/960640840208125952/4YTki-a-.jpg"
            ]
          }
        }
      ],
      "content": [
        "I hope this video makes your day❤️💪🏿"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
