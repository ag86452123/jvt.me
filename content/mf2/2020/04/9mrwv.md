{
  "date": "2020-04-11T22:15:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1249084862558068736"
    ],
    "in-reply-to": [
      "https://twitter.com/Marcus_Noble_/status/1249053899715162115"
    ],
    "name": [
      "Reply to https://twitter.com/Marcus_Noble_/status/1249053899715162115"
    ],
    "published": [
      "2020-04-11T22:15:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Used to but moved to Hetzner as performance and cost wasn't as good, and I had a few issues with downtime - one which left me with a fairly critical server dead for over 24 hours and needed restoring from backups"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/9mrwv",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249053899715162115"
      ],
      "url": [
        "https://twitter.com/Marcus_Noble_/status/1249053899715162115"
      ],
      "published": [
        "2020-04-11T19:17:10+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Marcus_Noble_"
            ],
            "numeric-id": [
              "14732545"
            ],
            "name": [
              "Marcus Noble"
            ],
            "nickname": [
              "Marcus_Noble_"
            ],
            "url": [
              "https://twitter.com/Marcus_Noble_",
              "https://marcusnoble.co.uk"
            ],
            "published": [
              "2008-05-11T09:51:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Some container"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Do I know anyone that uses / has used @Scaleway ?",
          "html": "Do I know anyone that uses / has used <a href=\"https://twitter.com/Scaleway\">@Scaleway</a> ?"
        }
      ]
    }
  },
  "tags": null,
  "client_id": "https://indigenous.realize.be"
}
