{
  "date" : "2020-04-25T21:36:00.734+02:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/Marcus_Noble_/status/1254130091086536705" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1254134670687637506" ],
    "name" : [ "Reply to https://twitter.com/Marcus_Noble_/status/1254130091086536705" ],
    "published" : [ "2020-04-25T21:36:00.734+02:00" ],
    "content" : [ {
      "html" : "",
      "value" : "Sorry, the Alpine one wouldn't be helpful anyway. The Arch package (https://www.archlinux.org/packages/community/x86_64/nextcloud-client/) has the `nextcloudcmd` executable, but it appears to need a few other things installed from the package, such as `libnextcloudsync.so`. What platform are you running your server on? It looks like the packages are available on https://help.nextcloud.com/t/linux-packages-status/10216 for each service - given the Arch package is the \"desktop client\", it appears that it's possible to install the client on your headless server and still interact with it via `nextcloudcmd`"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/pc8wp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254130091086536705" ],
      "url" : [ "https://twitter.com/Marcus_Noble_/status/1254130091086536705" ],
      "published" : [ "2020-04-25T19:28:08+00:00" ],
      "in-reply-to" : [ "https://twitter.com/edent/status/1254125903791230977" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Marcus_Noble_" ],
          "numeric-id" : [ "14732545" ],
          "name" : [ "Marcus Noble" ],
          "nickname" : [ "Marcus_Noble_" ],
          "url" : [ "https://twitter.com/Marcus_Noble_", "https://marcusnoble.co.uk" ],
          "published" : [ "2008-05-11T09:51:36+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "PID 1" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "The Alipne package just does a build of this repo: github.com/nextcloud/desk…",
        "html" : "The Alipne package just does a build of this repo: <a href=\"https://github.com/nextcloud/desktop/\">github.com/nextcloud/desk…</a>\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/edent\"></a>"
      } ]
    }
  },
  "client_id" : "https://micropublish.net"
}
