{
  "date" : "2020-04-18T19:25:46.457+02:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/anna_hax/status/1251562189774827523" ],
    "name" : [ "Like of @anna_hax's tweet" ],
    "published" : [ "2020-04-18T19:25:46.457+02:00" ],
    "like-of" : [ "https://twitter.com/anna_hax/status/1251562189774827523" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/xl6ss",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1251562189774827523" ],
      "url" : [ "https://twitter.com/anna_hax/status/1251562189774827523" ],
      "published" : [ "2020-04-18T17:24:13+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1251560385603305472" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:anna_hax" ],
          "numeric-id" : [ "394235377" ],
          "name" : [ "Anna 🏠" ],
          "nickname" : [ "anna_hax" ],
          "url" : [ "https://twitter.com/anna_hax", "https://annadodson.co.uk" ],
          "published" : [ "2011-10-19T19:37:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "This was an entertaining blast from the past!!\n@MrAndrew - TWannaWeb's on one 😂",
        "html" : "<div style=\"white-space: pre\">This was an entertaining blast from the past!!\n<a href=\"https://twitter.com/MrAndrew\">@MrAndrew</a> - TWannaWeb's on one 😂</div>\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "client_id" : "https://micropublish.net"
}
