{
  "date" : "2020-04-29T20:29:45.974+02:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/05/online-homebrew-website-club-nottingham-j91sUdJVM1bh" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/05/online-homebrew-website-club-nottingham-j91sUdJVM1bh" ],
    "published" : [ "2020-04-29T20:29:45.974+02:00" ],
    "event" : {
      "start" : [ "2020-05-13T17:30:00+01:00" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-05-13T19:30:00+01:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/05/online-homebrew-website-club-nottingham-j91sUdJVM1bh" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/04/ns0x7",
  "client_id" : "https://micropublish.net"
}
