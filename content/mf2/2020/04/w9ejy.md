{
  "date" : "2020-04-18T21:59:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/hexadecim8/status/1251467340207869953" ],
    "name" : [ "Like of @hexadecim8's tweet" ],
    "published" : [ "2020-04-18T21:59:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/hexadecim8/status/1251467340207869953" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/w9ejy",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1251467340207869953" ],
      "url" : [ "https://twitter.com/hexadecim8/status/1251467340207869953" ],
      "published" : [ "2020-04-18T11:07:19+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:hexadecim8" ],
          "numeric-id" : [ "703060885435244544" ],
          "name" : [ "Sister HxA full of trace(route)" ],
          "nickname" : [ "hexadecim8" ],
          "url" : [ "https://twitter.com/hexadecim8", "https://hexadecim8.com/site" ],
          "published" : [ "2016-02-26T03:35:52+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "DC" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1211809711558987777/xHKn0i48.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Saw this and thought of you @mzbat",
        "html" : "Saw this and thought of you <a href=\"https://twitter.com/mzbat\">@mzbat</a>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EV4a6XDXsAUfF2r.jpg" ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
