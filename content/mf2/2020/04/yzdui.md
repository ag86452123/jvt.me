{
  "date": "2020-04-11T22:56:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/NabilahforGA07/status/1248728359246401542"
    ],
    "name": [
      "Like of @NabilahforGA07's tweet"
    ],
    "like-of": [
      "https://twitter.com/NabilahforGA07/status/1248728359246401542"
    ],
    "published": [
      "2020-04-11T22:56:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/yzdui",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248728359246401542"
      ],
      "url": [
        "https://twitter.com/NabilahforGA07/status/1248728359246401542"
      ],
      "published": [
        "2020-04-10T21:43:35+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:NabilahforGA07"
            ],
            "numeric-id": [
              "78396305"
            ],
            "name": [
              "Nabilah Islam for Congress"
            ],
            "nickname": [
              "NabilahforGA07"
            ],
            "url": [
              "https://twitter.com/NabilahforGA07",
              "http://www.nabilahforcongress.com"
            ],
            "published": [
              "2009-09-29T19:38:43+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Lawrenceville, GA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1223275816504778757/CS1wXY0a.jpg"
            ]
          }
        }
      ],
      "content": [
        "Stock Market has best week since 1938 while 16.6 million people lose their jobs. There is something inherently wrong this system."
      ]
    }
  },
  "tags": null,
  "client_id": "https://indigenous.realize.be"
}
