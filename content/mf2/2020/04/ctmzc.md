{
  "date" : "2020-04-24T10:56:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/gabydunn/status/1253481152481910785" ],
    "name" : [ "Like of @gabydunn's tweet" ],
    "published" : [ "2020-04-24T10:56:00+01:00" ],
    "like-of" : [ "https://twitter.com/gabydunn/status/1253481152481910785" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/ctmzc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253481152481910785" ],
      "url" : [ "https://twitter.com/gabydunn/status/1253481152481910785" ],
      "published" : [ "2020-04-24T00:29:29+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:gabydunn" ],
          "numeric-id" : [ "20643592" ],
          "name" : [ "GABY DUNN 🏳️‍🌈✡️" ],
          "nickname" : [ "gabydunn" ],
          "url" : [ "https://twitter.com/gabydunn", "https://www.stitcher.com/podcast/stitcher/bad-with-money" ],
          "published" : [ "2009-02-12T00:39:26+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Los Angeles" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1084477778680893441/Cg0-x9SP.jpg" ]
        }
      } ],
      "content" : [ "I know there's a lot going on right now but I just Googled my childhood bully and she works in bullying advocacy and I am spiraling." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
