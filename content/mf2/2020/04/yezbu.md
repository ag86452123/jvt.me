{
  "date": "2020-04-13T23:29:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/rawkode/status/1249634926095458305"
    ],
    "name": [
      "Like of @rawkode's tweet"
    ],
    "published": [
      "2020-04-13T23:29:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/rawkode/status/1249634926095458305"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/yezbu",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249634926095458305"
      ],
      "url": [
        "https://twitter.com/rawkode/status/1249634926095458305"
      ],
      "published": [
        "2020-04-13T09:45:58+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rawkode"
            ],
            "numeric-id": [
              "22761152"
            ],
            "name": [
              "David McKay"
            ],
            "nickname": [
              "rawkode"
            ],
            "url": [
              "https://twitter.com/rawkode",
              "https://rawkode.com",
              "http://calendly.com/rawkode"
            ],
            "published": [
              "2009-03-04T12:06:23+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Scotland, Europe"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1242222745938800643/JAu0QRcc.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Ouch, I've just noticed that #Windows10 has truncated my username to \"rawko\" locally.\n\nThis may be enough for me to go back to Linux ... 😂",
          "html": "<div style=\"white-space: pre\">Ouch, I've just noticed that <a href=\"https://twitter.com/search?q=%23Windows10\">#Windows10</a> has truncated my username to \"rawko\" locally.\n\nThis may be enough for me to go back to Linux ... 😂</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
