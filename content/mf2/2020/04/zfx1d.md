{
  "date" : "2020-04-26T13:14:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/gymlifeanimal/status/1253795478694768642" ],
    "name" : [ "Like of @gymlifeanimal's tweet" ],
    "published" : [ "2020-04-26T13:14:00+01:00" ],
    "like-of" : [ "https://twitter.com/gymlifeanimal/status/1253795478694768642" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/zfx1d",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253795478694768642" ],
      "url" : [ "https://twitter.com/gymlifeanimal/status/1253795478694768642" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1253795371475664901/pu/vid/720x894/o6aNH1vZrA_ZqVjC.mp4?tag=10" ],
      "published" : [ "2020-04-24T21:18:31+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:gymlifeanimal" ],
          "numeric-id" : [ "1006394480973447168" ],
          "name" : [ "🌺K🌺" ],
          "nickname" : [ "gymlifeanimal" ],
          "url" : [ "https://twitter.com/gymlifeanimal", "https://twitter.com/search?q=from:@Kbrizz1/exclude:replies" ],
          "published" : [ "2018-06-12T04:35:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Florida, USA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1251985386320125958/yIz1y7n7.jpg" ]
        }
      } ],
      "content" : [ "When you really miss traveling..." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
