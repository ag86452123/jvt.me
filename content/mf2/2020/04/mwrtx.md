{
  "date" : "2020-04-22T23:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/thugcrowd/status/1253009960063119370" ],
    "name" : [ "Like of @thugcrowd's tweet" ],
    "published" : [ "2020-04-22T23:01:00+01:00" ],
    "like-of" : [ "https://twitter.com/thugcrowd/status/1253009960063119370" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/mwrtx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253009960063119370" ],
      "url" : [ "https://twitter.com/thugcrowd/status/1253009960063119370" ],
      "published" : [ "2020-04-22T17:17:08+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:thugcrowd" ],
          "numeric-id" : [ "973781719789789185" ],
          "name" : [ "ThugCrowd - #AirGap2020 Virtual Con May 2nd" ],
          "nickname" : [ "thugcrowd" ],
          "url" : [ "https://twitter.com/thugcrowd", "http://thugcrowd.com/" ],
          "published" : [ "2018-03-14T04:43:48+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "0.0.0.0/0" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1227608294904225793/_Ev_YPJK.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "curl -fsSLd get.docker.com -0 get-docker.sh | bash",
        "html" : "curl -fsSLd <a href=\"https://get.docker.com\">get.docker.com</a> -0 <a href=\"http://get-docker.sh\">get-docker.sh</a> | bash"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EWOUw8dWsAU0hVJ.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
