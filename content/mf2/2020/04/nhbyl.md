{
  "date" : "2020-04-22T23:01:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/trisweb/status/1252599107723636739" ],
    "name" : [ "Like of @trisweb's tweet" ],
    "published" : [ "2020-04-22T23:01:00+01:00" ],
    "like-of" : [ "https://twitter.com/trisweb/status/1252599107723636739" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/nhbyl",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252599107723636739" ],
      "url" : [ "https://twitter.com/trisweb/status/1252599107723636739" ],
      "published" : [ "2020-04-21T14:04:34+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:trisweb" ],
          "numeric-id" : [ "14145056" ],
          "name" : [ "Tristan Harward" ],
          "nickname" : [ "trisweb" ],
          "url" : [ "https://twitter.com/trisweb", "http://www.trisweb.com" ],
          "published" : [ "2008-03-14T04:56:10+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Massachusetts" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/928731960666611712/mFUuQNgN.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "So, lemme just share my screen... one second.\n\nOK can everyone see my screen?",
        "html" : "<div style=\"white-space: pre\">So, lemme just share my screen... one second.\n\nOK can everyone see my screen?</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
