{
  "date": "2020-04-15T17:00:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/DickKingSmith/status/1250321434779377667"
    ],
    "name": [
      "Like of @DickKingSmith's tweet"
    ],
    "published": [
      "2020-04-15T17:00:00+01:00"
    ],
    "category": [
      "cute"
    ],
    "like-of": [
      "https://twitter.com/DickKingSmith/status/1250321434779377667"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/l7dvu",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1250321434779377667"
      ],
      "url": [
        "https://twitter.com/DickKingSmith/status/1250321434779377667"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1250321304030330886/pu/vid/720x1272/_H__dKX8YHXGDqpV.mp4?tag=10"
      ],
      "published": [
        "2020-04-15T07:13:54+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:DickKingSmith"
            ],
            "numeric-id": [
              "2275883743"
            ],
            "name": [
              "Dick King-Smith HQ"
            ],
            "nickname": [
              "DickKingSmith"
            ],
            "url": [
              "https://twitter.com/DickKingSmith",
              "http://www.dickkingsmith.com"
            ],
            "published": [
              "2014-01-04T09:50:34+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/650626555991998464/iYH9jea4.jpg"
            ]
          }
        }
      ],
      "content": [
        "Interspecies love of the day."
      ]
    }
  },
  "tags": [
    "cute"
  ],
  "client_id": "https://indigenous.realize.be"
}
