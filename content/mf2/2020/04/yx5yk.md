{
  "date": "2020-04-11T16:02:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/MylesBorins/status/1248800043659821058"
    ],
    "name": [
      "Like of @MylesBorins's tweet"
    ],
    "like-of": [
      "https://twitter.com/MylesBorins/status/1248800043659821058"
    ],
    "published": [
      "2020-04-11T16:02:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/yx5yk",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248800043659821058"
      ],
      "url": [
        "https://twitter.com/MylesBorins/status/1248800043659821058"
      ],
      "published": [
        "2020-04-11T02:28:26+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MylesBorins"
            ],
            "numeric-id": [
              "150664007"
            ],
            "name": [
              "sMyle"
            ],
            "nickname": [
              "MylesBorins"
            ],
            "url": [
              "https://twitter.com/MylesBorins",
              "http://myles.dev"
            ],
            "published": [
              "2010-06-01T14:11:24+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "New York, NY"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1184008381788495874/VKLFDqgq.jpg"
            ]
          }
        }
      ],
      "content": [
        "Jewish devs... I regret to inform you that you must halt using serverless and must use VMs until next Friday as it is PAASover"
      ]
    }
  },
  "tags": null,
  "client_id": "https://indigenous.realize.be"
}
