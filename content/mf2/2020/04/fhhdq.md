{
  "kind": "likes",
  "slug": "2020/04/fhhdq",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245382658714144771"
      ],
      "url": [
        "https://twitter.com/nocontexttgp/status/1245382658714144771"
      ],
      "published": [
        "2020-04-01T16:08:58+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:nocontexttgp"
            ],
            "numeric-id": [
              "902965228937379842"
            ],
            "name": [
              "no context the good place"
            ],
            "nickname": [
              "nocontexttgp"
            ],
            "url": [
              "https://twitter.com/nocontexttgp",
              "http://www.multiverseradio.ca/forkinbullshirt.html"
            ],
            "published": [
              "2017-08-30T18:44:21+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "The Good Place"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/911487508126601216/P7oHjlor.jpg"
            ]
          }
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUh82IsXYAAT6Xc.jpg",
        "https://pbs.twimg.com/media/EUh83ngXsAAfNaN.jpg",
        "https://pbs.twimg.com/media/EUh85zGWsAIWCm-.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-02T13:20:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @nocontexttgp's tweet"
    ],
    "like-of": [
      "https://twitter.com/nocontexttgp/status/1245382658714144771"
    ],
    "published": [
      "2020-04-02T13:20:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/nocontexttgp/status/1245382658714144771"
    ]
  }
}
