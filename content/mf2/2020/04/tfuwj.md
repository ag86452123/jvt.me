{
  "date": "2020-04-17T23:25:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/djbaskin/status/1251219882563088384"
    ],
    "name": [
      "Like of @djbaskin's tweet"
    ],
    "published": [
      "2020-04-17T23:25:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/djbaskin/status/1251219882563088384"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/tfuwj",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251219882563088384"
      ],
      "url": [
        "https://twitter.com/djbaskin/status/1251219882563088384"
      ],
      "published": [
        "2020-04-17T18:44:01+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:djbaskin"
            ],
            "numeric-id": [
              "55662470"
            ],
            "name": [
              "Danielle Baskin"
            ],
            "nickname": [
              "djbaskin"
            ],
            "url": [
              "https://twitter.com/djbaskin",
              "https://www.daniellebaskin.com",
              "http://dialup.com"
            ],
            "published": [
              "2009-07-10T21:00:48+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "San Francisco, CA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1219233434322628608/ZD1qvkE-.jpg"
            ]
          }
        }
      ],
      "content": [
        "Once lockdown is lifted and we’re no longer on Zoom, I’m walking around with a tiny mirror above my face so you can continue to stare at your own face while we’re talking."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EV052ZzU0AAexec.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
