{
  "date" : "2020-04-27T20:20:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/kathleencodes/status/1254792070364975105" ],
    "name" : [ "Like of @kathleencodes's tweet" ],
    "published" : [ "2020-04-27T20:20:00+01:00" ],
    "like-of" : [ "https://twitter.com/kathleencodes/status/1254792070364975105" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/eft6j",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254792070364975105" ],
      "url" : [ "https://twitter.com/kathleencodes/status/1254792070364975105" ],
      "published" : [ "2020-04-27T15:18:37+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:kathleencodes" ],
          "numeric-id" : [ "18689607" ],
          "name" : [ "Kathleen Vignos" ],
          "nickname" : [ "kathleencodes" ],
          "url" : [ "https://twitter.com/kathleencodes", "https://www.twitter.com" ],
          "published" : [ "2009-01-06T19:03:45+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "San Francisco" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1176978382845669376/lVegN--a.jpg" ]
        }
      } ],
      "content" : [ "All this talk about how you can wear pajamas pants on zoom calls. But no one has seen the back of my hair/head for 2 months, Voldemort could be living back there" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
