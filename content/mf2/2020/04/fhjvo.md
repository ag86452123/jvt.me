{
  "kind": "replies",
  "slug": "2020/04/fhjvo",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1246069493962387458"
      ],
      "url": [
        "https://twitter.com/indyarni/status/1246069493962387458"
      ],
      "published": [
        "2020-04-03T13:38:12+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:indyarni"
            ],
            "numeric-id": [
              "2424076548"
            ],
            "name": [
              "Arnold Franke"
            ],
            "nickname": [
              "indyarni"
            ],
            "url": [
              "https://twitter.com/indyarni"
            ],
            "published": [
              "2014-04-02T16:50:50+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Karlsruhe, Baden-Württemberg"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/451484480281522176/3oc_xtJk.jpeg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Found a nice guide by @JamieTanna for simple unit testing of #slf4j logging. Just tried it, works for me.\n\njvt.me/posts/2019/09/…",
          "html": "<div style=\"white-space: pre\">Found a nice guide by <a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a> for simple unit testing of <a href=\"https://twitter.com/search?q=%23slf4j\">#slf4j</a> logging. Just tried it, works for me.\n\n<a href=\"https://www.jvt.me/posts/2019/09/22/testing-slf4j-logs/\">jvt.me/posts/2019/09/…</a></div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-03T14:45:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/indyarni/status/1246069493962387458"
    ],
    "in-reply-to": [
      "https://twitter.com/indyarni/status/1246069493962387458"
    ],
    "published": [
      "2020-04-03T14:45:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Thanks! Glad to hear it helped. The library mentioned at the end of the post has a bit nicer interface, if you prefer, but I was pretty happy with the initial version too"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1246072688319356928"
    ]
  }
}
