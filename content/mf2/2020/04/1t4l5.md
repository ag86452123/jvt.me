{
  "date" : "2020-04-21T18:58:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/edent/status/1252546233975087115" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1252704102108323842" ],
    "name" : [ "Reply to https://twitter.com/edent/status/1252546233975087115" ],
    "published" : [ "2020-04-21T18:58:00+01:00" ],
    "content" : [ {
      "html" : "",
      "value" : "Learning review is my go-to"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/1t4l5",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252546233975087115" ],
      "url" : [ "https://twitter.com/edent/status/1252546233975087115" ],
      "published" : [ "2020-04-21T10:34:28+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I've never liked the term \"post mortem\" as it relates to assessing a service.\nAnd, in health, it feels especially grim.\n\nWhat's a better phrase to use?",
        "html" : "<div style=\"white-space: pre\">I've never liked the term \"post mortem\" as it relates to assessing a service.\nAnd, in health, it feels especially grim.\n\nWhat's a better phrase to use?</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
