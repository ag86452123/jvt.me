{
  "date": "2020-04-10T23:08:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1248735745239629824"
    ],
    "in-reply-to": [
      "https://twitter.com/rwdrich/status/1248717785934544897"
    ],
    "name": [
      "Reply to https://twitter.com/rwdrich/status/1248717785934544897"
    ],
    "published": [
      "2020-04-10T23:08:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "You two ♥"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/amb3g",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248717785934544897"
      ],
      "url": [
        "https://twitter.com/rwdrich/status/1248717785934544897"
      ],
      "published": [
        "2020-04-10T21:01:34+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rwdrich"
            ],
            "numeric-id": [
              "107548386"
            ],
            "name": [
              "Richard Davies"
            ],
            "nickname": [
              "rwdrich"
            ],
            "url": [
              "https://twitter.com/rwdrich",
              "http://rwdrich.co.uk"
            ],
            "published": [
              "2010-01-22T23:14:30+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cambridge, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg"
            ]
          }
        }
      ],
      "content": [
        "Sophie and I dressed up for watching jesus christ superstar earlier, and now shes in Pjs eating ice cream watching the vicar of dibley. Get you a girl that can do both <3"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
