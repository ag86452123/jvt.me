{
  "kind": "rsvps",
  "slug": "2020/04/aj5ke",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-03T08:04:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/269697018/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/269697018/"
    ],
    "published": [
      "2020-04-03T08:04:00+01:00"
    ],
    "rsvp": [
      "yes"
    ],
    "syndication": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/269697018/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Online"
      ],
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/269697018/"
      ],
      "name": [
        "What's new in PHP 7.4, and coming up in PHP 8"
      ],
      "start": [
        "2020-04-09T18:30:00+01:00"
      ],
      "end": [
        "2020-04-09T20:30:00+01:00"
      ]
    }
  }
}
