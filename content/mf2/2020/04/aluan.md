{
  "kind": "likes",
  "slug": "2020/04/aluan",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245789989272719365"
      ],
      "url": [
        "https://twitter.com/ninjanails/status/1245789989272719365"
      ],
      "published": [
        "2020-04-02T19:07:33+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ninjanails"
            ],
            "numeric-id": [
              "167740453"
            ],
            "name": [
              "Seren Davies"
            ],
            "nickname": [
              "ninjanails"
            ],
            "url": [
              "https://twitter.com/ninjanails",
              "http://www.serendavies.me",
              "http://ninjanails.com"
            ],
            "published": [
              "2010-07-17T11:06:01+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Oxford / Bristol"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1146364466801577985/TvCfb49a.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Really enjoyed @paulienuh talk at #WiTNotts definitely feeling inspired to try and get blogging again",
          "html": "Really enjoyed <a href=\"https://twitter.com/paulienuh\">@paulienuh</a> talk at <a href=\"https://twitter.com/search?q=%23WiTNotts\">#WiTNotts</a> definitely feeling inspired to try and get blogging again"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-04-02T21:10:41.009+02:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ninjanails's tweet"
    ],
    "like-of": [
      "https://twitter.com/ninjanails/status/1245789989272719365"
    ],
    "published": [
      "2020-04-02T21:10:41.009+02:00"
    ],
    "syndication": [
      "https://twitter.com/ninjanails/status/1245789989272719365"
    ]
  }
}
