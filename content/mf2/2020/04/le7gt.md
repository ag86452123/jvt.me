{
  "date" : "2020-04-30T18:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.jvt.me/events/personal-events/2020-04-o936k/" ],
    "name" : [ "RSVP yes to https://www.jvt.me/events/personal-events/2020-04-o936k/" ],
    "published" : [ "2020-04-30T18:00:00+01:00" ],
    "event" : {
      "start" : [ "2020-04-30T18:00:00+0100" ],
      "name" : [ "Escape room" ],
      "end" : [ "2020-04-30T21:00:00+0100" ],
      "url" : [ "https://www.jvt.me/events/personal-events/2020-04-o936k/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/04/le7gt",
  "client_id" : "https://indigenous.realize.be"
}
