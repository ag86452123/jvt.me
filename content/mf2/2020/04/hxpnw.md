{
  "date": "2020-04-11T18:51:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1249033619072520198"
    ],
    "in-reply-to": [
      "https://twitter.com/pascaldoesgo/status/1249023601271099399"
    ],
    "name": [
      "Reply to https://twitter.com/pascaldoesgo/status/1249023601271099399"
    ],
    "published": [
      "2020-04-11T18:51:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "I guess you'll have to have another 😅"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/hxpnw",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249023601271099399"
      ],
      "url": [
        "https://twitter.com/pascaldoesgo/status/1249023601271099399"
      ],
      "published": [
        "2020-04-11T17:16:46+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:pascaldoesgo"
            ],
            "numeric-id": [
              "1003529600901767168"
            ],
            "name": [
              "Pascal Dennerly"
            ],
            "nickname": [
              "pascaldoesgo"
            ],
            "url": [
              "https://twitter.com/pascaldoesgo"
            ],
            "published": [
              "2018-06-04T06:51:16+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1199757075561222147/eYBkMUrZ.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "It's a warm day, been painting the garage door and now the barbeque is going.\n\nBut, quel horreur, my chilled beer appears to evaporated! What ever shall I do?",
          "html": "<div style=\"white-space: pre\">It's a warm day, been painting the garage door and now the barbeque is going.\n\nBut, quel horreur, my chilled beer appears to evaporated! What ever shall I do?</div>"
        }
      ]
    }
  },
  "tags": null,
  "client_id": "https://indigenous.realize.be"
}
