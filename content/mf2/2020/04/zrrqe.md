{
  "date" : "2020-04-21T16:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://events.indieweb.org/2020/05/online-homebrew-website-club-nottingham-q0LsZr0uDBS7" ],
    "name" : [ "RSVP yes to https://events.indieweb.org/2020/04/online-homebrew-website-club-nottingham-q0LsZr0uDBS7" ],
    "published" : [ "2020-04-21T16:17:00+01:00" ],
    "event" : {
      "start" : [ "2020-04-29T17:30:00+01:00" ],
      "name" : [ "ONLINE: Homebrew Website Club: Nottingham" ],
      "end" : [ "2020-04-29T19:30:00+01:00" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ],
      "url" : [ "https://events.indieweb.org/2020/05/online-homebrew-website-club-nottingham-q0LsZr0uDBS7" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/04/zrrqe",
  "client_id" : "https://indigenous.realize.be"
}
