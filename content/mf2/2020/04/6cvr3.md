{
  "date": "2020-04-17T23:27:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/hankchizljaw/status/1251273604685271047"
    ],
    "name": [
      "Like of @hankchizljaw's tweet"
    ],
    "published": [
      "2020-04-17T23:27:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/hankchizljaw/status/1251273604685271047"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/6cvr3",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251273604685271047"
      ],
      "url": [
        "https://twitter.com/hankchizljaw/status/1251273604685271047"
      ],
      "published": [
        "2020-04-17T22:17:29+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:hankchizljaw"
            ],
            "numeric-id": [
              "98734097"
            ],
            "name": [
              "Andy Bell"
            ],
            "nickname": [
              "hankchizljaw"
            ],
            "url": [
              "https://twitter.com/hankchizljaw",
              "https://hankchizljaw.com",
              "http://hankchizljaw.redbubble.com"
            ],
            "published": [
              "2009-12-22T22:28:06+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cheltenham, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1219208545138290689/XoucaVnN.jpg"
            ]
          }
        }
      ],
      "content": [
        "How do you write front-end?"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
