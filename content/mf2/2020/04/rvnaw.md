{
  "kind": "likes",
  "slug": "2020/04/rvnaw",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245771695807397895"
      ],
      "url": [
        "https://twitter.com/Marcus_Noble_/status/1245771695807397895"
      ],
      "published": [
        "2020-04-02T17:54:52+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Marcus_Noble_"
            ],
            "numeric-id": [
              "14732545"
            ],
            "name": [
              "Marcus Noble"
            ],
            "nickname": [
              "Marcus_Noble_"
            ],
            "url": [
              "https://twitter.com/Marcus_Noble_",
              "https://marcusnoble.co.uk"
            ],
            "published": [
              "2008-05-11T09:51:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Some container"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Loving being able to attend #WiTNotts virtually tonight! 😁\n\nLooking forward to @paulienuh talk!",
          "html": "<div style=\"white-space: pre\">Loving being able to attend <a href=\"https://twitter.com/search?q=%23WiTNotts\">#WiTNotts</a> virtually tonight! 😁\n\nLooking forward to <a href=\"https://twitter.com/paulienuh\">@paulienuh</a> talk!</div>"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-04-02T20:38:26.767+02:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @Marcus_Noble_'s tweet"
    ],
    "like-of": [
      "https://twitter.com/Marcus_Noble_/status/1245771695807397895"
    ],
    "published": [
      "2020-04-02T20:38:26.767+02:00"
    ],
    "syndication": [
      "https://twitter.com/Marcus_Noble_/status/1245771695807397895"
    ]
  }
}
