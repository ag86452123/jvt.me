{
  "date" : "2020-04-22T07:49:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/HRHSherlock/status/1252015174422163456" ],
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "name" : [ "Reply to https://twitter.com/HRHSherlock/status/1252015174422163456" ],
    "published" : [ "2020-04-22T07:49:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "9/11 at the age of 7. As a child of an ex-pat/immigrant in Cairo, Egypt, working for a large American Bank, with a lot of other American friends, I remember knowing something was up when everyone at school was rushed home, but didn't really understand it"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/ajbat",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252015174422163456" ],
      "url" : [ "https://twitter.com/HRHSherlock/status/1252015174422163456" ],
      "published" : [ "2020-04-19T23:24:13+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:HRHSherlock" ],
          "numeric-id" : [ "3455669954" ],
          "name" : [ "Lee, friendship possum ♥️" ],
          "nickname" : [ "HRHSherlock" ],
          "url" : [ "https://twitter.com/HRHSherlock", "https://curiouscat.me/HRHSherlock" ],
          "published" : [ "2015-09-05T04:52:05+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "SW-3082-8414-8236 /Seattle, WA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1129118467179737089/0OdWeomA.png" ]
        }
      } ],
      "content" : [ {
        "value" : "What is the first *major* news event that you remember? And, optionally, what's your age?\n\nThe first major news event that I remember was the Oklahoma City Bombing, and I'm 34.",
        "html" : "<div style=\"white-space: pre\">What is the first *major* news event that you remember? And, optionally, what's your age?\n\nThe first major news event that I remember was the Oklahoma City Bombing, and I'm 34.</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
