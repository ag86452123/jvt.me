{
  "date": "2020-04-15T09:47:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1250346323062034433"
    ],
    "published": [
      "2020-04-15T09:47:00+01:00"
    ],
    "repost-of": [
      "https://twitter.com/jrg1990/status/1250105452790272005"
    ]
  },
  "kind": "reposts",
  "slug": "2020/04/kcqyd",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1250105452790272005"
      ],
      "url": [
        "https://twitter.com/jrg1990/status/1250105452790272005"
      ],
      "published": [
        "2020-04-14T16:55:40+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:jrg1990"
            ],
            "numeric-id": [
              "14083191"
            ],
            "name": [
              "James Gordon"
            ],
            "nickname": [
              "jrg1990"
            ],
            "url": [
              "https://twitter.com/jrg1990",
              "http://ntu-books.blogspot.com"
            ],
            "published": [
              "2008-03-05T14:19:29+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/955763625129664512/VmehM-tf.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "THREAD: You may have heard Priti Patel several nights ago at the daily briefing say that if you are unable to speak in an emergency to dial 999 and then press 55 and you’ll be directly connected to the Police.\n\nThis isn’t STRICTLY true\n\n￼As a 999 operator by trade let me explain",
          "html": "<div style=\"white-space: pre\">THREAD: You may have heard Priti Patel several nights ago at the daily briefing say that if you are unable to speak in an emergency to dial 999 and then press 55 and you’ll be directly connected to the Police.\n\nThis isn’t STRICTLY true\n\n￼As a 999 operator by trade let me explain</div>"
        }
      ]
    }
  },
  "aliases": null,
  "client_id": "https://indigenous.realize.be"
}
