{
  "date" : "2020-04-23T20:42:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/tesseralis/status/1253145945220378624" ],
    "name" : [ "Like of @tesseralis's tweet" ],
    "published" : [ "2020-04-23T20:42:00+01:00" ],
    "like-of" : [ "https://twitter.com/tesseralis/status/1253145945220378624" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/neuv4",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253145945220378624" ],
      "url" : [ "https://twitter.com/tesseralis/status/1253145945220378624" ],
      "published" : [ "2020-04-23T02:17:30+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:tesseralis" ],
          "numeric-id" : [ "471768380" ],
          "name" : [ "Nat Alison 🍑" ],
          "nickname" : [ "tesseralis" ],
          "url" : [ "https://twitter.com/tesseralis", "http://www.tessera.li", "http://Ko-fi.com/tesseralis" ],
          "published" : [ "2012-01-23T07:18:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Koda Island" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1231468297649979392/t_7jReNz.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "\"Nooooooo you have separate text using paragraphs and style them you can't use an html element it's bad separation of concerns\"\n\n\"Haha line break go <br>\"",
        "html" : "<div style=\"white-space: pre\">\"Nooooooo you have separate text using paragraphs and style them you can't use an html element it's bad separation of concerns\"\n\n\"Haha line break go &lt;br&gt;\"</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
