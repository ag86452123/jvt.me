{
  "date": "2020-04-18T00:05:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/rizbizkits/status/1251262482901086210"
    ],
    "name": [
      "Like of @rizbizkits's tweet"
    ],
    "published": [
      "2020-04-18T00:05:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/rizbizkits/status/1251262482901086210"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/axomk",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251262482901086210"
      ],
      "url": [
        "https://twitter.com/rizbizkits/status/1251262482901086210"
      ],
      "published": [
        "2020-04-17T21:33:17+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:rizbizkits"
            ],
            "numeric-id": [
              "800085538954878976"
            ],
            "name": [
              "riz"
            ],
            "nickname": [
              "rizbizkits"
            ],
            "url": [
              "https://twitter.com/rizbizkits",
              "https://www.rizwanakhan.com"
            ],
            "published": [
              "2016-11-19T21:17:12+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/915004957807513609/vN0bht32.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Today - Not feeling like an imposter as a Dev. Though there's a long way to go, past-me would be shook(!) at the amount of stuff I now seem to have pieced together (some) understanding of 💪\n\nCelebrating this rare joy! 😅\n\n#NuggetOfJoy",
          "html": "<div style=\"white-space: pre\">Today - Not feeling like an imposter as a Dev. Though there's a long way to go, past-me would be shook(!) at the amount of stuff I now seem to have pieced together (some) understanding of 💪\n\nCelebrating this rare joy! 😅\n\n<a href=\"https://twitter.com/search?q=%23NuggetOfJoy\">#NuggetOfJoy</a></div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
