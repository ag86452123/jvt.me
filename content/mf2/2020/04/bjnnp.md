{
  "date" : "2020-04-21T22:13:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/AtilaFassina/status/1252684107584282630" ],
    "name" : [ "Like of @AtilaFassina's tweet" ],
    "published" : [ "2020-04-21T22:13:00+01:00" ],
    "like-of" : [ "https://twitter.com/AtilaFassina/status/1252684107584282630" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/bjnnp",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252684107584282630" ],
      "url" : [ "https://twitter.com/AtilaFassina/status/1252684107584282630" ],
      "published" : [ "2020-04-21T19:42:19+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:AtilaFassina" ],
          "numeric-id" : [ "276089562" ],
          "name" : [ "Atila.io 🧉" ],
          "nickname" : [ "AtilaFassina" ],
          "url" : [ "https://twitter.com/AtilaFassina", "https://atila.io" ],
          "published" : [ "2011-04-02T16:39:11+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Berlin, 🇩🇪" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1188305541044428800/2-TdvwkO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Using Twitter and other social medias as your comment platform. Citing people and letting them know about it straight up. Webmentions are awesome!! ❤️✨\n\n#IndieWeb",
        "html" : "<div style=\"white-space: pre\">Using Twitter and other social medias as your comment platform. Citing people and letting them know about it straight up. Webmentions are awesome!! ❤️✨\n\n<a href=\"https://twitter.com/search?q=%23IndieWeb\">#IndieWeb</a></div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
