{
  "date": "2020-04-11T10:29:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/thattommyhall/status/1248679614337241089"
    ],
    "name": [
      "Like of @thattommyhall's tweet"
    ],
    "like-of": [
      "https://twitter.com/thattommyhall/status/1248679614337241089"
    ],
    "published": [
      "2020-04-11T10:29:00+01:00"
    ],
    "category": [
      "latex"
    ]
  },
  "tags": [
    "latex"
  ],
  "kind": "likes",
  "slug": "2020/04/u9wo1",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248679614337241089"
      ],
      "url": [
        "https://twitter.com/thattommyhall/status/1248679614337241089"
      ],
      "published": [
        "2020-04-10T18:29:54+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:thattommyhall"
            ],
            "numeric-id": [
              "14658601"
            ],
            "name": [
              "thattommyhall"
            ],
            "nickname": [
              "thattommyhall"
            ],
            "url": [
              "https://twitter.com/thattommyhall",
              "http://www.thattommyhall.com"
            ],
            "published": [
              "2008-05-05T12:03:00+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/925648122860199936/HZeOPXVn.jpg"
            ]
          }
        }
      ],
      "content": [
        "So nice to see this in print at last"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EVQzfaMX0AcTzX4.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
