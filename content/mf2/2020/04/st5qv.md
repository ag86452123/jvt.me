{
  "date": "2020-04-16T18:41:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/holly/status/1250810307016044544"
    ],
    "name": [
      "Like of @holly's tweet"
    ],
    "published": [
      "2020-04-16T18:41:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/holly/status/1250810307016044544"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/st5qv",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1250810307016044544"
      ],
      "url": [
        "https://twitter.com/holly/status/1250810307016044544"
      ],
      "published": [
        "2020-04-16T15:36:30+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:holly"
            ],
            "numeric-id": [
              "7555262"
            ],
            "name": [
              "Holly Brockwell"
            ],
            "nickname": [
              "holly"
            ],
            "url": [
              "https://twitter.com/holly",
              "https://www.instagram.com/hollybrocks/"
            ],
            "published": [
              "2007-07-18T10:27:16+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "From Nottingham, in London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg"
            ]
          }
        }
      ],
      "content": [
        "Having been on the dating apps for the last few months as a bisexual, I can officially confirm that men's behaviour is much, much, much worse."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
