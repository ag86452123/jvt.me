{
  "date": "2020-04-10T10:25:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/anna_hax/status/1248313540676591618"
    ],
    "name": [
      "Like of @anna_hax's tweet"
    ],
    "like-of": [
      "https://twitter.com/anna_hax/status/1248313540676591618"
    ],
    "published": [
      "2020-04-10T10:25:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/oo9z2",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248313540676591618"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1248313540676591618"
      ],
      "published": [
        "2020-04-09T18:15:15+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna 🏠"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Enjoying my first online #PHPMiNDS! Very interesting talk with @derickr on the latest goodies in #PHP 7.4, stand by for typed properties! 🙌🎉🐘",
          "html": "Enjoying my first online <a href=\"https://twitter.com/search?q=%23PHPMiNDS\">#PHPMiNDS</a>! Very interesting talk with <a href=\"https://twitter.com/derickr\">@derickr</a> on the latest goodies in <a href=\"https://twitter.com/search?q=%23PHP\">#PHP</a> 7.4, stand by for typed properties! 🙌🎉🐘"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
