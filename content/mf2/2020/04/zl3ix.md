{
  "date" : "2020-04-23T19:26:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/davecheney/status/1253201549779587073" ],
    "name" : [ "Like of @davecheney's tweet" ],
    "published" : [ "2020-04-23T19:26:00+01:00" ],
    "like-of" : [ "https://twitter.com/davecheney/status/1253201549779587073" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/zl3ix",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253201549779587073" ],
      "url" : [ "https://twitter.com/davecheney/status/1253201549779587073" ],
      "published" : [ "2020-04-23T05:58:27+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:davecheney" ],
          "numeric-id" : [ "699263" ],
          "name" : [ "Dave Cheney" ],
          "nickname" : [ "davecheney" ],
          "url" : [ "https://twitter.com/davecheney", "https://dave.cheney.net/" ],
          "published" : [ "2007-01-25T06:33:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Department of Australia" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1153046271906549760/q3yYqNNk.jpg" ]
        }
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EWREKmxU4AAFYjI.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
