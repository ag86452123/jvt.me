{
  "date" : "2020-04-21T18:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.jvt.me/events/personal-events/2020-04-fivbw/" ],
    "name" : [ "RSVP yes to https://www.jvt.me/events/personal-events/2020-04-fivbw/" ],
    "published" : [ "2020-04-21T18:00:00+01:00" ],
    "event" : {
      "start" : [ "2020-04-21T18:00:00+0100" ],
      "name" : [ "Escape Room" ],
      "end" : [ "2020-04-21T21:00:00+0100" ],
      "url" : [ "https://www.jvt.me/events/personal-events/2020-04-fivbw/" ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/04/xrqnd",
  "client_id" : "https://indigenous.realize.be"
}
