{
  "date" : "2020-04-20T21:31:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/Liv_Lanes/status/1252025429965312000" ],
    "name" : [ "Like of @Liv_Lanes's tweet" ],
    "published" : [ "2020-04-20T21:31:00+01:00" ],
    "like-of" : [ "https://twitter.com/Liv_Lanes/status/1252025429965312000" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/ufezl",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252025429965312000" ],
      "url" : [ "https://twitter.com/Liv_Lanes/status/1252025429965312000" ],
      "published" : [ "2020-04-20T00:04:58+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:Liv_Lanes" ],
          "numeric-id" : [ "359712021" ],
          "name" : [ "Olivia Lanes" ],
          "nickname" : [ "Liv_Lanes" ],
          "url" : [ "https://twitter.com/Liv_Lanes" ],
          "published" : [ "2011-08-22T01:50:37+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "An infinite Hilbert space " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/946557915468623872/_uUUlUj9.jpg" ]
        }
      } ],
      "content" : [ "I asked my boyfriend to edit the introduction of my thesis and he actually had the audacity to edit it and not just tell me it was excellent." ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
