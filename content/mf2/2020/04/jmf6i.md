{
  "date" : "2020-04-26T23:09:57.404+02:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/rwdrich/status/1254515969449512960" ],
    "name" : [ "Like of @rwdrich's tweet" ],
    "published" : [ "2020-04-26T23:09:57.404+02:00" ],
    "like-of" : [ "https://twitter.com/rwdrich/status/1254515969449512960" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/jmf6i",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254515969449512960" ],
      "url" : [ "https://twitter.com/rwdrich/status/1254515969449512960" ],
      "published" : [ "2020-04-26T21:01:29+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1254396465306370048" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:rwdrich" ],
          "numeric-id" : [ "107548386" ],
          "name" : [ "Richard Davies" ],
          "nickname" : [ "rwdrich" ],
          "url" : [ "https://twitter.com/rwdrich", "http://rwdrich.co.uk" ],
          "published" : [ "2010-01-22T23:14:30+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Cambridge, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/917481919872491521/BwypzjEE.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I'm too invested in tech, I misread shelves as servers and didnt bat an eye...",
        "html" : "I'm too invested in tech, I misread shelves as servers and didnt bat an eye...\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/anna_hax\"></a>"
      } ]
    }
  },
  "client_id" : "https://micropublish.net"
}
