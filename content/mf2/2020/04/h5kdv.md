{
  "date": "2020-04-17T17:17:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/CarolSaysThings/status/1251146798518829058"
    ],
    "name": [
      "Like of @CarolSaysThings's tweet"
    ],
    "published": [
      "2020-04-17T17:17:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/CarolSaysThings/status/1251146798518829058"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/h5kdv",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251146798518829058"
      ],
      "url": [
        "https://twitter.com/CarolSaysThings/status/1251146798518829058"
      ],
      "published": [
        "2020-04-17T13:53:36+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:CarolSaysThings"
            ],
            "numeric-id": [
              "36382927"
            ],
            "name": [
              "Carol ⚡️"
            ],
            "nickname": [
              "CarolSaysThings"
            ],
            "url": [
              "https://twitter.com/CarolSaysThings",
              "https://carolgilabert.me/"
            ],
            "published": [
              "2009-04-29T15:22:13+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "🇧🇷🇪🇸🇬🇧 · Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1238515159594917889/C5994QPa.jpg"
            ]
          }
        }
      ],
      "location": [
        {
          "type": [
            "h-card",
            "p-location"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:7d7bdec12d2549d4"
            ],
            "name": [
              "Nottingham, England"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Settling in for an afternoon of #ReactSummit learning 🤓",
          "html": "Settling in for an afternoon of <a href=\"https://twitter.com/search?q=%23ReactSummit\">#ReactSummit</a> learning 🤓"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EVz3XwTUEAMADj_.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
