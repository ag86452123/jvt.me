{
  "date" : "2020-04-29T12:11:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/HlessHman/status/1254776826867576832" ],
    "name" : [ "Like of @HlessHman's tweet" ],
    "published" : [ "2020-04-29T12:11:00+01:00" ],
    "like-of" : [ "https://twitter.com/HlessHman/status/1254776826867576832" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/eq3x8",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254776826867576832" ],
      "url" : [ "https://twitter.com/HlessHman/status/1254776826867576832" ],
      "published" : [ "2020-04-27T14:18:02+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:HlessHman" ],
          "numeric-id" : [ "2426250971" ],
          "name" : [ "Headless horse, man" ],
          "nickname" : [ "HlessHman" ],
          "url" : [ "https://twitter.com/HlessHman", "https://mobile.twitter.com/HlessHman/timelines/1163929128934027267" ],
          "published" : [ "2014-03-21T05:56:44+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1077997593131712513/8jW2SZ1Y.jpg" ]
        }
      } ],
      "content" : [ "When everyone is getting off the zoom call but you’re struggling to find the leave meeting button so then it’s just you and the host" ],
      "photo" : [ "https://pbs.twimg.com/media/EWnc3vfU0AY_qH9.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
