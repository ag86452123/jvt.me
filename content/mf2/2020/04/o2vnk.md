{
  "date" : "2020-04-25T21:47:31.678+02:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/monotron_/status/1254114954107670530" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1254136211352031232" ],
    "name" : [ "Reply to https://twitter.com/monotron_/status/1254114954107670530" ],
    "published" : [ "2020-04-25T21:47:31.678+02:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Got myself a <a href=\"https://www.dell.com/en-uk/shop/accessories/apd/210-aves\">Dell UltraSharp 27 4K USB-C Monitor: U2720Q</a> which is very nice! Got a much nicer colour quality compared to my other, older, 4K monitor. I'll share a photo of my new setup tomorrow"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/o2vnk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254114954107670530" ],
      "url" : [ "https://twitter.com/monotron_/status/1254114954107670530" ],
      "published" : [ "2020-04-25T18:28:00+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1254056392522174464" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:monotron_" ],
          "numeric-id" : [ "3399167086" ],
          "name" : [ "Toby" ],
          "nickname" : [ "monotron_" ],
          "url" : [ "https://twitter.com/monotron_", "https://github.com/monotron" ],
          "published" : [ "2015-08-01T19:13:59+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "United Kingdom" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1235696023093329920/4HP8BiRi.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "What’d you go for?",
        "html" : "What’d you go for?\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://monocle.p3k.io/"
}
