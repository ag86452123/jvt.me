{
  "date": "2020-04-17T14:02:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/technottingham/status/1251131879962611715"
    ],
    "name": [
      "Like of @technottingham's tweet"
    ],
    "published": [
      "2020-04-17T14:02:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/technottingham/status/1251131879962611715"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/xacxo",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251131879962611715"
      ],
      "url": [
        "https://twitter.com/technottingham/status/1251131879962611715"
      ],
      "published": [
        "2020-04-17T12:54:19+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:technottingham"
            ],
            "numeric-id": [
              "384492431"
            ],
            "name": [
              "Tech Nottingham"
            ],
            "nickname": [
              "technottingham"
            ],
            "url": [
              "https://twitter.com/technottingham",
              "http://technottingham.com"
            ],
            "published": [
              "2011-10-03T19:47:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "We had a dress rehearsal of Monday's #TechNott yesterday, and we're really excited for it! Hope to see you there on Monday for some remote fun 🍉\n\nAll the deets are on:\nnott.tech/tn-april",
          "html": "<div style=\"white-space: pre\">We had a dress rehearsal of Monday's <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> yesterday, and we're really excited for it! Hope to see you there on Monday for some remote fun 🍉\n\nAll the deets are on:\n<a href=\"https://nott.tech/tn-april\">nott.tech/tn-april</a></div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EVzpz7kWkAAXoaX.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
