{
  "date" : "2020-04-19T10:52:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/edent/status/1251789962598584322" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1254060253878067200" ],
    "name" : [ "Reply to https://twitter.com/edent/status/1251789962598584322" ],
    "published" : [ "2020-04-19T10:52:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "What sort of binary file? I remember in uni digging into ARM assembly, and the structure of those, but that may not help!"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/wsazz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1251789962598584322" ],
      "url" : [ "https://twitter.com/edent/status/1251789962598584322" ],
      "published" : [ "2020-04-19T08:29:18+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:edent" ],
          "numeric-id" : [ "14054507" ],
          "name" : [ "Terence Eden" ],
          "nickname" : [ "edent" ],
          "url" : [ "https://twitter.com/edent", "https://shkspr.mobi/blog/", "https://edent.tel" ],
          "published" : [ "2008-02-28T13:10:25+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, UK" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1228067445153452033/_A8Uq2VY.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "I'm doing something I've not done since university - trying to understand the header structure of a binary file.\nIt amazes me how poor the RFC documentation is. I'm looking through some rando's open source project just to make sense of it.\n\nI'm not convinced this is relaxing!",
        "html" : "<div style=\"white-space: pre\">I'm doing something I've not done since university - trying to understand the header structure of a binary file.\nIt amazes me how poor the RFC documentation is. I'm looking through some rando's open source project just to make sense of it.\n\nI'm not convinced this is relaxing!</div>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
