{
  "date" : "2020-04-27T18:28:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/holly/status/1254750704213860352" ],
    "name" : [ "Like of @holly's tweet" ],
    "published" : [ "2020-04-27T18:28:00+01:00" ],
    "like-of" : [ "https://twitter.com/holly/status/1254750704213860352" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/zs3p2",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254750704213860352" ],
      "url" : [ "https://twitter.com/holly/status/1254750704213860352" ],
      "published" : [ "2020-04-27T12:34:14+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:holly" ],
          "numeric-id" : [ "7555262" ],
          "name" : [ "Holly Brockwell" ],
          "nickname" : [ "holly" ],
          "url" : [ "https://twitter.com/holly", "https://www.instagram.com/hollybrocks/" ],
          "published" : [ "2007-07-18T10:27:16+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "From Nottingham, in London" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1248671410714804225/A_9KJ1y8.jpg" ]
        }
      } ],
      "content" : [ "To all the people who say \"I don't know any men who act like the ones you describe\"" ],
      "photo" : [ "https://pbs.twimg.com/media/EWnE-RRXgAAtPr4.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
