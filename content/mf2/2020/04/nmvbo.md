{
  "date": "2020-04-08T17:22:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/KingstonTime/status/1247921783807901697"
    ],
    "name": [
      "Like of @KingstonTime's tweet"
    ],
    "like-of": [
      "https://twitter.com/KingstonTime/status/1247921783807901697"
    ],
    "published": [
      "2020-04-08T17:22:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/nmvbo",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1247921783807901697"
      ],
      "url": [
        "https://twitter.com/KingstonTime/status/1247921783807901697"
      ],
      "published": [
        "2020-04-08T16:18:33+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:KingstonTime"
            ],
            "numeric-id": [
              "38462153"
            ],
            "name": [
              "Jonathan Kingston"
            ],
            "nickname": [
              "KingstonTime"
            ],
            "url": [
              "https://twitter.com/KingstonTime",
              "https://deli.direct/"
            ],
            "published": [
              "2009-05-07T16:24:41+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1063622329706651648/lpgkGE_Y.jpg"
            ]
          }
        }
      ],
      "content": [
        "Biggest weight lifted from my shoulders, I accepted an offer today. I think the single most stressful experience of my life is over, time to join y'all in worrying about the main event."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be"
}
