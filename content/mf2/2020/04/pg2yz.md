{
  "date" : "2020-04-27T20:21:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/artbymoga/status/1253110386376626176" ],
    "name" : [ "Like of @artbymoga's tweet" ],
    "published" : [ "2020-04-27T20:21:00+01:00" ],
    "like-of" : [ "https://twitter.com/artbymoga/status/1253110386376626176" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/pg2yz",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253110386376626176" ],
      "url" : [ "https://twitter.com/artbymoga/status/1253110386376626176" ],
      "published" : [ "2020-04-22T23:56:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:artbymoga" ],
          "numeric-id" : [ "883425723511209984" ],
          "name" : [ "☕️🌿🐻Meg Adams🐻🌿☕️" ],
          "nickname" : [ "artbymoga" ],
          "url" : [ "https://twitter.com/artbymoga", "http://www.artbymoga.com/shop", "https://tapas.io/series/artbymoga" ],
          "published" : [ "2017-07-07T20:41:20+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Seattle, WA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1212850136818176000/ZHk4kaVn.jpg" ]
        }
      } ],
      "content" : [ "That’s Quarantine, Baby!" ],
      "photo" : [ "https://pbs.twimg.com/media/EWPxQMrVAAUSFkZ.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
