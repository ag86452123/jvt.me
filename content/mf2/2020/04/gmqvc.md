{
  "date": "2020-04-11T18:51:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1249033508003229697"
    ],
    "in-reply-to": [
      "https://twitter.com/Marcus_Noble_/status/1249009397793263617"
    ],
    "name": [
      "Reply to https://twitter.com/Marcus_Noble_/status/1249009397793263617"
    ],
    "published": [
      "2020-04-11T18:51:00+01:00"
    ],
    "content": [
      {
        "html": "",
        "value": "Can't comment on the Kubernetes angle, but I've been using https://hetzner.cloud and have had good experiences - fast, competitive price and no issues with unexpected downtime 👍🏽"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/gmqvc",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249009397793263617"
      ],
      "url": [
        "https://twitter.com/Marcus_Noble_/status/1249009397793263617"
      ],
      "published": [
        "2020-04-11T16:20:20+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Marcus_Noble_"
            ],
            "numeric-id": [
              "14732545"
            ],
            "name": [
              "Marcus Noble"
            ],
            "nickname": [
              "Marcus_Noble_"
            ],
            "url": [
              "https://twitter.com/Marcus_Noble_",
              "https://marcusnoble.co.uk"
            ],
            "published": [
              "2008-05-11T09:51:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Some container"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/776738772759277569/hfaM5zhA.jpg"
            ]
          }
        }
      ],
      "content": [
        "Who are y'all with for your personal kubernetes clusters?"
      ]
    }
  },
  "tags": null,
  "client_id": "https://indigenous.realize.be"
}
