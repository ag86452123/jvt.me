{
  "date" : "2020-04-23T20:44:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/HannahGraceLong/status/1253054494662955008" ],
    "name" : [ "Like of @HannahGraceLong's tweet" ],
    "published" : [ "2020-04-23T20:44:00+01:00" ],
    "like-of" : [ "https://twitter.com/HannahGraceLong/status/1253054494662955008" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/swa3u",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253054494662955008" ],
      "url" : [ "https://twitter.com/HannahGraceLong/status/1253054494662955008" ],
      "published" : [ "2020-04-22T20:14:06+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:HannahGraceLong" ],
          "numeric-id" : [ "517093176" ],
          "name" : [ "Hannah Long" ],
          "nickname" : [ "HannahGraceLong" ],
          "url" : [ "https://twitter.com/HannahGraceLong", "http://www.longish95.blogspot.com/" ],
          "published" : [ "2012-03-07T01:40:29+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Brooklyn, NY & Appalachian VA" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1083787344644395011/9XVAdmDq.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "How I used to begin work emails: Hi!\n\nHow I begin work emails now:",
        "html" : "<div style=\"white-space: pre\">How I used to begin work emails: Hi!\n\nHow I begin work emails now:</div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EWO-WKrXYAAwNCN.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
