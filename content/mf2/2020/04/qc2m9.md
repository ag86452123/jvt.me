{
  "date" : "2020-04-19T19:05:44+0100",
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://www.facebook.com/events/233892091000144/" ],
    "name" : [ "RSVP yes to https://www.facebook.com/events/233892091000144/" ],
    "published" : [ "2020-04-19T19:05:44+0100" ],
    "event" : {
      "url" : [ "https://www.facebook.com/events/233892091000144/" ],
      "name" : [ "The Big Quiz for Cancer Research UK - Live!" ],
      "start" : [ "2020-04-19T20:00:00+0100" ],
      "end" : [ "2020-04-19T21:00:00+0100" ],
      "location" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "name" : [ "Online" ]
        },
        "lang" : "en",
        "value" : "Online"
      } ]
    },
    "rsvp" : [ "yes" ]
  },
  "kind" : "rsvps",
  "slug" : "2020/04/qc2m9"
}
