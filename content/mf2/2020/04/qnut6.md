{
  "date" : "2020-04-20T21:32:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/SparkNotes/status/1252313825132646400" ],
    "name" : [ "Like of @SparkNotes's tweet" ],
    "published" : [ "2020-04-20T21:32:00+01:00" ],
    "like-of" : [ "https://twitter.com/SparkNotes/status/1252313825132646400" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/qnut6",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252313825132646400" ],
      "url" : [ "https://twitter.com/SparkNotes/status/1252313825132646400" ],
      "published" : [ "2020-04-20T19:10:57+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:SparkNotes" ],
          "numeric-id" : [ "14397399" ],
          "name" : [ "SparkNotes" ],
          "nickname" : [ "SparkNotes" ],
          "url" : [ "https://twitter.com/SparkNotes", "http://www.sparknotes.com/", "http://SparkNotes.com" ],
          "published" : [ "2008-04-15T15:13:15+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "New York, NY" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1035232838394757120/e3ZJhTk1.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Fun ways to end a Zoom call:\n\n- Farewell, my lords.\n- Away! thou'rt a knave.\n- Parting is such sweet sorrow, that I shall say good night till it be morrow.\n- [EXIT, PURSUED BY A BEAR.]",
        "html" : "<div style=\"white-space: pre\">Fun ways to end a Zoom call:\n\n- Farewell, my lords.\n- Away! thou'rt a knave.\n- Parting is such sweet sorrow, that I shall say good night till it be morrow.\n- [EXIT, PURSUED BY A BEAR.]</div>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
