{
  "date": "2020-04-11T19:47:00+01:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/VickyChandler/status/1248332902087213059"
    ],
    "name": [
      "Like of @VickyChandler's tweet"
    ],
    "like-of": [
      "https://twitter.com/VickyChandler/status/1248332902087213059"
    ],
    "published": [
      "2020-04-11T19:47:00+01:00"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/klhsl",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1248332902087213059"
      ],
      "url": [
        "https://twitter.com/VickyChandler/status/1248332902087213059"
      ],
      "published": [
        "2020-04-09T19:32:11+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:VickyChandler"
            ],
            "numeric-id": [
              "22354785"
            ],
            "name": [
              "Vicky Chandler"
            ],
            "nickname": [
              "VickyChandler"
            ],
            "url": [
              "https://twitter.com/VickyChandler",
              "http://www.instagram.com/vickychandler"
            ],
            "published": [
              "2009-03-01T11:31:26+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "SE London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1173706529683513348/JtoZ7ZEh.jpg"
            ]
          }
        }
      ],
      "content": [
        "Today I delivered a food parcel to a flat which is in the old Terry’s chocolate factory and the fountain outside was a giant chocolate orange"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EVL4JqNXgAEL9lU.jpg"
      ]
    }
  },
  "tags": null,
  "client_id": "https://indigenous.realize.be"
}
