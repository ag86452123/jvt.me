{
  "date": "2020-04-16T18:40:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "in-reply-to": [
      "https://twitter.com/kevinmarks/status/1250832684953239553"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1250842766558597120"
    ],
    "name": [
      "Reply to https://twitter.com/kevinmarks/status/1250832684953239553"
    ],
    "published": [
      "2020-04-16T18:40:00+01:00"
    ],
    "category": [

    ],
    "content": [
      {
        "html": "",
        "value": "I definitely started to over use it after finding out about it last year https://www.jvt.me/posts/2019/05/19/html-toggle/ but it is a pretty nifty thing!"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/tjnsu",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1250832684953239553"
      ],
      "url": [
        "https://twitter.com/kevinmarks/status/1250832684953239553"
      ],
      "published": [
        "2020-04-16T17:05:26+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/edent/status/1250831352301314050"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:kevinmarks"
            ],
            "numeric-id": [
              "57203"
            ],
            "name": [
              "Kevin Marks"
            ],
            "nickname": [
              "kevinmarks"
            ],
            "url": [
              "https://twitter.com/kevinmarks",
              "http://kevinmarks.com",
              "http://known.kevinmarks.com"
            ],
            "published": [
              "2006-12-11T11:43:36+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nunthorpe, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/826300294136868864/bB22s4pc.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I'm not sure what's obscgure, but i like <details><summary> for expandable things",
          "html": "I'm not sure what's obscgure, but i like &lt;details&gt;&lt;summary&gt; for expandable things\n<a class=\"u-mention\" href=\"https://twitter.com/edent\"></a>"
        }
      ]
    }
  },
  "tags": [

  ],
  "client_id": "https://indigenous.realize.be"
}
