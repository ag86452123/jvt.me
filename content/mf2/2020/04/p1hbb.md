{
  "date" : "2020-04-27T14:00:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/davecheney/status/1254531110085640192" ],
    "name" : [ "Like of @davecheney's tweet" ],
    "published" : [ "2020-04-27T14:00:00+01:00" ],
    "like-of" : [ "https://twitter.com/davecheney/status/1254531110085640192" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/p1hbb",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254531110085640192" ],
      "url" : [ "https://twitter.com/davecheney/status/1254531110085640192" ],
      "published" : [ "2020-04-26T22:01:39+00:00" ],
      "in-reply-to" : [ "https://twitter.com/mschoening/status/1254437013782007813" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:davecheney" ],
          "numeric-id" : [ "699263" ],
          "name" : [ "Dave Cheney" ],
          "nickname" : [ "davecheney" ],
          "url" : [ "https://twitter.com/davecheney", "https://dave.cheney.net/" ],
          "published" : [ "2007-01-25T06:33:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Department of Australia" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1153046271906549760/q3yYqNNk.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Loot crates",
        "html" : "Loot crates\n<a class=\"u-mention\" href=\"https://twitter.com/mschoening\"></a>"
      } ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
