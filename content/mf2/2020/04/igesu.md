{
  "date": "2020-04-14T22:06:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/SparkNotes/status/1250129187622567937"
    ],
    "name": [
      "Like of @SparkNotes's tweet"
    ],
    "published": [
      "2020-04-14T22:06:00+01:00"
    ],
    "like-of": [
      "https://twitter.com/SparkNotes/status/1250129187622567937"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/igesu",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1250129187622567937"
      ],
      "url": [
        "https://twitter.com/SparkNotes/status/1250129187622567937"
      ],
      "published": [
        "2020-04-14T18:29:59+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:SparkNotes"
            ],
            "numeric-id": [
              "14397399"
            ],
            "name": [
              "SparkNotes"
            ],
            "nickname": [
              "SparkNotes"
            ],
            "url": [
              "https://twitter.com/SparkNotes",
              "http://www.sparknotes.com/",
              "http://SparkNotes.com"
            ],
            "published": [
              "2008-04-15T15:13:15+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "New York, NY"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1035232838394757120/e3ZJhTk1.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "CALLING A LYFT\n- expensive\n\nHITCHING A RIDE WITH CHARON, FERRYMAN OF THE DEAD\n- only costs one coin\n- sure there's just one destination but still, what a deal",
          "html": "<div style=\"white-space: pre\">CALLING A LYFT\n- expensive\n\nHITCHING A RIDE WITH CHARON, FERRYMAN OF THE DEAD\n- only costs one coin\n- sure there's just one destination but still, what a deal</div>"
        }
      ]
    }
  },
  "aliases": null,
  "client_id": "https://indigenous.realize.be"
}
