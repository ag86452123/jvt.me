{
  "date" : "2020-04-19T11:17:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/LewisPugh/status/1251478981142097920" ],
    "name" : [ "Like of @LewisPugh's tweet" ],
    "published" : [ "2020-04-19T11:17:00+01:00" ],
    "category" : [ "cute" ],
    "like-of" : [ "https://twitter.com/LewisPugh/status/1251478981142097920" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/p8cgh",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1251478981142097920" ],
      "url" : [ "https://twitter.com/LewisPugh/status/1251478981142097920" ],
      "video" : [ "https://video.twimg.com/ext_tw_video/1251190558338531328/pu/vid/640x352/eznGD3TQ6Yt40sNI.mp4?tag=10" ],
      "published" : [ "2020-04-18T11:53:35+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:LewisPugh" ],
          "numeric-id" : [ "130499946" ],
          "name" : [ "Lewis Pugh" ],
          "nickname" : [ "LewisPugh" ],
          "url" : [ "https://twitter.com/LewisPugh", "http://www.lewispugh.com" ],
          "published" : [ "2010-04-07T13:25:50+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "South Atlantic " ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1216059006499860480/4gt_by9I.jpg" ]
        }
      } ],
      "content" : [ "BREAKING NEWS: Penguins taking back control in Cape Town. 🐧😃 " ]
    }
  },
  "tags" : [ "cute" ],
  "client_id" : "https://indigenous.realize.be"
}
