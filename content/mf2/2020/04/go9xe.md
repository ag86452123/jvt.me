{
  "date": "2020-04-12T11:59:24.752+02:00",
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/JamieTanna/status/1249277216220987394"
    ],
    "in-reply-to": [
      "https://twitter.com/fwilhe/status/1249268952666120193"
    ],
    "name": [
      "Reply to https://twitter.com/fwilhe/status/1249268952666120193"
    ],
    "published": [
      "2020-04-12T11:59:24.752+02:00"
    ],
    "content": [
      {
        "html": "",
        "value": "I followed the instructions in https://formulae.brew.sh/formula/coreutils which prompts you to update your `PATH`: `PATH=\"$(brew --prefix)/opt/coreutils/libexec/gnubin:$PATH\"` which worked wonders, and I now no longer need to prefix with `g`"
      }
    ]
  },
  "kind": "replies",
  "slug": "2020/04/go9xe",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1249268952666120193"
      ],
      "url": [
        "https://twitter.com/fwilhe/status/1249268952666120193"
      ],
      "published": [
        "2020-04-12T09:31:43+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1249028764044070912"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:fwilhe"
            ],
            "numeric-id": [
              "3404884084"
            ],
            "name": [
              "Florian Wilhelm 🇪🇺"
            ],
            "nickname": [
              "fwilhe"
            ],
            "url": [
              "https://twitter.com/fwilhe",
              "https://github.com/fwilhe"
            ],
            "published": [
              "2015-08-05T22:15:13+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Potsdam, Germany"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/943553031903350785/q_BhRXC9.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Is there a convenient way to replace bsd tools with gnu ones? I know some are available with a \"g\" prefix, do you use aliases then?",
          "html": "Is there a convenient way to replace bsd tools with gnu ones? I know some are available with a \"g\" prefix, do you use aliases then?\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/SysCast_Online\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/jpmens\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/mattiasgeniar\"></a>"
        }
      ]
    }
  },
  "tags": null,
  "client_id": "https://micropublish.net"
}
