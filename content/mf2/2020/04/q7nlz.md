{
  "date": "2020-04-17T22:25:00+01:00",
  "deleted": false,
  "h": "h-entry",
  "properties": {
    "syndication": [
      "https://twitter.com/HamillHimself/status/1251243529533153280"
    ],
    "name": [
      "Like of @HamillHimself's tweet"
    ],
    "published": [
      "2020-04-17T22:25:00+01:00"
    ],
    "category": [
      "politics"
    ],
    "like-of": [
      "https://twitter.com/HamillHimself/status/1251243529533153280"
    ]
  },
  "kind": "likes",
  "slug": "2020/04/q7nlz",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1251243529533153280"
      ],
      "url": [
        "https://twitter.com/HamillHimself/status/1251243529533153280"
      ],
      "published": [
        "2020-04-17T20:17:59+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:HamillHimself"
            ],
            "numeric-id": [
              "304679484"
            ],
            "name": [
              "Mark Hamill"
            ],
            "nickname": [
              "HamillHimself"
            ],
            "url": [
              "https://twitter.com/HamillHimself"
            ],
            "published": [
              "2011-05-24T22:52:49+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1212329411992682496/fBMvNTBp.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Last month was the first March without a school shooting  in 18 years.                                                                              \nThat's it. That's the tweet.",
          "html": "<div style=\"white-space: pre\">Last month was the first March without a school shooting  in 18 years.                                                                              \nThat's it. That's the tweet.</div>"
        }
      ]
    }
  },
  "tags": [
    "politics"
  ],
  "client_id": "https://indigenous.realize.be"
}
