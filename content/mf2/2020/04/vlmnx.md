{
  "date" : "2020-04-20T11:03:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/JamieTanna/status/1252177273740054529" ],
    "published" : [ "2020-04-20T11:03:00+01:00" ],
    "repost-of" : [ "https://twitter.com/technottingham/status/1252146897948684288" ],
    "category" : [ "tech-nottingham" ],
    "content" : [ {
      "html" : "",
      "value" : "Really looking forward to <a href=\"/tags/tech-nottingham/\">#TechNott</a> tonight which sounds like it's going to have some very timely advice from <span class=\"h-card\"><a class=\"u-url\" href=\"https://twitter.com/efinlay24\">@efinlay24</a></span>"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2020/04/vlmnx",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252146897948684288" ],
      "url" : [ "https://twitter.com/technottingham/status/1252146897948684288" ],
      "published" : [ "2020-04-20T08:07:38+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:technottingham" ],
          "numeric-id" : [ "384492431" ],
          "name" : [ "Tech Nottingham" ],
          "nickname" : [ "technottingham" ],
          "url" : [ "https://twitter.com/technottingham", "http://technottingham.com" ],
          "published" : [ "2011-10-03T19:47:31+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Nottingham" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "🚨 TONIGHT! 🚨\n\nJoin us this evening, from the comfort of your own home, for April's #TechNott 🍉\n\nWe have @efinlay24 speaking about the very relevant topic of building effective distributed teams. It's gonna be great!\n\nMore details:\nnott.tech/tn-april",
        "html" : "<div style=\"white-space: pre\">🚨 TONIGHT! 🚨\n\nJoin us this evening, from the comfort of your own home, for April's <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> 🍉\n\nWe have <a href=\"https://twitter.com/efinlay24\">@efinlay24</a> speaking about the very relevant topic of building effective distributed teams. It's gonna be great!\n\nMore details:\n<a href=\"http://nott.tech/tn-april\">nott.tech/tn-april</a></div>"
      } ],
      "photo" : [ "https://pbs.twimg.com/media/EWCE9ubWsAAXRFJ.jpg" ]
    }
  },
  "tags" : [ "tech-nottingham" ],
  "client_id" : "https://indigenous.realize.be"
}
