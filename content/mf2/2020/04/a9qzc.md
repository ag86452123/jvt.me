{
  "date" : "2020-04-18T20:10:34.098+02:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/MrAndrew/status/1251573613016297472" ],
    "name" : [ "Like of @MrAndrew's tweet" ],
    "published" : [ "2020-04-18T20:10:34.098+02:00" ],
    "like-of" : [ "https://twitter.com/MrAndrew/status/1251573613016297472" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/a9qzc",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1251573613016297472" ],
      "url" : [ "https://twitter.com/MrAndrew/status/1251573613016297472" ],
      "published" : [ "2020-04-18T18:09:37+00:00" ],
      "in-reply-to" : [ "https://twitter.com/anna_hax/status/1251562189774827523" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:MrAndrew" ],
          "numeric-id" : [ "9626182" ],
          "name" : [ "Andrew Seward" ],
          "nickname" : [ "MrAndrew" ],
          "url" : [ "https://twitter.com/MrAndrew", "http://www.technottingham.com" ],
          "published" : [ "2007-10-23T15:57:18+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Riddings, Derbyshire" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1246456234452140032/romkM0EV.jpg" ]
        }
      } ],
      "location" : [ {
        "type" : [ "h-card", "p-location" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:50f2d0272381533f" ],
          "name" : [ "East Midlands, England" ]
        }
      } ],
      "content" : [ {
        "value" : "Twanner works on an eeeeeeventual consistency model",
        "html" : "Twanner works on an eeeeeeventual consistency model\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/anna_hax\"></a>"
      } ]
    }
  },
  "client_id" : "https://micropublish.net"
}
