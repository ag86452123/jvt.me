{
  "date" : "2020-04-20T09:38:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/saronyitbarek/status/1252038372484358145" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1252155827710038017" ],
    "name" : [ "Reply to https://twitter.com/saronyitbarek/status/1252038372484358145" ],
    "published" : [ "2020-04-20T09:38:00+01:00" ],
    "category" : [ "indieweb" ],
    "content" : [ {
      "html" : "",
      "value" : "I've been doing a lot of <a href=\"/tags/indieweb/\">#IndieWeb</a> things recently which allow me to own my data a lot better - such as this tweet which is actually published to my site first, then gets syndicated to Twitter - been fun for many reasons"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/jowbi",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252038372484358145" ],
      "url" : [ "https://twitter.com/saronyitbarek/status/1252038372484358145" ],
      "published" : [ "2020-04-20T00:56:24+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:saronyitbarek" ],
          "numeric-id" : [ "158065720" ],
          "name" : [ "Saron" ],
          "nickname" : [ "saronyitbarek" ],
          "url" : [ "https://twitter.com/saronyitbarek", "http://saron.io" ],
          "published" : [ "2010-06-21T17:48:50+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "NYC" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1057577163300364290/XxPn2DSb.jpg" ]
        }
      } ],
      "content" : [ "When/if you code for fun, what do you build?" ]
    }
  },
  "tags" : [ "indieweb" ],
  "client_id" : "https://indigenous.realize.be"
}
