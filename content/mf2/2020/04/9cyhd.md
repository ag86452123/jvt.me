{
  "date" : "2020-04-27T20:09:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://twitter.com/galsteve/status/1254848672652840960" ],
    "syndication" : [ "https://twitter.com/JamieTanna/status/1254851673668927489" ],
    "name" : [ "Reply to https://twitter.com/galsteve/status/1254848672652840960" ],
    "published" : [ "2020-04-27T20:09:00+01:00" ],
    "category" : [ ],
    "content" : [ {
      "html" : "",
      "value" : "Noise cancelling headphones are wonderful things! Doesn't help for those hearing them on the microphone though 😅"
    } ]
  },
  "kind" : "replies",
  "slug" : "2020/04/9cyhd",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1254848672652840960" ],
      "url" : [ "https://twitter.com/galsteve/status/1254848672652840960" ],
      "published" : [ "2020-04-27T19:03:32+00:00" ],
      "in-reply-to" : [ "https://twitter.com/JamieTanna/status/1254396465306370048" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:galsteve" ],
          "numeric-id" : [ "240412871" ],
          "name" : [ "Stephen Galbraith" ],
          "nickname" : [ "galsteve" ],
          "url" : [ "https://twitter.com/galsteve" ],
          "published" : [ "2011-01-19T21:42:25+00:00" ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1220272263/image.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "But how do you both manage to hear over the clacks from those mechanical keyboards ???",
        "html" : "But how do you both manage to hear over the clacks from those mechanical keyboards ???\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/anna_hax\"></a>"
      } ]
    }
  },
  "tags" : [ ],
  "client_id" : "https://indigenous.realize.be"
}
