{
  "kind": "likes",
  "slug": "2020/04/vcdvg",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1246294565205630976"
      ],
      "url": [
        "https://twitter.com/joshgad/status/1246294565205630976"
      ],
      "published": [
        "2020-04-04T04:32:34+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:joshgad"
            ],
            "numeric-id": [
              "40100270"
            ],
            "name": [
              "Josh Gad"
            ],
            "nickname": [
              "joshgad"
            ],
            "url": [
              "https://twitter.com/joshgad",
              "https://www.facebook.com/joshgad"
            ],
            "published": [
              "2009-05-14T22:13:49+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Here"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1240120728332177409/wghSX4xG.jpg"
            ]
          }
        }
      ],
      "content": [
        "Ok. Whoever did this is a genius."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUu6SvWU0AU0tqr.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-04T22:12:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @joshgad's tweet"
    ],
    "like-of": [
      "https://twitter.com/joshgad/status/1246294565205630976"
    ],
    "published": [
      "2020-04-04T22:12:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/joshgad/status/1246294565205630976"
    ]
  }
}
