{
  "date" : "2020-04-20T20:12:08.559+02:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/apaleslimghost/status/1252192025702797312" ],
    "name" : [ "Like of @apaleslimghost's tweet" ],
    "published" : [ "2020-04-20T20:12:08.559+02:00" ],
    "like-of" : [ "https://twitter.com/apaleslimghost/status/1252192025702797312" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/g2vlk",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252192025702797312" ],
      "url" : [ "https://twitter.com/apaleslimghost/status/1252192025702797312" ],
      "published" : [ "2020-04-20T11:06:58+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:apaleslimghost" ],
          "numeric-id" : [ "20815869" ],
          "name" : [ "🏳️‍⚧️ a pale slim ghost" ],
          "nickname" : [ "apaleslimghost" ],
          "url" : [ "https://twitter.com/apaleslimghost", "http://ghost.computer", "http://ft.com", "http://pronoun.is/they?or=she" ],
          "published" : [ "2009-02-13T23:05:14+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "london" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1240757244188332033/c5ioNRcz.jpg" ]
        }
      } ],
      "content" : [ "\"this will notify 13 people in 7 timezones. are you sure?\"" ],
      "photo" : [ "https://pbs.twimg.com/media/EWCuAUaXQAEOQJG.png" ]
    }
  },
  "client_id" : "https://micropublish.net"
}
