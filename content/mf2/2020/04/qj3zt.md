{
  "date" : "2020-04-22T14:51:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2020-04-22T14:51:00+01:00" ],
    "repost-of" : [ "https://twitter.com/SparkleClass/status/1252949326629933059" ],
    "category" : [ "oggcamp", "wit-notts" ],
    "content" : [ {
      "html" : "",
      "value" : "I'm very much looking forward to this talk, it was awesome when I saw it at <a href=\"/tags/oggcamp/\">#oggcamp</a> last year, and <a href=\"/tags/wit-notts/\">#WiTNotts</a> is very fortunate to have @sparkleclass.com speak!"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2020/04/qj3zt",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1252949326629933059" ],
      "url" : [ "https://twitter.com/SparkleClass/status/1252949326629933059" ],
      "published" : [ "2020-04-22T13:16:12+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:SparkleClass" ],
          "numeric-id" : [ "928764030688296960" ],
          "name" : [ "Rachel Morgan-Trimmer" ],
          "nickname" : [ "SparkleClass" ],
          "url" : [ "https://twitter.com/SparkleClass", "http://www.sparkleclass.com" ],
          "published" : [ "2017-11-09T23:19:35+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "Manchester, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1235914325514125313/XnCY3M1M.jpg" ]
        }
      } ],
      "content" : [ {
        "value" : "Today I am mostly working on revising my talk \"The Power of Change: Learning to Live as a Weirdo\" so it works better as an online presentation.\n\nSign up here: meetup.com/Women-In-Tech-…\n\nOn Zoom, obvs. For @WiT_Notts on 7 May.",
        "html" : "<div style=\"white-space: pre\">Today I am mostly working on revising my talk \"The Power of Change: Learning to Live as a Weirdo\" so it works better as an online presentation.\n\nSign up here: <a href=\"https://www.meetup.com/Women-In-Tech-Nottingham/events/268711710/\">meetup.com/Women-In-Tech-…</a>\n\nOn Zoom, obvs. For <a href=\"https://twitter.com/WiT_Notts\">@WiT_Notts</a> on 7 May.</div>"
      } ]
    }
  },
  "tags" : [ "oggcamp", "wit-notts" ],
  "client_id" : "https://indigenous.realize.be"
}
