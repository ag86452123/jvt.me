{
  "date" : "2020-04-24T23:30:00+01:00",
  "deleted" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://twitter.com/TashJNorris/status/1253780845959282688" ],
    "name" : [ "Like of @TashJNorris's tweet" ],
    "published" : [ "2020-04-24T23:30:00+01:00" ],
    "like-of" : [ "https://twitter.com/TashJNorris/status/1253780845959282688" ]
  },
  "kind" : "likes",
  "slug" : "2020/04/e9uia",
  "context" : {
    "type" : [ "h-entry" ],
    "properties" : {
      "uid" : [ "tag:twitter.com:1253780845959282688" ],
      "url" : [ "https://twitter.com/TashJNorris/status/1253780845959282688" ],
      "published" : [ "2020-04-24T20:20:22+00:00" ],
      "author" : [ {
        "type" : [ "h-card" ],
        "properties" : {
          "uid" : [ "tag:twitter.com:TashJNorris" ],
          "numeric-id" : [ "19965271" ],
          "name" : [ "Tash Norris" ],
          "nickname" : [ "TashJNorris" ],
          "url" : [ "https://twitter.com/TashJNorris", "https://medium.com/@tashjnorris" ],
          "published" : [ "2009-02-03T11:25:57+00:00" ],
          "location" : [ {
            "type" : [ "h-card", "p-location" ],
            "properties" : {
              "name" : [ "London, England" ]
            }
          } ],
          "photo" : [ "https://pbs.twimg.com/profile_images/1084014099510558720/_2lwD_ZS.jpg" ]
        }
      } ],
      "content" : [ "Our CTO turned himself into a pickle during the tech LT meeting today and couldn’t switch it off and I could not hold it together." ],
      "photo" : [ "https://pbs.twimg.com/media/EWZTCA9XQAISm7x.jpg" ]
    }
  },
  "client_id" : "https://indigenous.realize.be"
}
