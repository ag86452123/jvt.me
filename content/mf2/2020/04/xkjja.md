{
  "kind": "likes",
  "slug": "2020/04/xkjja",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1245830584150024193"
      ],
      "url": [
        "https://twitter.com/Cromerty/status/1245830584150024193"
      ],
      "published": [
        "2020-04-02T21:48:52+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:Cromerty"
            ],
            "numeric-id": [
              "45835391"
            ],
            "name": [
              "Cromerty York - Voiceover & Voice Actor🎙️"
            ],
            "nickname": [
              "Cromerty"
            ],
            "url": [
              "https://twitter.com/Cromerty",
              "https://alteregovoices.com",
              "http://alteregovoices.com"
            ],
            "published": [
              "2009-06-09T13:25:38+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "At Home, In Studio. Stay Safe."
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1210983175532482561/tSlJYnvh.jpg"
            ]
          }
        }
      ],
      "content": [
        "I'm taking advantage of isolation by learning some new cuisines from around the world. Tonight, I cooked my favourite American dish"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EUoUUDRXYAEzwGY.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-04-03T23:59:00+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @Cromerty's tweet"
    ],
    "like-of": [
      "https://twitter.com/Cromerty/status/1245830584150024193"
    ],
    "published": [
      "2020-04-03T23:59:00+01:00"
    ],
    "syndication": [
      "https://twitter.com/Cromerty/status/1245830584150024193"
    ]
  }
}
