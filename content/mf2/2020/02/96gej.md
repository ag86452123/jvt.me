{
  "kind": "likes",
  "slug": "2020/02/96gej",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1230552701928378369"
      ],
      "url": [
        "https://twitter.com/PlayApex/status/1230552701928378369"
      ],
      "video": [
        "https://video.twimg.com/amplify_video/1230551220705579009/vid/1280x720/AJ8-AUM8MsvmL1Gb.mp4?tag=13"
      ],
      "published": [
        "2020-02-20T18:00:01+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:PlayApex"
            ],
            "numeric-id": [
              "1048018930785083392"
            ],
            "name": [
              "Apex Legends"
            ],
            "nickname": [
              "PlayApex"
            ],
            "url": [
              "https://twitter.com/PlayApex",
              "http://playapex.com"
            ],
            "published": [
              "2018-10-05T01:15:59+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "The Outlands"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1222942571376209920/raMKhiHq.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "We're going back to where it all started. 😎\n\nStarting tomorrow through Feb 24, in addition to World's Edge,  Season 1 Kings Canyon will also be available to play.",
          "html": "<div style=\"white-space: pre\">We're going back to where it all started. 😎\n\nStarting tomorrow through Feb 24, in addition to World's Edge,  Season 1 Kings Canyon will also be available to play.</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-20T22:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @PlayApex's tweet"
    ],
    "like-of": [
      "https://twitter.com/PlayApex/status/1230552701928378369"
    ],
    "published": [
      "2020-02-20T22:30:00Z"
    ],
    "syndication": [
      "https://twitter.com/PlayApex/status/1230552701928378369"
    ]
  }
}
