{
  "kind": "likes",
  "slug": "2020/02/5t4vr",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1232005377971150850"
      ],
      "url": [
        "https://twitter.com/twcuddleston/status/1232005377971150850"
      ],
      "published": [
        "2020-02-24T18:12:26+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:twcuddleston"
            ],
            "numeric-id": [
              "276466372"
            ],
            "name": [
              "Abby Tomlinson"
            ],
            "nickname": [
              "twcuddleston"
            ],
            "url": [
              "https://twitter.com/twcuddleston",
              "http://Instagram.com/abby.tomlinson"
            ],
            "published": [
              "2011-04-03T12:15:58+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Westminster, sadly "
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1226940595056336897/LX4BtAT_.jpg"
            ]
          }
        }
      ],
      "content": [
        "men when they have to face actual consequences for their actions"
      ],
      "photo": [
        "https://pbs.twimg.com/media/ERj2W6BWkAEU_iq.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-24T19:30:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @twcuddleston's tweet"
    ],
    "like-of": [
      "https://twitter.com/twcuddleston/status/1232005377971150850"
    ],
    "published": [
      "2020-02-24T19:30:00Z"
    ],
    "syndication": [
      "https://twitter.com/twcuddleston/status/1232005377971150850"
    ]
  }
}
