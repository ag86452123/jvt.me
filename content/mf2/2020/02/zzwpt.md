{
  "kind": "likes",
  "slug": "2020/02/zzwpt",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1227690478780473349"
      ],
      "url": [
        "https://twitter.com/mjg59/status/1227690478780473349"
      ],
      "published": [
        "2020-02-12T20:26:34+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:mjg59"
            ],
            "numeric-id": [
              "229502009"
            ],
            "name": [
              "Matthew Garrett"
            ],
            "nickname": [
              "mjg59"
            ],
            "url": [
              "https://twitter.com/mjg59",
              "http://mjg59.dreamwidth.org"
            ],
            "published": [
              "2010-12-22T15:28:52+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Oakland, CA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/459378005689630720/4f7Dml5q.png"
            ]
          }
        }
      ],
      "content": [
        "An internal exercise to test the SRE team's ability to remediate a malicious package being installed on the fleet manifested itself as their computers going HONK at random intervals and I have some of the best coworkers"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-12T22:48:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @mjg59's tweet"
    ],
    "like-of": [
      "https://twitter.com/mjg59/status/1227690478780473349"
    ],
    "published": [
      "2020-02-12T22:48:00Z"
    ],
    "syndication": [
      "https://twitter.com/mjg59/status/1227690478780473349"
    ]
  }
}
