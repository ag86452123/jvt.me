{
  "kind": "likes",
  "slug": "2020/02/is2n0",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1229842815582117888"
      ],
      "url": [
        "https://twitter.com/ohhelloana/status/1229842815582117888"
      ],
      "published": [
        "2020-02-18T18:59:11+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ohhelloana"
            ],
            "numeric-id": [
              "434457570"
            ],
            "name": [
              "Ana Rodrigues"
            ],
            "nickname": [
              "ohhelloana"
            ],
            "url": [
              "https://twitter.com/ohhelloana",
              "https://ohhelloana.blog"
            ],
            "published": [
              "2011-12-11T21:57:15+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1126838936519815168/F4HZ_Fmu.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "London IndieWeb friends: come work on your personal website with us tomorrow! events.indieweb.org/tag/hwclondon",
          "html": "London IndieWeb friends: come work on your personal website with us tomorrow! <a href=\"https://events.indieweb.org/tag/hwclondon\">events.indieweb.org/tag/hwclondon</a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-18T19:47:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ohhelloana's tweet"
    ],
    "like-of": [
      "https://twitter.com/ohhelloana/status/1229842815582117888"
    ],
    "published": [
      "2020-02-18T19:47:00Z"
    ],
    "syndication": [
      "https://twitter.com/ohhelloana/status/1229842815582117888"
    ]
  }
}
