{
  "kind": "likes",
  "slug": "2020/02/6dgnl",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1224374116075102209"
      ],
      "url": [
        "https://twitter.com/ianmiell/status/1224374116075102209"
      ],
      "published": [
        "2020-02-03T16:48:31+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ianmiell"
            ],
            "numeric-id": [
              "58017706"
            ],
            "name": [
              "Ian Miell"
            ],
            "nickname": [
              "ianmiell"
            ],
            "url": [
              "https://twitter.com/ianmiell",
              "http://zwischenzugs.com"
            ],
            "published": [
              "2009-07-18T19:59:32+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1120618937253793793/xb6_9WMl.jpg"
            ]
          }
        }
      ],
      "content": [
        "'Someone with the job title assassin viewed your profile'"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-04T08:32:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ianmiell's tweet"
    ],
    "like-of": [
      "https://twitter.com/ianmiell/status/1224374116075102209"
    ],
    "published": [
      "2020-02-04T08:32:00Z"
    ],
    "syndication": [
      "https://twitter.com/ianmiell/status/1224374116075102209"
    ]
  }
}
