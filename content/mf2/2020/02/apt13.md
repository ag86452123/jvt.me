{
  "kind": "reposts",
  "slug": "2020/02/apt13",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1223739787129040897"
      ],
      "url": [
        "https://twitter.com/WrrrdNrrrdGrrrl/status/1223739787129040897"
      ],
      "published": [
        "2020-02-01T22:47:55+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:WrrrdNrrrdGrrrl"
            ],
            "numeric-id": [
              "3237973655"
            ],
            "name": [
              "𝚁𝚊𝚌𝚑𝚎𝚕 𝚂𝚑𝚊𝚛𝚙"
            ],
            "nickname": [
              "WrrrdNrrrdGrrrl"
            ],
            "url": [
              "https://twitter.com/WrrrdNrrrdGrrrl",
              "https://wrrrdnrrrdgrrrl.com/"
            ],
            "published": [
              "2015-05-06T03:56:59+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "She/her, NY/VT"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1213216683822702594/fXWtx9F_.jpg"
            ]
          }
        }
      ],
      "content": [
        "An older woman came into the bookstore today. I made a joke about a credit card reader issue and she said \"these things are all programmed by twenty-five-year-old boys who don't comment their code\" and somehow we ended up having a great conversation about programming and biases."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-03T17:44:00Z",
  "h": "h-entry",
  "properties": {
    "published": [
      "2020-02-03T17:44:00Z"
    ],
    "repost-of": [
      "https://twitter.com/WrrrdNrrrdGrrrl/status/1223739787129040897"
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1224389762498334723"
    ]
  }
}
