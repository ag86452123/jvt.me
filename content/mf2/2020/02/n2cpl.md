{
  "kind": "replies",
  "slug": "2020/02/n2cpl",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226821529654243328"
      ],
      "url": [
        "https://twitter.com/jbjon/status/1226821529654243328"
      ],
      "published": [
        "2020-02-10T10:53:40+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/chris_emerson/status/1226821221188276226"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:jbjon"
            ],
            "numeric-id": [
              "7731772"
            ],
            "name": [
              "Jonathan"
            ],
            "nickname": [
              "jbjon"
            ],
            "url": [
              "https://twitter.com/jbjon",
              "http://www.mindthe.net/devices/"
            ],
            "published": [
              "2007-07-26T08:21:44+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, Nottingham (52.935"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1121021071430508557/JbseJNrA.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "It ensures disaster recovery too. If it's good enough to commit locally, it's good enough to be backed up.",
          "html": "It ensures disaster recovery too. If it's good enough to commit locally, it's good enough to be backed up.\n<a class=\"u-mention\" href=\"https://twitter.com/chris_emerson\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/pavlakis\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-10T12:39:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/jbjon/status/1226821529654243328"
    ],
    "in-reply-to": [
      "https://twitter.com/jbjon/status/1226821529654243328"
    ],
    "published": [
      "2020-02-10T12:39:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "You can also use the config in https://www.jvt.me/posts/2019/09/22/git-push-matching/ to always push to the same branch upstream 🙌🏼 saving a lot of typing"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1226854379627646981"
    ]
  }
}
