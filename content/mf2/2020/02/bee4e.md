{
  "kind": "replies",
  "slug": "2020/02/bee4e",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1228270763964293121"
      ],
      "url": [
        "https://twitter.com/dylanbeattie/status/1228270763964293121"
      ],
      "published": [
        "2020-02-14T10:52:24+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:dylanbeattie"
            ],
            "numeric-id": [
              "14802786"
            ],
            "name": [
              "Dylan Beattie 🇪🇺"
            ],
            "nickname": [
              "dylanbeattie"
            ],
            "url": [
              "https://twitter.com/dylanbeattie",
              "https://dylanbeattie.net"
            ],
            "published": [
              "2008-05-16T19:11:08+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1156626416378744834/ciF2CHCg.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Working with the git CLI:\n\nproject $ git push \nfatal: The current branch my-branch has no upstream branch.\nTo push the current branch and set the remote as upstream, use\n\n    git push --set-upstream origin my-branch\n\nWhy doesn't it then ask 'Is that what you want to do? [Y/n]'",
          "html": "<div style=\"white-space: pre\">Working with the git CLI:\n\nproject $ git push \nfatal: The current branch my-branch has no upstream branch.\nTo push the current branch and set the remote as upstream, use\n\n    git push --set-upstream origin my-branch\n\nWhy doesn't it then ask 'Is that what you want to do? [Y/n]'</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-14T17:24:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/dylanbeattie/status/1228270763964293121"
    ],
    "in-reply-to": [
      "https://twitter.com/dylanbeattie/status/1228270763964293121"
    ],
    "published": [
      "2020-02-14T17:24:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "FYI in this example you can set this to happen by default https://www.jvt.me/posts/2019/09/22/git-push-matching/"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1228370705886121984"
    ]
  }
}
