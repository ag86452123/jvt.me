{
  "kind": "likes",
  "slug": "2020/02/opryk",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1230515351185743872"
      ],
      "url": [
        "https://twitter.com/towernter/status/1230515351185743872"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1229910594905464832/pu/vid/608x1080/SRXLrCMznc_lMrQn.mp4?tag=10"
      ],
      "published": [
        "2020-02-20T15:31:36+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:towernter"
            ],
            "numeric-id": [
              "2967455656"
            ],
            "name": [
              "Tawanda☕💻"
            ],
            "nickname": [
              "towernter"
            ],
            "url": [
              "https://twitter.com/towernter",
              "https://github.com/towernter"
            ],
            "published": [
              "2015-01-08T07:54:23+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1230086409773559808/Y7jkYfHC.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "When you skip development and test a new feature in production\n#programming #coding #100daysofcode #webdev \n",
          "html": "<div style=\"white-space: pre\">When you skip development and test a new feature in production\n<a href=\"https://twitter.com/search?q=%23programming\">#programming</a> <a href=\"https://twitter.com/search?q=%23coding\">#coding</a> <a href=\"https://twitter.com/search?q=%23100daysofcode\">#100daysofcode</a> <a href=\"https://twitter.com/search?q=%23webdev\">#webdev</a> \n</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-20T18:13:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @towernter's tweet"
    ],
    "like-of": [
      "https://twitter.com/towernter/status/1230515351185743872"
    ],
    "published": [
      "2020-02-20T18:13:00Z"
    ],
    "syndication": [
      "https://twitter.com/towernter/status/1230515351185743872"
    ]
  }
}
