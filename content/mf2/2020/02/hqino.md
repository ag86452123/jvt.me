{
  "kind": "likes",
  "slug": "2020/02/hqino",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226923515988860933"
      ],
      "url": [
        "https://twitter.com/WiT_Notts/status/1226923515988860933"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EQbobilWoAgqsFh.mp4"
      ],
      "published": [
        "2020-02-10T17:38:56+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:WiT_Notts"
            ],
            "numeric-id": [
              "4339158441"
            ],
            "name": [
              "Women In Tech, Nottingham 🌈✨"
            ],
            "nickname": [
              "WiT_Notts"
            ],
            "url": [
              "https://twitter.com/WiT_Notts",
              "https://nott.tech/wit"
            ],
            "published": [
              "2015-12-01T11:22:25+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/913380298829844481/YvPcjPlE.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Heading to #TechNott this evening? Why not meet up with @LittleHelli and @anna_hax outside the Theatre Royal at 6pm to walk over together 🧡",
          "html": "Heading to <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a> this evening? Why not meet up with <a href=\"https://twitter.com/LittleHelli\">@LittleHelli</a> and <a href=\"https://twitter.com/anna_hax\">@anna_hax</a> outside the Theatre Royal at 6pm to walk over together 🧡"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-10T20:04:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @WiT_Notts's tweet"
    ],
    "like-of": [
      "https://twitter.com/WiT_Notts/status/1226923515988860933"
    ],
    "published": [
      "2020-02-10T20:04:00Z"
    ],
    "syndication": [
      "https://twitter.com/WiT_Notts/status/1226923515988860933"
    ]
  }
}
