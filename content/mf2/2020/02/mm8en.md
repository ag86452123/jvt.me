{
  "kind": "likes",
  "slug": "2020/02/mm8en",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225489850532552707"
      ],
      "url": [
        "https://twitter.com/MrsEmma/status/1225489850532552707"
      ],
      "published": [
        "2020-02-06T18:42:03+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:MrsEmma"
            ],
            "numeric-id": [
              "19500955"
            ],
            "name": [
              "Emma Seward"
            ],
            "nickname": [
              "MrsEmma"
            ],
            "url": [
              "https://twitter.com/MrsEmma",
              "https://www.emmasgarden.co.uk/jewellery"
            ],
            "published": [
              "2009-01-25T19:32:42+00:00"
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1220138479428030464/Oyvh-nqT.jpg"
            ]
          }
        }
      ],
      "content": [
        "Had a very mature and grown-up lunch today."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQHQf3tWsAcwRz3.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-06T22:06:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @MrsEmma's tweet"
    ],
    "like-of": [
      "https://twitter.com/MrsEmma/status/1225489850532552707"
    ],
    "published": [
      "2020-02-06T22:06:00Z"
    ],
    "syndication": [
      "https://twitter.com/MrsEmma/status/1225489850532552707"
    ]
  }
}
