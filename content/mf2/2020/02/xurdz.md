{
  "kind": "likes",
  "slug": "2020/02/xurdz",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226917099785904128"
      ],
      "url": [
        "https://twitter.com/albinokid/status/1226917099785904128"
      ],
      "published": [
        "2020-02-10T17:13:26+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:albinokid"
            ],
            "numeric-id": [
              "45285833"
            ],
            "name": [
              "Anthony Rapp"
            ],
            "nickname": [
              "albinokid"
            ],
            "url": [
              "https://twitter.com/albinokid",
              "https://www.cameo.com/antrapp"
            ],
            "published": [
              "2009-06-07T04:48:09+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "NYC"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/774501839915974658/MNJCMlev.jpg"
            ]
          }
        }
      ],
      "content": [
        "As you know, I’m marrying this wonderful, beautiful man. Wanted to share this photo from our engagement shoot. I’m feeling pretty damn lucky."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQbimFdW4AArijX.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-10T23:02:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @albinokid's tweet"
    ],
    "like-of": [
      "https://twitter.com/albinokid/status/1226917099785904128"
    ],
    "published": [
      "2020-02-10T23:02:00Z"
    ],
    "syndication": [
      "https://twitter.com/albinokid/status/1226917099785904128"
    ]
  }
}
