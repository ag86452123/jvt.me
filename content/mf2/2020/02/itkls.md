{
  "kind": "likes",
  "slug": "2020/02/itkls",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1224624577407524866"
      ],
      "url": [
        "https://twitter.com/miss_jwo/status/1224624577407524866"
      ],
      "published": [
        "2020-02-04T09:23:46+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:miss_jwo"
            ],
            "numeric-id": [
              "200519952"
            ],
            "name": [
              "Jenny Wong 🐝"
            ],
            "nickname": [
              "miss_jwo"
            ],
            "url": [
              "https://twitter.com/miss_jwo",
              "http://jwong.co.uk"
            ],
            "published": [
              "2010-10-09T14:29:15+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "United Kingdom"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/839845252412358656/PLSY74LG.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Annoyed at the fact i managed to get through #FOSDEM without catching conflu (yey!) ony to come home and partner has got a cold whilst i was away 🙃",
          "html": "Annoyed at the fact i managed to get through <a href=\"https://twitter.com/search?q=%23FOSDEM\">#FOSDEM</a> without catching conflu (yey!) ony to come home and partner has got a cold whilst i was away 🙃"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-04T13:58:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @miss_jwo's tweet"
    ],
    "like-of": [
      "https://twitter.com/miss_jwo/status/1224624577407524866"
    ],
    "published": [
      "2020-02-04T13:58:00Z"
    ],
    "syndication": [
      "https://twitter.com/miss_jwo/status/1224624577407524866"
    ]
  }
}
