{
  "kind": "rsvps",
  "slug": "2020/02/ro8n0",
  "client_id": "https://micropublish.net",
  "date": "2020-02-16T22:16:03.246+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/DevOps-Notts/events/267477483/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/DevOps-Notts/events/267477483/"
    ],
    "published": [
      "2020-02-16T22:16:03.246+01:00"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/DevOps-Notts/events/267477483/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Rebel Recruiters, Huntingdon St, Nottinghamshire, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/DevOps-Notts/events/267477483/"
      ],
      "name": [
        "DevOps Notts - February 2020"
      ],
      "start": [
        "2020-02-25T18:30:00Z"
      ],
      "end": [
        "2020-02-25T21:30:00Z"
      ]
    }
  }
}
