{
  "kind": "likes",
  "slug": "2020/02/absl5",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225211252349128704"
      ],
      "url": [
        "https://twitter.com/abbyfuller/status/1225211252349128704"
      ],
      "published": [
        "2020-02-06T00:15:00+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:abbyfuller"
            ],
            "numeric-id": [
              "51848919"
            ],
            "name": [
              "Abby Fuller"
            ],
            "nickname": [
              "abbyfuller"
            ],
            "url": [
              "https://twitter.com/abbyfuller",
              "https://github.com/aws/containers-roadmap/projects/1"
            ],
            "published": [
              "2009-06-28T21:25:20+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Seattle"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1222380615674122241/90EQ7ZTp.jpg"
            ]
          }
        }
      ],
      "content": [
        "amazing how quickly a thread can devolve into an argument about systemd"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-06T07:16:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @abbyfuller's tweet"
    ],
    "like-of": [
      "https://twitter.com/abbyfuller/status/1225211252349128704"
    ],
    "published": [
      "2020-02-06T07:16:00Z"
    ],
    "syndication": [
      "https://twitter.com/abbyfuller/status/1225211252349128704"
    ]
  }
}
