{
  "kind": "rsvps",
  "slug": "2020/02/j3ltt",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-06T12:10:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/Notts-Techfast/events/268502129/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Notts-Techfast/events/268502129/"
    ],
    "published": [
      "2020-02-06T12:10:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/Notts-Techfast/events/268502129/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Fothergill House, Fothergill House, King Street, Nottingham, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/Notts-Techfast/events/268502129/"
      ],
      "name": [
        "Audio Augmented Reality (feat. a demo app...and music composed by plants?!)"
      ],
      "start": [
        "2020-02-27T07:30:00Z"
      ],
      "end": [
        "2020-02-27T09:00:00Z"
      ]
    }
  }
}
