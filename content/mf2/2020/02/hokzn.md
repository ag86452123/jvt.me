{
  "kind": "replies",
  "slug": "2020/02/hokzn",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225907857461260288"
      ],
      "url": [
        "https://twitter.com/karlicoss/status/1225907857461260288"
      ],
      "published": [
        "2020-02-07T22:23:04+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/jborichevskiy/status/1225904161797132290"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:karlicoss"
            ],
            "numeric-id": [
              "119756204"
            ],
            "name": [
              "jestem króliczkiem"
            ],
            "nickname": [
              "karlicoss"
            ],
            "url": [
              "https://twitter.com/karlicoss",
              "https://beepb00p.xyz"
            ],
            "published": [
              "2010-03-04T15:11:55+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/791021819675410432/NSdzQWuZ.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Oh, also just remembered: jvt.me\n\n@JamieTanna wondering how you're generating your feed?",
          "html": "<div style=\"white-space: pre\">Oh, also just remembered: <a href=\"https://www.jvt.me\">jvt.me</a>\n\n<a href=\"https://twitter.com/JamieTanna\">@JamieTanna</a> wondering how you're generating your feed?</div>\n<a class=\"u-mention\" href=\"https://twitter.com/jborichevskiy\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-07T22:52:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/karlicoss/status/1225907857461260288"
    ],
    "in-reply-to": [
      "https://twitter.com/karlicoss/status/1225907857461260288"
    ],
    "published": [
      "2020-02-07T22:52:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "In terms of how I get the data? I publish content to my site first, then syndicate it elsewhere afterwards https://indieweb.org/POSSE - which mostly happens automagically\n\nI do this even for things like https://lobste.rs which doesn't have an API so I manually post it with a link back to the comment on my site"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1225916335563124742"
    ]
  }
}
