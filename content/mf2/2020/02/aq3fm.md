{
  "kind": "rsvps",
  "slug": "2020/02/aq3fm",
  "date": "2020-02-25T08:20:27+0000",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://devopsdays.org/events/2020-london/"
    ],
    "in-reply-to": [
      "https://devopsdays.org/events/2020-london/"
    ],
    "published": [
      "2020-02-25T08:20:27+0000"
    ],
    "rsvp": [
      "yes"
    ],
    "syndication": [

    ],
    "event": {
      "location": [
        "QEII Centre, Broad Sanctuary, Westminster "
      ],
      "url": [
        "https://devopsdays.org/events/2020-london/"
      ],
      "name": [
        "DevOpsDays London 2020"
      ],
      "start": [
        "2020-09-24T09:00:00Z"
      ],
      "end": [
        "2020-09-25T18:00:00Z"
      ]
    }
  }
}
