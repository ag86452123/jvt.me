{
  "kind": "replies",
  "slug": "2020/02/aa93n",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1230489068078800897"
      ],
      "url": [
        "https://twitter.com/SparkleClass/status/1230489068078800897"
      ],
      "published": [
        "2020-02-20T13:47:09+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:SparkleClass"
            ],
            "numeric-id": [
              "928764030688296960"
            ],
            "name": [
              "Rachel Morgan-Trimmer"
            ],
            "nickname": [
              "SparkleClass"
            ],
            "url": [
              "https://twitter.com/SparkleClass",
              "http://www.sparkleclass.com"
            ],
            "published": [
              "2017-11-09T23:19:35+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Manchester, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1067450771208380416/s9tl2WyF.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Should I do an events page on my website and if so, what plug-in thingy would be a good way to do it? I'm on #Wordpress. @pwaring @hflf",
          "html": "Should I do an events page on my website and if so, what plug-in thingy would be a good way to do it? I'm on <a href=\"https://twitter.com/search?q=%23Wordpress\">#Wordpress</a>. <a href=\"https://twitter.com/pwaring\">@pwaring</a> <a href=\"https://twitter.com/hflf\">@hflf</a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-20T16:33:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/SparkleClass/status/1230489068078800897"
    ],
    "in-reply-to": [
      "https://twitter.com/SparkleClass/status/1230489068078800897"
    ],
    "published": [
      "2020-02-20T16:33:00Z"
    ],
    "category": [
      "indieweb"
    ],
    "content": [
      {
        "html": "",
        "value": "I want to say that some of the folks in the <a href=\"/tags/indieweb/\">#IndieWeb</a> may be able to chime in on this too, as we try to publish events to our sites where possible"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1230532324472840192"
    ]
  },
  "tags": [
    "indieweb"
  ]
}
