{
  "kind": "likes",
  "slug": "2020/02/us0k1",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233170497103204352"
      ],
      "url": [
        "https://twitter.com/QuinnyPig/status/1233170497103204352"
      ],
      "published": [
        "2020-02-27T23:22:12+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:QuinnyPig"
            ],
            "numeric-id": [
              "97114171"
            ],
            "name": [
              "Corey Quinn"
            ],
            "nickname": [
              "QuinnyPig"
            ],
            "url": [
              "https://twitter.com/QuinnyPig",
              "http://www.duckbillgroup.com",
              "http://lastweekinaws.com",
              "http://screaminginthecloud.com"
            ],
            "published": [
              "2009-12-16T02:19:14+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "San Francisco, CA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1093667107278147584/rKaSDcaB.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "I have just been informed that locally “mocking cloud services” doesn’t mean what I thought it did and I owe @awscloud a stupendous apology.",
          "html": "I have just been informed that locally “mocking cloud services” doesn’t mean what I thought it did and I owe <a href=\"https://twitter.com/awscloud\">@awscloud</a> a stupendous apology."
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-28T17:39:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @QuinnyPig's tweet"
    ],
    "like-of": [
      "https://twitter.com/QuinnyPig/status/1233170497103204352"
    ],
    "published": [
      "2020-02-28T17:39:00Z"
    ],
    "syndication": [
      "https://twitter.com/QuinnyPig/status/1233170497103204352"
    ]
  }
}
