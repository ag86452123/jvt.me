{
  "kind": "rsvps",
  "slug": "2020/02/8suv9",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-27T16:54:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/digitallincoln/events/268750262/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/digitallincoln/events/268750262/"
    ],
    "published": [
      "2020-02-27T16:54:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/digitallincoln/events/268750262/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Mosaic, Thomas Parker House, Lincoln, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/digitallincoln/events/268750262/"
      ],
      "name": [
        "Missile Destroyers, Supercomputers, and Chernobyl: Psychological safety & teams."
      ],
      "start": [
        "2020-04-28T18:30:00+01:00"
      ],
      "end": [
        "2020-04-28T20:30:00+01:00"
      ]
    }
  }
}
