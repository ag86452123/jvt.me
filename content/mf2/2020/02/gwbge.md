{
  "kind": "likes",
  "slug": "2020/02/gwbge",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225465414899159041"
      ],
      "url": [
        "https://twitter.com/ashleyfryer/status/1225465414899159041"
      ],
      "published": [
        "2020-02-06T17:04:57+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ashleyfryer"
            ],
            "numeric-id": [
              "21207088"
            ],
            "name": [
              "Ashley Fryer"
            ],
            "nickname": [
              "ashleyfryer"
            ],
            "url": [
              "https://twitter.com/ashleyfryer",
              "http://www.peachtreesandbumblebees.com"
            ],
            "published": [
              "2009-02-18T15:42:40+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1004482135393275904/WKdb8-E7.jpg"
            ]
          }
        }
      ],
      "content": [
        "I don’t know who needs to hear this but you do not need to keep the 19 Gu ramekins currently cluttering up your kitchen cupboards."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-07T22:58:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @ashleyfryer's tweet"
    ],
    "like-of": [
      "https://twitter.com/ashleyfryer/status/1225465414899159041"
    ],
    "published": [
      "2020-02-07T22:58:00Z"
    ],
    "syndication": [
      "https://twitter.com/ashleyfryer/status/1225465414899159041"
    ]
  }
}
