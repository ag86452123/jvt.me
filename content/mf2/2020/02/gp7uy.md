{
  "kind": "rsvps",
  "slug": "2020/02/gp7uy",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-27T15:13:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP yes to https://www.meetup.com/PHPMiNDS-in-Nottingham/events/268999773/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/268999773/"
    ],
    "published": [
      "2020-02-27T15:13:00Z"
    ],
    "rsvp": [
      "yes"
    ],
    "syndication": [
      "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/268999773/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "JH,  34a Stoney Street, Nottingham, NG1 1NB., Nottingham, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/PHPMiNDS-in-Nottingham/events/268999773/"
      ],
      "name": [
        "SLIM 4: PHP'S MICROFRAMEWORK"
      ],
      "start": [
        "2020-03-12T19:00:00Z"
      ],
      "end": [
        "2020-03-12T21:00:00Z"
      ]
    }
  }
}
