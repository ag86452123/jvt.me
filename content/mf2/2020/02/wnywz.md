{
  "kind": "replies",
  "slug": "2020/02/wnywz",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1230269286796623872"
      ],
      "url": [
        "https://twitter.com/LukeDavisSEO/status/1230269286796623872"
      ],
      "published": [
        "2020-02-19T23:13:49+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1230197528823508992"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:LukeDavisSEO"
            ],
            "numeric-id": [
              "1156259462891692032"
            ],
            "name": [
              "The Millennial Shrug ✪"
            ],
            "nickname": [
              "LukeDavisSEO"
            ],
            "url": [
              "https://twitter.com/LukeDavisSEO",
              "http://lukealexdavis.co.uk"
            ],
            "published": [
              "2019-07-30T17:45:13+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Bradford → Luton → Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1213882698550435840/QdrhuhyA.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Oh wow, this is an amazing idea!",
          "html": "Oh wow, this is an amazing idea!\n<a class=\"u-mention\" href=\"https://twitter.com/CarolSaysThings\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-20T07:39:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/LukeDavisSEO/status/1230269286796623872"
    ],
    "in-reply-to": [
      "https://twitter.com/LukeDavisSEO/status/1230269286796623872"
    ],
    "published": [
      "2020-02-20T07:39:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Yep, the next one is the 4th March https://events.indieweb.org/2020/03/homebrew-website-club-nottingham-FWdZAqhKZBnq - would be good to see you there! "
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1230397646722027520"
    ]
  }
}
