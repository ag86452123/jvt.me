{
  "kind": "likes",
  "slug": "2020/02/lpmlf",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1227766087304921088"
      ],
      "url": [
        "https://twitter.com/alicegoldfuss/status/1227766087304921088"
      ],
      "published": [
        "2020-02-13T01:27:00+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:alicegoldfuss"
            ],
            "numeric-id": [
              "163154809"
            ],
            "name": [
              "bletchley punk"
            ],
            "nickname": [
              "alicegoldfuss"
            ],
            "url": [
              "https://twitter.com/alicegoldfuss",
              "http://blog.alicegoldfuss.com/",
              "http://twitch.tv/bletchleypunk"
            ],
            "published": [
              "2010-07-05T17:51:34+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "/usr/local/sin"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1149452202827759616/1blf87Kn.jpg"
            ]
          }
        }
      ],
      "content": [
        "*googles* what time do grocery stores mark down Valentine’s Day chocolate"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-13T07:38:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @alicegoldfuss's tweet"
    ],
    "like-of": [
      "https://twitter.com/alicegoldfuss/status/1227766087304921088"
    ],
    "published": [
      "2020-02-13T07:38:00Z"
    ],
    "syndication": [
      "https://twitter.com/alicegoldfuss/status/1227766087304921088"
    ]
  }
}
