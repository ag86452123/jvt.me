{
  "kind": "likes",
  "slug": "2020/02/ricoa",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1227687894162300929"
      ],
      "url": [
        "https://twitter.com/kieranmch/status/1227687894162300929"
      ],
      "published": [
        "2020-02-12T20:16:17+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:kieranmch"
            ],
            "numeric-id": [
              "50420707"
            ],
            "name": [
              "Kieran McHugh"
            ],
            "nickname": [
              "kieranmch"
            ],
            "url": [
              "https://twitter.com/kieranmch",
              "https://kieran.engineer"
            ],
            "published": [
              "2009-06-24T20:13:29+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1170973779675275264/cCZMxVEg.jpg"
            ]
          }
        }
      ],
      "content": [
        "WHO DID THIS"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQmfoagXUAA_6wi.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-12T20:21:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @kieranmch's tweet"
    ],
    "like-of": [
      "https://twitter.com/kieranmch/status/1227687894162300929"
    ],
    "published": [
      "2020-02-12T20:21:00Z"
    ],
    "category": [
      "psd2"
    ],
    "syndication": [
      "https://twitter.com/kieranmch/status/1227687894162300929"
    ]
  },
  "tags": [
    "psd2"
  ]
}
