{
  "kind": "replies",
  "slug": "2020/02/zkkih",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1223660493073797125"
      ],
      "url": [
        "https://twitter.com/ScribblingOn/status/1223660493073797125"
      ],
      "published": [
        "2020-02-01T17:32:50+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/ParisInBmore/status/1223656362913226753"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:ScribblingOn"
            ],
            "numeric-id": [
              "291009942"
            ],
            "name": [
              "Shubheksha ✨"
            ],
            "nickname": [
              "ScribblingOn"
            ],
            "url": [
              "https://twitter.com/ScribblingOn",
              "https://www.shubheksha.com"
            ],
            "published": [
              "2011-05-01T10:03:57+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "A bubble in England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1131333207780220928/Hpejkti5.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Fwiw it's a literal sea of white dudes and I feel really out of place 😭",
          "html": "Fwiw it's a literal sea of white dudes and I feel really out of place 😭\n<a class=\"u-mention\" href=\"https://twitter.com/IanColdwater\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/ParisInBmore\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-01T23:35:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/ScribblingOn/status/1223660493073797125"
    ],
    "in-reply-to": [
      "https://twitter.com/ScribblingOn/status/1223660493073797125"
    ],
    "published": [
      "2020-02-01T23:35:00Z"
    ],
    "category": [
      "fosdem",
      "diversity-and-inclusion"
    ],
    "content": [
      {
        "html": "",
        "value": "I'd gone in 2017 but not 2018, then went in 2019 and felt pretty uncomfortable as a half Indian man. After being so used to diverse and inclusive spaces it was incredibly jarring, so I'm sorry for how worse it would've been for you! "
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1223752781699391488"
    ]
  },
  "tags": [
    "fosdem",
    "diversity-and-inclusion"
  ]
}
