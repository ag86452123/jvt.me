{
  "kind": "likes",
  "slug": "2020/02/oktst",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1228705165022289921"
      ],
      "url": [
        "https://twitter.com/craigburgess/status/1228705165022289921"
      ],
      "video": [
        "https://video.twimg.com/ext_tw_video/1228705119019159558/pu/vid/720x1280/yZOuIrJjjiPgq7Wh.mp4?tag=10"
      ],
      "published": [
        "2020-02-15T15:38:34+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:craigburgess"
            ],
            "numeric-id": [
              "14912335"
            ],
            "name": [
              "Craig Burgess"
            ],
            "nickname": [
              "craigburgess"
            ],
            "url": [
              "https://twitter.com/craigburgess",
              "https://www.getdoingthings.com"
            ],
            "published": [
              "2008-05-26T19:26:06+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Barnsley, South Yorkshire, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1162042955974283266/7PD6uyTD.jpg"
            ]
          }
        }
      ],
      "content": [
        "Cats are weird but awesome."
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-15T16:04:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @craigburgess's tweet"
    ],
    "like-of": [
      "https://twitter.com/craigburgess/status/1228705165022289921"
    ],
    "published": [
      "2020-02-15T16:04:00Z"
    ],
    "syndication": [
      "https://twitter.com/craigburgess/status/1228705165022289921"
    ]
  }
}
