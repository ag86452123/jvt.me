{
  "kind": "likes",
  "slug": "2020/02/ijayv",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1232577282021187584"
      ],
      "url": [
        "https://twitter.com/anna_hax/status/1232577282021187584"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/ERr-gEHWkAEXc5_.mp4"
      ],
      "published": [
        "2020-02-26T08:04:58+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1232430087229800448"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:anna_hax"
            ],
            "numeric-id": [
              "394235377"
            ],
            "name": [
              "Anna ✨🙅‍♀️✨"
            ],
            "nickname": [
              "anna_hax"
            ],
            "url": [
              "https://twitter.com/anna_hax",
              "https://annadodson.co.uk"
            ],
            "published": [
              "2011-10-19T19:37:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1134736020669378561/0_EwVzms.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "html": "\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-26T08:08:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @anna_hax's tweet"
    ],
    "like-of": [
      "https://twitter.com/anna_hax/status/1232577282021187584"
    ],
    "published": [
      "2020-02-26T08:08:00Z"
    ],
    "syndication": [
      "https://twitter.com/anna_hax/status/1232577282021187584"
    ]
  }
}
