{
  "kind": "likes",
  "slug": "2020/02/a1bto",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1229093553269362689"
      ],
      "url": [
        "https://twitter.com/FiloSottile/status/1229093553269362689"
      ],
      "published": [
        "2020-02-16T17:21:53+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:FiloSottile"
            ],
            "numeric-id": [
              "51049452"
            ],
            "name": [
              "Filippo Valsorda"
            ],
            "nickname": [
              "FiloSottile"
            ],
            "url": [
              "https://twitter.com/FiloSottile",
              "https://filippo.io",
              "http://mkcert.dev",
              "http://age-encryption.org"
            ],
            "published": [
              "2009-06-26T13:10:33+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Manhattan, NY"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1028703543174225920/3cm3bMWC.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "whoami.filippo.io, the SSH server that knows who you are, got some newly refreshed intel! Try it out!\n\n$ ssh https://t.co/MSn1TF8ii2",
          "html": "<div style=\"white-space: pre\"><a href=\"http://whoami.filippo.io\">whoami.filippo.io</a>, the SSH server that knows who you are, got some newly refreshed intel! Try it out!\n\n$ ssh https://t.co/MSn1TF8ii2</div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQ6eEg2WsAQceip.png"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-16T23:45:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @FiloSottile's tweet"
    ],
    "like-of": [
      "https://twitter.com/FiloSottile/status/1229093553269362689"
    ],
    "published": [
      "2020-02-16T23:45:00Z"
    ],
    "syndication": [
      "https://twitter.com/FiloSottile/status/1229093553269362689"
    ]
  }
}
