{
  "kind": "replies",
  "slug": "2020/02/xjtsq",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225457807132155905"
      ],
      "url": [
        "https://twitter.com/sil/status/1225457807132155905"
      ],
      "published": [
        "2020-02-06T16:34:43+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/sil/status/1225457669483433984"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:sil"
            ],
            "numeric-id": [
              "1389781"
            ],
            "name": [
              "Stuart Langridge"
            ],
            "nickname": [
              "sil"
            ],
            "url": [
              "https://twitter.com/sil",
              "https://www.kryogenix.org/"
            ],
            "published": [
              "2007-03-18T01:52:55+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Birmingham, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/55108762/hackergotchi-simpler.png"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Maybe we should bring blogrolls back...",
          "html": "Maybe we should bring blogrolls back...\n<a class=\"u-mention\" href=\"https://twitter.com/hankchizljaw\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-11T08:01:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/sil/status/1225457807132155905"
    ],
    "in-reply-to": [
      "https://twitter.com/sil/status/1225457807132155905"
    ],
    "published": [
      "2020-02-11T08:01:00Z"
    ],
    "category": [
      "feed",
      "microformats"
    ],
    "content": [
      {
        "html": "",
        "value": "My blogroll https://www.jvt.me/blogroll/\nMy RSS feed https://www.jvt.me/feed.xml or https://www.jvt.me/posts/feed.xml if you just want my articles\nAnd https://www.jvt.me/ for my <a href=\"/tags/microformats/\">#Microformats</a> feed and https://www.jvt.me/kind/articles/ for blog"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1251558247817007109"
    ]
  },
  "tags": [
    "feed",
    "microformats"
  ]
}
