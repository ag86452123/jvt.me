{
  "kind": "likes",
  "slug": "2020/02/ee3oz",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233462434523947011"
      ],
      "url": [
        "https://twitter.com/holly/status/1233462434523947011"
      ],
      "published": [
        "2020-02-28T18:42:15+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:holly"
            ],
            "numeric-id": [
              "7555262"
            ],
            "name": [
              "Holly Brockwell"
            ],
            "nickname": [
              "holly"
            ],
            "url": [
              "https://twitter.com/holly",
              "https://www.instagram.com/hollybrocks/"
            ],
            "published": [
              "2007-07-18T10:27:16+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "From Nottingham, in London"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1220716137572421635/yK5hOJef.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "My phone keeps correcting mansplaining to nansplaining and now I need some nansplaining in my life.\n\n\"Oh, you see dear, he broke up with you because you had sex with him. Why buy the cow when you can get the milk for free?\"",
          "html": "<div style=\"white-space: pre\">My phone keeps correcting mansplaining to nansplaining and now I need some nansplaining in my life.\n\n\"Oh, you see dear, he broke up with you because you had sex with him. Why buy the cow when you can get the milk for free?\"</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-28T21:53:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @holly's tweet"
    ],
    "like-of": [
      "https://twitter.com/holly/status/1233462434523947011"
    ],
    "published": [
      "2020-02-28T21:53:00Z"
    ],
    "syndication": [
      "https://twitter.com/holly/status/1233462434523947011"
    ]
  }
}
