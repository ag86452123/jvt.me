{
  "kind": "likes",
  "slug": "2020/02/hs1og",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1233788641954148352"
      ],
      "url": [
        "https://twitter.com/QuinnyPig/status/1233788641954148352"
      ],
      "published": [
        "2020-02-29T16:18:29+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:QuinnyPig"
            ],
            "numeric-id": [
              "97114171"
            ],
            "name": [
              "Corey Quinn"
            ],
            "nickname": [
              "QuinnyPig"
            ],
            "url": [
              "https://twitter.com/QuinnyPig",
              "http://www.duckbillgroup.com",
              "http://lastweekinaws.com",
              "http://screaminginthecloud.com"
            ],
            "published": [
              "2009-12-16T02:19:14+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "San Francisco, CA"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1093667107278147584/rKaSDcaB.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "In honor of today's Leap Day, @office365 will be taking 24 hours of downtime to avoid having to change their name.",
          "html": "In honor of today's Leap Day, <a href=\"https://twitter.com/Office365\">@office365</a> will be taking 24 hours of downtime to avoid having to change their name."
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-29T23:46:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @QuinnyPig's tweet"
    ],
    "like-of": [
      "https://twitter.com/QuinnyPig/status/1233788641954148352"
    ],
    "published": [
      "2020-02-29T23:46:00Z"
    ],
    "syndication": [
      "https://twitter.com/QuinnyPig/status/1233788641954148352"
    ]
  }
}
