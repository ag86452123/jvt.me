{
  "kind": "likes",
  "slug": "2020/02/ln84p",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226149243787730946"
      ],
      "url": [
        "https://twitter.com/DudeWhoCode/status/1226149243787730946"
      ],
      "published": [
        "2020-02-08T14:22:15+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:DudeWhoCode"
            ],
            "numeric-id": [
              "1342542518"
            ],
            "name": [
              "Naren"
            ],
            "nickname": [
              "DudeWhoCode"
            ],
            "url": [
              "https://twitter.com/DudeWhoCode",
              "https://www.dudewho.codes"
            ],
            "published": [
              "2013-04-10T18:16:19+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "tty0"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1201728338097651713/d1p5FChW.jpg"
            ]
          }
        }
      ],
      "content": [
        "I am at an Indian wedding."
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQQoOzZU0AAnWci.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-08T15:04:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @DudeWhoCode's tweet"
    ],
    "like-of": [
      "https://twitter.com/DudeWhoCode/status/1226149243787730946"
    ],
    "published": [
      "2020-02-08T15:04:00Z"
    ],
    "syndication": [
      "https://twitter.com/DudeWhoCode/status/1226149243787730946"
    ]
  }
}
