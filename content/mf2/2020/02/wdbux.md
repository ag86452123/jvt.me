{
  "kind": "likes",
  "slug": "2020/02/wdbux",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226792075972300800"
      ],
      "url": [
        "https://twitter.com/PHPMinds/status/1226792075972300800"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EQZw4usXkAAuizy.mp4"
      ],
      "published": [
        "2020-02-10T08:56:38+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/PHPMinds/status/1226791069846773760"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:PHPMinds"
            ],
            "numeric-id": [
              "3439822277"
            ],
            "name": [
              "php[minds]"
            ],
            "nickname": [
              "PHPMinds"
            ],
            "url": [
              "https://twitter.com/PHPMinds",
              "http://phpminds.org"
            ],
            "published": [
              "2015-08-25T20:21:55+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/657150588598136832/ce53N4Je.png"
            ]
          }
        }
      ],
      "content": [
        "We all know that this is what cybersecurity really is! Wear your best hoodies"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-10T08:57:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @PHPMinds's tweet"
    ],
    "like-of": [
      "https://twitter.com/PHPMinds/status/1226792075972300800"
    ],
    "published": [
      "2020-02-10T08:57:00Z"
    ],
    "syndication": [
      "https://twitter.com/PHPMinds/status/1226792075972300800"
    ]
  }
}
