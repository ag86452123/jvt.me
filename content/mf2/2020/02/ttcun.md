{
  "kind": "likes",
  "slug": "2020/02/ttcun",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1230510014328115201"
      ],
      "url": [
        "https://twitter.com/SBinLondon/status/1230510014328115201"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EROmVQpWoAA6IzG.mp4"
      ],
      "published": [
        "2020-02-20T15:10:23+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:SBinLondon"
            ],
            "numeric-id": [
              "180283911"
            ],
            "name": [
              "Kate"
            ],
            "nickname": [
              "SBinLondon"
            ],
            "url": [
              "https://twitter.com/SBinLondon",
              "http://katebeard.co"
            ],
            "published": [
              "2010-08-19T07:16:23+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Where the good coffee is"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1222510978732036102/Dx46JyVK.jpg"
            ]
          }
        }
      ],
      "content": [
        "Just tested my first feature in production and about to send it out for real people to use it!!"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-20T16:13:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @SBinLondon's tweet"
    ],
    "like-of": [
      "https://twitter.com/SBinLondon/status/1230510014328115201"
    ],
    "published": [
      "2020-02-20T16:13:00Z"
    ],
    "syndication": [
      "https://twitter.com/SBinLondon/status/1230510014328115201"
    ]
  }
}
