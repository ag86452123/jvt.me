{
  "kind": "rsvps",
  "slug": "2020/02/uowys",
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-06T12:10:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "RSVP no to https://www.meetup.com/Notts-Dev-Workshop/events/268502946/"
    ],
    "in-reply-to": [
      "https://www.meetup.com/Notts-Dev-Workshop/events/268502946/"
    ],
    "published": [
      "2020-02-06T12:10:00Z"
    ],
    "rsvp": [
      "no"
    ],
    "syndication": [
      "https://www.meetup.com/Notts-Dev-Workshop/events/268502946/#rsvp-by-https%3A%2F%2Fwww.jvt.me"
    ],
    "event": {
      "location": [
        "Rebel Recruitment, Rebel Recruitment, 2nd floor, Blenheim Court, Nottingham, United Kingdom"
      ],
      "url": [
        "https://www.meetup.com/Notts-Dev-Workshop/events/268502946/"
      ],
      "name": [
        "Carters Boutique: An Introduction to Carter with Richard Tasker"
      ],
      "start": [
        "2020-03-02T18:30:00Z"
      ],
      "end": [
        "2020-03-02T21:30:00Z"
      ]
    }
  }
}
