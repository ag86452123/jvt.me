{
  "kind": "likes",
  "slug": "2020/02/sz73b",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1224993072234319872"
      ],
      "url": [
        "https://twitter.com/hansmollman/status/1224993072234319872"
      ],
      "published": [
        "2020-02-05T09:48:02+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:hansmollman"
            ],
            "numeric-id": [
              "86101978"
            ],
            "name": [
              "Mollie Goodfellow"
            ],
            "nickname": [
              "hansmollman"
            ],
            "url": [
              "https://twitter.com/hansmollman",
              "http://molliegoodfellow.co.uk"
            ],
            "published": [
              "2009-10-29T16:13:14+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "mollie.goodfellow@gmail.com"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1218109101609226240/GKBwqymh.jpg"
            ]
          }
        }
      ],
      "content": [
        "what if Greta Thunberg is in a kind of Ferris Bueller situation where she just wanted one day off school and it’s spiralled out of control"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-06T17:32:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @hansmollman's tweet"
    ],
    "like-of": [
      "https://twitter.com/hansmollman/status/1224993072234319872"
    ],
    "published": [
      "2020-02-06T17:32:00Z"
    ],
    "syndication": [
      "https://twitter.com/hansmollman/status/1224993072234319872"
    ]
  }
}
