{
  "kind": "likes",
  "slug": "2020/02/uogfq",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1232609495114821637"
      ],
      "url": [
        "https://twitter.com/freiksenet/status/1232609495114821637"
      ],
      "published": [
        "2020-02-26T10:12:59+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:freiksenet"
            ],
            "numeric-id": [
              "18200589"
            ],
            "name": [
              "31.5 business travel per-diems"
            ],
            "nickname": [
              "freiksenet"
            ],
            "url": [
              "https://twitter.com/freiksenet"
            ],
            "published": [
              "2008-12-17T21:35:49+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Helsinki, Finland"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/942473966035374082/Z7j9DrxM.jpg"
            ]
          }
        }
      ],
      "content": [
        "Open source issues in a nutshell."
      ],
      "photo": [
        "https://pbs.twimg.com/media/ERsboqMWAAUPNyw.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-26T21:26:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @freiksenet's tweet"
    ],
    "like-of": [
      "https://twitter.com/freiksenet/status/1232609495114821637"
    ],
    "published": [
      "2020-02-26T21:26:00Z"
    ],
    "syndication": [
      "https://twitter.com/freiksenet/status/1232609495114821637"
    ]
  }
}
