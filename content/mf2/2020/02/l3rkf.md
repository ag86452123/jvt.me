{
  "kind": "likes",
  "slug": "2020/02/l3rkf",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1224024833459134469"
      ],
      "url": [
        "https://twitter.com/technottingham/status/1224024833459134469"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EPycGBkWAAEtBZL.mp4"
      ],
      "published": [
        "2020-02-02T17:40:36+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/MrsEmma/status/1224014319924449281"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:technottingham"
            ],
            "numeric-id": [
              "384492431"
            ],
            "name": [
              "Tech Nottingham"
            ],
            "nickname": [
              "technottingham"
            ],
            "url": [
              "https://twitter.com/technottingham",
              "http://technottingham.com"
            ],
            "published": [
              "2011-10-03T19:47:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "html": "\n<a class=\"u-mention\" href=\"https://twitter.com/HackManchester\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/HackTheMidlands\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/JessPWhite\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/MrsEmma\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/TechLincs\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/ZackerTheHacker\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/monotron_\"></a>"
        }
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-02-02T18:43:33.915+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @technottingham's tweet"
    ],
    "like-of": [
      "https://twitter.com/technottingham/status/1224024833459134469"
    ],
    "published": [
      "2020-02-02T18:43:33.915+01:00"
    ],
    "syndication": [
      "https://twitter.com/technottingham/status/1224024833459134469"
    ]
  }
}
