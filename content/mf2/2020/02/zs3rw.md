{
  "kind": "replies",
  "slug": "2020/02/zs3rw",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1225918301693411328"
      ],
      "url": [
        "https://twitter.com/karlicoss/status/1225918301693411328"
      ],
      "published": [
        "2020-02-07T23:04:34+00:00"
      ],
      "in-reply-to": [
        "https://twitter.com/JamieTanna/status/1225916335563124742"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:karlicoss"
            ],
            "numeric-id": [
              "119756204"
            ],
            "name": [
              "jestem króliczkiem"
            ],
            "nickname": [
              "karlicoss"
            ],
            "url": [
              "https://twitter.com/karlicoss",
              "https://beepb00p.xyz"
            ],
            "published": [
              "2010-03-04T15:11:55+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, UK"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/791021819675410432/NSdzQWuZ.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Huh, that's an interesting approach! I was under the impression your activity feed was pull-based (i.e. silo to site)! I'll read the acticle, thanks.",
          "html": "Huh, that's an interesting approach! I was under the impression your activity feed was pull-based (i.e. silo to site)! I'll read the acticle, thanks.\n<a class=\"u-mention\" href=\"https://twitter.com/JamieTanna\"></a>\n<a class=\"u-mention\" href=\"https://twitter.com/jborichevskiy\"></a>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-07T23:17:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/karlicoss/status/1225918301693411328"
    ],
    "in-reply-to": [
      "https://twitter.com/karlicoss/status/1225918301693411328"
    ],
    "published": [
      "2020-02-07T23:17:00Z"
    ],
    "category": [
      "indieweb"
    ],
    "content": [
      {
        "html": "",
        "value": "Ah no - since getting involved in the <a href=\"/tags/indieweb/\">#IndieWeb</a> I've been trying to use my site as the owner for my content, wherever it's for, and then pushing it out to silos after"
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1225922734372380678"
    ]
  },
  "tags": [
    "indieweb"
  ]
}
