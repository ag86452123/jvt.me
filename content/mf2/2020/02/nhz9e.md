{
  "kind": "replies",
  "slug": "2020/02/nhz9e",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1227424568270368768"
      ],
      "url": [
        "https://twitter.com/swyx/status/1227424568270368768"
      ],
      "video": [
        "https://video.twimg.com/tweet_video/EQiwIR8XUAAIj7J.mp4"
      ],
      "published": [
        "2020-02-12T02:49:56+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:swyx"
            ],
            "numeric-id": [
              "33521530"
            ],
            "name": [
              "shawn swyx wang🤗"
            ],
            "nickname": [
              "swyx"
            ],
            "url": [
              "https://twitter.com/swyx",
              "http://swyx.io"
            ],
            "published": [
              "2009-04-20T14:04:41+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Jersey City NJ"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1201029434054041606/efWs7Lr9.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "✍️Clientside Webmentions: Joining the IndieWeb with @Sveltejs\n\nswyx.io/writing/client…\n\nFinally got around to an initial implementation of @aaronpk's wonderful Webmentions.io service. I found @mxstbr's clientside implementation a great balance of build risk and UX!",
          "html": "<div style=\"white-space: pre\">✍️Clientside Webmentions: Joining the IndieWeb with <a href=\"https://twitter.com/sveltejs\">@Sveltejs</a>\n\n<a href=\"https://www.swyx.io/writing/clientside-webmentions\">swyx.io/writing/client…</a>\n\nFinally got around to an initial implementation of <a href=\"https://twitter.com/aaronpk\">@aaronpk</a>'s wonderful <a href=\"http://Webmentions.io\">Webmentions.io</a> service. I found <a href=\"https://twitter.com/mxstbr\">@mxstbr</a>'s clientside implementation a great balance of build risk and UX!</div>"
        }
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-12T23:09:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Reply to https://twitter.com/swyx/status/1227424568270368768"
    ],
    "in-reply-to": [
      "https://twitter.com/swyx/status/1227424568270368768"
    ],
    "published": [
      "2020-02-12T23:09:00Z"
    ],
    "content": [
      {
        "html": "",
        "value": "Ooh nice! I've been using https://github.com/PlaidWeb/webmention.js for my client-side webmentions and have found it really nice ☺ "
      }
    ],
    "syndication": [
      "https://twitter.com/JamieTanna/status/1227732543618920448"
    ]
  }
}
