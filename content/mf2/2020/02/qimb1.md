{
  "kind": "likes",
  "slug": "2020/02/qimb1",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1232759081582112774"
      ],
      "url": [
        "https://twitter.com/daverichards01/status/1232759081582112774"
      ],
      "published": [
        "2020-02-26T20:07:23+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:daverichards01"
            ],
            "numeric-id": [
              "20516638"
            ],
            "name": [
              "Dave Richards"
            ],
            "nickname": [
              "daverichards01"
            ],
            "url": [
              "https://twitter.com/daverichards01",
              "http://uk.linkedin.com/in/daverichards01"
            ],
            "published": [
              "2009-02-10T14:38:01+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1016751191294926849/2y8ZBtcO.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "#Proud to be representing @IAmCapitalOne and OutFront, our LBGTQ+ Business Resource Group, at the Nottinghamshire Rainbow Heritage Awards tonight, alongwith lots of other amazing organisations across #Nottingham. #lifeatcapitalone #inclusion #LGBTHM2020",
          "html": "<a href=\"https://twitter.com/search?q=%23Proud\">#Proud</a> to be representing <a href=\"https://twitter.com/IAmCapitalOne\">@IAmCapitalOne</a> and OutFront, our LBGTQ+ Business Resource Group, at the Nottinghamshire Rainbow Heritage Awards tonight, alongwith lots of other amazing organisations across <a href=\"https://twitter.com/search?q=%23Nottingham\">#Nottingham</a>. <a href=\"https://twitter.com/search?q=%23lifeatcapitalone\">#lifeatcapitalone</a> <a href=\"https://twitter.com/search?q=%23inclusion\">#inclusion</a> <a href=\"https://twitter.com/search?q=%23LGBTHM2020\">#LGBTHM2020</a>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/ERuj2AYX0AEWMIA.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-26T22:20:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @daverichards01's tweet"
    ],
    "like-of": [
      "https://twitter.com/daverichards01/status/1232759081582112774"
    ],
    "published": [
      "2020-02-26T22:20:00Z"
    ],
    "syndication": [
      "https://twitter.com/daverichards01/status/1232759081582112774"
    ]
  }
}
