{
  "kind": "likes",
  "slug": "2020/02/runje",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226951714672168960"
      ],
      "url": [
        "https://twitter.com/technottingham/status/1226951714672168960"
      ],
      "published": [
        "2020-02-10T19:30:59+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:technottingham"
            ],
            "numeric-id": [
              "384492431"
            ],
            "name": [
              "Tech Nottingham"
            ],
            "nickname": [
              "technottingham"
            ],
            "url": [
              "https://twitter.com/technottingham",
              "http://technottingham.com"
            ],
            "published": [
              "2011-10-03T19:47:31+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Nottingham"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1023974499757293570/ZoPc_QsO.jpg"
            ]
          }
        }
      ],
      "content": [
        {
          "value": "Such an insightful talk from @slsmithwell on Femininity in Service Design.\n\nShe's encouraging us to \"get through the chaos to get to the point\". #TechNott",
          "html": "<div style=\"white-space: pre\">Such an insightful talk from <a href=\"https://twitter.com/slsmithwell\">@slsmithwell</a> on Femininity in Service Design.\n\nShe's encouraging us to \"get through the chaos to get to the point\". <a href=\"https://twitter.com/search?q=%23TechNott\">#TechNott</a></div>"
        }
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQb_9hpWoAEic4L.jpg"
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-02-10T20:42:46.587+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @technottingham's tweet"
    ],
    "like-of": [
      "https://twitter.com/technottingham/status/1226951714672168960"
    ],
    "published": [
      "2020-02-10T20:42:46.587+01:00"
    ],
    "syndication": [
      "https://twitter.com/technottingham/status/1226951714672168960"
    ]
  }
}
