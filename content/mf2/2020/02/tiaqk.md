{
  "kind": "likes",
  "slug": "2020/02/tiaqk",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1231930977271910401"
      ],
      "url": [
        "https://twitter.com/gabsmashh/status/1231930977271910401"
      ],
      "published": [
        "2020-02-24T13:16:47+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:gabsmashh"
            ],
            "numeric-id": [
              "1072944174448685063"
            ],
            "name": [
              "gabsmashh | Advanced Persistent Brunette"
            ],
            "nickname": [
              "gabsmashh"
            ],
            "url": [
              "https://twitter.com/gabsmashh",
              "https://twitter.com/gabsmashh?s=09"
            ],
            "published": [
              "2018-12-12T20:00:00+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "Cincinnati, OH"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1231216530375020545/GzLHFUCs.jpg"
            ]
          }
        }
      ],
      "content": [
        "award this star to someone who deserves it this week."
      ],
      "photo": [
        "https://pbs.twimg.com/media/ERiysghXUAAfpNI.jpg"
      ]
    }
  },
  "client_id": "https://indigenous.realize.be",
  "date": "2020-02-24T15:04:00Z",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @gabsmashh's tweet"
    ],
    "like-of": [
      "https://twitter.com/gabsmashh/status/1231930977271910401"
    ],
    "published": [
      "2020-02-24T15:04:00Z"
    ],
    "syndication": [
      "https://twitter.com/gabsmashh/status/1231930977271910401"
    ]
  }
}
