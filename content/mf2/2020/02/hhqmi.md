{
  "kind": "likes",
  "slug": "2020/02/hhqmi",
  "context": {
    "type": [
      "h-entry"
    ],
    "properties": {
      "uid": [
        "tag:twitter.com:1226932029331320832"
      ],
      "url": [
        "https://twitter.com/roobeekeane/status/1226932029331320832"
      ],
      "published": [
        "2020-02-10T18:12:45+00:00"
      ],
      "author": [
        {
          "type": [
            "h-card"
          ],
          "properties": {
            "uid": [
              "tag:twitter.com:roobeekeane"
            ],
            "numeric-id": [
              "1098627439402631169"
            ],
            "name": [
              "ruby🦎"
            ],
            "nickname": [
              "roobeekeane"
            ],
            "url": [
              "https://twitter.com/roobeekeane"
            ],
            "published": [
              "2019-02-21T16:56:07+00:00"
            ],
            "location": [
              {
                "type": [
                  "h-card",
                  "p-location"
                ],
                "properties": {
                  "name": [
                    "London, England"
                  ]
                }
              }
            ],
            "photo": [
              "https://pbs.twimg.com/profile_images/1210968792605634572/B58pPHmC.jpg"
            ]
          }
        }
      ],
      "content": [
        "if you’re wondering whether to put something in bold or italics in an email, why not take a leaf out of this medieval monk’s book and draw a penis in a basket pointing to the phrase you want to highlight"
      ],
      "photo": [
        "https://pbs.twimg.com/media/EQbwLSIXsAAIJFg.jpg"
      ]
    }
  },
  "client_id": "https://micropublish.net",
  "date": "2020-02-10T21:57:03.281+01:00",
  "h": "h-entry",
  "properties": {
    "name": [
      "Like of @roobeekeane's tweet"
    ],
    "like-of": [
      "https://twitter.com/roobeekeane/status/1226932029331320832"
    ],
    "published": [
      "2020-02-10T21:57:03.281+01:00"
    ],
    "syndication": [
      "https://twitter.com/roobeekeane/status/1226932029331320832"
    ]
  }
}
