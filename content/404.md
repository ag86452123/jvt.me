---
title: '404: Page Not Found'
---
I'm really sorry but unfortunately the link you've followed hasn't been found.

<p class="hidden" id="querystring-error">It appears that the page you're browsing to, <a id="this-url"></a>, contains an invalid querystring, therefore my hosting provider is returning a 404. Please remove the querystring (the <code>&...</code> part) and try again.</p>

<script>
const parsedQuerystring = new URLSearchParams(window.location.search);
// is there something that looks like a querystring, but isn't parsed as such? Chances are the user has an invalid URL that is breaking the web host
if (window.location.toString().includes('&') && "" === parsedQuerystring.toString()) {
  document.getElementById('querystring-error').classList.remove('hidden');
  document.getElementById('this-url').href = window.location;
  document.getElementById('this-url').text = window.location;
}
</script>

If you feel like it should be there, please [raise an issue](https://gitlab.com/jamietanna/jvt.me/new).

You may be able to find what you're looking for by [searching for it](/search/).
